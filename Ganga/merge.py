import subprocess as sb
import pickle
import os, glob

outdir = os.getenv('LCTUPLELOCATION')
indir = '/eos/lhcb/user/p/pluca/ganga'
gangapath = os.getenv('LCANAROOT')+'/Ganga'

pklpath = os.getenv('HOME')
avjobs = pickle.load( open( pklpath+"/.jobs.p", "rb" ) )
cmd = 'hadd -f {odir}/{jname}.root {pattern}'
done = []

for jname,j in avjobs.iteritems() :

    if j["merged"] : continue
    if 'CL' in jname : continue
    
    print 'Merging ', jname, 'from id', j["id"]
    pattern = '{idir}/{jid}/*/DVNtuple.root'.format(jid=j["id"],idir=indir)
    if len(glob.glob(pattern)) > 0 :
        #print cmd.format(jname=jname,odir=outdir,pattern=pattern)
        sb.call(cmd.format(jname=jname,odir=outdir,pattern=pattern),shell=True)
        if j["status"] == 'completed' : 
            j["merged"] = True
            pickle.dump( avjobs, open( pklpath+"/.jobs.p", "wb" ) )
            
    else : print "Sorry, no files yet"


