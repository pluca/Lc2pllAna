
decays = {

    'Lc_pmmOS' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^mu- ^mu+'
        },
    'Lc_pmm' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^mu- ^mu+'
        },

    'Lc_pmmSS' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^mu+ ^mu+'
        },
    'Lc_pee' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^e- ^e+'
        },
    'Lc_pem' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^e- ^mu+'
        },
    'Lc_pme' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^mu- ^e+'
        },
    'Lc_pKpi' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^K- ^pi+'
        },
    'Lc_ppipi' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^pi- ^pi+'
        },
    'Lc_pKK' : 
        {
            'descriptor' : 'Lambda_c+ ==> ^p+ ^K- ^K+'
        },
    'D_KKpi' : 
        {
            'descriptor' : 'D+ ==> ^pi+ ^K- ^K+'
        },
    'D_Kpipi' : 
        {
            'descriptor' : 'D+ ==> ^p+ ^K- ^pi+'
        },
    'D_pimumu' : 
        {
            'descriptor' : 'D+ ==> ^pi+ ^mu- ^mu+'
        },
    'D_Kmumu' : 
        {
            'descriptor' : 'D+ ==> ^K+ ^mu- ^mu+'
        },
    'Ds_KKpi' : 
        {
            'descriptor' : 'D_s+ ==> ^K+ ^K- ^pi+'
        },
    'SLLc_pmm' : 
        {
            'descriptor' : 'Lambda_b0 ==> ^(Lambda_c+ ==> ^p+ ^mu- ^mu+) ^mu-'
        },
    'SLLc_pKK' : 
        {
            'descriptor' : 'Lambda_b0 ==> ^(Lambda_c+ ==> ^p+ ^K- ^K+) ^mu-'
        },
    'SLLc_pKpi' : 
        {
            'descriptor' : 'Lambda_b0 ==> ^(Lambda_c+ ==> ^p+ ^K- ^pi+) ^mu-'
        },
    'SLLc_ppipi' : 
        {
            'descriptor' : 'Lambda_b0 ==> ^(Lambda_c+ ==> ^p+ ^pi- ^pi+) ^mu-'
        }
}
