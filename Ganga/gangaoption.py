import os, sys, re
gangapath = os.environ["LCANAROOT"]+"/Ganga"
sys.path.append( gangapath )
import pickle
from DB import decays
from glob import glob

LcROOT = os.environ["LCANAROOT"]
options = glob(LcROOT+"/Options/*.py")
options = [ LocalFile( f ) for f in options ]

template = '''isTest = False #### CHANGE HERE
isMC = {isMC}
mag = '{mag}'

import os, sys
#sys.path.append( os.environ["LCANAROOT"]+"/Options" )
sys.path.append(os.getcwd())

import {SL}Lcpll_CL16 as opt
from DV_Config import ConfigDaVinci

if isMC :

    from DV_Rutines import set_branches
    from DV_MCDecayTuple import MCTupTmp
    
    MCTuple = MCTupTmp.clone("MCTuple")
    MCTuple.Decay    = "[ {descriptor} ]CC"
    MCTuple.Branches = set_branches(MCTuple.Decay,opt.branches)

    opt.setalgs(globals())
    ConfigDaVinci("MC","12",opt.algs,Mag=mag,restrip=opt.restripSq,isTest=isTest)

else :

    opt.setalgs(globals())
    ConfigDaVinci("CL","15",opt.algs,RootInTES="Charm",isTest=isTest)
'''

def CreateJob(dataType,decay,year,mag,local = False) :
    
    jname = '{type}{year}_{dec}_{mag}'.format(dec=decay,year=year,type=dataType,mag=mag)
    j = Job(name = jname, backend=Dirac())
    if local : j.backend = Local()
    myApp = GaudiExec()
    myApp.directory = "/afs/cern.ch/user/p/pluca/cmtuser/DaVinciDev_v41r4"
    j.application = myApp
    #j.applicaton.useGaudiRun = True

    sl = ''
    if 'SL' in decay : sl = 'SL' 
    
    ### Make option file
    if dataType == "MC" :
        myoption = template.format(
            isMC = 'True', mag=mag, SL = sl,
            descriptor = decays[decay]['descriptor'] )
    else :
        myoption = template.format(
            isMC = 'False', mag=mag, SL = sl, descriptor = '' )

    foption = open('MyOption.py','w')
    foption.write(myoption)
    foption.close()
    j.application.options = ['$PWD/MyOption.py']

    ### Find data
    datafile = LcROOT+'/Data/'
    if 'CL' in dataType :
        if local : datafile += 'local_data.py'
        else : datafile += 'data_CL'+year+'_'+mag+'.py'
    else :
        if local :  datafile += 'data_MC12_pmm.py' 
        else : datafile += 'data_MC{y}_{decay}_{mag}.py'.format(
                y=year,decay=decay,mag=mag)
    
    if not os.path.exists(datafile) :
        print "Wrong datafile name: ", datafile
        return

    lfns = [ re.findall("LFN:.*?.dst",x)[0] for x in open(datafile).readlines() if 'LFN' in x ]
    dataset = LHCbDataset(lfns)
    j.inputdata = dataset

    #j.inputdata = DaVinci(version='v41r4').readInputData(datafile)
    #j.inputdata = DaVinci().readInputData(datafile)

    ### Finish initialisation
    j.inputfiles = options
    if dataType == 'CL' : j.splitter = SplitByFiles(filesPerJob=50)
    else : j.splitter = SplitByFiles(filesPerJob=20)
    f = MassStorageFile('*.root')
    j.outputfiles=[f]
    j.do_auto_resubmit = True
    #j.submit()

    avjobs[jname] = { 'id' : j.id, 'status' : 'new', 'merged' : False }
    DumpJobs(avjobs)

    return j

def DumpTestMCOption() :
    
    myoption = template.format(
        isMC = 'True', mag='MU', SL = '', descriptor = 'Lambda_c+ ==> ^p+ ^K- ^pi+' )
    foption = open('MyOption_MCtest.py','w')
    foption.write(myoption)
    foption.close()


def SubmitAll(datatype = None, dectype='', decays=None) :

    if 'CL' in datatype or datatype is None :
        j = CreateJob(datatype,dectype+'CHARM','16_S28','MU')
        j.submit()
        j = CreateJob(datatype,dectype+'CHARM','16_S28','MD')
        j.submit()

    if datatype == "MC12" or datatype is None :

        mydecays = ['Lc_pmmOS','Lc_pmmSS','Lc_pKpi']
        #mydecays = ['Lc_ppipi','D_KKpi','D_Kpipi','Ds_KKpi','D_Kmumu','D_pimumu','Lc_pKK']
        #mydecays = ['Lc_pKK']
        mydecays = ['Lc_pmmOS']

        if dectype == 'SL' :
            mydecays = ['SLLc_pmm','SLLc_pKK','SLLc_ppipi','SLLc_pKpi']
        
        if decays is not None : mydecays = decays
                    
        for dec in mydecays :
            print "Submitting... ", dec
            year = '12'
            if dec == "Lc_ppipi" : year = '11'
            j = CreateJob('MC',dec,year,'MU')
            j.submit()
            j = CreateJob('MC',dec,year,'MD')
            j.submit()
    
    if datatype == "MC16" or datatype is None :
        mydecays = ['Lc_pmm']#,'Lc_pKpi']

        for dec in mydecays :
            j = CreateJob('MC',dec,'16','MU')
            j.submit()
            j = CreateJob('MC',dec,'16','MD')
            j.submit()

    #os.remove('MyOption.py')


