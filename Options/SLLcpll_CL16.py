import os, sys
#sys.path.append( os.environ["LCANAROOT"]+"/Options" )
sys.path.append(os.getcwd())
from DV_Rutines import set_branches
from DV_RelatedInfo import getLoKiTool
from DV_DecayTuple import TupTmp, TupTmpMC
from DV_Config import ConfigDaVinci

from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay
from Configurables import TupleToolDecayTreeFitter
from Configurables import TupleToolTISTOS

from DV_Rutines import ReStrip
lines_to_restrip = [ 
        'Strippingb2LcMuXCharmFromBSemiLine',
        'Strippingb2LcMuXpMuMuCharmFromBSemiLine',
        'Strippingb2LcMuXpEECharmFromBSemiLine',
        'Strippingb2LcMuXpEMuCharmFromBSemiLine',
        'Strippingb2LcMuXpMuECharmFromBSemiLine' 
        ]
restrip, restripSq = ReStrip(lines_to_restrip,["CharmFromBSemi"],['Charm'])
####################################################################

def addFeatures(Tup):

    Tup.addTool( TupleToolDecay, name = "Lb" )
    Tup.addTool( TupleToolDecay, name = "muon" )

    Tup.Lb.addTool( TupleToolDecayTreeFitter, name = "DTF" )
    Tup.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF" ]
    Tup.Lb.addTool( Tup.Lb.DTF.clone( "DTF_PV",
                                      Verbose = True,
                                      constrainToOriginVertex = True ) )
    Tup.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]
    ######################################################################
    Tup.Lb.addTool( TupleToolTISTOS, name = "TISTOS" )
    Tup.Lb.ToolList += [ "TupleToolTISTOS/TISTOS" ]
    Tup.Lb.TISTOS.TriggerList = [
        "L0ElectronDecision",
        "L0HadronDecision",
        "L0MuonDecision",
        "L0PhotonDecision",

        "Hlt1TrackAllL0Decision",
        "Hlt1TrackMuonDecision",
    
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
        "Hlt2Topo4BodyDecision",
        "Hlt2TopoE2BodyDecision",
        "Hlt2TopoE3BodyDecision",
        "Hlt2TopoE4BodyDecision",
        "Hlt2RareCharmLc2PMuMuDecision",
        "Hlt2RareCharmLc2PMuMuSSDecision",
        "Hlt2RareCharmLc2PMueDecision",
        "Hlt2RareCharmLc2PeeDecision"
    ]
    Tup.Lb.TISTOS.Verbose = True

    Tup.muon.addTool( TupleToolTISTOS, name = "MuTISTOS" )
    Tup.muon.ToolList += [ "TupleToolTISTOS/MuTISTOS" ]
    Tup.muon.MuTISTOS.TriggerList = [
        "L0ElectronDecision",
        "L0HadronDecision",
        "L0MuonDecision"
    ]
    Tup.muon.MuTISTOS.Verbose = True

    return Tup

#####################################################################
#
# Define DecayTreeTuple tuple
#
######################################################################

branches = ["Lb","Lc","p","L1","L2","muon"]
algs = []

def setalgs(glob) :

    global TupTmp, TupTmpMC
    isMC = False
    if 'isMC' in glob:
        isMC = glob['isMC']
    if isMC : TupTmp = TupTmpMC
    TupTmp = addFeatures(TupTmp)

    Lc2peeTuple = TupTmp.clone("Lc2peeTuple")
    Lc2peeTuple.Inputs   = [ "Phys/b2LcMuXpEECharmFromBSemiLine/Particles" ]
    Lc2peeTuple.Decay    = "[ [Lambda_b0]cc -> ^(Lambda_c+ -> ^p+ ^e- ^e+) ^mu- ]CC"
    Lc2peeTuple.Branches = set_branches(Lc2peeTuple.Decay,branches)
    
    LoKi_ToolEE = getLoKiTool("EE","b2LcMuXpEECharmFromBSemiLine",isMC)
    Lc2peeTuple.Lb.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolEE"]
    Lc2peeTuple.Lb.addTool(LoKi_ToolEE)
    
    Lc2pmmTuple = TupTmp.clone("Lc2pmmTuple")
    Lc2pmmTuple.Inputs   = [ "Phys/b2LcMuXpMuMuCharmFromBSemiLine/Particles" ]
    Lc2pmmTuple.Decay    = "[ [Lambda_b0]cc -> ^(Lambda_c+ -> ^p+ ^mu- ^mu+) ^mu- ]CC"
    Lc2pmmTuple.Branches = set_branches(Lc2pmmTuple.Decay,branches)
    
    LoKi_ToolMuMu = getLoKiTool("MuMu","b2LcMuXpMuMuCharmFromBSemiLine",isMC)
    Lc2pmmTuple.Lb.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolMuMu"]
    Lc2pmmTuple.Lb.addTool(LoKi_ToolMuMu)
    
    Lc2pemTuple = TupTmp.clone("Lc2pemTuple")
    Lc2pemTuple.Inputs   = [ "Phys/b2LcMuXpMuECharmFromBSemiLine/Particles" ]
    Lc2pemTuple.Decay    = "[ [Lambda_b0]cc -> ^(Lambda_c+ -> ^p+ ^e- ^mu+) ^mu- ]CC"
    Lc2pemTuple.Branches = set_branches(Lc2pemTuple.Decay,branches)
    
    LoKi_ToolEMu = getLoKiTool("EMu","b2LcMuXpEMuCharmFromBSemiLine", isMC)
    Lc2pemTuple.Lb.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolEMu"]
    Lc2pemTuple.Lb.addTool(LoKi_ToolEMu)
    
    Lc2pmeTuple = TupTmp.clone("Lc2peeTuple")
    Lc2pmeTuple.Inputs   = [ "Phys/b2LcMuXpEMuCharmFromBSemiLine/Particles" ]
    Lc2pmeTuple.Decay    = "[ [Lambda_b0]cc -> ^(Lambda_c+ -> ^p+ ^mu- ^e+) ^mu- ]CC"
    Lc2pmeTuple.Branches = set_branches(Lc2pmeTuple.Decay,branches)
    
    LoKi_ToolMuE = getLoKiTool("MuE","b2LcMuXpMuECharmFromBSemiLine",isMC)
    Lc2pmeTuple.Lb.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolMuE"]
    Lc2pmeTuple.Lb.addTool(LoKi_ToolMuE)
    
    Lc2pKpiTuple = TupTmp.clone("Lc2pKpiTuple")
    Lc2pKpiTuple.Inputs   = [ "Phys/b2LcMuXCharmFromBSemiLine/Particles" ]
    Lc2pKpiTuple.Decay    = "[ [Lambda_b0]cc -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^mu- ]CC"
    Lc2pKpiTuple.Branches = set_branches(Lc2pKpiTuple.Decay,branches)
    
    LoKi_ToolKPi = getLoKiTool("KPi","b2LcMuXCharmFromBSemiLine",isMC)
    Lc2pKpiTuple.Lb.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolKPi"]
    Lc2pKpiTuple.Lb.addTool(LoKi_ToolKPi)

    global algs    
    algs = [ Lc2pmmTuple, Lc2peeTuple, Lc2pmeTuple, Lc2pemTuple, Lc2pKpiTuple ]

    if isMC and 'MCTuple' in glob :
        algs += [ glob['MCTuple'] ]


