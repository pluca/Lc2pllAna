from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import LoKi__Hybrid__TupleTool

def getLoKiToolsDictionary_Prompt():
    reldict = {'RELINFO':{}}
    cone_vars = {'Cone':{'name':'CONEANGLE'},
                 'Cone_Mult':{'name':'CONEMULT'},
                 'Cone_PTAsym':{'name':'CONEPTASYM'}}
    cone_angles = {}
    for angle in ['1.0','1.5','2.0'] :
        cone_angles[angle+'_h']  = {'location':'RelInfoConeVariables_'+angle+'_Dp_h'}
        cone_angles[angle+'_l1'] = {'location':'RelInfoConeVariables_'+angle+'_Dp_l1'}
        cone_angles[angle+'_l2'] = {'location':'RelInfoConeVariables_'+angle+'_Dp_l2'}

    for var,args_var in cone_vars.iteritems():
        for angle,args_angle in cone_angles.iteritems():
            reldict['RELINFO'][var+'_'+angle] = {'varName':args_var['name'],'Location':args_angle['location'],'Default':-1.}

    for v in [ 'VTXISONUMVTX', 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK', 'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK']:
        reldict['RELINFO']['VtxIso_Lc_'+v] = {'varName':v,'Location':'RelInfoVertexIsolation','Default':-1.}
    
    return reldict

def getLoKiToolsDictionary_SL():
    reldict = {'RELINFO':{}}
    cone_vars = {'Cone':{'name':'CONEANGLE'},
                 'Cone_Mult':{'name':'CONEMULT'},
                 'Cone_PTAsym':{'name':'CONEPTASYM'}}
    cone_angles = {}
    for angle in ['08','10','13','15','17','20'] :
        cone_angles[angle+'_1']  = {'location':'P2ConeVar'+angle+'_1'}
        cone_angles[angle+'_2']  = {'location':'P2ConeVar'+angle+'_2'}
        cone_angles[angle+'_P']  = {'location':'P2ConeVar'+angle+'_P'}
        cone_angles[angle+'_Mu'] = {'location':'P2ConeVar'+angle+'_Mu'}
        cone_angles[angle+'_Lc'] = {'location':'P2ConeVar'+angle+'_C'}
        cone_angles[angle+'_Lb'] = {'location':'P2ConeVar'+angle+'_B'}

    for var,args_var in cone_vars.iteritems():
        for angle,args_angle in cone_angles.iteritems():
            reldict['RELINFO'][var+'_'+angle] = {'varName':args_var['name'],'Location':args_angle['location'],'Default':-1.}

    for v in [ 'VTXISONUMVTX', 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK', 'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK']:
        reldict['RELINFO']['VtxIso_B_'+v] = {'varName':v,'Location':'VertexIsoInfo_B','Default':-1.}
        reldict['RELINFO']['VtxIso_Lc_'+v] = {'varName':v,'Location':'VertexIsoInfo_C','Default':-1.}
    
    return reldict

def getLoKiTool(name,line,isMC=True) :

    lokiDict = None
    if "MuX" in line : lokiDict = getLoKiToolsDictionary_SL()
    else : lokiDict = getLoKiToolsDictionary_Prompt()

    LoKi_Tool = LoKi__Hybrid__TupleTool('LoKi_Tool'+name)
    for name, args in lokiDict['RELINFO'].iteritems():
        if isMC : lokipath = '/AllStreams/Phys/'+line+'/'+args['Location']
        else : lokipath = '/Event/Charm/Phys/'+line+'/'+args['Location']
        LoKi_Tool.Variables[name] = "RELINFO('%s','%s',%f)"%(lokipath,args['varName'],args['Default'])
    return LoKi_Tool


