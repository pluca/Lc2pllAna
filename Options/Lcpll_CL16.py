import os, sys
#sys.path.append( os.environ["LCANAROOT"]+"/Options" )
sys.path.append(os.getcwd())
from DV_Rutines import set_branches
from DV_RelatedInfo import getLoKiTool
from DV_DecayTuple import TupTmp, TupTmpMC
from DV_Config import ConfigDaVinci
from Configurables import DeterministicPrescaler

from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay

######################################################################

from DV_Rutines import ReStrip
lines_to_restrip = [ 
        'StrippingD2XMuMuSS_Lambdac2PEELine',
        'StrippingD2XMuMuSS_Lambdac2PMuMuLine',
        'StrippingD2XMuMuSS_Lambdac2PEMuLine',
        'StrippingD2XMuMuSS_Lambdac2PMuELine',
        'StrippingD2XMuMuSS_Lambdac2PKPiLine',
        'StrippingLambdaCForPromptCharm'
        ]

restrip, restripSq = ReStrip(lines_to_restrip,["D2XMuMuSS","PromptCharm"],['Charm'])

########################################################################

from Gaudi.Configuration import *
from PhysSelPython.Wrappers import SimpleSelection , SelectionSequence, AutomaticData
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

def getExtraSel(Line) :     ##### Extra selection to skim huge tuples

    Inputs = "Phys/"+Line+"/Particles"
    cut = " (MAXTREE('p+'==ABSID, TRGHP) < 0.1) & \
            (MAXTREE('K+'==ABSID, TRGHP) < 0.1) & \
            (MAXTREE('pi+'==ABSID, TRGHP) < 0.1)  "  

    extraSelection = SimpleSelection('ExtraSelection'+Line,
                FilterDesktop,
                [AutomaticData(Location = Inputs)],
                Code = cut )

    if 'KPi' in Line or 'Prompt' in Line :
        prescale =  DeterministicPrescaler("MyPrescale", AcceptFraction = 0.05)
        extraSelSeq = SelectionSequence("LcANA_"+Line+"_Prescaled_SelSeq", TopSelection = extraSelection,  EventPreSelector = [ prescale ])
    else :
        extraSelSeq = SelectionSequence("LcANA_"+Line+"SelSeq", TopSelection = extraSelection)

    return extraSelSeq

#####################################################################
#
# Define DecayTreeTuple tuple
#
######################################################################

branches = ["Lc","p","L1","L2"]
algs = []

def setalgs(glob) :

    global TupTmp, TupTmpMC
    isMC = False
    if 'isMC' in glob:
        isMC = glob['isMC']
    if isMC : TupTmp = TupTmpMC

    Lc2peeTuple = TupTmp.clone("Lc2peeTuple")
    Lc2peeTuple.Inputs   = [ "Phys/D2XMuMuSS_Lambdac2PEELine/Particles" ]
    Lc2peeTuple.Decay    = "[ Lambda_c+ -> ^p+ ^e- ^e+ ]CC"
    Lc2peeTuple.Branches = set_branches(Lc2peeTuple.Decay,branches)
    
    LoKi_ToolEE = getLoKiTool("EE","D2XMuMuSS_Lambdac2PEELine",isMC)
    Lc2peeTuple.Lc.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolEE"]
    Lc2peeTuple.Lc.addTool(LoKi_ToolEE)
    
    Lc2pmmTuple = TupTmp.clone("Lc2pmmTuple")
    Lc2pmmTuple.Inputs   = [ "Phys/D2XMuMuSS_Lambdac2PMuMuLine/Particles" ]
    Lc2pmmTuple.Decay    = "[ Lambda_c+ -> ^p+ ^mu- ^mu+ ]CC"
    Lc2pmmTuple.Branches = set_branches(Lc2pmmTuple.Decay,branches)
    
    LoKi_ToolMM = getLoKiTool("MM","D2XMuMuSS_Lambdac2PMuMuLine",isMC)
    Lc2pmmTuple.Lc.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolMM"]
    Lc2pmmTuple.Lc.addTool(LoKi_ToolMM)
    
    Lc2pemTuple = TupTmp.clone("Lc2pemTuple")
    Lc2pemTuple.Inputs   = [ "Phys/D2XMuMuSS_Lambdac2PEMuLine/Particles" ]
    Lc2pemTuple.Decay    = "[ Lambda_c+ -> ^p+ ^e- ^mu+ ]CC"
    Lc2pemTuple.Branches = set_branches(Lc2pemTuple.Decay,branches)
    
    LoKi_ToolEM = getLoKiTool("EM","D2XMuMuSS_Lambdac2PEMuLine",isMC)
    Lc2pemTuple.Lc.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolEM"]
    Lc2pemTuple.Lc.addTool(LoKi_ToolEM)
    
    Lc2pmeTuple = TupTmp.clone("Lc2pmeTuple")
    Lc2pmeTuple.Inputs   = [ "Phys/D2XMuMuSS_Lambdac2PMuELine/Particles" ]
    Lc2pmeTuple.Decay    = "[ Lambda_c+ -> ^p+ ^mu- ^e+ ]CC"
    Lc2pmeTuple.Branches = set_branches(Lc2pmeTuple.Decay,branches)
    
    LoKi_ToolME = getLoKiTool("ME","D2XMuMuSS_Lambdac2PMuELine",isMC)
    Lc2pmeTuple.Lc.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolME"]
    Lc2pmeTuple.Lc.addTool(LoKi_ToolME)
    
    Lc2pKpiTuple = TupTmp.clone("Lc2pKpiTuple")
    pKpiLine = "D2XMuMuSS_Lambdac2PKPiLine"
    pKpiLineOld = pKpiLine
    #"LambdaCForPromptCharm"
    if isMC :
        Lc2pKpiTuple.Inputs   = [ "Phys/"+pKpiLine+"/Particles" ]
    else :
        pKpiLine = pKpiLineOld
        Lc2pKpiSel = getExtraSel(pKpiLine)
        Lc2pKpiTuple.Inputs   = [ Lc2pKpiSel.outputLocation() ]
    Lc2pKpiTuple.Decay    = "[ Lambda_c+ -> ^p+ ^K- ^pi+ ]CC"
    Lc2pKpiTuple.Branches = set_branches(Lc2pKpiTuple.Decay,branches)

    LoKi_ToolKPi = getLoKiTool("KPi","D2XMuMuSS_Lambdac2PKPiLine",isMC)
    Lc2pKpiTuple.Lc.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_ToolKPi"]
    Lc2pKpiTuple.Lc.addTool(LoKi_ToolKPi)

    Lc2pKpiTupleOld = None
    if isMC :
        Lc2pKpiTupleOld = TupTmp.clone("Lc2pKpiTupleOld")
        Lc2pKpiTupleOld.Inputs   = [ "Phys/"+pKpiLineOld+"/Particles" ]
        Lc2pKpiTupleOld.Decay    = "[ Lambda_c+ -> ^p+ ^K- ^pi+ ]CC"
        Lc2pKpiTupleOld.Branches = set_branches(Lc2pKpiTupleOld.Decay,branches)
    
    global algs
    algs = []
    if not isMC : algs +=  [ Lc2pKpiSel.sequence() ]
    algs += [ Lc2pmmTuple, Lc2peeTuple, Lc2pmeTuple, Lc2pemTuple, Lc2pKpiTuple ]
    if Lc2pKpiTupleOld is not None :
        algs += [ Lc2pKpiTupleOld ]
    
    if isMC and 'MCTuple' in glob :
        algs += [ glob['MCTuple'] ]


