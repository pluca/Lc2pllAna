from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *

#####################################################################
#
# Define template tuple
#
######################################################################

from Configurables import DecayTreeTuple
TupTmp          = DecayTreeTuple()
######################################################################
from Configurables import TupleToolDecay
TupTmp.addTool( TupleToolDecay, name = "L1" )
TupTmp.addTool( TupleToolDecay, name = "L2" )
TupTmp.addTool( TupleToolDecay, name = "Lc" )
TupTmp.addTool( TupleToolDecay, name = "p" )
######################################################################
TupTmp.ToolList += [
    "TupleToolANNPID",
    "TupleToolBremInfo",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolPrimaries",
    "TupleToolPropertime",
    "TupleToolRecoStats",
    "TupleToolTrackInfo"
    ]
######################################################################
from Configurables import TupleToolTISTOS
TupTmp.Lc.addTool( TupleToolTISTOS, name = "TISTOS" )
TupTmp.Lc.ToolList += [ "TupleToolTISTOS/TISTOS" ]
TupTmp.Lc.TISTOS.TriggerList = [
    "L0ElectronDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0PhotonDecision",

    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackMVADecision",
    "Hlt1TrackMuonMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt1SingleMuonDecision",

    "Hlt2Topo2BodyDecision",
    "Hlt2Topo3BodyDecision",
    "Hlt2Topo4BodyDecision",
    "Hlt2TopoE2BodyDecision",
    "Hlt2TopoE3BodyDecision",
    "Hlt2TopoE4BodyDecision",
    "Hlt2TopoMu2BodyDecision",
    "Hlt2TopoMu3BodyDecision",
    "Hlt2TopoMu4BodyDecision",
    "Hlt2TopoMuE2BodyDecision",
    "Hlt2TopoMuE3BodyDecision",
    "Hlt2TopoMuE4BodyDecision",
    "Hlt2RareCharmLc2PMuMuDecision",
    "Hlt2RareCharmLc2PMuMuSSDecision",
    "Hlt2RareCharmLc2PMueDecision",
    "Hlt2RareCharmLc2PeeDecision",
    "Hlt2CharmHadD2HHHDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2CharmSemilep3bodyD2KMuMuDecision",
    "Hlt2TopoMu2BodyBBDTDecision",

    "Hlt2CharmHadDpToKmKpKpTurboDecision",
    "Hlt2CharmHadDpToKmKpPipTurboDecision",
    "Hlt2CharmHadDpToKmPipPip_LTUNBTurboDecision",
    "Hlt2CharmHadDpToKmPipPipTurboDecision",
    "Hlt2CharmHadDpToKpKpPimTurboDecision",
    "Hlt2CharmHadDpToKpPimPipTurboDecision",
    "Hlt2CharmHadDpToPimPipPipTurboDecision",
    "Hlt2CharmHadDpToKmPipPip_ForKPiAsymDecision",

    "Hlt2CharmHadDspToKmKpKpTurboDecision",
    "Hlt2CharmHadDspToKmKpPip_LTUNBTurboDecision",
    "Hlt2CharmHadDspToKmKpPipTurboDecision",
    "Hlt2CharmHadDspToKmPipPipTurboDecision",
    "Hlt2CharmHadDspToKpKpPimTurboDecision",
    "Hlt2CharmHadDspToKpPimPipTurboDecision",
    "Hlt2CharmHadDspToPimPipPipTurboDecision",

    "Hlt2CharmHadLcpToPpKmKpTurboDecision",
    "Hlt2CharmHadLcpToPpKmPip_LTUNBTurboDecision",
    "Hlt2CharmHadLcpToPpKmPipTurboDecision",
    "Hlt2CharmHadLcpToPpKpPimTurboDecision"
    ]
    
TupTmp.Lc.TISTOS.Verbose = True
TupTmp.p.addTool( TupleToolTISTOS, name = "PTISTOS" )
TupTmp.p.ToolList += [ "TupleToolTISTOS/PTISTOS" ]
TupTmp.p.PTISTOS.TriggerList = [
    "L0ElectronDecision",
    "L0HadronDecision",
    "L0MuonDecision"
    ]
TupTmp.p.PTISTOS.Verbose = True

######################################################################
from Configurables import TupleToolSubMass
TupTmp.Lc.addTool( TupleToolSubMass )
TupTmp.Lc.ToolList += [ "TupleToolSubMass" ]

TupTmp.Lc.TupleToolSubMass.Substitution       += [ "p+ => pi+" ]
TupTmp.Lc.TupleToolSubMass.Substitution       += [ "p+ => K+" ]
TupTmp.Lc.TupleToolSubMass.Substitution       += [ "K- => pi-" ]
TupTmp.Lc.TupleToolSubMass.Substitution       += [ "e+ => mu+" ]
TupTmp.Lc.TupleToolSubMass.Substitution       += [ "mu+ => e+" ]

TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "mu+/p+ => p+/mu+" ]
TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "e+/p+ => p+/e+" ]

TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "mu+/mu- => pi+/pi-" ]
TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "mu+/e- => pi+/pi-" ]
TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "e+/e- => pi+/pi-" ]

TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "mu+/mu- => K+/pi-" ]
TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "mu+/e- => K+/pi-" ]
TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "mu-/e+ => K-/pi+" ]
TupTmp.Lc.TupleToolSubMass.DoubleSubstitution += [ "e+/e- => K+/pi-" ]


########################################################################

from Configurables import LoKi__Hybrid__TupleTool
MyLoKiTool = LoKi__Hybrid__TupleTool('LoKi_Tool_Lc')
MyLoKiTool.Variables =  {
    "MAXDOCA"   : "PFUNA(AMAXDOCA(''))"
    , "MINDOCA" : "PFUNA(AMINDOCA(''))"
    #, "Q2"      : "AM23"
    , "ETA"     : "ETA"
    #, "MAXDOCACHI2" : "PFUNA(AMAXDOCACHI2(''))"
    #, "MINDOCACHI2" : "PFUNA(AMINDOCACHI2(''))"
}
TupTmp.Lc.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Tool_Lc"]
TupTmp.Lc.addTool(MyLoKiTool)

from Configurables import TupleToolDecayTreeFitter
TupTmp.Lc.addTool( TupleToolDecayTreeFitter, name = "DTF" )
TupTmp.Lc.ToolList += [ "TupleToolDecayTreeFitter/DTF" ]
TupTmp.Lc.addTool( TupTmp.Lc.DTF.clone( "DTF_PV",
                                      Verbose = True,
                                      constrainToOriginVertex = True ) )
TupTmp.Lc.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]

########################################### MC truth info for simulated samples

TupTmpMC = TupTmp.clone("DecayTreeTupleForMC")

from Configurables import TupleToolMCTruth

TupTmpMC.addTool( TupleToolMCTruth )
TupTmpMC.ToolList += [ "TupleToolMCTruth" ]
TupTmpMC.TupleToolMCTruth.ToolList += [ "MCTupleToolHierarchy" ]
TupTmpMC.TupleToolMCTruth.ToolList += [ "MCTupleToolKinematic" ]

TupTmpMC.Lc.addTool( TupleToolMCTruth )
TupTmpMC.Lc.ToolList += [ "TupleToolMCTruth" ]

from Configurables import TupleToolMCBackgroundInfo
TupTmpMC.Lc.ToolList += [ "TupleToolMCBackgroundInfo" ]

