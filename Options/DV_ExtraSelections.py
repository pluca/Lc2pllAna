
def getExtraSel(Line) :

    from Gaudi.Configuration import *
    from PhysSelPython.Wrappers import SimpleSelection , SelectionSequence, AutomaticData
    from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

    Inputs = "Phys/"+Line+"/Particles"

    extraSelection = SimpleSelection(
                'ExtraSelection'+Line,
                FilterDesktop,
                [AutomaticData(Location = Inputs)],
                Code = "(MAXTREE('p+'==ABSID, TRGHP) < 0.1)"   
                            )
    extraSelSeq = SelectionSequence("LcANA_"+Line+"SelSeq", TopSelection = extraSelection)
    return extraSelSeq
