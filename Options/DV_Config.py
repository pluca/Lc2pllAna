
def ConfigDaVinci(DataType,DataYear,UserAlgs=[],RootInTES="",isTest=False,Mag="",restrip=[]) :
    
    from Configurables import DaVinci
    from Configurables import CondDB

    DaVinci().UserAlgorithms  += UserAlgs
    DaVinci().DataType = "20" + DataYear

    DaVinci().EvtMax    = -1
    DaVinci().PrintFreq = 10000
    if isTest :
        DaVinci().EvtMax     = 1000
        if DataType == "CL" :
            DaVinci().EvtMax = 1000
        DaVinci().PrintFreq  = 100

    DaVinci().TupleFile = "DVNtuple.root"

    if DataType == "CL" :
        
        DaVinci().InputType  = "MDST"
        DaVinci().RootInTES  = "/Event/"+RootInTES
        DaVinci().Simulation = False
        DaVinci().Lumi       = True
        CondDB( LatestGlobalTagByDataType = "20" + DataYear )

    if DataType == "MC" :
    
        DaVinci().appendToMainSequence( restrip )
        #DaVinci().InputType = "RDST"
        #importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_Run182594.py")
        #importOptions("$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py")
        
        from Configurables import EventTuple       
        DaVinci().UserAlgorithms += [ EventTuple("EventTuple") ]
        
        DaVinci().Simulation = True
        DaVinci().Lumi       = False
        TagDDDB              = "dddb-20150522"
        TagCondDB            = "sim-20150522"
        if DataYear == "11" :
            TagDDDB   += "-1"
            TagCondDB += "-1"
        if DataYear == "12" :
            TagDDDB   += "-2"
            TagCondDB += "-2"
        TagCondDB += "-vc"
        if Mag == "MD" or Mag == "Down" or Mag == "DOWN":
            TagCondDB += "-md100"
        if Mag == "MU" or Mag == "Up" or Mag == "UP":
            TagCondDB += "-mu100"
        DaVinci().DDDBtag   = TagDDDB
        DaVinci().CondDBtag = TagCondDB

