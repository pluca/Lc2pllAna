# Snakefile containing the full Lc2pmm analysis
#
#  Author : Marko Stamenkovic (marko.stamenkovic@epfl.ch)
#  
#  Last update : 04.07.2017
#
# Usage : each commands can be run separately to produce the given output from the inputs
#
#           Example : snakemake rawFit --config sample=Prompt
#           Performs the rawFit of the Lc2pKpi data sample for the Prompt mode 
# 
# Two types of inputs are possible : sample=Prompt or sample=SL                   
#
# Code tracking can be enabled by running the command 
# 
#   snakemake -R `snakemake --list-code-changes`
#
# in a shell terminal. This will rerun the command anytime the inputs have been modified.
# 
# Architecture of the analysis: 
#        cpp/
#          src/Lc2pmmAna.cpp                    ### main() of the analysis -> only the parser has to change 
#          includes/
#               LcAnaCuts.hpp 
#               sigprocesser.hpp
#          class/         
#            Makefile
#            fitter.hpp                         ### Class to Fit MC->Data / Data / 
#            fitter.cpp                         ### WARNING Location of tmp.root in /eos/ needs to be changed
#               lib/
#                 fitter.o
#          dat/
#            fitter/                            ### Contains .dat files with fit results depending on the part of the analysis (optimisations)
#               RAW/ 
#               ROC/
#               SIDEBAND/
#               SIGPHI/
#               PHI/
#               FULLPKPI/
#               MUON/
#               RARE/
#               PID/
#
#        python/   
#               variables/
#                   var_comparison.py           ### Compares data signal/sideband and MC for variables defined in variable_DB.py
#               proton/
#                   process_significance.py     ### Finds the best requirement on p_MC15TuneV1_ProbNNp
#                   draw_pPID                   ### Draws data after preselection + removed by ProbNN cut
#               BDT/
#                   merge_mvaclone.py           ### Merge the aV files (addVariables)
#                   generateMVAParam.py         ### BDT optimisation and maxima finding 
#                   Lcpmm_BDT_efficiency.py     ### find best BDT requirement 
#                   reduce_rootFile.py          ### reduces the root file for the MVA classifier training
#                   draw_BDT_LcMM.py            ### draws the the correlation of MVA output and Lc_M 
#               trigger/
#                   trigger.py                  ### plots the triggers efficiency in histograms for several variables (L0,Hlt1,Hlt2)
#                   hlt1.py                     ### computes effiencies of the HLT1 trigger   !!!! Needs to decomment dumpAll() in order to save the results in .pkl file !!!!
#                   hlt2.py                     ### computes efficiencies of the HLT2 trigger !!!! Needs to decomment dumpAll() in order to save the results in .pkl file !!!!
#               MVA/
#                   trainMVA.py                 ### runs the MVA analysis : use --readBestOpts for the optimal configuration  !!! Segmentation fault coming from the MLP classifier !!!
#                   setTMVA.py                  ### sets the MVA classifiers
#               fit/
#                   create_cutted_rootFile.py   ### applies the full selection and creates the root files required for the fit to the MC and data
#                   fitPhi.py                   ### parallelized optimisation of the muon requirement on the Lc2pphi channel 
#                   fitRare.py                  ### fit to the 1)MC 2)Phi mode 3) Rare mode (blinded) by extracting the fit parameters and fixing them from the phi mode
#                   fitExtraMuon.py             ### parallelized optimisation of the extra (SL) muon requirement
#                   fitLc2pKpi                  ### fit to the Lc2pKpi channel (previously for the normalisation and signal yield extraction)
#               efficiency/
#                   PIDcorr_efficiencies        ### computes the efficiencies of the full selection ( and for each requirement w.r.t the previous requirement)
#                   runPIDCalib                 ### data-driven computation of the efficiency for the ProbNN variables
#               backgrounds/
#                   misID_peaks.py              ### computes the misID peaks using the TGenPhasespace method from the ROOT framework
#                   bkg_study.py                ### computes the expected number of contamination events for Lc2pppipi and D2KKpi
#               results/
#                   fill_dict.py                ### fills the fitResult.pkl stored dictionary
#                   fill_template.py            ### fills the template for the Latex tables
#               routines/
#                   LcAnalysis.py               
#                   dictionaries.py             ### contains the dictionaries definition with infile and intree names for the analysis (python 2/3 compatible can be used in Snakefile)
#               tools/
#                   sigproc.py                  ### tools for significance processing
#                   efficiency_tools.py
#                   probNN_tools.py
#                   trigger_tools.py
#                   MVAparam.py
#                   writeEff.py                 ### writes the efficiency, BR, and number of events to a .dat file for the RARE fit
#                   eventtuple_tools.py         ### fetch the total number of events from GANGA separated root files
#                   plotting_tools.py           ### tools for histogram, graphs filling and plotting and also the scientific notation tools converter for Latex templates
#        PID/
#           correctVar.py                       ### applies the MCResampling
#           make_tables.py                      ### generates the root files for the efficiency as a function of momentum p and pseudo-rapidity eta
#               PerfHistsMuon/                  ### contains the root files with efficiency as a function of p and eta
#        LHCb/
#               db.pkl                          ### contains the dictionary for the effiencies on the low-, phi- and high- mass regions
#               effForTex.pkl                   ### dict with efficiencies converted in scientific notation for Latex
#               fitResult.pkl                   ### fit results of the Lc2pphi fit to the data
#               fitResult_forTex.pkl
#               full_efficiency.pkl             ### full efficiency of the selection for the rare decay (veto on the rho,omega and eta masses)
#               hlt*_{mode}.pkl                 ### trigger activation (computed from the number of event before the preselection and after the specific requirement)
#               MISID.pkl                       ### MISID efficiencies
#               MISID_forTex.pkl                
#               trigger.pkl                     ### outdated
#        tables/
#           *.tex                               ### Latex tables filled from templates
#               templates/
#                   *.tmp                       ### latex templates corresponding to the various dictionaries .pkl
#       
#
######## User guide ############
#
# Run the following commands to perform the full analysis
# 
#   snakemake 
#
#           1) var_comparison --config sample= Prompt or SL                     ### PLots the different variables and compares data signal - sideband and MC
#           2) PID --config sample= Prompt or SL                                ### Multi processor optimisation of p_MC15TuneV1_ProbNNp requirement w.r.t significance maximum
#           3) plot_PID --config sample=Prompt or SL                            ### Draws data after preselection with events removed by the p_MC15TuneV1_ProbNNp optimal requirement
#           3) addVariables --config sample=Prompt                              ### Merges DVNTuple.root and before_BDT.root (containing extra variables)
#                                           SL                                      Using batch LSF interface for paraellel usage 
#                                           MCLc2pKpi
#                                           MCSLLc2pKpi
#           4) merge_pKpi --config   sample=Prompt                              ### Merges all the root files (using hadd) from the previous computation (addVariables)
#                                           SL                                      Outputs are Lc2pKpi root files stored in $TUPLELOCATION
#                                           MCLc2pKpi
#                                           MCSLLc2pKpi                         ### Note that this requirement could be linked with the previous one via the last root files 
#                                                                                   but unpractical
#          5) reduce_pKpi --config sample = Prompt or SL maxEvents=100000       ### reduces the data number of events up to {maxEvents} (for MVA optimisation)
#          6) optimizeMVA --config sample = Prompt or SL                        ### sends jobs saved in $EOSJOBSLOCATION with 3 different parameters optimized for BDT, BDTG and MLP
#          7) findBestMVA --config sample = Prompt or SL threshold=0.025        ### finds best parameters for BDT, BDTG and MLP and (inefficiently) checks for overtraining 
#                                                                                  by requiring that the overtraining check for train(test) ROC integral difference lies below {threshold}
#                                                                                  writes the best configurations results in ressource/bestMVA*.dat      
#          8) trainBestMVA --config sample = Prompt or SL                       ### trains the BDT, BDTG and MLP classifiers with best configurations from ressource/bestMVA{sample}.dat
#          9) displayMVA --config sample= Prompt or SL                          ### moves the and LcMVA_{sample}.root, .C and .xml files to mva/{sample}/ and runs the GUI of TMVA 
#          10) addMVA --config sample=Prompt                                    ### Adds MVA (BDT in this case) to the DVNTuple.root and before_BDT.root
#                                     MCLc2pKpi
#                                     MCLc2pmm
#                                     MCLc2ppipi
#                                     MCLc2pKK
#                                     MCD2KKpi
#                                     MCD2Kpipi
#                                     MCD2Kmumu
#                                     MCD2pimumu
#                                     SL
#                                     MCSLLc2pKpi
#                                     MCSLLc2pmm
#                                     MCSLLc2ppipi
#                                     MCSLLc2pKK
#          11) merge_PIDAndBDT --config sample = (see list above)               ### merges all the files separately in a CL16/MC16_{sample}_PIDAndBDT.root 
#
#          11bis) merge_Prompt_PIDAndBDT                                        ### merges all the files, e.g, Prompt MCLc2pKpi MCLc2pmm at ones using 11)
#                 merge_SL_PIDAndBDT
#                 merge_Prompt_PIDAndBDT_bkg
#                 merge_SL_PIDAndBDT_bkg 
#          12) correlationBDTLcM --config sample=Prompt or SL                   ### tests the correlation of the <BDT> and Lc_M
#          13) bestMVACut --config sampel = Prompt or SL                        ### optimisation of BDT requirement using Punzi FoM (to optimise MLP needs to change the cpp exec cmd in 
#                                                                                   tools/efficiency_tools.py 
#          14) plot_triggers --config sample= Prompt or SL                      ### plots triggers activation
#          15) hltone --config sample=Prompt or SL                              ### computes hltone/hlttwo trigger activation (Latex table filled automatically) 
#          16) hlttwo --config sample=Prompt or SL                                  !!!Remove comment on dumpAll() !!!                     
#          17) reduceFile --config sample = Prompt or SL                        ### applies full selection and creates CL16/MC16_{}_forFit.root files
#          18) fitPHI --config sample= Prompt or SL                             ### first performs fit on MC and stores MC parameters in ressource/
#                                                                                   then optimize the best requiremnt on L1/L2_MC15TuneV1_ProbNNmu at the same time in Lc2pphi channel
#          19) fitExtraMuon                                                     ### first fit the MCSLLc2pmm to find the parameters and then optimizes the extra muon 
#                                                                                   mu_MC15TuneV1_ProbNNmu requirement
#          20) fitRare --config sample=Prompt or SL                             ### fits 1) MC 2) Lc2pphi mode and fixes the parameters of the fit for 3) blind Lc2pmm fit
#                                                                                   stores the fit model in ressource/ so it can be used for CLs computation
#                                                                                   stores fit results of Lc2pphi fit in dat/fitter/RARE/*.dat
#          21) misID_peaks                                                      ### computes TGenPhaseSpace representation of Lc2pmm contaminations
#          22) misID_eff                                                        ### Effiency of the MISID for Lc2ppipi and D2KKpi stored in MISID.pkl
#                                                                                   !!! Remove comment on dumpAll() !!!
#          23) misID_study                                                      ### computes the expected number of events for the two contamination channels
#
#          23prime) sendMCResampling and applyMCResampling                      ### MCResampling corrections but not necessary 
#
#          24) runPIDCalib                                                      ### efficiency of the ProbNN variables
#                                                                                   !!! Remove comment on dumpAll() !!!
#          25) efficiency                                                       ### compute efficiencies and stores them in db (effForTex) .pkl dictionaries + fill_templates
#                                                                                   !!! Remove comment on dumpAll() !!!
#          26) fill_fitResult                                                   ### fills dictionaries with fitResult required by the rare fit in order to compute the CLs
#          27) fill_templates                                                   ### fills the template for the Latex tables
#
#          28) CLs --config sample=Prompt SL                                    ### comutes the CLs method for the fit model saved in the fitRare command, in order to work properly
#                                                                                   the fit results of the phi mode need to be filled in from the fill_fitResult
#
#
#
#
# Please see the official documentation for any further informations 
#
# http://snakemake.readthedocs.io/en/stable/index.html


######## Load config file ############

configfile: "cfg.yml"

######## parse options ############
if config["local"] == 'None':
    local = ''
else : 
    local='--local'
if config["force"] == 'None':
    force = ''
else:
    force = '--force'

######### Setup for the analysis (USER sensitive) ########
import os
tuplePath = os.getenv('TUPLELOCATION') # Impossible to use routines.LcAnalysis because of python3 or ROOT setup missing when activating snakemake

######### RULES ###########

rule compileFitter: #compiles the Lc2pmmAna.out executable to run all the scripts
    input : code = 'cpp/src/Lc2pmmAna.cpp',
            fitter = 'cpp/class/fitter.cpp',
            header = 'cpp/class/fitter.hpp'
    output: exe = 'cpp/Lc2pmmAna.out'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && cd $LCANAROOT/cpp && make && cd -"  

rule rawFit:
    input: 
        exe = 'cpp/Lc2pmmAna.out',
        script = 'python/variables/var_comparison.py'
    output:
        fitResult = expand('cpp/dat/fitter/RAW/{param}variables.dat',param=config["sample"])
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --fitRaw --mode {config[sample]}"

rule var_comparison: 
    input:
        script = 'python/variables/var_comparison.py',
        fitResult = expand('cpp/dat/fitter/RAW/{param}variables.dat',param=config["sample"])
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --draw --addVarDB --mode {config[sample]}"

rule PID:
    input:
        script = 'python/proton/process_significance.py',
        exe = 'cpp/Lc2pmmAna.out'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}" 

rule plot_PID:
    input:
        script = 'python/proton/draw_pPID.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

rule addVariables: ### Create separate aV.root with all the variables in one root file, sample = Prompt, SL, MCLc2pKpi, MCSLLc2pKpi
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 $LCANAROOT/python/routines/process_data.py -T aV -t Lc2pKpiTuple/DecayTree -d {config[sample]} -q 8nh -w addVariables '{config[sample]}' 'BDT' --friends 'before_BDT' --clone --makeFriends {force} --checkoutfile {local}"

################# parse filename and tree name #######################

from python.routines.dictionaries import datafiles as dataID, typeID, fitID, checkType

rule merge_pKpi: ### merge the different root files 
    input:
        script = 'python/BDT/merge_mvaclone.py'
    params:
        fileName = dataID[config["sample"]+'_beforeBDT'][0]        
    output:
        infile = tuplePath + dataID[config["sample"]+'_beforeBDT'][0]
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --dtype {config[sample]} --friendsPath friends --filePath aV --saveName {params.fileName}"

rule reduce_pKpi: # reduce the file for the MVA analysis (100 000 events)
    input:
        script = 'python/BDT/reduce_rootFile.py',
        infile = tuplePath + dataID[config["sample"]+'_beforeBDT'][0]
    params:
        dtype =  config["sample"]+'_beforeBDT',
        outname = dataID[config["sample"]+'_reduced'][0]
    output:
        infile = tuplePath + dataID[config["sample"]+'_reduced'][0]
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --rootfilesID {params.dtype} --saveName {params.outname} --tuplePath '{tuplePath}' --maxEvent {config[maxEvents]}"

rule optimizeMVA:
    input:
        script = 'python/BDT/generateMVAParam.py',
        trainMVA = 'python/MVA/trainMVA.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh &&$PY2 {input.script} --sendBDT --sendBDTG --sendMLP --mode {config[sample]}"    

rule findBestMVA:
    input:
        script = 'python/BDT/generateMVAParam.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --findMaxBDT --findMaxBDTG --findMaxMLP --threshold {config[threshold]} --mode {config[sample]}"

rule trainBestMVA: #Problems because of the segmentation fault in trainMVA.py with MLP
    input:
        script = 'python/MVA/trainMVA.py'
    output:
        weightC = 'python/dataset/weights/TMVAClassification_BDT.class.C',
        weightXML = 'python/dataset/weights/TMVAClassification_BDT.weights.xml',
        rootFile = expand('python/LcMVA_{mode}.root',mode=config["sample"])
    params:
        exe = 'MVA/trainMVA.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && cd python  && $PY2 {params.exe} --mode {config[sample]} --BDT --MLP --BDTG --readBestOpts && cd .. "

rule displayMVA:
    input:
        initWeightC = 'python/dataset/weights/TMVAClassification_BDT.class.C',
        initWeightXML = 'python/dataset/weights/TMVAClassification_BDT.weights.xml',
        rootFile = expand('python/LcMVA_{mode}.root',mode=config["sample"]) 
    output:
        weightC = expand('mva/{mode}/weights/TMVAClassification_BDT.class.C',mode=config["sample"]),
        weightXML = expand('mva/{mode}/weights/TMVAClassification_BDT.weights.xml',mode=config["sample"]),
        outputFile = expand('mva/{mode}/LcMVA_{mode}.root',mode=config["sample"])
    params:
        infolder = 'python/dataset/',
        outfolder = expand('mva/{mode}/',mode=config["sample"])
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && "
        "mv {params.infolder}/weights/* {params.outfolder}/weights/ &&"
        "mv {input.rootFile} mva/{config[sample]}/ && cd mva/{config[sample]}/ &&"
        "root -l GUI.C"

##### Redefinition of IDS for application of MVA #############

mode = typeID[config["sample"]][0]
tree = typeID[config["sample"]][1]
 
rule addMVA:
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 $LCANAROOT/python/routines/process_data.py -T {mode} -t '{tree}' -d {config[sample]} -q 1nh -w addVariablesAndMVA {mode} {config[MVA]} --friends 'before_BDT' --clone --makeFriends {force} --cuts cutStripPID {config[sample]} {local}"

rule merge_PIDAndBDT:
    input:
        script = 'python/BDT/merge_mvaclone.py'
    output:
        rootFile = tuplePath + dataID[config["sample"]+'_PIDAndBDT'][0]
    params:
        outname = dataID[config["sample"]+'_PIDAndBDT'][0],
        filepath = typeID[config["sample"]][0],
        dtype = config["sample"]
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --dtype {params.dtype} --filePath {params.filepath} --friendsPath friends --saveName {params.outname}"

rule merge_Prompt_PIDAndBDT:
    input:
        Prompt = tuplePath + dataID['Prompt_PIDAndBDT'][0],
        MCLc2pKpi = tuplePath + dataID['MCLc2pKpi_PIDAndBDT'][0],
        MCLc2pmm = tuplePath + dataID['MCLc2pmm_PIDAndBDT'][0]

rule merge_SL_PIDAndBDT:
    input:
        SL = tuplePath + dataID['SL_PIDAndBDT'][0],
        MCSLLc2pKpi = tuplePath + dataID['MCSLLc2pKpi_PIDAndBDT'][0],
        MCSLLc2pmm = tuplePath + dataID['MCSLLc2pmm_PIDAndBDT'][0]

rule merge_Prompt_PIDAndBDT_bkg:
    input:
        MCLc2ppipi = tuplePath + dataID['MCLc2ppipi_PIDAndBDT'][0],
        MCLc2pKK = tuplePath + dataID['MCLc2pKK_PIDAndBDT'][0],
        MCD2KKpi = tuplePath + dataID['MCD2KKpi_PIDAndBDT'][0],
        MCD2Kpipi = tuplePath + dataID['MCD2Kpipi_PIDAndBDT'][0],
        MCD2Kmumu = tuplePath + dataID['MCD2Kmumu_PIDAndBDT'][0],
        MCD2pimumu = tuplePath + dataID['MCD2pimumu_PIDAndBDT'][0]

rule merge_SL_PIDAndBDT_bkg:
    input:
        MCSLLc2ppipi = tuplePath + dataID['MCSLLc2ppipi_PIDAndBDT'][0],
        MCSLLc2pKK = tuplePath + dataID['MCSLLc2pKK_PIDAndBDT'][0]

rule correlationBDTLcM:
    input:
        script = 'python/BDT/draw_BDT_LcMM.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

rule bestMVACut:
    input:
        script = 'python/BDT/Lcpmm_BDT_efficiency.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

################### Trigger ###############################

rule plotTriggers:
    input:
        script = 'python/trigger/trigger.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

rule hltone:
    input:
        script ="python/trigger/hlt1.py"
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

rule hlttwo:
    input:
        script = "python/trigger/hlt2.py"
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"
##################### For Fit #############################

dtype = checkType[config["sample"]]


rule reduceFile:
    input:
        script = 'python/fit/create_cutted_rootFile.py'
    output:
        infile = tuplePath + fitID[dtype][0],
        inMCfile = tuplePath + fitID[dtype][1]
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

rule fitPHIMC:
    input:
        script = 'python/fit/fitPhi.py',
        exe = 'cpp/Lc2pmmAna.out'
    output:
        MCdat = expand('cpp/ressource/{mode}PHIIpatiaMCparsVariables.dat',mode=config["sample"]),
        MCroot = expand('cpp/ressource/{mode}PHIIpatiaMCparsVariables.root',mode=config["sample"])
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]} --fitPhi"

rule fitPHI:
    input:
        script = 'python/fit/fitPhi.py',
        exe = 'cpp/Lc2pmmAna.out',
        MCdat = expand('cpp/ressource/{mode}PHIIpatiaMCparsVariables.dat',mode=config["sample"]),
        MCroot = expand('cpp/ressource/{mode}PHIIpatiaMCparsVariables.root',mode=config["sample"])
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]} --fitPhi --NoMC --plotSigni"

rule fitExtraMuonMC:
    input:
        script = 'python/fit/fitExtraMuon.py',
        exe = 'cpp/Lc2pmmAna.out'
    output:
        MCdat = 'cpp/ressource/SLMUONIpatiaMCparsVariablesdat',
        MCroot ='cpp/ressource/SLMUONIpatiaMCparsVariables.root'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]} --fitMuon"

rule fitExtraMuon:
    input:
        script = 'python/fit/fitExtraMuon.py',
        exe = 'cpp/Lc2pmmAna.out',
        MCdat = 'cpp/ressource/SLMUONIpatiaMCparsVariables.dat',
        MCroot = 'cpp/ressource/SLMUONIpatiaMCparsVariables.root'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --fitMuon --NoMC --plotSigni"

rule fitRare:
    input:
        script = 'python/fit/fitRare.py',
        exe = 'cpp/Lc2pmmAna.out'
    output:
        model_dat = expand('cpp/ressource/{mode}RAREmodel_varNames.dat',mode=config["sample"]),
        model_root = expand('cpp/ressource/{mode}RAREmodel_varNames.root',mode=config["sample"])
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]} --fitBinning 45"

rule fitpKpi:
    input:
        script = 'python/fit/fitLc2pKpi.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --mode {config[sample]}"

###################### Bkg Study  #######################  

rule setProxy:
    output:
        proxySet = '.proxySet'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh &&"
        "lb-run Urania/latest && source  $LCANAROOT/scripts/setup_path.sh && "
        "source SetupProject.sh LHCbDirac v8r2p55 && lhcb-proxy-init &&"
        "echo >.proxySet"
       

rule misID_peaks:
    input:
        script = 'python/backgrounds/misID_peaks.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"

path_ressource = 'PID/PerfHistsMuon/'
pkl_path = 'LHCb/'
rule make_tables: # Needs to SetupProject LHCbDirac + lhcb-proxy-init (doesn't work like this but the shell commands are correct)
    input:
        script = 'PID/make_tables.py',
        proxy = '.proxySet'
    output:
#        Up_Mu = expand('{path}PerfHists_Mu_Turbo16_MagUp_P_ETA.root',path=path_ressource),
#        Up_Pi = expand('{path}PerfHists_Pi_Turbo16_MagUp_P_ETA.root',path=path_ressource),
#        Up_P = expand('{path}PerfHists_P_Turbo16_MagUp_P_ETA.root',path=path_ressource),
#        Up_K = expand('{path}PerfHists_K_Turbo16_MagUp_P_ETA.root',path=path_ressource),
#        Up_K2Mu = expand('{path}PerfHists_K2Mu_Turbo16_MagUp_P_ETA.root',path=path_ressource),
#        Down_Mu = expand('{path}PerfHists_Mu_Turbo16_MagDown_P_ETA.root',path=path_ressource),
#        Down_Pi = expand('{path}PerfHists_Pi_Turbo16_MagDown_P_ETA.root',path=path_ressource),
#        Down_P = expand('{path}PerfHists_P_Turbo16_MagDown_P_ETA.root',path=path_ressource),
#        Down_K = expand('{path}PerfHists_K_Turbo16_MagDown_P_ETA.root',path=path_ressource),
#        Down_K2Mu = expand('{path}PerfHists_K2Mu_Turbo16_MagDown_P_ETA.root',path=path_ressource)
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} && rm .proxySet"

rule misID_eff:
    input:
        script = 'PID/MISIDeff.py',
        Up_Mu = expand('{path}PerfHists_Mu_Turbo16_MagUp_P_ETA.root',path=path_ressource),
        Up_Pi = expand('{path}PerfHists_Pi_Turbo16_MagUp_P_ETA.root',path=path_ressource),
        Up_P = expand('{path}PerfHists_P_Turbo16_MagUp_P_ETA.root',path=path_ressource),
        Up_K = expand('{path}PerfHists_K_Turbo16_MagUp_P_ETA.root',path=path_ressource),
        Up_K2Mu = expand('{path}PerfHists_K2Mu_Turbo16_MagUp_P_ETA.root',path=path_ressource),
        Down_Mu = expand('{path}PerfHists_Mu_Turbo16_MagDown_P_ETA.root',path=path_ressource),
        Down_Pi = expand('{path}PerfHists_Pi_Turbo16_MagDown_P_ETA.root',path=path_ressource),
        Down_P = expand('{path}PerfHists_P_Turbo16_MagDown_P_ETA.root',path=path_ressource),
        Down_K = expand('{path}PerfHists_K_Turbo16_MagDown_P_ETA.root',path=path_ressource),
        Down_K2Mu = expand('{path}PerfHists_K2Mu_Turbo16_MagDown_P_ETA.root',path=path_ressource)
#    output:
#        misid = expand('{path}MISID.pkl',path=pkl_path)
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"


rule misID_study:
    input:
        script = 'python/backgrounds/bkg_study.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"
    

##################### MCRESAMPLING ###################################################
### Channel defintion for MCResampling
 
if config["channel"] == 'Lc2pmm':
    chan = 'MuMu'
else : chan = 'KPi'

rule sendMCResampling: # Requires SetupProject LHCbDirac + lhcb-init-proxy but shell command is correct
    input:
        script = 'PID/correctVar.py',
        proxy = '.proxySet'
    output : 
#        calib_file = expand('{path}PIDHists_Turbo16_{magnet}_{decay}.root',path=tuplePath,magnet=config["Mag"],decay=chan)
    shell:
        "source $LCANAROOT/scripts/setup_path.sh &&"
        "lb-run Urania/latest && source  $LCANAROOT/scripts/setup_path.sh && "
        "source SetupProject.sh LHCbDirac v8r2p55 && lhcb-proxy-init &&"
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --sendDistribution --channel {config[channel]} --mode {config[sample]} --Mag {config[Mag]} &&"
        "mv PIDHists_* {output.calib_file}"

rule applyMCResampling: # Requires Proxy (see previous one)
    input:
        script = 'PID/correctVar.py',
        calib_file = expand('{path}PIDHists_Turbo16_{magnet}_{decay}.root',path=tuplePath,magnet=config["Mag"],decay=chan),
        proxy = '.proxySet'
    shell:  
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} --sendCorrection --channel {config[channel]} --mode {config[sample]} --Mag {config[Mag]}  && rm .proxySet"
       
##################### NEED TO REMOVE .proxySet IN ORDER TO RUN THE PROXY AGAIN ########
##################### If not already removed ##########################################

###################### Efficiency #######################  

rule runPIDCalib: # Needs to be executed with a new bash terminal under Uran (lb-run Urania/latest bash)
    input:
        script = 'python/efficiency/runPIDCalib.py',
        proxy = '.proxySet'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script} && rm .proxySet" 

rule efficiency:
    input:
        script = 'python/efficiency/PIDcorr_efficiencies.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"

###################### Results in pkl + tex #############

rule fill_fitResult:
    input:
        script = 'python/results/fill_dict.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"
rule fill_templates:
    input:
        script = 'python/results/fill_template.py'
    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"

###################### CLs ##############################
rule CLs:
    input:
        script = 'python/CLs/CLs_LcAna.py',
        model_dat = expand('cpp/ressource/{mode}RAREmodel_varNames.dat',mode=config["sample"]),
        model_root = expand('cpp/ressource/{mode}RAREmodel_varNames.root',mode=config["sample"])

    shell:
        "source $LCANAROOT/scripts/setup_path.sh && $PY2 {input.script}"
