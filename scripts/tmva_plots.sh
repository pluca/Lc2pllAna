tmvadir='/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.36/x86_64-slc6-gcc48-opt/root/tmva/test/'
mvadir=${LCANAROOT}'/mva/'$1
mvafile=${mvadir}'/LcMVA_'$1'.root'

#root -l ${tmvadir}'/TMVAGui.C("LcMVA.root")'

cd ${mvadir}

root -l -q ${tmvadir}'/variables.C("'${mvafile}'")'
root -l -q ${tmvadir}'/correlations.C("'${mvafile}'")'
root -l -q ${tmvadir}'/correlationscatter.C("'${mvafile}'")'
root -l -q ${tmvadir}'/mvas.C("'${mvafile}'")'
root -l -q ${tmvadir}'/mvaseffs.C("'${mvafile}'")'
root -l -q ${tmvadir}'/efficiencies.C("'${mvafile}'")'
#root -l -q ${tmvadir}'/paracoor.C("'${mvafile}'")'
root -l -q ${tmvadir}'/BDTControlPlots.C("'${mvafile}'")'
#root -l -q ${tmvadir}'/rulevis.C("'${mvafile}'")'
#root -l -q ${tmvadir}'/PlotFoams.C("'${mvafile}'")'
#root -l -q ${tmvadir}'/mvarefs.C("'${mvafile}'")'
#root -l -q ${tmvadir}'/BDT.C("'${mvafile}'")'

cd -

