\def\misid {{\begin{{table}}[h]
\begin{{center}}
   \caption{{\label{{MISID}} Identification probability for candidates from the \ppipi and $\decay{{D^+}}{{K^+K^-\pi^+}}$ decays to be reconstructed as \pmm.}}
   \begin{{tabular}}{{c|ccc}}
    \hline
 \textbf{{Efficiency \boldsymbol{{$\varepsilon$}}}} & \textbf{{Magnet Up}} & \textbf{{Magnet Down}} & \textbf{{Mean value}}  \\
    \hline
 $p(\rightarrow p)$  & ${MCLc2ppipi[PUp][0]} {MCLc2ppipi[PUp][1]} {MCLc2ppipi[PUp][2]}$ &  ${MCLc2ppipi[PDown][0]} {MCLc2ppipi[PDown][1]} {MCLc2ppipi[PDown][2]}$ &  ${MCLc2ppipi[P][0]} {MCLc2ppipi[P][1]} {MCLc2ppipi[P][2]}$   \\
 $\pi(\rightarrow \mu)$  & ${MCLc2ppipi[Pi1Up][0]} {MCLc2ppipi[Pi1Up][1]} {MCLc2ppipi[Pi1Up][2]}$ &  ${MCLc2ppipi[Pi1Down][0]} {MCLc2ppipi[Pi1Down][1]} {MCLc2ppipi[Pi1Down][2]}$ &  ${MCLc2ppipi[Pi1][0]} {MCLc2ppipi[Pi1][1]} {MCLc2ppipi[Pi1][2]}$   \\
 $K(\rightarrow p)$  & ${MCD2KKpi[KUp][0]} {MCD2KKpi[KUp][1]} {MCD2KKpi[KUp][2]}$ &  ${MCD2KKpi[KDown][0]} {MCD2KKpi[KDown][1]} {MCD2KKpi[KDown][2]}$ &  ${MCD2KKpi[K][0]} {MCD2KKpi[K][1]} {MCD2KKpi[K][2]}$   \\
 $K(\rightarrow \mu)$  & ${MCD2KKpi[K2Up][0]} {MCD2KKpi[K2Up][1]} {MCD2KKpi[K2Up][2]}$ &  ${MCD2KKpi[K2Down][0]} {MCD2KKpi[K2Down][1]} {MCD2KKpi[K2Down][2]}$ &  ${MCD2KKpi[K2][0]} {MCD2KKpi[K2][1]} {MCD2KKpi[K2][2]}$   \\
    \hline
$p\pi^+\pi^-(\rightarrow p\mumu)$ & - & - & ${MCLc2ppipi[FullEff][0]} {MCLc2ppipi[FullEff][1]} {MCLc2ppipi[FullEff][2]}$   \\
$K^+K^-\pi^+(\rightarrow p\mumu)$ & - & - & ${MCD2KKpi[FullEff][0]} {MCD2KKpi[FullEff][1]} {MCD2KKpi[FullEff][2]}$   \\
    \hline
   \end{{tabular}}
\end{{center}}
\end{{table}}
}}

\def\expected2 {{\begin{{table}}[h]
\begin{{center}}
   \caption{{\label{{Expected}} Branching fraction, efficiency of the full selection, efficiency of the mis-identifcation and expected number of events for the \ppipi contamination mode. }}
   \begin{{tabular}}{{c|c}}
    \hline
 Decay & $\mathcal{{B}}$  &Efficiency Selection &Efficiency mis-identification & Expected events \\
    \hline
 \ppipi &${BR_MCLc2ppipi[0]} {BR_MCLc2ppipi[1]} {BR_MCLc2ppipi[2]}$ &${Eff_MCLc2ppipi[0]} {Eff_MCLc2ppipi[1]} {Eff_MCLc2ppipi[2]}$& ${MCLc2ppipi[FullEff][0]} {MCLc2ppipi[FullEff][1]} {MCLc2ppipi[FullEff][2]}$ & < {ExpectedLimit_MCLc2ppipi:.3f} at 90 \% CL  \\
    \hline
   \end{{tabular}}
\end{{center}}
\end{{table}}
}}

\def\expected {{\begin{{table}}[h]
\begin{{center}}
   \caption{{\label{{Expected}} Branching fraction, efficiency of the full selection, efficiency of the mis-identifcation and upper limit on the expected number of events for the \ppipi and $\decay{{D^+}}{{K^+K^-\pi^+}}$ contamination modes. }}
   \begin{{tabular}}{{c|cc}}
    \hline
 \textbf{{Parameter}} & \textbf{{\boldsymbol{{$\Lambda_c^+\rightarrow p\pi^+\pi^-$}}}} &  \textbf{{\boldsymbol{{$D^+\rightarrow K^+K^-\pi^+$}}}} \\
    \hline
Branching fraction $\mathcal{{B}}$& ${BR_MCLc2ppipi[0]} {BR_MCLc2ppipi[1]} {BR_MCLc2ppipi[2]}$ &${BR_MCD2KKpi[0]} {BR_MCD2KKpi[1]} {BR_MCD2KKpi[2]}$ \\
Efficiency selection & ${Eff_MCLc2ppipi[0]} {Eff_MCLc2ppipi[1]} {Eff_MCLc2ppipi[2]}$ & ${Eff_MCD2KKpi[0]} {Eff_MCD2KKpi[1]} {Eff_MCD2KKpi[2]}$  \\
Efficiency mis-identification & ${MCLc2ppipi[FullEff][0]} {MCLc2ppipi[FullEff][1]} {MCLc2ppipi[FullEff][2]}$ &${MCD2KKpi[FullEff][0]} {MCD2KKpi[FullEff][1]} {MCD2KKpi[FullEff][2]}$  \\
    \hline
Expected $N$ & < {ExpectedLimit_MCLc2ppipi:.3f} at 90 \% CL & ${ExpectedLimit_MCD2KKpi[0]} {ExpectedLimit_MCD2KKpi[1]} {ExpectedLimit_MCD2KKpi[2]}$ \\
    \hline
   \end{{tabular}}
\end{{center}}
\end{{table}}
}}

