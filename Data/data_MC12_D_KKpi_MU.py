#-- GAUDI jobOptions generated on Sat Dec  3 08:45:50 2016
#-- Contains event types : 
#--   21163002 - 30 files - 605267 events - 158.99 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-125363 

#--  StepId : 125363 
#--  StepName : Merge14 for Sim08 2012 Charm Filtered Productions (Amato)  
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v33r5 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DVMergeDST.py;$APPCONFIGOPTS/DaVinci/DataType-2012.py;$APPCONFIGOPTS/Merging/WriteFSR.py;$APPCONFIGOPTS/Merging/MergeFSR.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r164 
#--  Visible : N 


#--  Processing Pass Step-126557 

#--  StepId : 126557 
#--  StepName : Stripping20Filtered for Charm WG (Gobel) 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v33r5 
#--  OptionFiles : $CHARMCONFIGOPTS/MCFiltering/D2HHHStripTriggerFiltering.py;$APPCONFIGOPTS/DaVinci/DataType-2012.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r164;CharmConfig.v3r12 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000001_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000002_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000003_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000004_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000005_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000006_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000007_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000008_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000009_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000010_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000011_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000012_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000013_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000014_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000015_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000016_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000017_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000018_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000019_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000020_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000021_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000022_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000023_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000024_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000025_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000026_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000027_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000036_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000037_1.d2hhh.hltstrip.dst',
'LFN:/lhcb/MC/2012/D2HHH.HLTSTRIP.DST/00036742/0000/00036742_00000038_1.d2hhh.hltstrip.dst'
], clear=True)
