#-- GAUDI jobOptions generated on Fri Nov 18 21:01:47 2016
#-- Contains event types : 
#--   25113000 - 101 files - 2039399 events - 580.73 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-124834 

#--  StepId : 124834 
#--  StepName : Reco14a for MC 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v43r2p7 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2012.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r164 
#--  Visible : Y 


#--  Processing Pass Step-125836 

#--  StepId : 125836 
#--  StepName : Stripping20-NoPrescalingFlagged for Sim08 - Implicit merging. 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v32r2p1 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping20-Stripping-MC-NoPrescaling.py;$APPCONFIGOPTS/DaVinci/DataType-2012.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r164 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000001_2.AllStreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000002_2.AllStreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000003_2.AllStreams.dst'#,
#'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000004_2.AllStreams.dst',
#'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000005_2.AllStreams.dst',
#'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000006_2.AllStreams.dst',
#'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00043463/0000/00043463_00000105_2.AllStreams.dst'
], clear=True)
