#ifndef SIGPROCESSER_H
#define SIGPROCESSER_H
#include "analyser.hpp"
#include "LcAnaCuts.hpp"
#include "general_functions.hpp"
#include <fstream>
#include <cstdlib>


namespace sigproc{

void process_p_ProbNNp_sig(const std::string& sl,const std::string& intree,const  std::string& infile, RooRealVar* vM,const std::string& units, Str2VarMap& MCpars, TFile* tmpout,const int& itmax,TH2F* hist_significance,TH2F* hist_covQual,TH2F* hist_edm,const std::string& fitopt,const double& lowerLimit  ){
  	 double store(0);
  	 //TH2F* hist_significance=new TH2F("Significance","significane",10,0,1,200,0,200);
  	 double step((1-lowerLimit)/itmax);
	 for (int i(0); i<itmax; i++)
   	 {
   	 TCut dataCut = LcAnaCuts::convergence;
   	 std::string str="p_MC15TuneV1_ProbNNp >"+std::to_string(step*i+lowerLimit);
   	 TCut p_PID     = str.c_str();
 	 dataCut+=p_PID;
   
      	 Analysis * ana = new Analysis(sl+"Lc2pKpi","#Lambda_{c}#rightarrowpK#pi",intree,infile,vM,&dataCut);
  	 ana->SetUnits(units);
 	 ana->AddAllVariables();
    	 ana->SetSignal("Ipatia",5.e4,"",MCpars);
     	 ana->AddBkgComponent("Comb","Cheb1-v1[0.,-0.4,0.4]");//-v2[0.,-0.9,0.9]");
         tmpout->cd();
         ana->Initialize("-docuts");
     	 ana->Fit(90,false,fitopt);
      	 store=ana->GetNSigVal()/sqrt(ana->GetNSigVal()+ana->GetNBkgVal());
         hist_significance->Fill(i*step+lowerLimit,store);
         RooFitResult* result= ana->GetFitRes();
	 hist_covQual->Fill(i*step+lowerLimit,result->covQual());
	 hist_edm->Fill(i*step+lowerLimit,result->edm());
   	 }
	};

TCut add_p_PID_cut(double& lower_cut){
	string str="p_MC15TuneV1_ProbNNp >"+std::to_string(lower_cut);
	return TCut(str.c_str());
	};	 	
double compute_significance(int& NSig, int& NBkg){
	return  NSig/sqrt(NSig+NBkg);};
void write_variables(const double& lower_cut,const double& NSig,const double& NBkg,const int& covQual,const double& edm, const string& sl,const string& mode,const string& dtype=""){
	std::ofstream out;
	std::string append;
    std::string name;
        if(sl=="")    append="Prompt";
        else   { append=sl;}
        if (dtype==""){
	name="dat/"+append+"/"+mode+"/"+append+"_lowerLimit_"+std::to_string(lower_cut)+"_"+"variables.dat";}
        else {
	 name="dat/"+append+"/"+mode+"/"+dtype+"/"+append+"_lowerLimit_"+std::to_string(lower_cut)+"_"+"variables.dat";}
        cout << name << endl;
    out.open(name.c_str());
	out<<lower_cut<<" " << NSig <<" "<<NBkg << " "<<covQual <<" "<<edm <<std::endl;
    std::cout<<lower_cut<<" " << NSig <<" "<<NBkg << " "<<covQual <<" "<<edm <<std::endl;
	out.close();
	};
void write_Str2VarMap(const string& append,RooWorkspace* ws, const Str2VarMap& MCpars,string Ipatianame=""){
	std::ofstream out;
        std::string dat_name="ressource/"+append+Ipatianame+"MCparsVariables.dat";
	std::string root_name="ressource/"+append+Ipatianame+"MCparsVariables.root";
	out.open(dat_name.c_str());
        for (auto el: MCpars){ out<< el.second->GetName()<<std::endl;}
        ws->writeToFile(root_name.c_str());
        out.close();
    cout << "Writing finished! "<<endl;    
	};
Str2VarMap read_Str2VarMap (string& append,string Ipatia=""){

	Str2VarMap MCpars;
	
        std::string dat_name="ressource/"+append+Ipatia+"MCparsVariables.dat";
	std::string ws_name=append+Ipatia+"MCparsVariables";
	std::string root_name="ressource/"+ws_name+".root";

	TFile f(root_name.c_str());
	
	RooWorkspace* ws= (RooWorkspace*)f.Get(ws_name.c_str());

	std::ifstream in_dat;
	in_dat.open(dat_name.c_str());
	std::string line;
	
		while(getline(in_dat,line)){
			std::string str;
			str.push_back(line[0]);
			RooAbsReal* roo_obj=(RooAbsReal*)ws->obj(line.c_str());
			MCpars.insert(pair<string, RooAbsReal*>(str,roo_obj));			 
					  }				 
	in_dat.close();
	return MCpars;
	 };
	void write_ROCvariables(const string& nameK,const double& cutK ,const string& namePi,const double& cutPi ,const double& NSig,const double& NBkg,const int& covQual,const double& edm, const string& sl,const string& mode,const string& dtype=""){
	std::ofstream out;
	std::string append;
    std::string name;
        if(sl=="")    append="Prompt";
        else   { append=sl;}
        if (dtype==""){
	name="dat/"+append+"/"+mode+"/"+nameK+to_string(cutK)+namePi+to_string(cutPi)+"_"+"variables.dat";}
        else {
	 name="dat/"+append+"/"+mode+"/"+dtype+"/"+nameK+std::to_string(cutK)+namePi+std::to_string(cutPi)+"_"+"variables.dat";}
        cout << name << endl;
    out.open(name.c_str());
	out<<cutK<<" " << cutPi<<" " << NSig <<" "<<NBkg << " "<<covQual <<" "<<edm <<std::endl;
    std::cout<<cutK<<" " << cutPi<<" " << NSig <<" "<<NBkg << " "<<covQual <<" "<<edm <<std::endl;
	out.close();
	};


};
namespace fomprocesser{

void write_events(const string& append, const double& events, const double& bdtCut){          
        string path="dat/"+append+"/FoM/"+"Lc2pmmBkgFit_BDTcut_"+to_string(bdtCut)+".dat";
        ofstream out;
        out.open (path.c_str());
        out << events<< endl;
        out.close();};

                      };

#endif
