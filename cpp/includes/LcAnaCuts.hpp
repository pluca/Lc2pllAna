#ifndef LCANACUTS_H
#define LCANACUTS_H

#include "TCut.h"
#include <string>

namespace LcAnaCuts {

	TCut p_PID     = "p_MC15TuneV1_ProbNNp > 0.805";
    TCut inv_p_PID = "p_MC15TuneV1_ProbNNp < 0.805";

    TCut p_PID_SL  = "p_MC15TuneV1_ProbNNp > 0.155";
    TCut inv_p_PID_SL = "p_MC15TuneV1_ProbNNp < 0.155";
    //Prompt p_PID Analysis 
	TCut pi_PID    = "L2_MC15TuneV1_ProbNNpi > 0.5 && L2_MC15TuneV1_ProbNNK > 0.5 && L2_MC15TuneV1_ProbNNmu < 0.1 && L2_MC15TuneV1_ProbNNe < 0.4";
	TCut K_PID     = "L1_MC15TuneV1_ProbNNK > 0.5 && L1_MC15TuneV1_ProbNNpi > 0.5 && L1_MC15TuneV1_ProbNNmu < 0.1 && L1_MC15TuneV1_ProbNNe < 0.4";

	TCut mumu_PID  = "L1_MC15TuneV1_ProbNNmu > 0.7 && L2_MC15TuneV1_ProbNNmu > 0.7";
	TCut ee_PID    = "L1_MC15TuneV1_ProbNNe > 0.7 && L2_MC15TuneV1_ProbNNe > 0.7";
	TCut emu_PID   = "((L1_MC15TuneV1_ProbNNe > 0.7 && L2_MC15TuneV1_ProbNNmu > 0.7) || (L1_MC15TuneV1_ProbNNmu > 0.7 && L2_MC15TuneV1_ProbNNe > 0.7)) && L1_MC15TuneV1_ProbNNpi < 0.5 && L2_MC15TuneV1_ProbNNpi < 0.5";

	TCut pKpi_PID  = p_PID && pi_PID && K_PID;
	TCut pmumu_PID = p_PID && mumu_PID;
	TCut pee_PID   = p_PID && ee_PID;
	TCut pemu_PID  = p_PID && emu_PID;

    TCut p_PIDCalib      = "Brunel_MC15TuneV1_ProbNNp > 0.805";
    TCut mu_PIDCalib     = "Brunel_MC15TuneV1_ProbNNmu > 0.225";
    TCut p_PIDCalib_SL   = "Brunel_MC15TuneV1_ProbNNp > 0.155";
    TCut mu_PIDCalib_SL  = "Brunel_MC15TuneV1_ProbNNmu > 0.005";
    TCut K_PIDCalib      = "Brunel_MC15TuneV1_ProbNNK > 0.8";
    TCut pi_PIDCalib     = "Brunel_MC15TuneV1_ProbNNpi > 0.8";

    TCut matchNoComb = "TMath::Abs(Lc_TRUEID)==4122";
    //TCut match_Lc2pKpi_Full = matchNoComb && 
    TCut match_Lc2pKpi_Full = matchNoComb && "TMath::Abs(p_TRUEID)==2212 && ( (TMath::Abs(L1_TRUEID)==321 && TMath::Abs(L2_TRUEID)==211) || (TMath::Abs(L1_TRUEID)==211 && TMath::Abs(L2_TRUEID)==321) )";
    
    TCut match_Lc2pmm_Full = matchNoComb && "TMath::Abs(p_TRUEID)==2212 && ( (TMath::Abs(L1_TRUEID)==13 && TMath::Abs(L2_TRUEID)==13) )";
    TCut sideBand = "(Lc_DTF_MM < 2255 || Lc_DTF_MM > 2320) && Lc_DTF_MM < 2350";
    TCut sigMassWin = "(Lc_DTF_MM > 2260 && Lc_DTF_MM < 2310)";
    //TCut sideBand_SL = "(Lc_DTF_M < 2260 || Lc_DTF_MM > 2320) && Lc_DTF_MM < 2350";
    //TCut sigMassWin_SL = "(Lc_DTF_M > 2260 && Lc_DTF_MM < 2310)"; 

    TCut sideBand_raw = "(Lc_DTF_PV_M[0] < 2260 || Lc_DTF_PV_M[0] > 2320) && Lc_DTF_PV_M[0] < 2350";
    TCut sigMassWin_raw = "(Lc_DTF_PV_M[0] > 2265 && Lc_DTF_PV_M[0] < 2295)";
    
    TCut sW_weight = "nsig_Lc2pKpi_sw";
    TCut sW_masswin = "(Lc_DTF_MM > 2240 && Lc_DTF_MM < 2330)";
    TCut sWcut = sW_weight * sW_masswin;
    
    TCut blinding_sideband = "(Lc_MM <2260 || Lc_MM >2320) && Lc_MM < 2350";

    TCut convergence = "Lc_DTF_chi2[0] > 0 && TMath::Abs(Lc_TAU) < 10";
    TCut convergence2 = "Lc_DTF_chi2_0 > 0 && TMath::Abs(Lc_TAU) < 10";

    TCut newStripping = "Lc_MAXDOCA<0.15 && (Lc_ENDVERTEX_CHI2/Lc_ENDVERTEX_NDOF)<5 && Lc_IPCHI2_OWNPV<25 && Lc_DIRA_OWNPV>0.9999 &&(p_P >2000 && L1_P>2000 && L2_P>2000) && (p_PT>300 && L1_PT>300 && L2_PT>300) && (p_IPCHI2_OWNPV>5 && L1_IPCHI2_OWNPV>5 && L2_IPCHI2_OWNPV>5) && (p_TRACK_CHI2NDOF<5 && L1_TRACK_CHI2NDOF<5 && L2_TRACK_CHI2NDOF<5) && (p_TRACK_GhostProb<0.1 && L1_TRACK_GhostProb<0.1 && L2_TRACK_GhostProb<0.1) ";
                        
    TCut oldStripping = "(299.792458*Lc_TAU)>0.1 &&Lc_PT>1000 && Lc_ENDVERTEX_CHI2<25 && ( p_TRACK_GhostProb<0.5 && L1_TRACK_GhostProb<0.5 && L2_TRACK_GhostProb<0.5) && (p_IPCHI2_OWNPV>9 && L1_IPCHI2_OWNPV>9 && L2_IPCHI2_OWNPV>9) && ( p_PT>250 && L1_PT >250 && L2_PT>250) && (p_P>10000 && L1_P>3200 && L2_P>3200)";                    


    TCut bdt = " BDTout > 0.14";
    TCut bdt_SL = "BDTout > 0.00";
    TCut probNNCut = "L1_ProbNNk>0.8 && L2_ProbNNpi>0.8 "; //To be modified following test/probNN_analysis.py

    TCut trigger_hadron_configuration = " Lc_Hlt1TrackAllL0Decision_TOS && Lc_L0HadronDecision_TOS "; 
    //See test/trigger.py 
    TCut trigger_muon_configuration = " ( Lc_Hlt1TrackAllL0Decision_TOS || Lc_Hlt1TrackMuonDecision_TOS ) && Lc_L0MuonDecision_TOS ";
    TCut trigger_muon_configuration_SL = " ( Lc_Hlt1TrackAllL0Decision_TOS || Lc_Hlt1TrackMuonDecision_TOS ) ";
    //See test/trigger.py
    TCut probNNmu_cut = " L1_MC15TuneV1_ProbNNmu > 0.36 && L2_MC15TuneV1_ProbNNmu > 0.36";
    //Check to make sure the variable is correctly initialised


    TCut trigger_SL = "Lb_L0MuonDecision_TOS";
//No PID 
    TCut full_cut = newStripping && oldStripping && bdt && convergence2;
    TCut Lc2pmm_probNN = p_PID && probNNmu_cut;

    TCut Lc2pKpi_probNN = p_PID && probNNCut;

    // New Stripping S28 configurations:
    //
    TCut newStripping_ghostProb = "(p_TRACK_GhostProb<0.1 && L1_TRACK_GhostProb<0.1 && L2_TRACK_GhostProb<0.1)";
    TCut convergence_newVars = "Lc_VtxIso_Lc_VTXISODCHI2MASSTWOTRACK > 0 && Lc_Cone_PTAsym_1.0_h < 1 &&  Lc_Cone_PTAsym_1.0_l1 < 1 &&  Lc_Cone_PTAsym_1.0_l2 < 1";
    TCut bdt_cut = "BDTout > 0.210 ";
    TCut bdt_cut_SL ="BDTout > 0.0";
    TCut L0_triggers = "Lc_L0MuonDecision_TOS" ; 
    TCut Hlt1_triggers = " Lc_Hlt1TrackMuonDecision_TOS || Lc_Hlt1TrackMVADecision_TOS || Lc_Hlt1TrackMuonMVADecision_TOS || Lc_Hlt1TwoTrackMVADecision_TOS " ;
    TCut Hlt2_triggers = "Lc_Hlt2Phys_TOS";
    TCut L0_triggers_SL = "Lb_L0MuonDecision_TOS " ;
    TCut Hlt1_triggers_SL = " Lc_Hlt1TrackAllL0Decision_TOS || Lc_Hlt1TrackMuonDecision_TOS";
    TCut L0_pKpi = "Lc_L0HadronDecision_TOS";
    TCut Hlt1_pKpi = " Lc_Hlt1TrackMVADecision_TOS || Lc_Hlt1TwoTrackMVADecision_TOS";
    //Triggers
    TCut triggers = L0_triggers && Hlt1_triggers && Hlt2_triggers; 
    TCut triggers_SL = L0_triggers_SL && Hlt1_triggers_SL ;
    TCut triggers_pKpi = L0_pKpi && Hlt1_pKpi;
    TCut triggers_pKpi_SL = L0_triggers_SL;

    //Phi requirement
    TCut phi_mass_window = "TMath::Abs(TMath::Sqrt(q2) - 1.01946 ) < 0.035 " ;
    //ProbNN
    TCut probNN_mu = "L1_MC15TuneV1_ProbNNmu > 0.225 && L2_MC15TuneV1_ProbNNmu > 0.225";
//    TCut probNN_mu_SL = "L1_MC15TuneV1_ProbNNmu > 0.005 && L2_MC15TuneV1_ProbNNmu > 0.005";
    TCut probNN_mu_SL = "L1_MC15TuneV1_ProbNNmu > 0.005 && L2_MC15TuneV1_ProbNNmu > 0.005";
    TCut extra_mu_probNN_mu_SL = "muon_MC15TuneV1_ProbNNmu > 0.800";
    // Blinding 
    TCut blind_phi = "TMath::Abs(TMath::Sqrt(q2) - 1.01946 ) > 0.04";
    TCut blind_omega = "TMath::Abs(TMath::Sqrt(q2) - 0.775 ) > 0.04";
    TCut blind_eta = "TMath::Abs(TMath::Sqrt(q2) - 0.547862 ) > 0.04";
    //Corrected PID variables for MC only
    TCut p_PID_corr = "p_MC15TuneV1_ProbNNpcorr > 0.805";
    TCut probNN_mu_corr = "L1_MC15TuneV1_ProbNNmucorr > 0.225 && L2_MC15TuneV1_ProbNNmucorr > 0.225"; 
    //Phi mass regions definition
    TCut low_mass_phi = "TMath::Sqrt(q2) < 1.01946 - 0.035";
    TCut in_mass_phi = "TMath::Abs(TMath::Sqrt(q2)-1.01946) < 0.035";
    TCut high_mass_phi = "TMath::Sqrt(q2) > 1.01946 + 0.035";
     
    //Optimisation phi channel
    TCut bdt_forPhi = "BDTout > -0.44";


    // Old configuration for test
    TCut old_config = "p_MC15TuneV1_ProbNNp > 0.685 && BDTout > 0.14 && L1_MC15TuneV1_ProbNNmu > 0.36 && L2_MC15TuneV1_ProbNNmu > 0.36 ";
//    old_config = old_config && L0_triggers && convergence2 && newStripping_ghostProb ;  
    TCut plot_LcTau = "Lc_TAU > 0";
    //phi mass in GeV/c^2
    //Configuration for RAW datafit in order to plot discriminating variables
    TCut config_RAW = convergence ;
    TCut config_RAW_SL = convergence2;

    //Configuration for p PID Significance optimisation on Lc2pKpi
    TCut config_PID_SL = config_RAW_SL ;
    TCut config_PID = config_RAW  ;
    
    //Configuration for BDT/MLP test/training 
    TCut config_BDT = config_PID && p_PID  && newStripping_ghostProb && plot_LcTau  ;
    TCut config_BDT_SL = config_PID_SL && p_PID_SL && plot_LcTau ;

    //Configuration to apply on Lc2pmm after BDT
    TCut config_Lc2pmm_applyBDT_MC = config_PID && newStripping_ghostProb && match_Lc2pmm_Full;
    TCut config_Lc2pKpi_applyBDT_MC = config_PID && newStripping_ghostProb && match_Lc2pKpi_Full;
    TCut config_applyBDT = config_BDT ;
    
    TCut config_Lc2pmm_applyBDT_MC_SL = config_PID_SL && match_Lc2pmm_Full; 
    TCut config_Lc2pKpi_applyBDT_MC_SL = config_PID_SL  && match_Lc2pKpi_Full;
    TCut config_applyBDT_SL = config_BDT_SL ;

    //Configuration for the sideband fit
    TCut config_sidebandFit = config_PID && p_PID && newStripping_ghostProb;
    TCut config_sidebandFit_SL = config_PID_SL && p_PID_SL; 
    
    TCut config_sidebandFit_MC = config_PID && newStripping_ghostProb && match_Lc2pmm_Full;
    TCut config_sidebandFit_MC_SL = config_PID_SL && match_Lc2pmm_Full; 
    
    // for triggers
    TCut config_forTriggers = config_PID && bdt_cut && newStripping_ghostProb;
    TCut config_forTriggers_SL = config_PID_SL && bdt_cut_SL ;

    // apply full Selection (no mu cut)
    TCut config_forFit = config_PID && bdt_cut && newStripping_ghostProb && L0_triggers && Hlt1_triggers;
    TCut config_forFit_SL = config_PID_SL && bdt_cut_SL && triggers_SL;

    // fit Phi configuration
    TCut config_fitPhi = config_forTriggers && L0_triggers && phi_mass_window && Hlt1_triggers && Hlt2_triggers; 
    TCut config_fitPhi_SL = convergence2 && p_PID_SL && bdt_cut_SL && triggers_SL && phi_mass_window && extra_mu_probNN_mu_SL;
    // Full selection cut
    TCut full_selection = convergence2 && p_PID && newStripping_ghostProb && bdt_cut && L0_triggers && probNN_mu && Hlt1_triggers &&Hlt2_triggers;
    TCut full_selection_MC = convergence2 && p_PID_corr && newStripping_ghostProb && bdt_cut && L0_triggers && probNN_mu_corr && Hlt1_triggers && Hlt2_triggers;
    TCut full_selection_SL = convergence2 && p_PID_SL && bdt_cut_SL && triggers_SL && probNN_mu_SL && extra_mu_probNN_mu_SL;
    //For backgroudn studies
    TCut config_full_Lc2pKpi = convergence2 && p_PID && bdt_cut ;//&& triggers_pKpi ;
    TCut config_full_Lc2pKpi_SL = convergence2 && p_PID_SL && bdt_cut_SL && triggers_pKpi_SL && probNN_mu_SL && extra_mu_probNN_mu_SL;
    TCut config_bkg_study = bdt_cut && newStripping_ghostProb && L0_triggers && Hlt1_triggers && Hlt2_triggers && blind_phi && blind_omega && blind_eta;
    TCut config_bkg_study_MC = convergence;
    // For efficiency only Prompt
    TCut config_efficiency = full_selection ;
    // For phi significance optimisation in SL mode
    TCut config_sigphi_SL = p_PID_SL && triggers_SL && phi_mass_window;  
    TCut config_extraMuon_SL = p_PID_SL && triggers_SL && bdt_cut_SL && phi_mass_window;
    TCut config_extraMuon_phiMode_SL = p_PID_SL && triggers_SL && phi_mass_window && bdt_forPhi;   

}

#endif
