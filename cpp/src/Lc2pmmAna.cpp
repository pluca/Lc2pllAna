#include "general_functions.hpp"
#include "analyser.hpp"
#include "multi_analyser.hpp"
#include "LcAnaCuts.hpp"
#include "sigprocesser.hpp"
#include <cstdlib>
#include "../class/fitter.hpp"
int main(int argc, char *argv[]) {
    gROOT->ProcessLine(".x /afs/cern.ch/user/m/mstamenk/Lc2pllAna/scripts/lhcbStyle.C");
    
    //Default parameters
    string tuplePath = (string)(std::getenv("MARKOTUPLELOCATION"))+ "/";
    string inMCfile = "MC16_Lc2pKpi_S28.root";
    string infile = "CL16_Lc2pKpi_S28_100k.root";
    string inMCtree = "DecayTree";
    string intree = "DecayTree"; 
    binned bin;
    bin.push_back(2285); bin.push_back(2250); bin.push_back(2330);
    string model="Johnson"; 
    TCut MCcut=LcAnaCuts::match_Lc2pKpi_Full; 
    TCut dataCut=TCut("Lc_M<2350"); 
    string var="Lc_M"; 
    string fitName="Lc2pKpi"; 
    string label="#Lambda_{c}^{+}#rightarrow pK#pi";
    string id="0.0";
    string option="RAW";
    string cut="0";
    string mvaVar="BDT";
    bool fitMC=true;
    bool oldConfig=false;
    bool sideband=false;
    bool phimode=false;
    double min_blind(0);
    double max_blind(0);
    string mode="Prompt";
    int fitBinning = 90;
//****************** Argument Parser ***********************
    for (int i(0); i<argc;i++){
        if((string)argv[i]=="-intree") intree=string(argv[i+1]);
        if((string)argv[i]=="-infile") infile=string(argv[i+1]);
        if((string)argv[i]=="-inMCtree") inMCtree=string(argv[i+1]);
        if((string)argv[i]=="-inMCfile") inMCfile=string(argv[i+1]);
        if((string)argv[i]=="-tuplePath") tuplePath=string(argv[i+1]);
        if((string)argv[i]=="-mode") mode=string(argv[i+1]);  
        if((string)argv[i]=="-model") model=string(argv[i+1]);
        if((string)argv[i]=="-var") var=string(argv[i+1]);
        if((string)argv[i]=="-binning") {bin[0]=atof(argv[i+1]);bin[1]=atof(argv[i+2]);bin[2]=atof(argv[i+3]);}
        if((string)argv[i]=="-fitPdfName") fitName=string(argv[i+1]);
        if((string)argv[i]=="-labelName") label=string(argv[i+1]);
        if((string)argv[i]=="-id") id=string(argv[i+1]);
        if((string)argv[i]=="-mvaVar") { mvaVar=string(argv[i+1]); cut = string(argv[i+2]);}
        if((string)argv[i]=="-option") { option=string(argv[i+1]);
                                         if(option=="PID" || option == "p_PID") { cut= string(argv[i+2]); 
                                                             dataCut = TCut(("p_MC15TuneV1_ProbNNp >"+string(argv[i+2])).c_str());
                                         }
                                         else if (option=="sideband"||option=="SIDEBAND"||option=="side" ){
                                                             min_blind=atof(argv[i+2]); max_blind=atof(argv[i+3]);
                                                             sideband=true;
                                         }
                                         else if (option=="phi" || option=="PHI") {
                                                             MCcut = LcAnaCuts::match_Lc2pmm_Full;
                                                             cut=string(argv[i+2]); //Li_MC15TuneV1_ProbNNmu > cut 
                                                             dataCut = TCut(("L1_MC15TuneV1_ProbNNmu > "+cut +" && L2_MC15TuneV1_ProbNNmu > "+ cut ).c_str());
                                         }
                                         else if (option=="muon" || option=="MUON") { //extra muon for SL mode only
                                                             MCcut = LcAnaCuts::match_Lc2pmm_Full;
                                                             cut=string(argv[i+2]); //Li_MC15TuneV1_ProbNNmu > cut 
                                                             dataCut = TCut(("muon_MC15TuneV1_ProbNNmu > "+cut).c_str());
                                         }
                                         else if (option=="sigphi" || option=="SIGPHI") { //extra muon for SL mode only
                                                             MCcut = LcAnaCuts::match_Lc2pmm_Full;
                                                             cut=string(argv[i+2]); //Li_MC15TuneV1_ProbNNmu > cut 
                                                             dataCut = TCut(("BDTout >  " + cut).c_str());
                                         }

                                         else if (option=="rare" || option=="RARE"){
                                                             min_blind=atof(argv[i+2]); max_blind=atof(argv[i+3]); sideband=true;  
                                                             MCcut = LcAnaCuts::match_Lc2pmm_Full ;
                                                                
                                         
                                         }                   
        }
        if((string)argv[i]=="-matchLc2pKpi") MCcut=LcAnaCuts::match_Lc2pKpi_Full;
        if((string)argv[i]=="-matchLc2pmm") MCcut=LcAnaCuts::match_Lc2pmm_Full;
        if((string)argv[i]=="-noMCFit") fitMC=false;   
        if((string)argv[i]=="-oldConfig") oldConfig=true;   
        if((string)argv[i]=="-fitBinning") fitBinning=atoi(argv[i+1]);
        if((string)argv[i]=="-phiMode") phimode=true;
    }

    
    Fitter fit(tuplePath+infile,intree,tuplePath+inMCfile,inMCtree);
    RooDataSet::setDefaultStorageType(RooAbsData::Tree);
//******************** Options *********************
    if (option=="RAW") { if (mode=="Prompt") {dataCut =  LcAnaCuts::config_RAW; }
                         else {dataCut = LcAnaCuts::config_RAW_SL;} cout << LcAnaCuts::config_RAW_SL.GetTitle()<<endl;
                         fit.setFitMC(fitMC);
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);   }
    else if (option=="FULLPKPI") { if (mode=="Prompt") {dataCut =  LcAnaCuts::config_full_Lc2pKpi; cout << dataCut.GetTitle()<< endl;}
                         else {dataCut = LcAnaCuts::config_full_Lc2pKpi_SL;} 
                         fit.setFitMC(fitMC);
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);   }

    else if (option=="p_PID"||option=="PID") { id=cut; 
                         if (mode=="Prompt") {dataCut+=LcAnaCuts::config_PID;}
                         else { dataCut += LcAnaCuts::config_PID_SL;}
                         fit.setFitMC(fitMC);
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);   
    }
    else if (option=="phi" || option=="PHI") {
                         id=cut;
                         if (mode=="Prompt" || mode=="oldConfig" ) {if(oldConfig) {dataCut = LcAnaCuts::old_config && LcAnaCuts::L0_triggers && LcAnaCuts::newStripping_ghostProb;}
                                              else {dataCut+=LcAnaCuts::config_fitPhi;}}
                         else {               dataCut+=LcAnaCuts::config_fitPhi_SL;}
                         fit.setFitMC(fitMC);
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);   
    }
    else if (option=="sigphi" || option=="SIGPHI") {
                         id=cut;
                         if (mode=="Prompt" || mode=="oldConfig" ) {if(oldConfig) {dataCut = LcAnaCuts::old_config && LcAnaCuts::L0_triggers && LcAnaCuts::newStripping_ghostProb;}
                                              else {dataCut+=LcAnaCuts::config_fitPhi;}}
                         else {               dataCut+=LcAnaCuts::config_sigphi_SL;}
                         fit.setFitMC(fitMC);
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);   
    }
    else if (option=="muon" || option=="MUON") {
                         id=cut;
                         if (phimode){dataCut+=LcAnaCuts::config_extraMuon_phiMode_SL;}
                         else {dataCut+=LcAnaCuts::config_extraMuon_SL;}
                         fit.setFitMC(fitMC);
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);

    }
    else if (option=="SIDEBAND" || option=="sideband" || option=="side") { 
                         id=cut;
                         if (mvaVar!="p_MC15TuneV1_ProbNNp") {mvaVar+="out";}
                         string mvaC = mvaVar + " >  " + cut ;
                         dataCut = TCut(mvaC.c_str());
                         if (mode=="Prompt") { dataCut+=LcAnaCuts::config_sidebandFit;}
                         else { dataCut+=LcAnaCuts::config_sidebandFit_SL;}    
                         fit.Initialize(model,dataCut,var,bin,fitName,label,id,mode);
                         fit.setBlindRegion(min_blind,max_blind);
    }
    else if (option=="rare" || option=="RARE"){
                         id="";
                         if (mode=="Prompt" || mode == "oldConfig"){  if(oldConfig) { dataCut = LcAnaCuts::old_config && LcAnaCuts::L0_triggers && LcAnaCuts::newStripping_ghostProb; }
                                               else {dataCut = LcAnaCuts::full_selection; }
                         MCcut+=LcAnaCuts::bdt_cut + LcAnaCuts::triggers + LcAnaCuts::p_PID + LcAnaCuts::probNN_mu;}
                         else { dataCut = LcAnaCuts::full_selection_SL;  }
                         fit.Initialize(model,MCcut,dataCut,var,bin,fitName,label,id,mode);   
                         fit.setVetoCut(LcAnaCuts::blind_phi&&LcAnaCuts::blind_omega && LcAnaCuts::blind_eta); 
                         fit.setWindowCut(LcAnaCuts::phi_mass_window);
                         fit.setBlindRegion(min_blind,max_blind);
                         fit.setFitMC(fitMC);
    }


    cout << dataCut.GetTitle() << endl;
    
    fit.setFitBinning(fitBinning);   
    fit.Fit(option);

    return 0;
}
