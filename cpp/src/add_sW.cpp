#include "general_functions.hpp"
#include "analyser.hpp"
#include "multi_analyser.hpp"
#include "LcAnaCuts.hpp"

#include <cstdlib>

int main(int argc, char *argv[]) {

    string sl = "";
    for (int i = 0; i < argc; i++) {
         if((string)argv[i]=="-SL") sl = "SL";       
    }

    gROOT->ProcessLine(".x $LCANAROOT/scripts/lhcbStyle.C");

    RooDataSet::setDefaultStorageType(RooAbsData::Tree);
    TFile * tmpout = new TFile("tmp.root","recreate");

    // Set paths
    const char* path =  std::getenv("LCTUPLELOCATION");
    if (!path) std::exit(EXIT_FAILURE);
    string tuplePath = (string)path;

    string inMCtree = "DecayTree";
    string inMCfile = tuplePath+"MC12_"+sl+"Lc2pKpi_reformat.root";

    string intree = "DecayTree";
    string infile = tuplePath+"/CL16_"+sl+"Lc2pKpi_for_sW_100k.root";

    // Define variables to fit

    //RooRealVar *vM = new RooRealVar("Lc_DTF_MM", "Lc_DTF_MM", 2290, 2240, 2330);
    //RooRealVar *vM = new RooRealVar("Lc_DTF_MM", "Lc_DTF_MM", 2290, 2220, 2350);
    RooRealVar *vM = new RooRealVar("Lc_MM", "Lc_MM", 2290, 2240, 2340);
    RooRealVar *vM_MC = new RooRealVar("Lc_MM", "Lc_MM", 2290, 2270, 2310);

    // Fit options
  
    string fitopt = "-nochi2-linlog-andpulls-nocost-xM(pK#pi)";
    fitopt += "-layout[0.7,0.5,0.99,0.99]-font0.03-leg[0.15,0.7,0.35,0.9]-attach-allformats";
    string fitoptMC = fitopt+"-noleg-noextended";
    string units = "MeV/#it{c}^{2}";

    // Fit MC

    TCut * MCcut = new TCut("TMath::Abs(Lc_TRUEID)==4122 && TMath::Abs(p_TRUEID)==2212 && TMath::Abs(L1_TRUEID)==321 && TMath::Abs(L2_TRUEID)==211");

    Analysis * anaMC = new Analysis("Lc2pKpi_MC","#Lambda_{c}#rightarrowpK#pi (MC)",inMCtree,inMCfile,vM_MC,MCcut);
    anaMC->SetUnits(units);
    //anaMC->SetSignal("Ipatia-n[2,0,5]-a[1.5,-5.,5.]-z[1.,0.1,10]-l[-0.5,-10,.]-s[6.,2.,20.]-b[0.01,-10.,10.]-m[2285.,2270.,2310.]");
    anaMC->SetSignal("Johnson-s[6.,2.,20.]-m[2285.,2270.,2310.]-nu[-4.-10,10]-tau[0.7,0.,5.]");
    //anaMC->SetSignal("Gauss-s[6.,2.,20.]-m[2285.,2270.,2310.]");
    anaMC->Initialize("-docuts");
    anaMC->Fit(70,true,fitoptMC);
    Str2VarMap MCpars = anaMC->GetSigParams();

    ((RooRealVar*)MCpars["tau"])->setConstant();
    ((RooRealVar*)MCpars["nu"])->setConstant();
    //((RooRealVar*)MCpars["n"])->setConstant();
    //((RooRealVar*)MCpars["a"])->setConstant();
    //((RooRealVar*)MCpars["n2"])->setConstant();
    //((RooRealVar*)MCpars["a2"])->setConstant();
    //((RooRealVar*)MCpars["z"])->setConstant();
    //((RooRealVar*)MCpars["b"])->setConstant();
    //((RooRealVar*)MCpars["l"])->setConstant();

    
    // Fit data

    TCut dataCut = LcAnaCuts::convergence;
    if (sl=="Prompt") dataCut += LcAnaCuts::p_PID; 

    Analysis * ana = new Analysis(sl+"Lc2pKpi","#Lambda_{c}#rightarrowpK#pi",intree,infile,vM,&dataCut);
    ana->SetUnits(units);
    ana->AddAllVariables();
    ana->SetSignal("Johnson",5.e4,"",MCpars);
    ana->AddBkgComponent("Comb","Cheb1-v1[0.,-0.4,0.4]");//-v2[0.,-0.9,0.9]");
    tmpout->cd();
    ana->Initialize("-docuts");
    ana->Fit(90,false,fitopt);

    // Calculate sWeights and save them

    ana->CalcSWeightRooFit();
    TFile * fout = new TFile((TString)tuplePath+sl+"Lc2pKpi_sW.root","recreate");
    ana->GetReducedTree()->Write("Lc2pKpi_sW");
    fout->Close();

}
