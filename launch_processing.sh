shopt -s expand_aliases

## Data, will run in batch if you don't specify --local

python $LCANAROOT/python/routines/process_data.py -w addVariables  -t 'Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype SL --makeFriends -T before_BDT --checkoutfile 
#force

exit

#python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype PromptS28 --makeFriends -T before_BDT

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCLc2ppipi --makeFriends -T before_BDT --local

exit

## Simulation

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCLc2pmm --makeFriends -T before_BDT --local
python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCLc2pmmSS --makeFriends -T before_BDT --local
python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCLc2pKpi --makeFriends -T before_BDT --local
python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCLc2ppipi --makeFriends -T before_BDT --local

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCLc2pKK --makeFriends -T before_BDT --local

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCD2Kpipi --makeFriends -T before_BDT --local

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCD2KKpi --makeFriends -T before_BDT --local

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCD2Kmumu --makeFriends -T before_BDT --local

python $LCANAROOT/python/routines/process_data.py -w addVariables -t 'Lc2pKpiTupleOld/DecayTree;Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree' --dtype MCD2pimumu --makeFriends -T before_BDT --local


