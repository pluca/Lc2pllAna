import os, sys
from particles_DB import pdgid_db as ID_dict
from routines.LcAnalysis import * 

l1part = "L1"
l2part = "L2"
ppart  = "p"
mompart = "Lc"

match_Lc2pXX = ("abs("+ppart+"_MC_MOTHER_ID)=={Lambda_c+} && abs("+l1part+"_MC_MOTHER_ID)=={Lambda_c+} \
         && abs("+l2part+"_MC_MOTHER_ID)=={Lambda_c+} && abs("+mompart+"_TRUEID)=={Lambda_c+} && abs("+ppart+"_TRUEID)=={p+}").format(**ID_dict)

class PIDinfo :

  def __init__(self, decay, matching, IDs, files) :
    self.decay = decay
    self.matching = matching
    self.files = files
    self.IDs = IDs
    
decays = [
  
  ## Signal channels
  PIDinfo(
    decay    = "Lc2pmm",
    matching = match_Lc2pXX,
    files    = [],
    IDs      = {
      ("(abs("+ppart+"_TRUEID)==abs({p+}) && abs("+l1part+"_TRUEID)==abs({mu-}) && abs("+l2part+"_TRUEID)==abs({mu-}))").format(**ID_dict) 
            : { ppart : "P -> P", l1part : "Mu -> Mu", l2part : "Mu -> Mu" }
    }
  ),
    
  PIDinfo(
    decay    = "Lc2pKpi",
    matching = match_Lc2pXX,
    files    = [],
    IDs      = {
      ("(abs("+ppart+"_TRUEID)==abs({p+}) && abs("+l1part+"_TRUEID)==abs({K-}) && abs("+l2part+"_TRUEID)==abs({pi-}))").format(**ID_dict) 
            : { ppart : "P -> P", l1part : "K -> K", l2part : "Pi -> Pi" }

    }
  ),

  ## Mis-ID as Lc2pmm
  PIDinfo(
    decay    = "Lc2pKpi_to_Lc2pmm",
    matching = match_Lc2pXX,
    files    = [],
    IDs      = {
      ("(abs("+ppart+"_TRUEID)==abs({p+}) && abs("+l1part+"_TRUEID)==abs({K-}) && abs("+l2part+"_TRUEID)==abs({pi-}))").format(**ID_dict) 
            : { ppart : "P -> P", l1part : "K -> Mu", l2part : "Pi -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({p+}) && abs("+l1part+"_TRUEID)==abs({pi-}) && abs("+l2part+"_TRUEID)==abs({K-}))").format(**ID_dict) 
            : { ppart : "P -> P", l1part : "Pi -> Mu", l2part : "K -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({K-}) && abs("+l1part+"_TRUEID)==abs({p+}) && abs("+l2part+"_TRUEID)==abs({pi-}))").format(**ID_dict) 
            : { ppart : "K -> P", l1part : "P -> Mu", l2part : "Pi -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({pi-}) && abs("+l1part+"_TRUEID)==abs({p+}) && abs("+l2part+"_TRUEID)==abs({K-}))").format(**ID_dict) 
            : { ppart : "Pi -> P", l1part : "P -> Mu", l2part : "K -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({K-}) && abs("+l1part+"_TRUEID)==abs({pi-}) && abs("+l2part+"_TRUEID)==abs({p+}))").format(**ID_dict) 
            : { ppart : "K -> P", l1part : "Pi -> Mu", l2part : "P -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({pi-}) && abs("+l1part+"_TRUEID)==abs({K-}) && abs("+l2part+"_TRUEID)==abs({p+}))").format(**ID_dict) 
            : { ppart : "Pi -> P", l1part : "K -> Mu", l2part : "P -> Mu" }
      }
  ),

  PIDinfo(
    decay    = "Lc2ppipi_to_Lc2pmm",
    matching = match_Lc2pXX,
    files    = [],
    IDs      = {
      ("(abs("+ppart+"_TRUEID)==abs({p+}) && abs("+l1part+"_TRUEID)==abs({pi-}) && abs("+l2part+"_TRUEID)==abs({pi-}))").format(**ID_dict) 
            : { ppart : "P -> P", l1part : "Pi -> Mu", l2part : "Pi -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({pi-}) && abs("+l1part+"_TRUEID)==abs({p+}) && abs("+l2part+"_TRUEID)==abs({pi-}))").format(**ID_dict) 
            : { ppart : "Pi -> P", l1part : "P -> Mu", l2part : "K -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({pi-}) && abs("+l1part+"_TRUEID)==abs({pi-}) && abs("+l2part+"_TRUEID)==abs({p+}))").format(**ID_dict) 
            : { ppart : "Pi -> P", l1part : "Pi -> Mu", l2part : "P -> Mu" }
    }
  ),

  PIDinfo(
    decay    = "Lc2pKK_to_Lc2pmm",
    matching = match_Lc2pXX,
    files    = [],
    IDs      = {
      ("(abs("+ppart+"_TRUEID)==abs({p+}) && abs("+l1part+"_TRUEID)==abs({K-}) && abs("+l2part+"_TRUEID)==abs({K-}))").format(**ID_dict) 
            : { ppart : "P -> P", l1part : "K -> Mu", l2part : "K -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({K-}) && abs("+l1part+"_TRUEID)==abs({p+}) && abs("+l2part+"_TRUEID)==abs({K-}))").format(**ID_dict) 
            : { ppart : "K -> P", l1part : "P -> Mu", l2part : "K -> Mu" },
      ("(abs("+ppart+"_TRUEID)==abs({K-}) && abs("+l1part+"_TRUEID)==abs({K-}) && abs("+l2part+"_TRUEID)==abs({p+}))").format(**ID_dict) 
            : { ppart : "K -> P", l1part : "K -> Mu", l2part : "P -> Mu" }
    }
  ),

## Continue from here

  PIDinfo(
    decay    = "D2pimm_to_Lc2pmm",
    matching = ("abs("+epart+"_MC_MOTHER_ID)=={B0} && abs("+mupart+"_MC_MOTHER_ID)=={B0} && abs("+mompart+"_TRUEID)=={B0}").format(**ID_dict),
    files    = [],
    IDs      = {
      ("((abs("+epart+"_TRUEID)==abs({pi+}) && abs("+mupart+"_TRUEID)==abs({K+})))").format(**ID_dict) : { mupart : "P -> Mu", epart : "P -> e" },
      ("((abs("+epart+"_TRUEID)==abs({K+}) && abs("+mupart+"_TRUEID)==abs({pi+})))").format(**ID_dict) : { mupart : "P -> Mu", epart : "P -> e" }
    }
  ),
 
  PIDinfo(
    decay    = "D2Kmm_to_Lc2pmm",
    matching = ("abs("+epart+"_MC_MOTHER_ID)=={B0} && abs("+mupart+"_MC_MOTHER_ID)=={B0} && abs("+mompart+"_TRUEID)=={B0}").format(**ID_dict),
    files    = [],
    IDs      = {
      ("((abs("+epart+"_TRUEID)==abs({pi+}) && abs("+mupart+"_TRUEID)==abs({K+})))").format(**ID_dict) : { mupart : "K -> Mu", epart : "Pi -> e" },
      ("((abs("+epart+"_TRUEID)==abs({K+}) && abs("+mupart+"_TRUEID)==abs({pi+})))").format(**ID_dict) : { mupart : "Pi -> Mu", epart : "K -> e" }
    }
  ),

  PIDinfo(
    decay    = "D2KKpi_to_Lc2pmm",
    matching = ("abs("+epart+"_MC_MOTHER_ID)=={B0} && abs("+mupart+"_MC_MOTHER_ID)=={B0} && abs("+mompart+"_TRUEID)=={B0}").format(**ID_dict),
    files    = [],
    IDs      = {
      ("((abs("+epart+"_TRUEID)==abs({pi+}) && abs("+mupart+"_TRUEID)==abs({K+})))").format(**ID_dict) : { mupart : "Pi -> Mu", epart : "Pi -> e" },
      ("((abs("+epart+"_TRUEID)==abs({K+}) && abs("+mupart+"_TRUEID)==abs({pi+})))").format(**ID_dict) : { mupart : "Pi -> Mu", epart : "Pi -> e" }
    }
  ),

  PIDinfo(
    decay    = "D2Kpipi_to_Lc2pmm",
    matching = ("abs("+epart+"_MC_MOTHER_ID)=={B0} && abs("+mupart+"_MC_MOTHER_ID)=={B0} && abs("+mompart+"_TRUEID)=={B0}").format(**ID_dict),
    files    = [],
    IDs      = {
      ("((abs("+epart+"_TRUEID)==abs({pi+}) && abs("+mupart+"_TRUEID)==abs({K+})))").format(**ID_dict) : { mupart : "K -> Mu", epart : "K -> e" },
      ("((abs("+epart+"_TRUEID)==abs({K+}) && abs("+mupart+"_TRUEID)==abs({pi+})))").format(**ID_dict) : { mupart : "K -> Mu", epart : "K -> e" }
    }
  )

  ]
