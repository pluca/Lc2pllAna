import os
from python.routines.LcAnalysis import loc, cuts, parseDatafiles, parsePIDfiles

####### Parser

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--sendDistribution', action = 'store_true')
parser.add_argument('--sendCorrection', action = 'store_true')
parser.add_argument('--Mag', default = 'MagUp')
parser.add_argument('--channel', default = 'Lc2pmm')
parser.add_argument('--bkg',action='store_true')
parser.add_argument('--mode',default='Prompt')
args = parser.parse_args()

#script_path = os.getenv('PIDPERFSCRIPTSROOT')+'/scripts/python/MCResampling/'
script_path = loc.PIDCALIB+'PIDPerfScripts/scripts/python/MCResampling/'
tuplePath = loc.TUPLELOCATION
cut_to_do = ''
dtype_pmm='MCLc2pmm_S28_PIDAndBDT'
dtpye_pKpi='MCLc2pKpi_S28_PIDAndBDT'

if args.mode=='SL':
    dtype_pmm = 'MCSLLc2pmm_S28_PIDAndBDT'
    dtpye_pKpi = 'MCSLLc2pKpi_S28_PIDAndBDT'

if args.channel=='Lc2pmm':
    particles = '[P,Mu]'
    variables = '[MC15TuneV1_ProbNNp,MC15TuneV1_ProbNNmu]'
    cut_to_do = '--cuts="[lab1:ALL,lab2:IsMuon==1]"'
    parts = '[P:DEFAULT/P,Mu:DEFAULT/Mu]'
    inMCfile, inMCtree = parseDatafiles(dtype_pmm)
    PIDfile = parsePIDfiles("PIDCalib_%s_MuMu"%args.Mag)
elif args.bkg:
    particles = '[P,Mu]'
    variables = '[MC15TuneV1_ProbNNp,MC15TuneV1_ProbNNmu]'
    parts = '[P:DEFAULT/P,Mu:DEFAULT/Mu]'
    inMCfile, inMCtree = parseDatafiles(args.channel)
    PIDfile = parsePIDfiles("PIDCalib_%s_MuMu"%args.Mag)

elif args.channel=='Lc2pKpi':
    particles = '[P,K,Pi]'
    variables = '[MC15TuneV1_ProbNNp,MC15TuneV1_ProbNNpi,MC15TuneV1_ProbNNK]'
    parts = '[P:DEFAULT/P,K:DEFAULT/K, Pi:DEFAULT/Pi]'
    inMCfile, inMCtree = parseDatafiles(dtype_pKpi)
    PIDfile = parsePIDfiles("PIDCalib_%s_Kpi"%args.Mag)
####### Parse Monte-Carlo to be adde


######## Command one
#os.chdir(script_path)
if args.sendDistribution:
    command1 = 'MakePIDdistributionsRunRange.py'
    python_exec = "python %s 'Turbo16' '%s' '%s' '%s' "%(script_path+command1, args.Mag, particles,variables)
elif args.sendCorrection:
    command2 = "MakePIDCorrection.py"
    python_exec = "python %s --trueID '%s' '%s' '%s' '[p,L1,L2]' '%s' '%s'"%(script_path+command2,tuplePath+inMCfile,inMCtree,parts,variables,tuplePath+PIDfile)


print python_exec
os.system(python_exec)
