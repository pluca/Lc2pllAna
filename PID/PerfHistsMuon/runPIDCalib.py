import os
import argparse
import subprocess
import re
parser = argparse.ArgumentParser()
parser.add_argument("--Mag",default = 'MagUp')
parser.add_argument("--Vars",default = 'P')
args = parser.parse_args()

def find_eff(cmd):
    output = subprocess.check_output(cmd,shell=True)
    val = re.findall("(\d+\.\d+)",output)
    eff = float(val[len(val)-2])
    err = float(val[len(val)-1])
    return (eff,err)

Magnet = ["MagUp","MagDown"]
Variables = { 'P' : "'[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]'",
              'L' : "'[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'",
              'Both' :"'[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]' '[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'"
            }

if args.Vars == 'P':
    var = "'[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]'"
elif args.Vars =='L':
    var = "'[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'"
else:
    var = " '[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]' '[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'"

path= os.getenv("PIDPERFSCRIPTSROOT")
tuplePath = os.getenv("MARKOTUPLELOCATION")

infile = "/MC16_Lc2pmm_PIDAndBDT_reduced.root"
intree = "Lc2pmmTuple_Prompt"

path += "/scripts/python/MultiTrack/PerformMultiTrackCalib.py "

for var in Variables:
    for m in Magnet:
        cmd = "python %s 'Turbo16' '%s' '%s' '%s' 'MyResults.root' %s -X 'P' -Y 'ETA' -Z '' "%(path,m,tuplePath+infile,intree,Variables[var])
        Eff = find_eff(cmd)
        print var, m, Eff
