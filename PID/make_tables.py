import os, sys
from routines.LcAnalysis import *
import subprocess as sb

urania = os.environ["PIDCALIB"]
script = urania+"/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py"

#binsloc = repo+'/PID/Binning/neweMubinning_p_pt.py'
tcuts = [cuts.p_PIDCalib.GetTitle()]
pidcuts = "'[ "+','.join(tcuts)+"]'"
muoncuts = "'[ "+cuts.mu_PIDCalib.GetTitle()+"]'"

#binning = '-X "P" -Y "PT" -Z "" --binSchemeFile="{bins}" --schemeName="ricci_binning"'.format(bins=binsloc)
binning = ' -X "P" -Y "ETA" -Z "" '

def runpid(cuts,particles,extracuts="") :

    for mag in ['MagUp','MagDown'] :
        for p in particles :
            cmd = "python {script} ".format(script=script)
            if extracuts != "" and p in ['P','K','Pi','Mu','e'] :
                cmd += " -c '{ecuts}' ".format(ecuts=extracuts)
            cmd += " {bins} 'Turbo16' '{mag}' '{part}' {cuts}".format(bins=binning,part=p,cuts=cuts,mag=mag)
            print 
            print cmd
            print
            sb.call(cmd,shell=True)



particles = ["P"]
runpid(pidcuts,particles)

os.system("mv PerfHists_* "+loc.PID+"PerfHists/ && ls")

particles = ["K","Pi"]
#particles = ["Pi"]
runpid(muoncuts,particles,extracuts="Unbias_HLT1==1&&InMuonAcc==1")

particles = ["K"]
runpid(pidcuts,particles)

particles = ["P"]
runpid(muoncuts,particles,extracuts="Unbias_HLT12==1&&InMuonAcc==1")
particles = ["Mu"]
runpid(muoncuts,particles,extracuts="InMuonAcc==1")
os.system("mv PerfHists_* "+loc.PID+"PerfHist/")



