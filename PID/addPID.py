import os, sys
from routines.LcAnalysis import *
from configure_PID import decays
import array
from value import Value

def findEff(orig,reco,p,pt,magpol, correct=True) :

    # Function to retrieve the right histogram containind PID eff orig -> reco

    mag = "Down"
    if magpol > 0 : mag = "Up"
    
    pidbase = loc.REPO+"/PID/PerfHists/"
    if reco == 'Mu' :
        pidbase = loc.REPO+"/PID/PerfHistsMuon/"
    f = TFile.Open(pidbase+"PerfHists_{part}_Strip21_Mag{mag}_P_PT.root".format(part=orig,mag=mag))
    
    cut = pidcd[reco]

    h = f.Get(orig+"_"+cut+"_All")
    thebin = h.FindBin(p,pt)
    
    return Value(h.GetBinContent(thebin),h.GetBinError(thebin))

def loop_IDs(IDs) :

    w = Value(-1,0)
    # Loop over possible mis-id combinations
    for curid, parts in IDs.iteritems() :
            
        ww = Value(1,0)
        formula = TTreeFormula("cutid",curid, t);
        t.GetEntry(i)
    
        found = False
        if formula.EvalInstance() :
            # Loop over the particles in the decay
            for part,swap in parts.iteritems() :
                
                if swap == "" : continue
                orig_part = swap.split()[0]
                reco_part = swap.split()[2]
                
                # Find kinematics of the particle
                pt = t.GetLeaf(part+"_PT").GetValue()
                p = t.GetLeaf(part+"_P").GetValue()
                magpol = t.GetLeaf("Polarity").GetValue()
 
                # Retrieve the efficienciency from the histograms
                temp_ww = findEff(orig_part,reco_part,p,pt,magpol)

                # Multiply the id/mis-id probabilities for all particles in the decay
                if (temp_ww.get_val()>0.): ww *= temp_ww
                else: ww *= Value(0., temp_ww.err)

            # N.B.: The sum over the combinations should be automatically done by the
            # fact that in the tuple all combinations should be present.
            w = ww
            break

    #if w.val==-1 : 
    #    print "\nEplus (id, momid): ", t.GetLeaf("eplus_TRUEID").GetValue(), t.GetLeaf("eplus_MC_MOTHER_ID").GetValue(),
    #    print "Muminus (id,momid) : ", t.GetLeaf("muminus_TRUEID").GetValue(), t.GetLeaf("muminus_MC_MOTHER_ID").GetValue()
    return w

def get_weight(pid) :

    return loop_IDs(pid.IDs)

## Start program

for pid in decays:

    print "Analysing", pid.decay, "......."
    print pid.files 
    print pid.matching

    t = TChain("B2emuTuple/DecayTree")
    for f in pid.files : t.AddFile(f)
    
    nevts = t.GetEntries()
    #mt = t.CopyTree(pid.matching)
    #nevts = mt.GetEntries(pid.matching)
    #t = mt

    pidw = array.array("d", [0.0])
    pidw_err = array.array("d", [0.0])
    
    newfile = TFile(repo+"/PID/rootfiles/"+pid.decay+"_pid.root","RECREATE")
    newtree = t.CloneTree(0)
    newBranch = newtree.Branch( "PIDw" , pidw, "PIDw/D" )
    newBranch = newtree.Branch( "PIDw_err" , pidw_err, "PIDw_err/D" )
        
    uavg, davg, ravg, uerravg, n  = 0, 0, 0, 0, 0

    print "Entries: ", nevts
    for i in range(nevts):

        t.GetEntry(i)
        print "\rProgress... ", i, "/", nevts,
        w = get_weight(pid)
        
        pidw[0] = 0.
        pidw_err[0] = 0.
        if w.get_err() < 0.15 :
            pidw[0] = w.get_val()
            pidw_err[0] = w.get_err()
        else:
            print "Setting to zero (", w, ")" 

        if w.get_val() >= 0. :
            if w.get_err() < 0.15 :
                uavg += w.get_val()
                uerravg += w.get_err()
            n +=1

        #if pidw[0] > -0.1 :
        newtree.Fill()

    if n > 0 :
        print
        print uavg / n, uerravg / n
        print uerravg/uavg
    else : print "n = 0"

    newfile.cd()
    newtree.Write()






