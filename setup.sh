export LCANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$1" = "snake" ]; then
    echo "Setup with snakemake"
    source activate snake
else
    echo "Setup without snakemake"
    source $LCANAROOT/scripts/setup_path.sh
fi

export PYTHONPATH=$PYTHONPATH:$LCANAROOT:$LCANAROOT/Option:$LCANAROOT/Ganga:$LCANAROOT/python
export PYTHONPATH=$PYTHONPATH:$HOME/setup/python/analysis

export LCCUTSLOCATION=$LCANAROOT/cpp/includes/LcAnaCuts.hpp
export LCTUPLELOCATION=/eos/lhcb/user/p/pluca/Analysis/Lc2pll/Tuple/
export LCPLOTSLOCATION=$LCANAROOT/plots/
export LCJOBSLOCATION=/eos/lhcb/user/p/pluca/ganga/

export MARKOTUPLELOCATION=/afs/cern.ch/work/m/mstamenk/public/Lc2pll/Tuple
export MARKOJOBSLOCATION=/afs/cern.ch/work/m/mstamenk/public/Lc2pll/jobs

export MARKOTMPROOT=/eos/lhcb/user/m/mstamenk/rootFiles/
export LUCATMPROOT=$LCTUPLELOCATION/Tmp/

export MARKOEOSJOBS=/eos/lhcb/user/m/mstamenk/jobs/
export LUCAEOSJOBS=/eos/lhcb/user/p/pluca/Analysis/Lc2pll/Optimisation/

if [ "$USER" = "pluca" ]
then
   export CPPTMPROOT=$LUCATMPROOT
   export TUPLELOCATION=$LCTUPLELOCATION
   export EOSJOBS=$LUCAEOSJOBS
elif [ "$USER" = "mstamenk" ]
then
   export CPPTMPROOT=$MARKOTMPROOT 
   export TUPLELOCATION=/afs/cern.ch/work/m/mstamenk/public/Lc2pll/Tuple/test/
   export EOSJOBS=$MARKOEOSJOBS
else
   echo "Please set the temporary folder $CPPTMPROOT to store fits .root files"
fi 

export CLASSTOOLS=$LCANAROOT/cpp/class
export PIDCALIB=$HOME/UraniaDev_v6r2p1/PIDCalib
setupganga() {
    if [ -z "$GANGASYSROOT" ]; then
        source SetupProject.sh Ganga v602r3
    fi
}

alias goLc2pll='cd $LCANAROOT'
#alias runganga='setupganga && ganga -i $LCANAROOT/Ganga/gangaoption.py'
alias runganga='ganga -i $LCANAROOT/Ganga/gangaoption.py'

alias process='python $LCANAROOT/python/routines/process_data.py ' 
alias pythonLc='/afs/cern.ch/sw/lcg/releases/LCG_79/Python/2.7.9.p1/x86_64-slc6-gcc49-opt/bin/python'
alias setpath='source $LCANAROOT/scripts/setup_path.sh'

export PY2='/afs/cern.ch/sw/lcg/releases/LCG_79/Python/2.7.9.p1/x86_64-slc6-gcc49-opt/bin/python'


