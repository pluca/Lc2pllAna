import os
from routines.LcAnalysis import loc, parseDatafiles

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--mode",default='Prompt')
parser.add_argument("--fitBinning",default='25')
args = parser.parse_args()

if args.mode == 'Prompt':
    dataID = 'Lc2pKpi_S28_PIDAndBDT'
    mcID = 'MCLc2pKpi_S28_PIDAndBDT'
else :  
    dataID = 'SLLc2pKpi_S28_PIDAndBDT'
    mcID = 'MCSLLc2pKpi_S28_PIDAndBDT'

infile, intree = parseDatafiles(dataID)
inMCfile,inMCtree = parseDatafiles(mcID)
tuplePath = loc.MARKOTUPLE

cpp_exec = "./Lc2pmmAna.out -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -mode %s -model Ipatia -var Lc_M -binning 2287 2200 2380 -fitPdfName 'Lc2pKpi' -labelName '#Lambda_{c}^{+}#rightarrow pK^{-}#pi^{+}' -id '' -matchLc2pKpi -option FULLPKPI -fitBinning %s"%(infile,intree,inMCfile,inMCtree,tuplePath,args.mode,args.fitBinning)

print cpp_exec
os.chdir(loc.CPP)
os.system(cpp_exec)
