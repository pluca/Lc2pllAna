import os
from routines.LcAnalysis import cuts, loc, parseDatafiles
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--mode",default='Prompt')
parser.add_argument("--fitBinning",default='45')
args = parser.parse_args()



################# fetch root files ##############
if args.mode == 'Prompt':
    infile,intree = parseDatafiles('CL16_Lc2pmm_S28_forFit')
    inMCfile,inMCtree = parseDatafiles('MCLc2pmm_S28_forFit')
if args.mode == 'oldConfig':
    infile,intree = parseDatafiles('Lc2pmm_S28_PIDAndBDT')
    inMCfile,inMCtree = parseDatafiles('MCLc2pmm_S28_PIDAndBDT')
if args.mode == 'SL':
    infile,intree =parseDatafiles('CL16_SLLc2pmm_S28_forFit')
    inMCfile,inMCtree = parseDatafiles('MCSLLc2pmm_S28_forFit')
#else : 
#    infile,intree = parseDatafiles('')
#    inMCfile,inMCtree = parseDatafiles('')

cpp_exec = "./Lc2pmmAna.out -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -mode %s -model Ipatia -var Lc_M -binning 2287 2200 2380 -fitPdfName 'Lc2pmm' -labelName '#Lambda_{c}^{+}#rightarrow p#mu^{-}#mu^{+}' -id '' -matchLc2pmm -option RARE 2260 2320 -fitBinning %s"%(infile,intree,inMCfile,inMCtree,loc.TUPLELOCATION,args.mode, args.fitBinning) #cpp_exec = "./fitter.out -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -mode %s -model Johnson -var Lc_M -binning 2287 2200 2380 -fitPdfName 'Lc2pmm' -labelName '#Lambda_{c}^{+}#rightarrow pK^{-}#pi^{+}' -id '' -matchLc2pmm -option PHI 0.15 -fitBinning %s"%(infile,intree,inMCfile,inMCtree,loc.MARKOTUPLE,args.mode, args.fitBinning)

print cpp_exec

os.chdir(loc.CPP)
os.system(cpp_exec)

