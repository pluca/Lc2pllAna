from routines.LcAnalysis import *
import os
from ROOT import *
import tools.plotting_tools as plt
import numpy as np
import tools.sigproc as sp
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--fitMuon",action='store_true')
parser.add_argument("--plotSigni",action='store_true')
parser.add_argument("--NoMC",action='store_true')
parser.add_argument("--phiMode",action='store_true')
args=parser.parse_args()

save_path=loc.PLOTS+"MUON/"
infile,intree = parseDatafiles('SLLc2pmm_S28_PIDAndBDT')
inMCfile, inMCtree = parseDatafiles('MCSLLc2pmm_S28_PIDAndBDT')
tuplePath = loc.MARKOTUPLE

xmin=0
xmax=1
step=float(xmax-xmin)/20
mu_cuts=np.arange(xmin,xmax,step)
noMC=""
noPhi=""
if args.phiMode:
    noPhi="-phiMode"
if args.NoMC:
    noMC="-noMCFit"

if args.fitMuon:
    os.chdir(loc.CPP)
    if args.NoMC:
        for c in mu_cuts:
            cpp_exec = "./Lc2pmmAna.out -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -mode SL -model Ipatia -var Lc_M -binning 2287 2200 2380 -fitPdfName 'Lc2pmm' -labelName '#Lambda_{c}^{+}#rightarrow p#mu^{+}#mu^{-}' -matchLc2pmm -option MUON %f -fitBinning 45 %s %s"%(infile,intree,inMCfile,inMCtree,tuplePath,c,noMC,noPhi)
            print cpp_exec
            os.system(cpp_exec) 
        NCut,NSig,NBkg=sp.compute_numpy_arrays_s28("SL","","MUON")
        Sig=sp.compute_significance(NSig,NBkg)
        max_cut=NCut[np.argmax(Sig)]
        new_step=step/10
        new_cuts=np.arange(max_cut-step,max_cut+step,new_step)
        if(new_cuts[0]<0):
            new_cuts+=step
        print new_cuts
        for nc in new_cuts:
            cpp_exec = "./Lc2pmmAna.out -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -mode SL -model Ipatia -var Lc_M -binning 2287 2200 2380 -fitPdfName 'Lc2pmm' -labelName '#Lambda_{c}^{+}#rightarrow p#mu^{+}#mu^{-}' -matchLc2pmm -option MUON %f -fitBinning 45 %s %s"%(infile,intree,inMCfile,inMCtree,tuplePath,nc,noMC,noPhi)
            os.system(cpp_exec)
        print max_cut, np.argmax(Sig)
    else:
        cpp_exec = "./Lc2pmmAna.out -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -mode SL -model Ipatia -var Lc_M -binning 2287 2200 2380 -fitPdfName 'Lc2pmm' -labelName '#Lambda_{c}^{+}#rightarrow p#mu^{+}#mu^{-}' -matchLc2pmm -option MUON %f -fitBinning 45 %s %s"%(infile,intree,inMCfile,inMCtree,tuplePath,0,noMC,noPhi)
        print cpp_exec
        os.system(cpp_exec) 

if args.plotSigni:
    histNSig,histNBkg,histSigni,new_Sig,new_err_Sig,new_NCut=sp.compute_hist("SL","","#mu ProbNN #mu","MUON")
    c1,t1=sp.draw_NSig_NBkg(histNSig,histNBkg)
    c2=sp.draw_Significance(histSigni)
    bestCut=new_NCut[np.argmax(new_Sig)]
    errSigni=new_err_Sig[np.argmax(new_Sig)]
    print "Maximum %f +/- %f at cut %f"%(np.amax(new_Sig),errSigni,bestCut)
    c1.Print(save_path+"SL"+"NsigNBkg.pdf")
    c2.Print(save_path+"SL"+"Significance.pdf")


