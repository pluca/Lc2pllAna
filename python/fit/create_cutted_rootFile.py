from routines.LcAnalysis import loc,cuts,parseDatafiles

# Import and Export Path
LucaTuple=loc.TUPLE
MarkoTuple=loc.TUPLELOCATION
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="Prompt")
parser.add_argument("--createS28",action='store_true')
parser.add_argument("--decay",default="Lc2pKpi")
args=parser.parse_args()
import tools.eventtuple_tools as et
import os
from ROOT import *
# Function to cut file
def cut_file(outName,inFile,treeName,cut):
    outFile=MarkoTuple+outName
    print outFile
    print inFile
    print cut.GetTitle()
    tree=TChain(treeName)
    tree.AddFile(inFile)
    newFile=TFile(outFile,"recreate")
    newTree=tree.CopyTree(cut.GetTitle())
    newTree.Write()
    newFile.Close()

# Cut files
if args.mode=="Prompt":
    MC12_In,tupleNameMC = parseDatafiles('MCLc2pmm_S28_PIDAndBDT')
    CL16_In, tupleName = parseDatafiles('Lc2pmm_S28_PIDAndBDT')
    MC12_In = loc.TUPLELOCATION + MC12_In
    CL16_In = loc.TUPLELOCATION + CL16_In
    fitPhiCut=cuts.config_forFit
    secondConfig = fitPhiCut 
    dataSecond=secondConfig
#    MC12_pmm_firstConfig="MC12_HadronConfig_forPhiFit.root"
    MC12_pmm_secondConfig="MC16_Lc2pmm_forFit.root"
#    CL16_pmm_firstConfig="CL16_HadronConfig_forPhiFit.root"
    CL16_pmm_secondConfig="CL16_Lc2pmm_forFit.root"
elif args.mode=="SL":
    MC12_In,tupleNameMC = parseDatafiles('MCSLLc2pmm_S28_PIDAndBDT')
    CL16_In, tupleName = parseDatafiles('SLLc2pmm_S28_PIDAndBDT')
    MC12_In = loc.TUPLELOCATION + MC12_In
    CL16_In = loc.TUPLELOCATION + CL16_In
    fitPhiCut=cuts.config_forFit_SL
    secondConfig = fitPhiCut 
    dataSecond=secondConfig
    MC12_pmm_secondConfig="MC16_SLLc2pmm_forFit.root"
    CL16_pmm_secondConfig="CL16_SLLc2pmm_forFit.root"
'''    tupleNameMC="Lc2pmmTuple_SL"
    tupleName="Lc2pmmTuple_SL"
    fitPhiCut=cuts.bdt_SL + cuts.trigger_SL
    dataCut=fitPhiCut+cuts.p_PID_SL
    firstConfig=fitPhiCut+cuts.trigger_hadron_configuration
    secondConfig=fitPhiCut#+cuts.trigger_muon_configuration_SL
    dataFirst=dataCut+cuts.trigger_hadron_configuration
    dataSecond=dataCut#+cuts.trigger_muon_configuration_SL
#    MC12_pmm_firstConfig="MC12_SLHadronConfig_forPhiFit.root"
    MC12_pmm_secondConfig="MC12_SLMuonConfig_forPhiFit.root"
    MC12_In=MarkoTuple+"MC12_SLLc2pmm_CutStripPIDAndBDT.root"
#    CL16_pmm_firstConfig="CL16_SLHadronConfig_forPhiFit.root"
    CL16_pmm_secondConfig="CL16_SLMuonConfig_forPhiFit.root"
    CL16_In=MarkoTuple+"CL16_SLLc2pmm_CutStripPIDAndBDT.root"i'''
if args.createS28:  
    tupleN=args.decay+"Tuple"
    print tupleN
    save_data=TFile(loc.TUPLELOCATION+"CL16_"+args.decay+"_S28_100k.root","recreate")
    data=et.compute_tree("Prompt",tupleN+"/DecayTree")
    data.SetName(tupleN)
    data=data.CopyTree("","",100000)
    data.Write()
    save_data.Write()
    save_data.Close()
#    save_MC=TFile(loc.TUPLELOCATION+"MC16_"+args.decay+"_S28.root","recreate")
#    mc=et.compute_tree("MC"+args.decay,tupleN+"/DecayTree")
#    mc=mc.CopyTree("")
#    mc.Write()
#    save_MC.Write()
#    save_MC.Close()


#************** Apply Hadron and Muon config *******************
#cut_file(MC12_pmm_firstConfig,MC12_In,tupleNameMC,firstConfig)
#cut_file(CL16_pmm_firstConfig,CL16_In,tupleName,dataFirst) #No PID on MC

cut_file(MC12_pmm_secondConfig,MC12_In,tupleNameMC,secondConfig + cuts.match_Lc2pmm_Full)
cut_file(CL16_pmm_secondConfig,CL16_In,tupleName,dataSecond) #No PID on MC

#cut_file("CL16_SLLc2pmm_CutStripPIDAndBDT.root",MarkoTuple+"CL16_SLLc2pmm_CutStripPID.root","Lc2pmmTuple_SL",cuts.bdt_SL)
#cut_file("MC12_SLLc2pmm_CutStripPIDAndBDT.root",MarkoTuple+"MC12_SLLc2pmm_CutStripPID.root","Lc2pmmTuple_SL",cuts.bdt_SL)

