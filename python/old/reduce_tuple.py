from ROOT import *
from argparse import ArgumentParser
from array import array
import os, sys, glob
from routines.LcAnalysis import loc, datafiles, strings
from routines.utils import getPartLV
from routines.getResVars import getResVars

parser = ArgumentParser()
parser.add_argument("-m","--maxev",   default=1e14,type=float)
parser.add_argument("-o","--outfile", default="out")
parser.add_argument("-T","--outtree", default=None)
parser.add_argument("-t","--tname",   default="DecayTreeTuple/DecayTree")
parser.add_argument("-d","--dtype",   default=None,type=str)
parser.add_argument("--fromid",       default=None,type=str)
parser.add_argument("--clone",        action="store_true", type=str)
parser.add_argument("--make_friends", action="store_true", type=str)
parser.add_argument("-i","--inputs",  nargs="+")
args = parser.parse_args()

ncategories = 10

tree = TChain(args.tname)
files = args.inputs
if args.inputs is None :
    if args.dtype is not None :
        files = datafiles[args.dtype]
    elif args.fromid is not None :
        myids = args.fromid.split(',')
        files = []
        for myid in myids :
            files += glob.glob(loc.JOBS+myid+"/*/DVNtuple.root")
    else :
        print "No files to reduce"
        sys.exit()

for f in files : 
    print "Adding file: ", f
    tree.AddFile(f)

newfile   = TFile(loc.TUPLE+args.outfile.replace(".root","")+".root","RECREATE") 
newtree   = TTree(opts.outtree)
if opts.clone : newTree = tree.CloneTree(0)
print "Writing to: ", newfile.GetName()

vnames = ["Lc_DTF_MM","Lc_DTF_chi2_0","q2","category","sPH1","sPH2","sH1H2","cosTheta","cosPhi","thetaH1H2"]
vv = { name : array("d", [0.0]) for name in vnames } 

for name,arr in vv.iteritems() :
    newtree.Branch( name, arr, name+"/D")

nevts = 0
for ie,ev in enumerate(tree) :

    if ev.Lc_M < 2350. :
        vv["Lc_DTF_MM"][0]   = ev.Lc_DTF_PV_M[0]
        vv["Lc_DTF_chi2_0"][0] = ev.Lc_DTF_PV_chi2[0]
        vv["category"][0] = nevts % ncategories

        formula = TTreeFormula("q2",strings.q2, tree);
        tree.GetEntry(ie)
        vv["q2"][0]       = formula.EvalInstance()

        ( vv["sPH1"][0], vv["sH1H2"][0], vv["sPH2"][0],
          vv["cosTheta"][0], vv["cosPhi"][0], vv["thetaH1H2"][0] ) = getResVars(
                     TLorentzVector(),
                     getPartLV('Lc',ev),
                     getPartLV('p', ev),
                     getPartLV('L1',ev),
                     getPartLV('L2',ev)
                     )
        
        newtree.Fill()
        nevts+=1
        if nevts%20==0 : print "\rNumber of events saved: ", nevts, " / ", args.maxev,
        if nevts >= args.maxev : break

newfile.cd()
newtree.Write()
newfile.Close()



