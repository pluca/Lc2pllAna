from variables_DB import vardb, vardbSL, vardbS28
from ROOT import *
from routines.LcAnalysis import cuts, loc
#import test.eventtuple_tools as et

import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--ConeVariable",action='store_true')
parser.add_argument("--mode",default="Prompt")
args=parser.parse_args()

if args.mode=="Prompt":
    varDB=vardb
    sl=""
    mcName="MC12_Lc2pKpi_100k.root"
elif args.mode=="SL":
    varDB=vardbSL
    sl="SL"
    mcName="MC12_SL_Lc2pKpi_CutAndBDT.root"
else:
    varDB=vardbS28.copy()
    varDB.update(vardb)
    sl=""
    mcName="MC16_Lc2pKpi_S28.root" #default
decay = "Lc2pKpi"

#database = '/eos/lhcb/user/p/pluca/ganga/167'
#DataTree = TChain(decay+"Tuple/DecayTree")
#DataTree.AddFile(database+"/0/DVNtuples.root")
#DataTree.AddFile(database+"/1/DVNtuples.root")
#DataTree.AddFile(database+"/11/DVNtuples.root")

mcbase = loc.TUPLE
database = mcbase
print database
DataTree = TChain("Lc2pKpi_sW")
DataTree.AddFile(database+sl+"Lc2pKpi_sW.root")


decay = "Lc2pKpiTupleOld"

#MCTree = TChain(decay+"TupleOld/DecayTree")
MCTree = TChain(decay)
#MCTree.AddFile(mcbase+'MC12_'+decay.replace('2','_')+"_MU.root")
#MCTree.AddFile(mcbase+'MC12_'+decay.replace('2','_')+"_MD.root")
MCTree.AddFile(mcbase+mcName)

if args.ConeVariable:
    tupleN="Lc2pKpiTuple/DecayTree"
    DataTree=et.compute_tree("PromptS28",tupleN)
    MCTree=et.compute_tree("MCLc2pKpi",tupleN)
c = TCanvas()

#signal = "nsig_Lc2pKpi_sw*(Lc_DTF_MM > 2240 && Lc_DTF_MM < 2330)"
signal = "(Lc_DTF_M > 2277 && Lc_DTF_M < 2297)"
side   = "(Lc_DTF_M < 2260 || Lc_DTF_M > 2320)"
mc = "TMath::Abs(Lc_TRUEID)==4122 && TMath::Abs(p_TRUEID)==2212 && ( (TMath::Abs(L1_TRUEID)==321 && TMath::Abs(L2_TRUEID)==211) || (TMath::Abs(L1_TRUEID)==211 && TMath::Abs(L2_TRUEID)==321) )"
for vname,v in varDB.iteritems():
    print v['formula']

for vname,v in varDB.iteritems() :

    if 'Lc_DTF_PV_M' in v['formula']: continue

    expr = "{formula}>>[hname]({nbins},{xmin},{xmax})".format(**v).replace('[hname]','{hname}')
    
    data_expr = expr.format(hname="hdata")
    mc_expr = expr.format(hname="hmc")
    side_expr = expr.format(hname="hside")
    print v['formula']
    DataTree.Draw(data_expr,signal,"E")
    hData = gPad.GetPrimitive("hdata")
    DataTree.Draw(side_expr,side,"E")
    hSide = gPad.GetPrimitive("hside")
    MCTree.Draw(mc_expr,mc,"E")
    hMC = gPad.GetPrimitive("hmc") 

    #hData.SetMarkerColor(3)
    #hData.SetMarkerSize(1)
    #hData.SetMarkerStyle(22)
    hData.SetFillStyle(3004)
    hData.SetFillColor(4)
    hData.GetXaxis().SetTitle(vname)
    hData.Scale(1./hData.Integral())
    hMC.SetMarkerColor(2)
    hMC.SetMarkerSize(1)
    hMC.SetMarkerStyle(21)
    hMC.GetXaxis().SetTitle(vname)
    hMC.Scale(1./hMC.Integral())
    #hSide.SetMarkerColor(1)
    #hSide.SetMarkerSize(1)
    #hSide.SetMarkerStyle(20)
    hSide.SetFillColor(1)
    hSide.SetFillStyle(3005)
    hSide.Scale(1./hSide.Integral())
    hSide.GetXaxis().SetTitle(vname)

    if hSide.GetMaximum() > hData.GetMaximum() and hSide.GetMaximum() > hMC.GetMaximum() :
        hSide.Draw("hist")
        hData.Draw("hist same")
        hMC.Draw("same")
    elif hMC.GetMaximum() > hData.GetMaximum() and hMC.GetMaximum() > hSide.GetMaximum() :
        hMC.Draw()
        hSide.Draw("hist same")
        hData.Draw("hist same")
    else :
        hData.Draw("hist")
        hSide.Draw("hist same")
        hMC.Draw("same")

    leg = TLegend(0.70,0.75,0.99,0.99)
    leg.AddEntry(hData,"Data signal","F")
    leg.AddEntry(hSide,"Data sideband","F")
    leg.AddEntry(hMC,"MC #Lambda_{c}#rightarrow pK#pi","P")
    leg.Draw()

    if v['log'] : c.SetLogy(True)
    else : c.SetLogy(False)

    if 'nick' not in v : v['nick'] = v['formula']
    c.Print(loc.PLOTS+v['nick']+'.pdf')



    
