from ROOT import *
import sys, os, argparse
from routines.LcAnalysis import *
retrieve_files()

parser = argparse.ArgumentParser()
parser.add_argument("--dtype", default="Prompt",type=str)
opts = parser.parse_args()

data = TChain("Lc2pmmTuple/DecayTree")
for f in datafiles[opts.dtype] : data.AddFile(f)

#pidcut = TCut("L1_MC15TuneV1_ProbNNmu > 0.95 && L2_MC15TuneV1_ProbNNmu > 0.95 && p_MC15TuneV1_ProbNNp > 0.9")
pidcut = TCut("L1_MC15TuneV1_ProbNNmu > 0.95 && L2_MC15TuneV1_ProbNNmu > 0.95 && p_MC15TuneV1_ProbNNp > 0.9")
q2 = strings.q2

print q2
q2cut = TCut("({q2} > 0.85 && {q2} < 1.1)".format(q2=q2))

c = TCanvas()
data.Draw(q2+">>hq2(100,0,3)", pidcut+cuts.convergence+cuts.oldStripping+cuts.newStripping,"E")
hq2 = gPad.GetPrimitive('hq2')
hq2.GetXaxis().SetTitle("q^{2} [GeV/#it{c}^{2}]")
c.Print(loc.PLOTS+"q2.pdf")
data.Draw("Lc_DTF_PV_M[0]>>hm(30,2150,2450)", pidcut + q2cut,"E")
c.Print(loc.PLOTS+"mass.pdf")
#raw_input("Type to end")



