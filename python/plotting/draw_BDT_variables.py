from ROOT import *
import os
from routines.LcAnalysis import loc,cuts
import test.plotting_tools as plt

home=os.path.expanduser("~")
gROOT.ProcessLine(".x "+home+"/Lc2pllAna/scripts/lhcbStyle.C")


tuple_path=loc.TUPLE+"/CL16_BDT_200k.root"

pKpiTuple=TChain("Lc2pKpiTuple")
pKpiTuple.AddFile(tuple_path)

pmmTuple=TChain("Lc2pmmTuple")
pmmTuple.AddFile(tuple_path)

c=TCanvas()
binning=(100,2200,2350)
plot_cut=cuts.blinding_sideband+cuts.oldStripping + cuts.convergence
#plot_cut=cuts.oldStripping
#pKpiTuple.Draw("Lc_MM>>LcMM",plot_cut)
#pKpiTuple.Draw("Lc_MM>>LcMM",plot_cut)
pmmTuple.Draw("Lc_MM>>pmmLcMM(%d,%d,%d)"%binning,plot_cut)



#LcMM = gPad.GetPrimitive("LcMM")
#LcMM.Scale(1./LcMM.Integral())
pmmLcMM.SetMarkerStyle(22)
pmmLcMM.SetMarkerSize(1)
pmmLcMM.SetMarkerColor(2)

#LcMM.Draw()
c=TCanvas()
c.SetGrid()
plt.hist_param(pmmLcMM,"m(p#mu#mu )[MeV/c^{2}]","Events / (1.25 (Mev/c^{2})")
pmmLcMM.Draw()
c.SaveAs(home+"/report/fig/CL_Lc2pmm_Blinded_Lc_MM.pdf")

print pmmLcMM.GetEntries()


