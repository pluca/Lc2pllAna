from glob import glob
from ROOT import *
import sys, os, argparse
from variables_DB import vardb, findVar
from routines.LcAnalysis import cuts, loc, dataids
from routines.get_data import getData

parser = argparse.ArgumentParser()
parser.add_argument("--nbins",     default=50, type=int, help="Number of bins")
parser.add_argument("--cut",       default=False, help="Extra cut to apply")
parser.add_argument("--maxf",      default=1e4, type=int, help="Maximum number of files to consider")
parser.add_argument("--dtype",     default="Prompt", type=str, help="Data type: Prompt (default) or SL")
parser.add_argument("--tuple",     default="Lc2pKpiTuple",type=str, help="Tuple name and path")
parser.add_argument("--tuplefile", default=None, type=str, help="Tuple file name")
parser.add_argument("--sigbkg",    action='store_true', help="Draw signal and sideband")
parser.add_argument("--log",       action='store_true', help="Use log vertical scale")
parser.add_argument("--leftleg",   action='store_true', help="Legend on the top left (default is top right)")
parser.add_argument("variable")
opts = parser.parse_args()

treename = opts.tuple+"/DecayTree"
data = TChain(treename)

if opts.tuplefile is None :
    data = getData([treename],friends=[],opts=opts)[treename]
#    for cid in dataids[opts.dtype] :
#        files += glob("/eos/lhcb/user/p/pluca/ganga/"+str(cid)+"/*/DVNtuple.root")
#    for fi,f in enumerate(files) :
#        if fi > opts.maxf : break
#        data.AddFile(f)
#    addFriends(data,files,['beforeBDT'])
else :
    for f in glob(opts.tuplefile) : data.AddFile(f)

# Define eventual cut
cut = TCut()
if opts.cut : cut = TCut(opts.cut)

sidecut = cut+cuts.sideBand_raw
if opts.sigbkg : cut+= cuts.sigMassWin_raw


## Fetch variable
var = findVar(opts.variable,'all')
if var is None : 
    formula = opts.variable
    name = formula
    title = formula
    vmin = vmax = -1
else :
    (formula,title,info) = var
    if "nick" in info : name = info['nick']
    else : name = formula
    vmin = info['xmin']
    vmax = info['xmax']

name += "_"+opts.dtype

print "Drawing..."
print "Name    :", name
print "Title   :", title
print "Formula :", formula

## Draw
c = TCanvas()
if vmin==vmax :
    data.Draw(formula+">>h", cut,"E")
else :
    data.Draw(formula+">>h({bins},{min},{max})".format(bins=opts.nbins,min=vmin,max=vmax), cut,"E")
h = gPad.GetPrimitive('h')
h.GetXaxis().SetTitle(title)
if not opts.sigbkg :
    c.Print(loc.PLOTS+name+".pdf")
    sys.exit()

#cut.Print()
#sidecut.Print()
data.Draw(formula+">>hSide({bins},{min},{max})".format(bins=opts.nbins,min=vmin,max=vmax), sidecut,"E")

hSide = gPad.GetPrimitive('hSide')
h.SetFillColor(4)
h.SetFillStyle(3004)
h.Scale(1./h.Integral())
hSide.SetFillColor(1)
hSide.SetFillStyle(3005)
hSide.Scale(1./hSide.Integral())
hSide.GetXaxis().SetTitle(title)

if hSide.GetMaximum() > h.GetMaximum() :
    hSide.Draw("hist")
    h.Draw("hist same")
else :
    h.Draw("hist")
    hSide.Draw("hist same")

leg = TLegend(0.70,0.75,0.99,0.99)
if opts.leftleg : leg = TLegend(0.20,0.75,0.5,0.99)
leg.AddEntry(h,"Signal (2265 < m < 2295)","F")
leg.AddEntry(hSide,"Sideband (m < 2240 | m > 2320) ","F")
#    leg.AddEntry(hMC,"MC #Lambda_{c}#rightarrow pK#pi","P")
leg.Draw()

if opts.log : c.SetLogy()
c.Print(loc.PLOTS+name+"_sigbkg.pdf")

