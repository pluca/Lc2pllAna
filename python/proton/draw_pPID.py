from ROOT import *
import os
from routines.LcAnalysis import loc,cuts, parseDatafiles

home=os.path.expanduser("~")
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default='Prompt')
args=parser.parse_args()


kCut=0.
name=args.mode
if name=="Prompt":
    infile,intree = parseDatafiles('Lc2pKpi_S28_100k')
    tuple_path=loc.MARKOTUPLE
    cut=cuts.config_RAW + TCut("Lc_M > 2240 && Lc_M < 2340")#cuts.config_PID 
    cut_pid= cuts.inv_p_PID
else: 
    infile,intree = parseDatafiles('SLLc2pKpi_S28')
    tuple_path=loc.TUPLE
    cut=cuts.config_RAW_SL + TCut("Lc_M > 2240 && Lc_M < 2340")
    cut_pid= cuts.inv_p_PID_SL
cut_side=TCut("Lc_M<2260 || Lc_M>2320")
cut_kaon=TCut("L1_MC15TuneV1_ProbNNk > %.1f && L2_MC15TuneV1_ProbNNk > %.1f"%(kCut,kCut))

chain=TChain(intree)
chain.AddFile(tuple_path+infile)
cw=600
ch=500
c=TCanvas()
#c.SetCanvasSize(cw,ch)
chain.Draw("Lc_M>>hLc_M(67,2225,2350)",cut )
chain.Draw("Lc_M>>hSideBand(67,2225,2350)",cut+cut_side)
chain.Draw("Lc_M>>hCut(67,2225,2350)",cut+cut_pid )
if name=="Prompt" : 
    chain.Draw("Lc_M>>hKaon(67,2225,2350=",cut+cut_pid)    
    chain.Draw("Lc_M>>hLc_MKaon(67,2225,2350=",cut)
pid_cut_name=cut_pid.GetTitle()
pid_cut_name=pid_cut_name.replace("p_MC15TuneV1_ProbNNp","p_ProbNNp")
if name=="Prompt": 
    cut_kaon_name=cut_kaon.GetTitle()
    cut_kaon_name=cut_kaon_name.replace("L1_MC15TuneV1_ProbNNk","K ProbNN k")
    cut_kaon_name=cut_kaon_name.replace("L2_MC15TuneV1_ProbNNpi","#pi ProbNN #pi")
print cut.GetTitle()
print pid_cut_name
hLc_M.SetLineStyle(2)
hSideBand.SetLineStyle(4)
hSideBand.SetFillColorAlpha(kBlue,0.35)
hSideBand.SetFillStyle(3005)
leg=TLegend(0.62,0.6,0.92,0.9)
leg.AddEntry(hLc_M,"#Lambda_{c} #rightarrow pK#pi","lep")
leg.AddEntry(hSideBand,"Bkg sideband","f")
leg.AddEntry(hCut,pid_cut_name,"lep")
name="La"
if name=="Prompt": 
    leg.AddEntry(hLc_MKaon,"True #Lambda_{c}^{+}#rightarrowpK#pi","l")
    leg.AddEntry(hKaon,"ProbNN K,#pi >%.2f"%kCut,"l")
leg.SetTextSize(0.04)
hLc_M.GetXaxis().SetTitle("m(pK#pi ) [MeV/c^{2}]")
hLc_M.GetYaxis().SetTitle("Events / (5 MeV/c^{2})")
scale=hLc_M.Integral()
hLc_M.Scale(1./scale)
hSideBand.Scale(1./scale)
hCut.Scale(1./scale)
hCut.SetMarkerColor(kRed)
if name =="Prompt": 
    hLc_MKaon.SetLineStyle(2)
    hLc_MKaon.SetLineColor(kRed)
    hKaon.SetLineColor(kRed)
hLc_M.Draw("hist e")
hSideBand.Draw("same hist e")
hCut.Draw("same hist e")
leg.Draw("same e ")
c.SaveAs(loc.PLOTS+"PID/"+args.mode+"Lc2pKpi_p_ProbbNNp_Cut.pdf")
