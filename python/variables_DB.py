from routines.LcAnalysis import strings

## Variables DB

vardb = {
        '#Lambda_{c} log(#chi^{2}_{ENDVTX}/ndf)': { 'formula' : 'TMath::Log(Lc_ENDVERTEX_CHI2/Lc_ENDVERTEX_NDOF)', 'nbins' : 50, 'xmin' : -2., 'xmax' : 4., 'log': False, 'nick' : 'log_Lc_ENDVERTEX_CHI2NDF' },
        '#Lambda_{c} FD'                        : { 'formula' : 'Lc_FD_OWNPV', 'nbins' : 50, 'xmin' : 0., 'xmax' : 15., 'log': True },
        '#Lambda_{c} log(#chi^{2}_{FD})'        : { 'formula' : 'TMath::Log(Lc_FDCHI2_OWNPV)', 'nbins' : 50, 'xmin' : 0., 'xmax' : 15., 'log': True , 'nick' : 'log_Lc_FDCHI2'},
        '#Lambda_{c} p'                         : { 'formula' : 'Lc_P', 'nbins' : 50, 'xmin' : 0., 'xmax' : 400.e3, 'log': True },
        '#Lambda_{c} p_{T}'                     : { 'formula' : 'Lc_PT', 'nbins' : 50, 'xmin' : 0., 'xmax' : 5.e4, 'log': False },
        '#Lambda_{c} DIRA'                      : { 'formula' : 'Lc_DIRA_OWNPV', 'nbins' : 50, 'xmin' : 0.9999, 'xmax' : 1., 'log': True, 'nick' : 'DIRA' },
        '#Lambda_{c} log(DIRA)'                 : { 'formula' : 'abs(TMath::Log(Lc_DIRA_OWNPV))', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2e-4, 'log': True, 'nick' : 'log_DIRA' },        
        '#Lambda_{c} #chi^{2}_{DTF}'            : { 'formula' : 'Lc_DTF_chi2_0', 'nbins' : 50, 'xmin' : 0., 'xmax' : 50., 'log': False, 'nick' : 'Lc_DTF_chi2' },
        '#Lambda_{c} #tau'                      : { 'formula' : 'Lc_TAU', 'nbins' : 50, 'xmin' : 0., 'xmax' : 0.02, 'log': False, 'nick':'#Lambda_{c}^{+}#rightarrow #tau' },
        '#Lambda_{c} IP#chi^{2}'                : { 'formula' : 'Lc_IPCHI2_OWNPV', 'nbins' : 50, 'xmin' : 0., 'xmax' : 200., 'log': True },
        '#Lambda_{c} log(IP#chi^{2})'           : { 'formula' : 'TMath::Log(Lc_IPCHI2_OWNPV)', 'nbins' : 50, 'xmin' : -3., 'xmax' : 5., 'log': True, 'nick' : 'log_Lc_IPCHI2' },
        '#Lambda_{c} IP'                        : { 'formula' : 'Lc_IP_OWNPV', 'nbins' : 50, 'xmin' : -3., 'xmax' : 5., 'log': True, 'nick' : 'Lc_IP' },
        'min(IP#chi^{2})'                       : { 'formula' : 'TMath::Min(TMath::Min(p_IPCHI2_OWNPV,p_IPCHI2_OWNPV),L2_IPCHI2_OWNPV)', 'nbins' : 50, 'xmin' : 0., 'xmax' : 200., 'log': True, 'nick': 'min_IPCHI2' },
        'max(GhostProb)'                        : { 'formula' : 'TMath::Max(TMath::Max(p_TRACK_GhostProb,L1_TRACK_GhostProb),L2_TRACK_GhostProb)', 'nbins' : 50, 'xmin' : 0., 'xmax' : 0.5, 'log': True, 'nick': 'max_GhostProb' },
        'min(GhostProb)'                        : { 'formula' : 'TMath::Min(TMath::Min(p_TRACK_GhostProb,L1_TRACK_GhostProb),L2_TRACK_GhostProb)', 'nbins' : 50, 'xmin' : 0., 'xmax' : 0.5, 'log': True, 'nick': 'min_GhostProb' },
        'p IP#chi^{2}'                          : { 'formula' : 'p_IPCHI2_OWNPV', 'nbins' : 50, 'xmin' : 0., 'xmax' : 500., 'log': True },
        'p P'                                   : { 'formula' : 'p_P', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.e5, 'log': True },
        'p PT'                                  : { 'formula' : 'p_PT', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.e4, 'log': False },
        'p ProbNNp'                             : { 'formula' : 'p_MC15TuneV1_ProbNNp', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'p ProbNN#pi'                           : { 'formula' : 'p_MC15TuneV1_ProbNNpi', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'p ProbNN#mu'                           : { 'formula' : 'p_MC15TuneV1_ProbNNmu', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'p ProbNNe'                             : { 'formula' : 'p_MC15TuneV1_ProbNNe', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'K IP#chi^{2}'                          : { 'formula' : 'L1_IPCHI2_OWNPV', 'nbins' : 50, 'xmin' : 0., 'xmax' : 500., 'log': True },
        'K P'                                   : { 'formula' : 'L1_P', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.e5, 'log': True },
        'K PT'                                  : { 'formula' : 'L1_PT', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.e4, 'log': False },
        #'K ProbNNp'                             : { 'formula' : 'L1_MC15TuneV1_ProbNNp', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        #'K ProbNN#pi'                           : { 'formula' : 'L1_MC15TuneV1_ProbNNpi', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'K ProbNN#mu'                           : { 'formula' : 'L1_MC15TuneV1_ProbNNmu', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'K ProbNNe'                             : { 'formula' : 'L1_MC15TuneV1_ProbNNe', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        '#pi IP#chi^{2}'                        : { 'formula' : 'L2_IPCHI2_OWNPV', 'nbins' : 50, 'xmin' : 0., 'xmax' : 500., 'log': True },
        '#pi P'                                 : { 'formula' : 'L2_P', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.e5, 'log': True },
        '#pi PT'                                : { 'formula' : 'L2_PT', 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.e4, 'log': False },
        #'#pi ProbNNp'                           : { 'formula' : 'L2_MC15TuneV1_ProbNNp', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        #'#pi ProbNN#pi'                         : { 'formula' : 'L2_MC15TuneV1_ProbNNpi', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        '#pi ProbNN#mu'                         : { 'formula' : 'L2_MC15TuneV1_ProbNNmu', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        '#pi ProbNNe'                           : { 'formula' : 'L2_MC15TuneV1_ProbNNe', 'nbins' : 50, 'xmin' : 0., 'xmax' : 1., 'log': True },
        'm(#Lambda_{c}) [MeV/c^{2}]'            : { 'formula' : "Lc_DTF_PV_M", 'nbins' : 50, 'xmin' : 2200., 'xmax' : 2400, 'log': False, 'nick' : 'mLc' },
        'q^{2} [GeV^{2}/c^{4}]'                 : { 'formula' : strings.q2, 'nbins' : 50, 'xmin' : 0., 'xmax' : 2.5, 'log': False, 'nick' : 'q2' },
        'log(max(DOCA))'                        : { 'formula' : 'TMath::Log(Lc_MAXDOCA)', 'nbins' : 50., 'xmin' : -10., 'xmax' : 10, 'log': False, 'nick' : 'log_MAXDOCA' },
        'log(min(DOCA))'                        : { 'formula' : 'TMath::Log(Lc_MINDOCA)', 'nbins' : 50., 'xmin' : -10., 'xmax' : 10, 'log': False, 'nick' : 'log_MINDOCA' },
#        'cos(#theta)'                        : { 'formula' : 'cosTheta', 'nbins' : 50., 'xmin' : -1., 'xmax' : 1, 'log': False, 'nick' : 'cosTheta' }
        }

vardbSL = {
        'Lb_IPCHI2'                        : { 'formula' : 'Lb_IPCHI2_OWNPV', 'nbins' : 50., 'xmin' : 0, 'xmax' : 2000, 'log': False, 'nick' : 'Lb_IPCHI2' },
        'Lb_ENDVERTEXCHI2'                        : { 'formula' : 'Lb_ENDVERTEX_CHI2/Lb_ENDVERTEX_NDOF', 'nbins' : 50., 'xmin' : 0, 'xmax' : 6, 'log': False, 'nick' : 'Lb_CHI2_NDOF' },
        'muon_IPCHI2_OWNPV'                        : { 'formula' : 'muon_IPCHI2_OWNPV', 'nbins' : 50., 'xmin' : 0, 'xmax' : 20000, 'log': False },
        'muon_TRACK_GhostProb'                        : { 'formula' : 'muon_TRACK_GhostProb', 'nbins' : 50., 'xmin' : 0, 'xmax' : 0.1, 'log': False },
        'muon_TRACK_CHI2NDOF'                        : { 'formula' : 'muon_TRACK_CHI2NDOF', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
#        'Lb_DIRA_OWNPV'                        : { 'formula' : 'Lb_DIRA_OWNPV', 'nbins' : 50., 'xmin' : 0, 'xmax' : 0.1, 'log': False },
        'Lb_PT'                        : { 'formula' : 'Lb_PT', 'nbins' : 50., 'xmin' : 0, 'xmax' : 40000, 'log': False },
        'muon_PT'                        : { 'formula' : 'muon_PT', 'nbins' : 50., 'xmin' : 0, 'xmax' : 20000, 'log': False }
        }

vardbS28 = {
        'Lc_Cone_1.0_h'                        : { 'formula' : 'Lc_Cone_1.0_h', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_1.0_l1'                        : { 'formula' : 'Lc_Cone_1.0_l1', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_1.0_l2'                        : { 'formula' : 'Lc_Cone_1.0_l2', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_1.5_h'                        : { 'formula' : 'Lc_Cone_1.5_h', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_1.5_l1'                        : { 'formula' : 'Lc_Cone_1.5_l1', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_1.5_l2'                        : { 'formula' : 'Lc_Cone_1.5_l2', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_2.0_h'                        : { 'formula' : 'Lc_Cone_2.0_h', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_2.0_l1'                        : { 'formula' : 'Lc_Cone_2.0_l1', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
        'Lc_Cone_2.0_l2'                        : { 'formula' : 'Lc_Cone_2.0_l2', 'nbins' : 50., 'xmin' : 0, 'xmax' : 3, 'log': False },
         'Lc_Cone_Mult_1.0_h'                        : { 'formula' : 'Lc_Cone_Mult_1.0_h', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_1.0_l1'                        : { 'formula' : 'Lc_Cone_Mult_1.0_l1', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_1.0_l2'                        : { 'formula' : 'Lc_Cone_Mult_1.0_l2', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_1.5_h'                        : { 'formula' : 'Lc_Cone_Mult_1.5_h', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_1.5_l1'                        : { 'formula' : 'Lc_Cone_Mult_1.5_l1', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_1.5_l2'                        : { 'formula' : 'Lc_Cone_Mult_1.5_l2', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_2.0_h'                        : { 'formula' : 'Lc_Cone_Mult_2.0_h', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_2.0_l1'                        : { 'formula' : 'Lc_Cone_Mult_2.0_l1', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
        'Lc_Cone_Mult_2.0_l2'                        : { 'formula' : 'Lc_Cone_Mult_2.0_l2', 'nbins' : 50., 'xmin' : 0, 'xmax' : 100, 'log': False },
          'Lc_Cone_PTAsym_1.0_h'                        : { 'formula' : 'Lc_Cone_PTAsym_1.0_h', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_1.0_l1'                        : { 'formula' : 'Lc_Cone_PTAsym_1.0_l1', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_1.0_l2'                        : { 'formula' : 'Lc_Cone_PTAsym_1.0_l2', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_1.5_h'                        : { 'formula' : 'Lc_Cone_PTAsym_1.5_h', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_1.5_l1'                        : { 'formula' : 'Lc_Cone_PTAsym_1.5_l1', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_1.5_l2'                        : { 'formula' : 'Lc_Cone_PTAsym_1.5_l2', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_2.0_h'                        : { 'formula' : 'Lc_Cone_PTAsym_2.0_h', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_2.0_l1'                        : { 'formula' : 'Lc_Cone_PTAsym_2.0_l1', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_Cone_PTAsym_2.0_l2'                        : { 'formula' : 'Lc_Cone_PTAsym_2.0_l2', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Sum_PTAsym_2.0'          : { 'formula' : 'Lc_Cone_PTAsym_2.0_h+Lc_Cone_PTAsym_2.0_l1+Lc_Cone_PTAsym_2.0_l2', 'nbins' : 50., 'xmin' : -3, 'xmax' : 3, 'log': False ,'nick' : 'Sum_PTAsym_2.0'},
        'Sum_PTAsym_1.5'          : { 'formula' : 'Lc_Cone_PTAsym_1.5_h+Lc_Cone_PTAsym_1.5_l1+Lc_Cone_PTAsym_1.5_l2', 'nbins' : 50., 'xmin' : -3, 'xmax' : 3, 'log': False ,'nick' : 'Sum_PTAsym_1.5'},
        'Sum_PTAsym_1.0'          : { 'formula' : 'Lc_Cone_PTAsym_1.0_h+Lc_Cone_PTAsym_1.0_l1+Lc_Cone_PTAsym_1.0_l2', 'nbins' : 50., 'xmin' : -3, 'xmax' : 3, 'log': False ,'nick' : 'Sum_PTAsym_1.0'},
        'Lc_VtxIso_Lc_VTXISODCHI2MASSONETRACK'                        : { 'formula' : 'Lc_VtxIso_Lc_VTXISODCHI2MASSONETRACK', 'nbins' : 50., 'xmin' : 0, 'xmax' : 10000, 'log': False },
        'Lc_VtxIso_Lc_VTXISODCHI2MASSTWOTRACK'                        : { 'formula' : 'Lc_VtxIso_Lc_VTXISODCHI2MASSTWOTRACK', 'nbins' : 50., 'xmin' : 0, 'xmax' : 20000, 'log': False },
        'Lc_VtxIso_Lc_VTXISODCHI2ONETRACK'                        : { 'formula' : 'Lc_VtxIso_Lc_VTXISODCHI2ONETRACK', 'nbins' : 50., 'xmin' : -1, 'xmax' : 1, 'log': False },
        'Lc_VtxIso_Lc_VTXISODCHI2TWOTRACK'                        : { 'formula' : 'Lc_VtxIso_Lc_VTXISODCHI2TWOTRACK', 'nbins' : 50., 'xmin' : 0, 'xmax' : 20, 'log': False },
        'Lc_VtxIso_Lc_VTXISONUMVTX'                        : { 'formula' : 'Lc_VtxIso_Lc_VTXISONUMVTX', 'nbins' : 50., 'xmin' : 0, 'xmax' : 10, 'log': False }
        }


## Function to easily retrieve variables from the DB

def findVar(name,ret='formula',varDB=vardb) :

    for v,prop in varDB.iteritems() :
        if v == name or prop['formula'] == name or ('nick' in prop and prop['nick'] == name) :
            if ret=='formula' : return prop['formula']
            return (prop['formula'], v, prop)

def findVarSL(name,ret='formula') :

    for v,prop in vardbSL.iteritems() :
        if v == name or prop['formula'] == name or ('nick' in prop and prop['nick'] == name) :
            if ret=='formula' : return prop['formula']
            return (prop['formula'], v, prop)


## List of variables for MVA training.

MVAvariables = [
                    findVar("log_Lc_ENDVERTEX_CHI2NDF"),
                   findVar("Lc_TAU")
                  #, findVar("log_Lc_FDCHI2")
                  , findVar("Lc_PT")
                  , findVar("DIRA")
                  , findVar("Lc_DTF_chi2")
#                  , findVar("log_Lc_IPCHI2")
                  , findVar("Lc_IP")
                # , findVar("min_IPCHI2")
                  , findVar("max_GhostProb")
                  , findVar("log_MAXDOCA")
                #  , findVar("cosTheta")
                  ]
MVAvariablesSL = [
                     findVar("log_Lc_ENDVERTEX_CHI2NDF")
                   , findVar("Lc_TAU")
                  #, findVar("log_Lc_FDCHI2")
                   , findVar("Lc_PT")
                   , findVar("DIRA")
                #  , findVar("Lc_DTF_chi2")
                #   , findVar("Lc_IPCHI2_OWNPV")
                  , findVar("Lc_IP")
                # , findVar("min_IPCHI2")
                   , findVar("max_GhostProb")
                #   , findVar("log_MAXDOCA")
                #  , findVar("cosTheta")
                     #findVarSL('Lb_IPCHI2'),
                    ,findVarSL('Lb_CHI2_NDOF')
                     #findVarSL('muon_IPCHI2_OWNPV'),
                    ,findVarSL('muon_TRACK_GhostProb')
                    ,findVarSL('muon_TRACK_CHI2NDOF')
                 #    findVarSL('Lb_PT')
                  #  findVarSL('muon_PT')
                 ]
MVAvariablesS28 = [
                    findVar('Lc_Cone_PTAsym_1.0_h','formula',vardbS28),
                    findVar('Lc_Cone_PTAsym_1.0_l1','formula', vardbS28),
                    findVar('Lc_Cone_PTAsym_1.0_l2','formula', vardbS28),
                    findVar('Lc_VtxIso_Lc_VTXISODCHI2MASSTWOTRACK','formula', vardbS28)
                 ]

