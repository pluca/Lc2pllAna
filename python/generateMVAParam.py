import os
import numpy as np
import test.MVAparam as param 
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="Prompt")
parser.add_argument("--sendBDT",action='store_true')
parser.add_argument("--sendBDTG",action='store_true')
parser.add_argument("--sendMLP",action='store_true')
parser.add_argument("--findMaxBDT",action='store_true')
parser.add_argument("--findMaxBDTG",action='store_true')
parser.add_argument("--findMaxMLP",action='store_true')
parser.add_argument("--threshold",default=float(0.01),help='Compares the training/testing values of the MVA for the three efficiencies: |train - test|< threshold, for the three values')
args=parser.parse_args()
sl=args.mode

if sl=="Prompt":
    time="1nh"
elif sl=="SL":
    time="1nh"
else:
    time="8nm"
print time

MaxDepth=np.arange(2,6,1)
MinNodeSize=np.arange(0.05,0.11,0.01)
AdaBoostBeta=np.arange(0.1,0.9,0.1)
NTrees=np.arange(400,2200,200)
Cycles=np.arange(500,1500,100)
Hidden=np.arange(1,14,1)
TestRate=np.arange(5,15,5)

rocBDT,nameBDT=param.send_BDTJobs(MaxDepth,MinNodeSize,AdaBoostBeta,args.sendBDT,sl,time,args.threshold)
rocBDTG,nameBDTG=param.send_BDTGJobs(MaxDepth,MinNodeSize,NTrees,args.sendBDTG,sl,time, args.threshold)
rocMLP, nameMLP= param.send_MLPJobs(Cycles,Hidden,TestRate,args.sendMLP,sl,time, args.threshold)

if args.findMaxBDT:
    param.findMax(rocBDT,nameBDT,args.mode,'BDT')
if args.findMaxBDTG:
    param.findMax(rocBDTG,nameBDTG,args.mode,'BDTG')
if args.findMaxMLP:
    param.findMax(rocMLP,nameMLP,args.mode,'MLP')
