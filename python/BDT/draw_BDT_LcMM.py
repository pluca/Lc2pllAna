from routines.LcAnalysis import loc,cuts, parseDatafiles
import os
from ROOT import *
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default='Prompt')
parser.add_argument("--dataFile",default="CL16_Lc2pKpi_CutStripPID.root") # Useless since refactoring, only mode counts
parser.add_argument("--MCFile",default="MC12_Lc2pKpi_CutStripPID.root")
parser.add_argument("--dataTuple",default="Lc2pKpiTuple_mvaclone2")
parser.add_argument("--MCTuple",default="Lc2pKpiTupleOld_mvaclone2")
parser.add_argument("--BDTG",action='store_true')
parser.add_argument("--MLP",action='store_true')
args=parser.parse_args()
base = loc.MARKOTUPLE
home = os.path.expanduser("~")
path_TMVA=loc.PLOTS + '/BDT/'



#gROOT.ProcessLine(".x "+home+"/Lc2pllAna/scripts/lhcbStyle.C")
binning=(100,2200,2350)
BDTBinning=(150,-1,1)

if args.BDTG:
    var="BDTGout"
    varName="<BDTG>"
elif args.MLP:
    var="MLPout"
    varName="<MLP>"
    BDTBinning=(75,0,1)
else:
    var="BDTout"
    varName="<BDT>"



############## Cut determination #############
cut=cuts.convergence
if args.mode=="Prompt":
    mcCut=cuts.config_Lc2pKpi_applyBDT_MC
    dataCut=cuts.config_applyBDT
    appendMC,mcTupleName=parseDatafiles('MCLc2pKpi_S28_PIDAndBDT')
    appendData,dataTupleName=parseDatafiles('Lc2pKpi_S28_PIDAndBDT')
    
else:
    mcCut=cuts.config_Lc2pKpi_applyBDT_MC_SL
    dataCut=cuts.config_applyBDT_SL
    appendMC,mcTupleName=parseDatafiles('MCSLLc2pKpi_S28_PIDAndBDT')
    appendData,dataTupleName=parseDatafiles('SLLc2pKpi_S28_PIDAndBDT')
 

print args.mode
mcCut+=TCut("Lc_M<2350 &&Lc_M>2220")
dataCut+=TCut("%s>-1&& (Lc_M<2260||Lc_M>2320)&& Lc_M<2350 && Lc_M > 2220 "%var )




############### Files loading and trees building


bdtfile = base +    appendData # "/CL16_BDT_200k.root"
mcfile= base +  appendMC       #"/MC12_Lc2pKpi_BDT.root"

bdtTree = TChain(dataTupleName)
bdtTree.AddFile(bdtfile)

mcTree = TChain(mcTupleName)
mcTree.AddFile(mcfile)

############### DATA ##########################
c=TCanvas()
MaxBDT=0.09
num=float(BDTBinning[2]-BDTBinning[1])/BDTBinning[0]
bdtTree.Draw(var+":Lc_M>>hProf(%d,%d,%d)"%(binning),dataCut)
bdtTree.Draw(var+":Lc_M>>hProf2(%d,%d,%d)"%(binning),dataCut,"prof")
bdtTree.Draw("Lc_M>>hLcMM(%d,%d,%d)"%binning,dataCut,"E")
bdtTree.Draw(var+">>hBDT(%d,%d,%d)"%(BDTBinning),dataCut)

#Data Plotting
c.SetGrid()
import test.plotting_tools as plt

hProf2.SetMarkerSize(1)
plt.hist_param(hProf,"m(pK#pi ) [MeV/c^{2}]",varName)

hProf.SetMarkerColorAlpha(19,0.999)
hProf.Draw()
hProf2.Draw("same")

c.SaveAs(path_TMVA+"%s_Lc_M.pdf"%args.mode)
c.SaveAs(home+"/report/BDT/%s_Lc_M.png"%args.mode)

c3=TCanvas("DataDetailed","DataDetailed",1600,600)
c3.Divide(2)
c3.cd(1)
gPad.SetGrid()
plt.hist_param(hLcMM,"m(pK#pi ) [MeV/c^{2}]","Events / (1.25 MeV/c^{2})")
hLcMM.Scale(1./hLcMM.Integral())
hLcMM.Draw()
c3.cd(2)
gPad.SetGrid()
plt.hist_param(hBDT,"BDT","Events / %.3f "%num)
hBDT.SetMarkerColor(kBlue)
hBDT.Scale(1./hBDT.Integral())
hBDT.SetMaximum(MaxBDT)
hBDT.Draw()

############### MONTE CARLO ###################

c2=TCanvas()
c2.SetGrid()
#binningMC=(75,2260,2320)
mcTree.Draw(var+":Lc_M>>hProfMC(%d,%d,%d)"%(binning),mcCut)
mcTree.Draw(var+":Lc_M>>hProf2MC(%d,%d,%d)"%(binning),mcCut,"prof")
mcTree.Draw("Lc_M>>hLcMMMC(%d,%d,%d)"%binning,mcCut,"E")
mcTree.Draw(var+">>hBDTMC(%d,%d,%d)"%(BDTBinning),mcCut) 

hProf2MC.SetMarkerSize(1)
hProfMC.GetYaxis().SetTitle(varName)
hProfMC.GetXaxis().SetTitle("m(pK#pi ) [MeV/c^{2}]")
hProfMC.SetMarkerColorAlpha(19,0.999)
hProfMC.Draw()
hProf2MC.Draw("same")
     
c2.SaveAs(loc.PLOTS+"/BDT/"+args.mode+"MC%s_Lc_M.png"%args.mode)

c4=TCanvas("MCDetailed","MCDetailed",1600,600)
c4.Divide(2)
c4.cd(1)
gPad.SetGrid()
plt.hist_param(hLcMMMC,"m(pK#pi ) [MeV/c^{2}]","Events / (1.25 MeV/c^{2})")
hLcMMMC.Draw()
c4.cd(2)
gPad.SetGrid()
plt.hist_param(hBDTMC,"BDT","Events / %.3f "%num)
hBDTMC.SetMarkerColor(kRed)
hBDTMC.Scale(1./hBDTMC.Integral())
hBDTMC.SetMaximum(MaxBDT)
hBDTMC.SetMinimum(0)
hBDTMC.Draw()
hBDT.Draw("same")
c4.SaveAs(loc.PLOTS+"/BDT/%s_Lc_M.pdf"%args.mode)
c3.cd(2)
hBDTMC.Draw("same")
