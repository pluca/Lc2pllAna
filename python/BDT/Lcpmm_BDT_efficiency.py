import os
from routines.LcAnalysis import loc,cuts,parseDatafiles
from ROOT import *
base = loc.TUPLE
home = os.path.expanduser("~")
import tools.sigproc as sp
#************** Parse Arguments ********************
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="Prompt")
args=parser.parse_args()
#************** Prompt or SL ***********************
if args.mode=="Prompt":
    sl=args.mode
    appendMC,mcTuple = parseDatafiles('MCLc2pmm_S28_PIDAndBDT')
    appendData,dataTuple = parseDatafiles('Lc2pmm_S28_PIDAndBDT')
    cut=cuts.config_sidebandFit_MC + cuts.p_PID
    mva="BDT"
    mcfile=loc.TUPLELOCATION+appendMC
else :
    appendMC,mcTuple = parseDatafiles('MCSLLc2pmm_S28_corrected')
    appendData,dataTuple = parseDatafiles('SLLc2pmm_S28_PIDAndBDT')
    mcfile=loc.TUPLELOCATION + appendMC
    sl=args.mode
    mva="BDT"
    cut=cuts.config_sidebandFit_MC_SL + cuts.p_PID_SL

#*************** Efficiency on the MC ***************
mcTree=TChain(mcTuple)
mcTree.AddFile(mcfile)

import numpy as np

BDT_cut=np.arange(-0.9,1,0.1)
N=mcTree.GetEntries()
print N
mcTree.Draw("Lc_DTF_M>>hist",cut)
Ntot=hist.GetEntries()
print Ntot
import tools.efficiency_tools as eff_tools
import tools.plotting_tools as plt
hist_res,eff=eff_tools.compute_efficiency_S28(mcTree,BDT_cut,cut ,Ntot,sl,"normal")


c=TCanvas()
c.SetGrid()

hist_res.Draw()
c.SaveAs(home+"/report/fig/Lc2pmm_MC_efficiency.pdf")
#*************** Polynomial fit on the blinded CL16_BDT_200k.root **********
#First create the root file 
#save_name=loc.MARKOTUPLE+"CL16_Lc2pmm_CutStripPID.root"
#if  not os.path.isfile(save_name):
#    cut_file=TFile(save_name,"recreate")
#    raw_tree=TChain("Lc2pmmTuple")
#    raw_file = base + "/CL16_BDT_200k.root"
#    raw_tree.AddFile(raw_file)
#    full_cuts=cuts.convergence + cuts.oldStripping
#    cut_tree=raw_tree.CopyTree(full_cuts.GetTitle())
#    cut_file.cd()
#    cut_tree.Write()
#    cut_file.Close()
eff_tools.multi_process_exec(eff_tools.process_bdt_exec_S28,sl,BDT_cut)
NBkg=eff_tools.compute_NBkg_S28(sl,BDT_cut)
#**************** Compute FoM ****************************
eff=np.array(eff)
FoM=eff_tools.compute_FoM(eff,NBkg)
#**************** Find Optimum *****************************
max_cut=BDT_cut[np.argmax(FoM)]
new_BDT_cut=np.arange(max_cut-0.1,max_cut+0.1,0.01)
eff_tools.multi_process_exec(eff_tools.process_bdt_exec_S28,sl,new_BDT_cut)
new_hist_res,new_eff=eff_tools.compute_efficiency_S28(mcTree,new_BDT_cut,cut,Ntot,sl,"zoom")
new_eff=np.array(new_eff)
new_NBkg=eff_tools.compute_NBkg_S28(sl,new_BDT_cut)
new_FoM=eff_tools.compute_FoM(new_eff,new_NBkg)
FoM/=np.amax(new_FoM)
new_FoM/=np.amax(new_FoM)
print new_eff
hist,hist_NBkg,hist_den=eff_tools.fill_hist_S28(FoM,NBkg,BDT_cut,sl,"normal")
new_hist,new_hist_NBkg, new_hist_den=eff_tools.fill_hist_S28(new_FoM,new_NBkg, new_BDT_cut,sl,"zoom")
#*************** Plotting  *********************************
c3=TCanvas()
#c3.SetGrid()
hist.SetLineWidth(4)
#hist.Draw("AC")
plt.draw_graph(c3,hist,"AC",-1,1,0,1)
hist_res.SetLineColor(kRed)
hist_res.SetLineWidth(4)
hist_res.Draw("C")
hist_den.SetLineColor(kBlue)
hist_den.SetLineWidth(4)
hist_den.Draw("C")
'''new_hist.SetLineWidth(4)
new_hist.Draw("C")
new_hist_res.SetLineColor(kRed)
new_hist_res.SetLineWidth(4)
new_hist_den.SetLineColor(kBlue)
new_hist_den.SetLineWidth(4)
new_hist_res.Draw("C")
new_hist_den.Draw("C")'''
leg=TLegend(0.16,0.18,0.5,0.5)
leg.AddEntry(hist,"FoM #frac{#epsilon}{5/2 + #sqrt{NBkg}}","l")
leg.AddEntry(hist_res,"MC efficiency #epsilon","l")
leg.AddEntry(hist_den,"#frac{1}{5/2 + #sqrt{NBkg}}","l")
leg.SetTextSize(0.04)
leg.Draw()
#leg2=TLegend(0.55,0.7,0.95,0.95)
#leg2.AddEntry(hist,"Max at %s > %.3f"%(mva,new_BDT_cut[np.argmax(new_FoM)]),"l")
#leg2.Draw()
c4=TCanvas()
c4.cd()
c4.SetGrid()
hist_NBkg.Draw()
new_hist_NBkg.Draw("same")
print np.amax(new_FoM),new_BDT_cut[np.argmax(new_FoM)]
c3.SaveAs(loc.PLOTS+'/SIDEBAND/'+args.mode+"Lc2pmm_FoM.pdf")
c4.SaveAs(loc.PLOTS+'/SIDEBAND/'+args.mode+"Lc2pmm_NBkg.pdf")



#*************** Remove Created .root files ******************
#os.chdir(loc.REPO+"/cpp/")
#os.system("rm rootFiles/*.root")
#os.system("mv *.pdf plots/")






