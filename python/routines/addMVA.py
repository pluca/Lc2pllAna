import os, sys
from ROOT import *
from array import array
from variables_DB import MVAvariables as varsPrompt
from variables_DB import MVAvariablesSL as varsSL
from variables_DB import MVAvariablesS28 as varsS28
from routines.LcAnalysis import loc

args = None 

## Variables that need to be shared
variables = {}

vrefs = {}
reader = TMVA.Reader()
method = None

def setup():
    
    global variables, reader, vrefs, method

    dtype  = args[0]
    method = args[1] 

    if dtype =="SL": vars=varsSL
    else: vars=varsPrompt+varsS28
    
    wdir  = loc.MVARES+dtype+'/weights/'
    wfile = wdir+'TMVAClassification_'+method+'.weights.xml'

    print "----- Setting up: ", method, wfile

    vrefs = {}
    reader = TMVA.Reader()
    for v in vars :
	    vrefs[v] = array('f', [0.])
	    reader.AddVariable(v,vrefs[v])
    
    reader.BookMVA(method,wfile)

    vnames    = [method+'out',method+'out_err']
    variables = { name : array("d", [0.0]) for name in vnames }


def addVariables(tree,ev,index,vv,extra) :

    tree.GetEntry(index)
    for v,ref in vrefs.iteritems() :
        formula = TTreeFormula(v,v,tree);
        ref[0] = formula.EvalInstance()
    
    vv[method+'out'][0]     = reader.EvaluateMVA( method )
    vv[method+'out_err'][0] = reader.GetMVAError()

    
