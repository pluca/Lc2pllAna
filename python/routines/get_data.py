from ROOT import *
from glob import glob
import re
from routines.LcAnalysis import loc, dataids

class dict2obj :
    def __init__(self,entries) :
        self.__dict__.update(**entries)

class defopts :
    dtype  = 'Prompt'
    maxf   = 1e10
    fromid = None 
    def __init__(self) : pass

def getData(trees = ['Lc2pKpiTuple/DecayTree','Lc2pmmTuple/DecayTree'], friends = ['before_BDT'], opts = None) :

    out = {}
    for treename in trees :
        
        files, friends = getDataFiles(friends, opts = opts)
        data = buildChain(treename,files,friends)
        out[treename.replace('/DecayTree','')] = data

    return dict2obj(out)
 

def getDataFiles(friends = ['before_BDT'], opts = None) :

    if opts is None :
        opts = defopts()
    
    if opts.dtype in dataids :
        ids = dataids[opts.dtype]
    elif opts.fromid is not None : 
        ids = opts.fromid.split(',')
    else :
        print "No files found"
        return

    files = []
    for cid in ids :
        print "Looking for /eos/lhcb/user/p/pluca/ganga/"+str(cid)+"/*/DVNtuple.root"
        files += glob("/eos/lhcb/user/p/pluca/ganga/"+str(cid)+"/*/DVNtuple.root")
    
    friends = findFriendFiles(files,friends)

    return files, friends


def findFriendFiles(files, friends) :

    out = []

    for filename in files :

        friendFiles = { k : None for k in friends}

        ids = re.findall('/(\d+)/(\d+)/',filename)
        if len(ids) != 1 or len(ids[0]) < 2 : 
            print "Path", filename, "can't have friends"
            continue

        for fr in friends :
            path = loc.FRIENDS+"{fr}/{ids}/{fr}.root".format(fr=fr,ids='/'.join(ids[0]))
            if len(glob(path)) != 1 : 
                print "Path", filename, "has no", "'"+fr+"'", "friend in path", path
                continue
            friendFiles[fr] = path

        out.append(friendFiles)
    
    return out


def buildChain(tname, files, friends = None, check = False) :

    chain = TChain(tname)

    if friends is None :
        for ff in files :
            if not chain.AddFile("root://eoslhcb.cern.ch/"+mainfile) :
                print "Tree", tname, "missing in file", mainfile
                continue
        return chain

    if len(friends) != len(files) and len(files) > 0: 
        print "Files and friends have to be of same length"
        return None

    friendChains = { f : TChain(tname.replace("/DecayTree","")+'_'+f) for f in friends[0] }

    for mainfile, friendfiles in zip(files,friends) :

        skip = False
        for frname, frfile in friendfiles.iteritems() :
            if frfile is None :
                print "File", mainfile, "has no friend", frname, ". Skipping..."
                skip = True
                break

            if not check : continue
            ff = TFile.Open(frfile)
            if not ff.Get(friendChains[frname].GetName()) :
                print "Friend", frname, "has no tree", friendChains[frname].GetName(), "in", frfile
                skip = True
                break

        if skip : continue

        if not chain.AddFile("root://eoslhcb.cern.ch/"+mainfile) :
            print "Tree", tname, "missing in file", mainfile
            continue
        
        for frname, frfile in friendfiles.iteritems() :
            friendChains[frname].AddFile("root://eoslhcb.cern.ch/"+frfile)
        
        for frname, frchain in friendChains.iteritems() :
            chain.AddFriend(frchain)
    
    return chain


