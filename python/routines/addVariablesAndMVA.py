import addVariables as addV
import addMVA as addM
from ROOT import *
from array import array 

args = None

variables = {}

def setup() : 

    global variables

    print "------ Both modules setup."
    addM.args = args
    addM.setup()

    variables = dict(addV.variables)
    variables.update(addM.variables)
    
    print "Variables that will be added: " 
    print variables.keys()

def addVariables(tree,ev,index,vv,extra):
    addV.addVariables(tree,ev,index,vv,extra)
    addM.addVariables(tree,ev,index,vv,extra)
