from routines.utils import getPartLV
from routines.getResVars import getResVars
from ROOT import *
from routines.LcAnalysis import strings
from array import array
from math import atanh

vnames = ["Lc_DTF_MM","Lc_DTF_chi2_0","q2","category","sPH1","sPH2","sH1H2","cosTheta","cosPhi","thetaH1H2", "MuMu_M", "p_TRACK_Eta","L1_TRACK_Eta","L2_TRACK_Eta"]
variables = { name : array("d", [0.0]) for name in vnames }

def setup() : pass

def addVariables(tree,ev,index,vv,extra) :

    vv["Lc_DTF_MM"][0]   = ev.Lc_DTF_PV_M[0]
    vv["Lc_DTF_chi2_0"][0] = ev.Lc_DTF_PV_chi2[0]
    vv["category"][0] = extra['nevts'] % extra['ncategories']
    vv["p_TRACK_Eta"][0] = atanh(ev.p_PZ/ev.p_P)
    vv["L1_TRACK_Eta"][0] = atanh(ev.L1_PZ/ev.L1_P)
    vv["L2_TRACK_Eta"][0] = atanh(ev.L2_PZ/ev.L2_P)

    formula = TTreeFormula("q2",strings.q2, tree);
    tree.GetEntry(index)
    vv["q2"][0]       = formula.EvalInstance()
    formulaM = TTreeFormula("MuMu_M",strings.mMuMu, tree);
    vv["MuMu_M"][0]       = formulaM.EvalInstance()

    ( vv["sPH1"][0], vv["sH1H2"][0], vv["sPH2"][0],
      vv["cosTheta"][0], vv["cosPhi"][0], vv["thetaH1H2"][0] ) = getResVars(
                TLorentzVector(),
                getPartLV('Lc',ev),
                getPartLV('p', ev),
                getPartLV('L1',ev),
                getPartLV('L2',ev)         )

      
