import os, sys, glob

import warnings
warnings.filterwarnings( action='ignore', category=RuntimeWarning, message='creating converter.*' )

if os.getenv('LCANAROOT') is None :
    print("Attention, you did not setup. Run 'source setup.sh' before doing anything")
    sys.exit()

from ROOT import *
gROOT.ProcessLine('.x '+os.getenv('LCANAROOT')+'/scripts/lhcbStyle.C')

################ Cuts all defined into cpp/includes/LcAnaCuts.hpp
################ They are accessible both from cpp and python

from cut_converter import CutConverter
cuts = CutConverter(os.getenv("LCCUTSLOCATION"))

############### USER fetch #######################
user = os.getenv("USER")
################## Location in repository or on EOS

class loc : pass
loc.ROOT     = os.getenv('LCANAROOT')
loc.PLOTS    = os.getenv('LCPLOTSLOCATION')
loc.JOBS     = os.getenv('LCJOBSLOCATION')
loc.TUPLE    = os.getenv('LCTUPLELOCATION')
loc.FRIENDS  = os.getenv('LCTUPLELOCATION')+'friends/'
loc.REPO     = loc.ROOT+'/'
loc.MVARES   = loc.ROOT+'/mva/'
loc.SCRIPTS  = loc.ROOT+'/scripts/'
loc.PYTHON   = loc.ROOT+'/python/'
loc.PID      = loc.ROOT+'/PID/'
loc.LHCB     = loc.ROOT+'/LHCb/'
loc.TABS      = loc.ROOT+'/tables/'
loc.TMPS      = loc.ROOT+'/tables/templates/'
loc.MARKOTUPLE   = os.getenv('MARKOTUPLELOCATION')+"/"
loc.MARKOFRIENDS = loc.MARKOTUPLE + "friends/"
loc.MARKOFRIENDS_PMM = loc.MARKOTUPLE + "friendsLc2pmm/"
loc.CPP      = loc.ROOT+'/cpp/'
loc.CPPFITTER = loc.CPP + 'dat/fitter/'
loc.REPORT = os.path.expanduser('~')+'/report/'
loc.REPORTRAW = loc.REPORT +'RAW/'
loc.REPORTPID = loc.REPORT +'PID/'
loc.MARKOJOBS = os.getenv('MARKOJOBSLOCATION')+"/"
loc.MARKOEOSJOBS = '/eos/lhcb/user/m/mstamenk/jobs/'
loc.PIDCALIB = os.getenv('PIDCALIB')+'/'

loc.PYTHONRESSOURCE = loc.PYTHON+"ressource/"

loc.PIDRESSOURCE = loc.PID + 'PerfHistsMuon/'

oc.TUPLELOCATION = os.getenv('TUPLELOCATION')+'/' # User sensitive from setup.sh
loc.TUPLEFRIENDS = loc.TUPLELOCATION + "friends/"
loc.EOSJOBS = os.getenv('EOSJOBS')

################## Data files

dataids = {
        'Prompt'       : [506,507],  # S28
        'SL'           : [525,526],  # S28

        'MCLc2pmm'     : [536,537],
        'MCLc2pmmSS'   : [499,500],
        'MCLc2ppipi'   : [478,479],
        'MCLc2pKK'     : [490,491], 
        'MCLc2pKpi'    : [512,513],

        'MCD2KKpi'     : [480,481],
        'MCD2Kpipi'    : [482,483],
        'MCD2Kmumu'    : [486,487],
        'MCD2pimumu'   : [488,489],

        'MCSLLc2pmm'   : [391,392], 
        'MCSLLc2pKK'   : [393,394], 
        'MCSLLc2ppipi' : [395,396], 
        'MCSLLc2pKpi'  : [403,404] 
        }

from dictionaries import datafiles

brIDS = { ##Branching fraction of contaiminations channels
        'MCLc2pKpi' : (6.35e-2,0.33e-2),
        'MCLc2ppipi': (4.3e-3,0.4e-3),
        'MCLc2pKK'  : (10e-4,4e-4),
        'MCD2KKpi'  : (9.51e-3,0.34e-3),
        'MCD2Kpipi' : (5.19e-4,0.26e-4),
        'MCD2Kmumu' : (4.3e-6,0),
        'MCD2pimumu': (7.3e-8,0),
        'MCLc2pphi' : (2.87e-4*1.04e-3,0.63e-7),
        }


PIDfiles = {
        'PIDCalib_MagUp_MuMu'    :  'PIDHists_Turbo16_MagUp_MuMu.root',
        'PIDCalib_MagDown_MuMu'  :  'PIDHists_Turbo16_MagDown_MuMu.root',
        'PIDCalib_MagUp_Kpi'    :  'PIDHists_Turbo16_MagUp_Kpi.root',
        'PIDCalib_MagDown_Kpi'    :  'PIDHists_Turbo16_MagDown_Kpi.root',
        }


def checkedFileList(name,ids) :
    datafiles[name] = []
    for dataid in ids :
        files = glob.glob(loc.JOBS+"/"+str(dataid)+"/*/DVNtuple.root")
        for f in files : 
            #ff = TFile.Open(f)
            #if not ff : 
            #    print "skip"
            #    continue
            #t  = ff.Get("Lc2pmmTuple/DecayTree")
            #if not t :
            #    print "skip tree ", f
            #    continue
            datafiles[name].append(f)
def parseDatafiles(dtype):
    fileName=datafiles[dtype][0]
    treeName=datafiles[dtype][1]
    return fileName, treeName

def parsePIDfiles(dtype):
    fileName=PIDfiles[dtype]
    return fileName


def retrieve_files() :

    print("Retrieving valid datafiles")
    for curname,curids in dataids.iteritems() :
        checkedFileList(curname,curids)

    print("Files retrieved :")
    print('SL -> {} files'.format(len(datafiles['SL'])))
    print('Prompt -> {} files'.format(len(datafiles['Prompt'])))
################### Miscellanous strings

class strings : pass

## q2 definition
p_2 = "(TMath::Power(L1_PX+L2_PX,2) + TMath::Power(L1_PY+L2_PY,2) + TMath::Power(L1_PZ+L2_PZ,2))"
E1 = "TMath::Sqrt(105.65836*105.65836 + L1_PZ*L1_PZ + L1_PX*L1_PX + L1_PY*L1_PY)"
E2 = "TMath::Sqrt(105.65836*105.65836 + L2_PZ*L2_PZ + L2_PX*L2_PX + L2_PY*L2_PY)"
sumE1E2 ="({E1}+{E2})".format(E1=E1,E2=E2)
sumE11E22 ="(L1_PE + L2_PE)"
strings.q2 = "(TMath::Power({sumE},2) - {p2})/1000000.".format(sumE=sumE1E2,p2=p_2)
strings.mMuMu = "TMath::Sqrt(TMath::Power({sumE},2) - {p2})".format(sumE=sumE1E2,p2=p_2)

strings.q2E = "(TMath::Power({sumE},2) - {p2})/1000000.".format(sumE=sumE11E22,p2=p_2)





################# Values database and outputfiles




import pickle

db = pickle.load(open(loc.LHCB+"db.pkl"))
effForTex = pickle.load(open(loc.LHCB+"effForTex.pkl"))
full_eff = pickle.load(open(loc.LHCB+"full_efficiency.pkl"))
full_effForTex = pickle.load(open(loc.LHCB+"full_efficiencyForTex.pkl"))
trigger =  pickle.load(open(loc.LHCB+"trigger.pkl"))
hlt1_Prompt = pickle.load(open(loc.LHCB+"hlt1_Prompt.pkl"))
hlt1_SL = pickle.load(open(loc.LHCB+"hlt1_SL.pkl"))
hlt2_Prompt = pickle.load(open(loc.LHCB+"hlt2_Prompt.pkl"))

fitResult = pickle.load(open(loc.LHCB+"fitResult.pkl"))
fitResult_forTex = pickle.load(open(loc.LHCB+"fitResult_forTex.pkl"))

MISID = pickle.load(open(loc.LHCB+"MISID.pkl"))
MISID_forTex = pickle.load(open(loc.LHCB+"MISID_forTex.pkl"))

def dumpEffForTex() :
    pickle.dump(effForTex,open(loc.LHCB+"effForTex.pkl","w"))
def dumpTrigger() :
    pickle.dump(trigger, open(loc.LHCB+"trigger.pkl","w"))
def dumpDB() :
    pickle.dump(db,open(loc.LHCB+"db.pkl","w"))
def dumpFullEff():
    pickle.dump(full_eff,open(loc.LHCB+"full_efficiency.pkl","w"))
def dumpFullEffForTex():
    pickle.dump(full_effForTex,open(loc.LHCB+"full_efficiencyForTex.pkl","w"))
def dumpFitResult():
    pickle.dump(fitResult,open(loc.LHCB+"fitResult.pkl","w"))
    pickle.dump(fitResult_forTex,open(loc.LHCB+"fitResult_forTex.pkl","w"))
def dumpHLT():
    pickle.dump(hlt1_Prompt,open(loc.LHCB+"hlt1_Prompt.pkl","w"))
    pickle.dump(hlt1_SL,open(loc.LHCB+"hlt1_SL.pkl","w"))
    pickle.dump(hlt2_Prompt,open(loc.LHCB+"hlt2_Prompt.pkl","w"))

def dump(dataB,name):
    pickle.dump(dataB,open(loc.LHCB+name,"w"))

def dumpAll():
    dumpEffForTex()
    dumpTrigger()
    dumpDB()
    dumpFullEff()
    dumpFullEffForTex()
    dumpHLT()
    dumpFitResult()
    dump(MISID,"MISID.pkl")
    dump(MISID_forTex,"MISID_forTex.pkl")


################## Branching ratio of the p mm channel
Br_Lc2pphi = (2.87e-4*1.04e-3,0.63e-7) # ratio +- err
N_Lc2pphi = (133.338,14.4) #Obsolete : case sensitive handled directly in script by using fitResult["{mode}NSig"]


from routines.formatter import PartialFormatter as Formatter


class Outfiles :

    def __init__(self) :
        
        if not os.path.exists(loc.LHCB+"outfiles_list.txt") :
            f = open(loc.LHCB+"outfiles_list.txt","w")
            f.close()

        lines = open(loc.LHCB+"outfiles_list.txt").readlines()
        self.files = {}
        for l in lines :
            toks = l.split()
            self.files[toks[0]] = toks[1]

    def writeline(self,name,text,clear=False) :

        self.write(name,text+"\n",clear)

    def write(self,name,text,clear=False) :

        if clear : f = open(self.files[name],"w")
        else : f = open(self.files[name],"a")
        f.write(text)
        f.close()

    def fill_template(self,name,template,dic) :
        
        tmp = open(loc.TMPS+template)
        
        fmt = Formatter()
        out = fmt.format(tmp.read(),**dic)

        self.write(name,out,clear=True)

    def create(self,name,filename=None, extension=".txt") :

        if filename == None : filename = name
        if "." not in filename : filename += extension

        path = loc.TABS+filename
        self.files[name] = path

        f = open(loc.LHCB+"outfiles_list.txt","w")
        for n,p in self.files.iteritems() :
            f.write(n+" "+p)

outfiles = Outfiles()

