# Dictionary files : in order to perform an analysis with respect to the year of exploitation



### Datafiles: Dict with key : [fileName,treeName]
datafiles = {
        #Raw fit + PID
        'Lc2pKpi_S28'     : ['CL16_Lc2pKpi_before_BDT_1m.root ','Lc2pKpiTuple_aV'],
        'Lc2pKpi_S28_100k': ['CL16_Lc2pKpi_S28_100k.root','DecayTree'],
        'MCLc2pKpi_S28'   : ['MC16_Lc2pKpi_S28.root','DecayTree'],
        'SLLc2pKpi_S28'   : ['SLLc2pKpi_sW.root', 'Lc2pKpi_sW'], # for sW because no other tuple available
        'MCSLLc2pKpi_S28' : ['MC12_SL_Lc2pKpi_CutAndBDT.root','Lc2pKpiTuple'], # MC with BDT applied but no cuts
        # MVA training : generated in order to have same number of signal and background events !!! ONLY SIDEBAND !!!!
        'MVA'     : ['CL16_Lc2pKpi_forMVA.root ','Lc2pKpiTuple_aV'],
        #MVA training/testing
        'Lc2pKpi_S28_beforeBDT'  : ['CL16_Lc2pKpi_before_BDT.root', 'Lc2pKpiTuple_aV'],
        'MCLc2pKpi_S28_beforeBDT': ['MC16_Lc2pKpi_before_BDT.root','Lc2pKpiTuple_aV'],
        'Lc2pKpi_S28_beforeBDT_100k'  : ['CL16_Lc2pKpi_before_BDT_100k.root', 'Lc2pKpiTuple_aV'],        
        'SLLc2pKpi_S28_beforeBDT': ['CL16_SL_Lc2pKpi_100k.root','Lc2pKpiTuple'],
        'MCSLLc2pKpi_S28_beforeBDT' : ['MC12_SLLc2pKpi.root', 'Lc2pKpiTuple'],     
        #### Lc2pKpi before BDT rootfiles + reduced for the MVA optimisation
        'Prompt_beforeBDT'      : ['CL16_Lc2pKpi_before_BDT.root', 'Lc2pKpiTuple_aV'],
        'SL_beforeBDT'          : ['CL16_SLLc2pKpi_before_BDT.root', 'Lc2pKpiTuple_aV'],
        'MCLc2pKpi_beforeBDT'   : ['MC16_Lc2pKpi_before_BDT.root','Lc2pKpiTuple_aV'],
        'MCSLLc2pKpi_beforeBDT' : ['MC12_SLLc2pKpi_before_BDT.root', 'Lc2pKpiTuple_aV'], 

        'Prompt_reduced'        : ['CL16_Lc2pKpi_before_BDT_reduced.root', 'Lc2pKpiTuple_aV'], 
        'SL_reduced'            : ['CL16_SLLc2pKpi_before_BDT_reduced.root', 'Lc2pKpiTuple_aV'],
        'MCLc2pKpi_reduced'     : ['MC16_Lc2pKpi_reduced.root','Lc2pKpiTuple_aV'],
        'MCSLLc2pKpi_reduced'   : ['MC12_SLLc2pKpi_reduced.root', 'Lc2pKpiTuple_aV'], 

        #### PID and BDT for Snakemake 
        
        #MVA Applied, ready for MVA optimisation + triggers analysis
        'Lc2pKpi_S28_PIDAndBDT'  : ['CL16_PIDAndBDT.root', 'Lc2pKpiTuple_Prompt'],
        'MCLc2pKpi_S28_PIDAndBDT': ['MC16_Lc2pKpi_PIDAndBDT.root','Lc2pKpiTuple_Prompt'],
        'MCLc2pmm_S28_PIDAndBDT': ['MC16_Lc2pmm_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'Lc2pmm_S28_PIDAndBDT'  : ['CL16_PIDAndBDT.root', 'Lc2pmmTuple_Prompt'],
        #SL Tuples
        'SLLc2pKpi_S28_PIDAndBDT' : ['CL16_SLLc2pKpi_PIDAndBDT.root','Lc2pKpiTuple_SL_pKpi'],
        'SLLc2pmm_S28_PIDAndBDT' : ['CL16_SLLc2pmm_PIDAndBDT.root','Lc2pmmTuple_SL_pmm'],
        'MCSLLc2pKpi_S28_PIDAndBDT': ['MC16_SLLc2pKpi_PIDAndBDT.root','Lc2pKpiTuple_SL'],
        'MCSLLc2pmm_S28_PIDAndBDT': ['MC16_SLLc2pmm_PIDAndBDT.root','Lc2pmmTuple_SL'],
        #Old SL tuples
        'SLLc2pKpi_S28_PIDAndBDT_old'  : ['CL16_SLLc2pKpi_CutStripPID.root', 'Lc2pKpiTuple_SL'],
        'SLLc2pmm_S28_PIDAndBDT_old'  : ['CL16_SLLc2pmm_CutStripPID.root', 'Lc2pKpiTuple_SL'],
        'MCSLLc2pKpi_S28_PIDAndBDT_old': ['MC12_SLLc2pKpi_CutStripPID.root','Lc2pKpiTuple_SL'],
        'MCSLLc2pmm_S28_PIDAndBDT_old': ['MC12_SLLc2pmm_CutStripPID.root','Lc2pKpiTuple_SL'],
        #Triggers analysis 
        'MCLc2pKpi_forTriggers' : ['MC16_Lc2pKpi_forTriggers', 'Lc2pKpiTuple_Prompt'],
        'MCLc2pmm_forTriggers' : ['MC16_Lc2pmm_forTriggers', 'Lc2pmmTuple_Prompt'],
        # For phi and rare Fit (full selection)
        'MCLc2pmm_S28_forFit' : ['MC16_Lc2pmm_forFit.root','Lc2pmmTuple_Prompt'],
        'MCLc2Kpi_S28_fullSelection' : ['MC16_Lc2pKpi_fullSelection.root','Lc2pKpiTuple_Prompt'],
        'CL16_Lc2pmm_S28_forFit' : ['CL16_Lc2pmm_forFit.root','Lc2pmmTuple_Prompt'],
        'CL16_Lc2pKpi_S28_forFit' : ['CL16_Lc2pKpi_forFit.root','Lc2pKpiTuple_Prompt'],
        # MC Corrected variable
        'MCLc2pmm_S28_corrected' : ['MC16_Lc2pmm_PIDAndBDT_PID-corrected_new.root','Lc2pmmTuple_Prompt'], # Mag Up , convergence cut on the L2_MC15TuneV1_ProbNNmucorr > 0

        #MVA Applied, ready for MVA optimisation + triggers analysis
        'Prompt_PIDAndBDT'      : ['CL16_PIDAndBDT.root', 'Lc2pKpiTuple_Prompt'],
        'MCLc2pKpi_PIDAndBDT'   : ['MC16_Lc2pKpi_PIDAndBDT.root','Lc2pKpiTuple_Prompt'],
        'MCLc2pmm_PIDAndBDT'    : ['MC16_Lc2pmm_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        #SL Tuples
        'SL_PIDAndBDT'          : ['CL16_SL_PIDAndBDT.root','Lc2pKpiTuple_SL_pKpi'],
        'MCSLLc2pKpi_PIDAndBDT' : ['MC16_SLLc2pKpi_PIDAndBDT.root','Lc2pKpiTuple_SL'],
        'MCSLLc2pmm_PIDAndBDT'  : ['MC16_SLLc2pmm_PIDAndBDT.root','Lc2pmmTuple_SL'],
        
        #Background study
        'MCLc2ppipi_PIDAndBDT'  : ['MC16_Lc2ppipi_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'MCLc2pKK_PIDAndBDT'    : ['MC16_Lc2pKK_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'MCD2KKpi_PIDAndBDT'    : ['MC16_D2KKpi_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'MCD2Kpipi_PIDAndBDT'   : ['MC16_D2Kpipi_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'MCD2Kmumu_PIDAndBDT'   : ['MC16_D2Kmumu_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'MCD2pimumu_PIDAndBDT'  : ['MC16_D2pimumu_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        #SL
        'MCSLLc2ppipi_PIDAndBDT'  : ['MC16_SLLc2ppipi_PIDAndBDT.root','Lc2pmmTuple_Prompt'],
        'MCSLLc2pKK_PIDAndBDT'    : ['MC16_SLLc2pKK_PIDAndBDT.root','Lc2pmmTuple_Prompt'],}

typeID = { 
          'Prompt'      : ['Prompt','Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree'],
          'MCLc2pKpi'   : ['Prompt','Lc2pKpiTuple/DecayTree'],
          'MCLc2pmm'    : ['Prompt','Lc2pmmTuple/DecayTree'], 
          'SL'          : ['SL','Lc2pKpiTuple/DecayTree;Lc2pmmTuple/DecayTree'],
          'MCSLLc2pKpi' : ['SL','Lc2pKpiTuple/DecayTree'],
          'MCSLLc2pmm'  : ['SL','Lc2pmmTuple/DecayTree'],
          # Background study
          'MCLc2ppipi'  : ['Prompt','Lc2pmmTuple/DecayTree'],
          'MCLc2pKK'    : ['Prompt','Lc2pmmTuple/DecayTree'],
          'MCD2KKpi'    : ['Prompt','Lc2pmmTuple/DecayTree'],
          'MCD2Kpipi'   : ['Prompt','Lc2pmmTuple/DecayTree'],
          'MCD2Kmumu'   : ['Prompt','Lc2pmmTuple/DecayTree'],
          'MCD2pimumu'  : ['Prompt','Lc2pmmTuple/DecayTree'],
          #SL
          'MCSLLc2ppipi': ['SL','Lc2pmmTuple/DecayTree'],
          'MCSLLc2pKK'  : ['SL','Lc2pmmTuple/DecayTree'],
          }
        
fitID = { 'Prompt'      : ('CL16_Lc2pmm_forFit.root','MC16_Lc2pmm_forFit.root'),   
          'SL'          : ('CL16_SLLc2pmm_forFit.root','MC16_SLLc2pmm_forFit.root'),   
          'None'        : ('None')}

checkType = { 'Prompt': 'Prompt',  
              'SL': 'SL',   
              'MCLc2pmm': 'None',   
              'MCSLLc2pmm': 'None',
              'MCLc2pKpi': 'None',
              'MCSLLc2pKpi': 'None'}



