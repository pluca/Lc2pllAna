import os, sys
from ROOT import *

#from array import array
#from variables_DB import MVAvariables as vars
from routines.LcAnalysis import cuts

args = ["MC"]

def cutEvents(tree,ev,index,extra) :

    if args[0]=="Prompt":
        cut = (cuts.config_applyBDTG).GetTitle() 
    elif args[0]=="SL":
        cut = (cuts.config_applyBDT_SL).GetTitle() 
    else:
        if args[0]=="MCLc2pKpi":
            cut = (cuts.config_Lc2pKpi_applyBDTG_MC).GetTitle()
        elif args[0]=="MCLc2pmm" :
            cut = (cuts.config_Lc2pmm_applyBDTG_MC).GetTitle()
        elif args[0] == "MCSLLc2pKpi":
            cut = (cuts.config_Lc2pKpi_applyBDT_MC_SL).GetTitle()
        elif args[0]=='MC':
            cut = (cuts.config_bkg_study_MC).GetTitle()
        else:
            cut = (cuts.config_Lc2pmm_applyBDT_MC_SL).GetTitle()
    tree.GetEntry(index)
    formula = TTreeFormula(cut,cut,tree);
    return formula.EvalInstance()

    
