from ROOT import *
from argparse import ArgumentParser
from array import array
import os, sys, re, pickle
from routines.LcAnalysis import loc, datafiles
from routines.utils import loopTrees, checkDoneCorrectly
import subprocess as sb
import importlib
from glob import glob
from get_data import *

parser = ArgumentParser()
parser.add_argument("-m","--maxev",   default=1e14,type=float)
parser.add_argument("-O","--outfile", default="out")
parser.add_argument("-T","--outtree", default=None)
parser.add_argument("-t","--tname",   default="DecayTreeTuple/DecayTree")
parser.add_argument("-d","--dtype",   default=None)
parser.add_argument("-i","--fromid",  default=None)
parser.add_argument("-q","--queue",   default="1nd")
parser.add_argument("-w","--worker",  default=[None], nargs='*',help='-w addMVA/addVariablesAndMVA MODE MVA_TYPE ')
parser.add_argument("-c","--cuts",    default=[None], nargs='*', help='-c cutStripPID Prompt or SL or MCSLLc2pKpi or MCLc2pmm/MCLc2pmm')
parser.add_argument("--friends",      default="")
parser.add_argument("--local",        action="store_true")
parser.add_argument("--clone",        action="store_true")
parser.add_argument("--makeFriends",  action="store_true")
parser.add_argument("--force",        action="store_true")
parser.add_argument("--checkjobs",    action="store_true")
parser.add_argument("--checkoutfile", action="store_true")
parser.add_argument("-f","--inputs",  nargs="+")
args = parser.parse_args()

## Find worker and cutter
if args.local :
   
    worker, cutter = None, None
    if args.worker[0] is not None :
        worker = importlib.import_module("routines."+args.worker[0])
        worker.args = args.worker[1:]
    if args.cuts[0] is not None :
        cutter = importlib.import_module("routines."+args.cuts[0])
        cutter.args = args.cuts[1:]

## Retrieve TChains

files = args.inputs
friends = None
if files is None :
    if args.dtype is None and args.fromid is None :
        print "No files to reduce"
        sys.exit()

    myfriends = []
    if args.friends != "" : myfriends = args.friends.split(',')
    print "Retrieving files and friends (", myfriends, ")"
    files, friends = getDataFiles(opts=args,friends = myfriends)

## Extra arguments

args.extra = {'ncategories' : 10}

## Merge into one single file (only 1 job possible so no batch)
if not args.makeFriends :

    trees = []
    print "Files retrieved, building chains"
    for tname in args.tname.split(';') :
        if friends is not None : tree = buildChain(tname,files,friends,check=False)
        else : tree = buildChain(tname,files,check = Fase)
        
        print "Tree", tname,":", tree.GetEntries(), "entries found"
        trees.append(tree)

    loopTrees(trees,worker,cutter=cutter,args=args)
    sys.exit()


## Merge into many files (possibility of batch)
usr=os.getenv('USER')
if usr=="pluca": tuplePath=loc.FRIENDS
elif usr=="mstamenk": tuplePath=loc.MARKOFRIENDS
else:
    print "Please specify friends tuple path"
    sys.exit()

## Output directory
rootdir = tuplePath+args.outtree.replace(".root","")
os.system("mkdir -p "+rootdir)
print "Output will go into", rootdir


for fi,f in enumerate(files) : 

    ids = re.findall('/(\d+)/(\d+)/',f)[0]
    os.system("mkdir -p "+rootdir+'/'+str(ids[0]))
    curdir = rootdir+'/'+'/'.join(ids)
    os.system("mkdir -p "+curdir)

    args.outtree = args.outtree.replace(".root","")
    args.outfile = curdir + "/{name}.root".format(name=args.outtree)

    ## Check if output not there because job still running

    if args.checkjobs :
        jobs = sb.check_output('bjobs')
        if '_'.join(ids) in jobs : 
            print "Job", ids, "already submitted"
            continue

    ## Check if already done correctly if not relaunch
    
    if len(glob(args.outfile)) > 0 :

        redo = False
        if args.checkoutfile : redo = checkDoneCorrectly(f,args)
                
        if not redo and not args.force :
            print '\rProcessed {i} / {tot} files'.format(i=fi+1,tot=len(files)),
            continue

    ## Launch job locally

    if args.local : 
        
        trees = []
        for tname in args.tname.split(';') : 
            if friends is not None : tree = buildChain(tname,[f],[friends[fi]])
            else : tree = buildChain(tname,[f])
            trees.append(tree)

        loopTrees(trees,worker,cutter=cutter,args=args)

        print '\nProcessed {i} / {tot} files'.format(i=fi+1,tot=len(files))

    ## Launch job in batch

    else :

        pickle.dump(args,open(curdir+'/opts.pkl','w'))
        runf = open(curdir+'/run.sh','w')
        runf.write("python "+loc.PYTHON+"routines/process_single_file.py -f {filename} -w {worker} \"{wparams}\" -c {cutter} \"{cparams}\"  --opts {options}".format(
            filename = f, worker = args.worker[0], wparams = args.worker[1:] , cutter = args.cuts[0], cparams = args.cuts[1:], options = curdir+'/opts.pkl' ))
        runf.close()
        cmd = "bsub -R 'pool>30000' -o {dir}/out -e {dir}/err -q {queue} -J {jname} < {dir}/run.sh ".format(
            dir = curdir, jname = args.outtree+'_'+'_'.join(ids), queue = args.queue )
        print cmd
        os.system(cmd)

    



