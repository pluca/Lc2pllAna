import sys, pickle
from glob import glob
import importlib
from argparse import ArgumentParser
from ROOT import *
from routines.utils import loopTrees
from get_data import *

parser = ArgumentParser()
parser.add_argument("-f","--filename", default=None,type=str)
parser.add_argument("-w","--worker",   default=[None,None,None], nargs='*' , help='-w {worker} {ARGS}',type=str)
parser.add_argument("-c","--cuts",     default=[None,None], nargs='*', help='-c {cutter} {ARGS}',type=str)
parser.add_argument("--opts",          default=None,type=str)
opts = parser.parse_args()

worker, cutter, args = None, None, None

if opts.worker[0] is None and opts.worker[0] == 'None': 
    print "No worker set, no variables will be added"
else : 
    worker = importlib.import_module("routines."+opts.worker[0])
    worker.args = eval(opts.worker[1])

if opts.cuts[0] is not None and opts.cuts[0] != 'None':
    cutter = importlib.import_module("routines."+opts.cuts[0])
    cutter.args = eval(opts.cuts[1])

if len(glob(opts.opts)) > 0 :
    args = pickle.load(open(opts.opts))

## Find inputs
myfriends = []
if args.friends != "" : myfriends = args.friends.split(',')
friends = findFriendFiles([opts.filename], myfriends)

tnames = args.tname.split(';')
trees = []
for tname in tnames :
    tree = buildChain(tname,[opts.filename],friends,check=True)
    trees.append(tree)

loopTrees(trees,worker,cutter,args)




