from ROOT import *
TMVA.Tools.Instance()
class MVAnalysis :

    def __init__(self, name, variables, sigtree, bkgtree):

        self.fout = TFile(name+".root","RECREATE")
        self.factory = TMVA.Factory("TMVAClassification", self.fout,
                ":".join([
                    "!V",
                    "!Silent",
                    "Color",
                    "DrawProgressBar",
                    "Transformations=I;D;P;G,D",
                    "AnalysisType=Classification"]
                    ))
        self.dataLoader=TMVA.DataLoader('dataset')
        self.factory.Print()
        for v in variables :
            print "Adding ", v 
            self.dataLoader.AddVariable(v,"F")

        self.sigtree = sigtree
        self.bkgtree = bkgtree
        self.dataLoader.AddSignalTree(sigtree)
        self.dataLoader.AddBackgroundTree(bkgtree)

        self.sigcut = TCut("")
        self.bkgcut = TCut("")

    def set_cuts(self, sigcut, bkgcut) :

        self.sigcut = TCut(sigcut)
        self.bkgcut = TCut(bkgcut)

    def train(self) :

        self.dataLoader.PrepareTrainingAndTestTree(
                self.sigcut, self.bkgcut,
                ":".join([
                    "nTrain_Signal=0",
                    "nTrain_Background=0",
                    "SplitMode=Random",
                    "NormMode=NumEvents",
                    "!V"
                    ]))
        
        self.factory.TrainAllMethods()
        self.factory.TestAllMethods()
        self.factory.EvaluateAllMethods()
        
    def set_BookMethod(self,TMVAType,TMVAName,options):
         self.method = self.factory.BookMethod(self.dataLoader,TMVAType,TMVAName,options) 
