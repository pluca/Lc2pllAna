#!/afs/cern.ch/sw/lcg/releases/LCG_79/Python/2.7.9.p1/x86_64-slc6-gcc49-opt/bin/python
from python.MVA.setTMVA import MVAnalysis as MVA
from variables_DB import MVAvariables as vars
from variables_DB import MVAvariablesSL as varsSL
from variables_DB import MVAvariablesS28 as varsS28
from ROOT import * 
TMVA.Tools.Instance()
import os, sys, re, argparse
from routines.LcAnalysis import cuts, loc, dataids, parseDatafiles
from test.MVAparam import read_best_opts as readOptions
parser = argparse.ArgumentParser()
parser.add_argument("--mode", default="Prompt",type=str)
parser.add_argument("--optBDT",default=":".join(["!H","!V","NTrees=2000","MinNodeSize=0.01","MaxDepth=2","BoostType=AdaBoost","AdaBoostBeta=0.4","SeparationType=GiniIndex","nCuts=20","PruneMethod=NoPruning",]))
#parser.add_argument("--optBDTG",default=":".join(["!H","!V","NTrees=3000","MinNodeSize=0.05","MaxDepth=3","BoostType=Grad","AdaBoostBeta=0.5","SeparationType=GiniIndex","nCuts=20","PruneMethod=NoPruning",]))
parser.add_argument("--optBDTG",default="H:!V:NTrees=2000:MinNodeSize=0.05:MaxDepth=2:BoostType=Grad:Shrinkage=0.1:UseBaggedGrad:GradBaggingFraction=0.5:SeparationType=GiniIndex:nCuts=20:PruneMethod=CostComplexity:PruneStrength=50:NNodesMax=5")
parser.add_argument("--optMLP",default= "!H:!V:NeuronType=tanh:VarTransform=N:NCycles=1200:HiddenLayers=5:TestRate=5")
parser.add_argument("--BDT",action='store_true')
parser.add_argument("--BDTG",action='store_true')
parser.add_argument("--MLP",action='store_true')
parser.add_argument("--bestPromptOpt",action='store_true')
parser.add_argument("--bestSLOpt",action='store_true')
parser.add_argument("--bestS28Opt",action='store_true')
parser.add_argument("--bestPromptOptNoPID",action='store_true')
parser.add_argument("--readBestOpts",action='store_true')
parser.add_argument("--moveFiles",action='store_true')
opts = parser.parse_args()

if opts.mode == 'Prompt' :
    sigfile,mcTreeName = parseDatafiles('MCLc2pKpi_beforeBDT')
    bkgfile,dataTreeName = parseDatafiles('Prompt_reduced')
    base = loc.TUPLELOCATION
    vars+=(varsS28)
elif opts.mode=='SL' :
    sigfile,mcTreeName = parseDatafiles('MCSLLc2pKpi_beforeBDT')
    bkgfile, dataTreeName = parseDatafiles('SL_reduced')
    base = loc.TUPLELOCATION
    print "****************************SL*******************************************"
    vars=varsSL
print vars
#sigtree = TChain("DecayTree")
sigtree= TChain(mcTreeName)
sigtree.AddFile(base+sigfile)
sigtree  = sigtree.CopyTree(cuts.match_Lc2pKpi_Full.GetTitle())
bkgtree = TChain(dataTreeName)
bkgtree.AddFile(base+bkgfile)

##### Best optimised options ##############
if opts.bestPromptOpt:
    opts.optBDT=":".join([
                        "!H",
                        "!V",
                        "NTrees=2000",
                        "MinNodeSize=0.1",
                        "MaxDepth=2",
                        "BoostType=AdaBoost",
                        "AdaBoostBeta=0.1",
                        "SeparationType=GiniIndex",
                        "nCuts=20",
                        "PruneMethod=NoPruning",
                            ])
    opts.optMLP=":".join([
                         "!H",
                         "!V",
                         "NeuronType=tanh",
                         "VarTransform=N",
                         "NCycles=500",
                         "HiddenLayers=7",
                         "TestRate=10",
                        ])
    opts.optBDTG=":".join([
                    "!H",
                    "!V",
                    "NTrees=400",
                    "BoostType=Grad",
                    "MinNodeSize=0.05",
                    "MaxDepth=2",
                    "Shrinkage=0.3",
                    "SeparationType=GiniIndex",
                    "nCuts=20",
                    "UseBaggedBoost",
                    "GradBaggingFraction=0.5",
                    "PruneMethod=CostComplexity",
                    "PruneStrength=50",
                    "NegWeightTreatment=Pray"
                    ])
if opts.bestSLOpt:
    opts.optBDT=":".join([
                        "!H",
                        "!V",
                        "NTrees=2000",
                        "MinNodeSize=0.07",#6",
                        "MaxDepth=2",#5",
                        "BoostType=AdaBoost",
                        "AdaBoostBeta=0.3",#2",
                        "SeparationType=GiniIndex",
                        "nCuts=20",
                        "PruneMethod=NoPruning",
                            ])
    opts.optMLP=":".join([
                         "!H",
                         "!V",
                         "NeuronType=tanh",
                         "VarTransform=N",
                         "NCycles=800",
                         "HiddenLayers=3",
                         "TestRate=5",
                         ])
    opts.optBDTG=":".join([
                    "!H",
                    "!V",
                    "NTrees=400",
                    "BoostType=Grad",
                    "MinNodeSize=0.05",
                    "MaxDepth=2",
                    "Shrinkage=0.3",
                    "SeparationType=GiniIndex",
                    "nCuts=20",
                    "UseBaggedBoost",
                    "GradBaggingFraction=0.5",
                    "PruneMethod=CostComplexity",
                    "PruneStrength=50",
                    "NegWeightTreatment=Pray"
                    ])
if opts.bestS28Opt:
    opts.optBDT=":".join([
                        "!H",
                        "!V",
                        "NTrees=2000",
                        "MinNodeSize=0.07",
                        "MaxDepth=2",
                        "BoostType=AdaBoost",
                        "AdaBoostBeta=0.5",
                        "SeparationType=GiniIndex",
                        "nCuts=20",
                        "PruneMethod=NoPruning",
                            ])
    opts.optMLP=":".join([
                         "!H",
                         "!V",
                         "NeuronType=tanh",
                         "VarTransform=N",
                         "NCycles=500",
                         "HiddenLayers=6",
                         "TestRate=5",
                         ])
    opts.optBDTG=":".join([
                    "!H",
                    "!V",
                    "NTrees=600",##########
                    "BoostType=Grad",
                    "MinNodeSize=0.05", ##############
                    "MaxDepth=2", #############
                    "Shrinkage=0.3",
                    "SeparationType=GiniIndex",
                    "nCuts=20",
                    "UseBaggedBoost:GradBaggingFraction=0.5",
                    "PruneMethod=CostComplexity",
                    "PruneStrength=50",
                    ])
 
    
    

if opts.bestPromptOptNoPID:
    opts.optBDT=":".join([
                        "!H",
                        "!V",
                        "NTrees=2000",
                        "MinNodeSize=0.01",
                        "MaxDepth=2",
                        "BoostType=AdaBoost",
                        "AdaBoostBeta=0.4",
                        "SeparationType=GiniIndex",
                        "nCuts=20",
                        "PruneMethod=NoPruning",
                            ])
    opts.optMLP=":".join([
                         "!H",
                         "!V",
                         "NeuronType=tanh",
                         "VarTransform=N",
                         "NCycles=1200",
                         "HiddenLayers=5",
                         "TestRate=5",
                         ])
if opts.readBestOpts:
    opts.optBDT,opts.optBDTG,opts.optMLP = readOptions(opts.mode)
print "************** Trainig MVA *****************"
print "Signal from: ", sigfile
print "Cuts: ", cuts.matchNoComb.GetTitle()
print "Background from: ", bkgfile
print "Cuts: ", cuts.sideBand.GetTitle(), "\n\n"
sl=""
#cut=cuts.convergence2
if (opts.mode == 'Prompt'):
    signalCut = cuts.config_BDT
    sidebandCut= signalCut +cuts.sideBand
else :
    signalCut= cuts.config_BDT_SL
    sidebandCut= signalCut + cuts.sideBand
    sl="SL"
    
####Printing
print "Signal cut: ", signalCut.GetTitle()
print "SideBand cut: ", sidebandCut.GetTitle()


mva = MVA("LcMVA_"+opts.mode,vars,sigtree,bkgtree)
#mva.set_cuts(cuts.convergence+cuts.matchNoComb,cuts.convergence+cuts.sideBand)
#mva.set_cuts(cuts.convergence,cuts.convergence+cuts.sideBand)
#mva.set_cuts(cuts.convergence2+cuts.newStripping + cuts.oldStripping+cuts.p_PID, cuts.convergence2+cuts.newStripping+cuts.sideBand+cuts.oldStripping+cuts.p_PID)
mva.set_cuts(signalCut,sidebandCut)
if opts.BDT:
    print "Set BDT with options : " , opts.optBDT
    mva.set_BookMethod(TMVA.Types.kBDT,"BDT",opts.optBDT)
if opts.BDTG:
    print "Set BDTG with options : " , opts.optBDTG
    mva.set_BookMethod(TMVA.Types.kBDT,"BDTG",opts.optBDTG)
if opts.MLP:
    print "Set MLP with options : " , opts.optMLP
    mva.set_BookMethod(TMVA.Types.kMLP,"MLP",opts.optMLP)
mva.train()
if opts.moveFiles:
    os.system("mv dataset/weights/* "+loc.MVARES+opts.mode+"/weights/")
    os.system("mv "+ "LcMVA*.root "+loc.MVARES+opts.mode+"/")

