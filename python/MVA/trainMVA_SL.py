from setTMVA import MVAnalysis as MVA
from variables_DB import MVAvariables as vars
from ROOT import * 
import os, sys, re
from LcAnalysis import cuts, loc

base = loc.TUPLE
sigfile = base+"MC12_Lc_pKpi_reformat.root"
bkgfile = base+"Lc2pKpi_sW.root"

sigtree = TChain("DecayTree")
sigtree.AddFile(sigfile)
bkgtree = TChain("Lc2pKpi_sW")
bkgtree.AddFile(bkgfile)

print "************** Trainig MVA *****************"
print "Signal from: ", sigfile
print "Cuts: ", cuts.matchNoComb.GetTitle()
print "Background from: ", bkgfile
print "Cuts: ", cuts.sideBand.GetTitle(), "\n\n"

mva = MVA("LcMVA_SL",vars,sigtree,bkgtree)
#mva.set_cuts(cuts.convergence+cuts.matchNoComb,cuts.convergence+cuts.sideBand)
mva.set_cuts(cuts.convergence,cuts.convergence+cuts.sideBand)
#mva.set_cuts("",cuts.sideBand)
mva.train()

# Open GUI
# gROOT.ProcessLine( "TMVAGui(\"LcMVA.root\")" )
# gApplication.Run()


