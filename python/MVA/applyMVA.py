import os, sys, argparse
from ROOT import *
from array import array
from variables_DB import MVAvariables as vars
from routines.LcAnalysis import loc

parser = argparse.ArgumentParser()
parser.add_argument("-m","--method",default="BDT")
parser.add_argument("-w","--wfile",default=None)
parser.add_argument("-f","--files", nargs='+', required = True)
parser.add_argument("-t","--tree", required = True)
opts = parser.parse_args()

## Set all needed variables, change here if needed

wdir = loc.MVARES+'/weights/'

if opts.wfile is None :
	opts.wfile = wdir+'TMVAClassification_'+opts.method+'.weights.xml'

tree = TChain(opts.tree)
for f in opts.files :
	tree.AddFile(f)

vrefs = {}

##################### Setting the reader

reader = TMVA.Reader()
for v in vars :
	vrefs[v] = array('f', [0.])
	reader.AddVariable(v,vrefs[v])
reader.BookMVA("BDT",opts.wfile)

#################### Start Applying the MVA

for i in range(tree.GetEntries()) :

    tree.GetEntry(i)
    for v,ref in vrefs.iteritems() :
        formula = TTreeFormula(v,v,tree);
        ref[0] = formula.EvalInstance()

    mvaValRaw = reader.EvaluateMVA( opts.method )
    mvaVal = ( 0.5 + mvaValRaw )
    #mvaVal = reader.EvaluateMVA( opts.method )
    mvaErr = reader.GetMVAError()

    #print mvaVal, mvaErr

