from ROOT import *
from routines.LcAnalysis import *
import math
def compute_eff(NSig,NTot):
    eff=float(NSig)/NTot
    error_eff=0
    if float(NSig)>0 : error_eff = eff * math.sqrt(1./NSig + 1./NTot)
    return eff, error_eff
def compute_eff_err (Ns,Nt):
    eff = float(Ns[0])/Nt[0]
    err = eff * math.sqrt(float(Ns[1]*Ns[1])/(Ns[0]*Ns[0])+float(Nt[1]*Nt[1])/(Nt[0]*Nt[0]))
    return eff, err
def to_print_eff(eff,e):
    return "%.5f +- %.5f"%(eff,e)
def process_efficiency(EventTuple,TreeTuple,dtype,cuts):
    retrieve_files()
    total = TChain(EventTuple)
    new = TChain(TreeTuple)
    for f in datafiles[dtype] : total.AddFile(f)
    for g in datafiles[dtype] : new.AddFile(g)
    efficiency, error= compute_eff(new.GetEntries(cuts.GetTitle()),total.GetEntries())
    print "Lc2pmm "
    print "Cuts : " ,cuts.GetTitle()
    print "Efficiency : ", to_print_eff(efficiency,error)
def findNTot(dtype,cut=TCut(),tupleN="EventTuple/EventTuple"):
    retrieve_files()
    print tupleN
    total = TChain(tupleN)
    for f in datafiles[dtype] : total.AddFile(f)
    return total.GetEntries(cut.GetTitle())
def compute_tree(dtype,tupleN):
    retrieve_files()
    tree=TChain(tupleN)
    print tupleN
    for f in datafiles[dtype]: tree.AddFile(f)
    return tree





