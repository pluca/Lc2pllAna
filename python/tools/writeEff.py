import os
from routines.LcAnalysis import loc,db,full_eff

def write_to_file(effN, effR, Bf):
    with open(loc.CPP+'dat/fitter/RARE/Promptvariables.dat','a') as myfile:
        myfile.write(" %f %f %.10f"%(effN,effR,Bf))


write_to_file(db['FinalPIDCorr']['in'][0], full_eff['rare'][0], 2.98e-7) # 2.97 e -7 is the branching fraction of the Lc2pphi channel
