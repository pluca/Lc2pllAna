import os
from routines.LcAnalysis import loc, parseDatafiles, cuts, brIDS, fitResult, db, MISID, MISID_forTex, dumpAll
import tools.eventtuple_tools as et
import tools.plotting_tools as plt
from ROOT import *
from math import sqrt
##Parse phi numbr of events
NPhi = fitResult["PromptNSig"]
BrPhi = brIDS['MCLc2pphi']
effPhi = db['FinalPIDCorr']['in']
#print NPhi, BrPhi, effPhi
def Rerr(obs): #squared ratio of errors for error propagation computations
    return (obs[1]/obs[0])**2
def expectedNEvents(Nn,Bn,effn,Bexp,effexp):
    Nexp = Nn[0] * Bexp[0]/Bn[0] * effexp[0]/effn[0]
#    print "Error computation " ,Rerr(Nn),Rerr(Bn),Rerr(effn),Rerr(Bexp),Rerr(effexp)
#    print "Sqrt () ", sqrt(Rerr(Nn)+Rerr(Bn)+Rerr(effn)+Rerr(Bexp)+Rerr(effexp))
    err_exp = Nexp * sqrt(Rerr(Nn)+Rerr(Bn)+Rerr(effn)+Rerr(Bexp)+Rerr(effexp))
    return (Nexp,err_exp)
def writeInMISID(name,obj): #Write in MISID
    MISID[name] = obj
    MISID_forTex[name] = plt.scientific_notation(obj[0],obj[1])
def multiply(e1,e2):
    etot = e1[0]*e2[0]
    err = etot * sqrt(Rerr(e1)+Rerr(e2))
    return (etot,err)

modes = ['MCLc2ppipi','MCD2KKpi']


for m in modes:
    infile, intree = parseDatafiles(m+'_PIDAndBDT')
#    print loc.TUPLELOCATION+infile
    mcTree = TChain(intree)
    mcTree.AddFile(loc.TUPLELOCATION+infile)
    Nmc = mcTree.GetEntries()
    Ns = mcTree.GetEntries(cuts.config_bkg_study.GetTitle())
    if Ns == 0: 
        Ns=2.3
        print "Number of signal event after the selection was 0 -> set to %s at 90 percent CL"%Ns
    NTot = et.findNTot(m)
#    print  Ns, Nmc, NTot
    eff,err = et.compute_eff(Ns,NTot)    
    effExp = (eff,err) 
#    print "NPhi : %.10f+-%.10f BrPhi : %.10f+-%.10f effPhi: %.10f+-%.10f brPpipi: %.10f+-%.10f effPpipi : %.10f+-%.10f "%(NPhi[0],NPhi[1],BrPhi[0],BrPhi[1],effPhi[0],effPhi[1],brIDS[m][0],brIDS[m][1],effExp[0],effExp[1])
    Nexp = expectedNEvents(NPhi,BrPhi,effPhi,brIDS[m],effExp)
#    print "Number of expected events:", Nexp
#    print "Expected events :",Nexp[0]*MISID[m]['FullEff'][0]
#    print "MISID Proton eff", MISID["ProtonEff"]
    writeInMISID('Eff_'+m,effExp)
    writeInMISID('BR_'+m,brIDS[m])
    writeInMISID('Nexp_'+m,Nexp)
    Nfinal = multiply(Nexp,MISID[m]['FullEff'])
    print "Number of final events for the %s mode : %f +- %f"%(m,Nfinal[0],Nfinal[1])
    MISID_forTex["ExpectedLimit_"+m]=Nfinal[0]
    if m == 'MCD2KKpi' :MISID_forTex["ExpectedLimit_"+m]=plt.scientific_notation(Nfinal[0],Nfinal[1])
#dumpAll()


    

    

