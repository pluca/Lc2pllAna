from ROOT import *
from array import array
from routines.LcAnalysis import *
import re

gSystem.Load("libPhysics");
gStyle.SetOptStat(0)
rdm = TRandom3(0)

m = { 'Lc' : 2286.46, 'p' : 938, 'pi' : 139, 'mu' : 105.6, 'D+' : 1800, 'K' : 493 ,'pi0': 134.97, 'null':0}

def calcMass(decay,nevts,name = "", sigma = 8) :

    arr = decay.index('->')+2
    daughs = decay[decay.index('->')+2:].split()
    print daughs
    mom = decay[:decay.index('->')].strip()

    masses, new_masses = [], []
    for part in daughs :
        matches = re.findall('(.*?){(.*?)}',part)
        if len(matches)!= 1 :
            print "Something is wrong with your decay"
            sys.exit()
        
        masses     += [m[matches[0][0]]]
        new_masses += [m[matches[0][1]]]

    #print daughs, masses, new_masses
    event = TGenPhaseSpace()
    hM_misID = TH1D("histo_M_mis_"+name, "", 150, 1500, 2700)
    hM = TH1D("histo_M_"+name, "", 150, 1500, 2700)
    
    for n in range(int(nevts)) :

        print "Generating... {n} / {tot}\r".format(n=n,tot=nevts),

        momP = TLorentzVector(0.0, 0.0, 0.0, rdm.Gaus(m[mom],sigma))
        event.SetDecay(momP,len(daughs),array('d',masses))
        event.Generate();
    
        daus, corr_daus = [], []
        for i in range(len(daughs)) :
            cdau = event.GetDecay(i)
            daus.append(cdau)
            dau = TLorentzVector(cdau)
            dau.SetXYZM(cdau.Px(),cdau.Py(),cdau.Pz(),new_masses[i])
            corr_daus.append(dau)
            #print cdau.M(), dau.M()

        corr_mom = TLorentzVector()
        for dau in corr_daus : corr_mom += dau

        hM_misID.Fill(corr_mom.M())
        hM.Fill(momP.M())

    print
    hM.SetLineColor(4)
    hM.GetXaxis().SetTitle("m(3-body) MeV/#it{c}^{2}")
    hM.Scale(1./hM.Integral())
    hM_misID.Scale(1./hM_misID.Integral())

    leg = TLegend(0.17,0.7,0.55,0.9)
    leg.AddEntry(hM,"Signal","L")
    leg.AddEntry(hM_misID,"Mis-ID","L")

    c = TCanvas()
    hM.Draw("hist")
    hM_misID.Draw("hist same")
    leg.Draw()
    c.Print(loc.PLOTS+name+"_M.pdf")

    return hM_misID, hM 

h_Lc2ppipi, h = calcMass('Lc -> p{p} pi{mu} pi{mu}',1e4, "Lc2ppipi_pmumu")
h_Lc2pKpi, hx = calcMass('Lc -> p{p} K{mu} pi{mu}', 1e4, "Lc2pKpi_pmumu")
h_Lc2pKK, hx  = calcMass('Lc -> p{p} K{mu} K{mu}',  1e4, "Lc2pKK_pmumu")
h_D2KKpi, hx  = calcMass('D+ -> K{p} K{mu} pi{mu}', 1e4, "D2KKpi_pmumu")
h_D2Kpipi, hx = calcMass('D+ -> K{p} pi{mu} pi{mu}',1e4, "D2Kpipi_pmumu")
h_Lc2ppipipi0 , hx = calcMass('Lc -> p{p} pi{mu} pi{mu} pi0{null}',1e4 ,"Lc2ppipipi0_pmumu")
h_Lc2pKpipi0, hx = calcMass('Lc -> p{p} K{K} pi{pi} pi0{null}',1e4,"Lc2pKpipi0_pKpi")
h_Lc2pKpi_true, hx = calcMass('Lc -> p{p} K{K} pi{pi}',1e4,"Lc2pKpi_pKpi")
h_Lc2pKK_pKpi, hx = calcMass('Lc -> p{p} K{K} K{pi} ',1e4,"Lc2pKK_pKpi")
h_Lc2ppipi_pKpi, hx = calcMass('Lc -> p{p} pi{K} pi{pi} ',1e4,"Lc2ppipi_pKpi")
h_Lc2pKpipi0, hx = calcMass('Lc -> p{p} K{K} pi{pi} pi0{null}',1e4,"Lc2pKpipi0_pKpi")

leg = TLegend(0.15,0.6,0.5,0.9)
c = TCanvas("C1","C1",1200,600)
h.SetLineWidth(3)
h.SetLineColor(1)
leg.AddEntry(h,"#Lambda_{c}#rightarrow p#mu#mu","L")
h.Draw("hist")


h_Lc2ppipi.SetLineWidth(3)
h_Lc2pKpi.SetLineWidth(3)
h_Lc2pKK.SetLineWidth(3)
h_D2KKpi.SetLineWidth(3)
h_D2Kpipi.SetLineWidth(3)

h_Lc2ppipi.SetLineColor(6)
leg.AddEntry(h_Lc2ppipi,"#Lambda_{c}#rightarrow p#pi(#rightarrow#mu)#pi(#rightarrow#mu)","L")
h_Lc2ppipi.Draw("hist same")
h_Lc2pKpi.SetLineColor(2)
leg.AddEntry(h_Lc2pKpi,"#Lambda_{c}#rightarrow pK(#rightarrow#mu)#pi(#rightarrow#mu)","L")
h_Lc2pKpi.Draw("hist same")
h_Lc2pKK.SetLineColor(3)
leg.AddEntry(h_Lc2pKK,"#Lambda_{c}#rightarrow pK(#rightarrow#mu)K(#rightarrow#mu)","L")
h_Lc2pKK.Draw("hist same")
h_D2KKpi.SetLineColor(4)
leg.AddEntry(h_D2KKpi,"D#rightarrow K(#rightarrow p)K(#rightarrow#mu)K(#rightarrow#mu)","L")
leg.SetTextSize(0.06)
h_D2KKpi.Draw("hist same")
h_D2Kpipi.SetLineColor(7)
leg.AddEntry(h_D2Kpipi,"D#rightarrow K(#rightarrow p)K(#rightarrow#mu)#pi(#rightarrow#mu)","L")
h_D2Kpipi.Draw("hist same")

leg.Draw()
c.Print(loc.PLOTS+"BKG/Background_masses.pdf")




h_Lc2pKpi_true.SetLineWidth(3)
h_Lc2pKK_pKpi.SetLineWidth(3)
h_Lc2ppipi_pKpi.SetLineWidth(3)
h_Lc2pKpipi0.SetLineWidth(3)

h_Lc2pKpi_true.GetXaxis().SetTitle("m(pK#pi) MeV/#it{c}^{2}")
h_Lc2pKpi_true.Draw("hist")
h_Lc2pKpipi0.SetLineColor(kRed)
h_Lc2pKK_pKpi.SetLineColor(kGreen)
h_Lc2ppipi_pKpi.SetLineColor(kBlue)
leg2 = TLegend(0.15,0.6,0.4,0.9)
leg2.AddEntry(h_Lc2pKpi_true,"#Lambda_{c}#rightarrow pK#pi","L")
leg2.AddEntry(h_Lc2pKpipi0,"#Lambda_{c}#rightarrow p(#rightarrow p)K(#rightarrow K)#pi(#rightarrow#pi)#pi^{0}(#rightarrow 0)","L")
leg2.AddEntry(h_Lc2pKK_pKpi,"#Lambda_{c}#rightarrow p(#rightarrow p)K(#rightarrow K)K(#rightarrow#pi)","L")
leg2.AddEntry(h_Lc2ppipi_pKpi,"#Lambda_{c}#rightarrow p(#rightarrowp)#pi(#rightarrow K)#pi(#rightarrow#pi)","L")
leg2.SetTextSize(0.06)
h_Lc2pKpipi0.Draw("same hist")
h_Lc2pKK_pKpi.Draw("same hist")
h_Lc2ppipi_pKpi.Draw("same hist")
leg2.Draw()

c.Print(loc.PLOTS+"BKG/Background_massesPKPI.pdf")
