from ROOT import *
from routines.LcAnalysis import loc, cuts, parseDatafiles, db, full_eff

import os
tools = os.getenv('TOOLSSYS')+'/'
gROOT.ProcessLine('.L '+tools+'roofit/RooJohnson.cpp+')
gROOT.ProcessLine('.L '+tools+'roofit/RooIpatia.cpp+')

import python.tools.plotting_tools as plt

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--mode',default='Prompt')
args = parser.parse_args()

## Create a mode
Import = getattr(RooWorkspace, 'import')
fileName="ressource/%sRAREmodel_varNames"%args.mode
rootFile = TFile.Open(loc.CPP+fileName+'.root')
f=open(loc.CPP+fileName+'.dat','r')
print loc.CPP+fileName+".root",rootFile.IsOpen()
ws_name = f.readline()
ws_name=ws_name.replace("\n","")
print ws_name
print rootFile
w = RooWorkspace()
w=rootFile.Get(ws_name)
for line in f:
    line=line.replace("\n","")
    if 'model_' in line:
        model=line
        print model
    elif 'totsig_' in line:
        sig=line
        print sig
    elif 'Npmm' in line:
        nsig = line
        print nsig
    elif 'totbkg_' in line:
        bkg = line
        print bkg
    elif 'data_' in line:
        data=line
        print data
    elif 'nbkg_' in line:
        nbkg = line
        print nbkg
    elif 'm_sig' in line:
        m = line
        print m
    elif 's_sig' in line:
        s = line
        print s
    elif 'nu_sig' in line:
        nu = line
        print nu
    elif 'tau_sig' in line:
        tau = line
        print tau
    elif 'a_sig' in line:
        a = line
        print a
    elif 'b_sig' in line:
        b = line
        print b
    elif 'l_sig' in line:
        l = line
        print l
    elif 'n_sig' in line:
        n = line
        print n
    elif 'z_sig' in line:
        z = line
        print z
    elif line=='Lc_M':
        obs=line
    elif 'c0' in line:
        c0=line
        print c0
    elif 'B(#Lambda_{c}' in line:
        BF=line
        print BF
    elif line == 'Bphi':
        Bphi = line
    elif line == 'Emu':
        Emu = line
    elif line == 'Ephi':
        Ephi = line
    elif line == 'Npphi':
        Npphi = line
'''v = RooRealVar("x","x",100,0,200)
m = RooRealVar("m","m",100,95,105)
s = RooRealVar("s","s",10,0,30)
b = RooRealVar("b","b",0.0,-0.5,0)
nsig = RooRealVar("nsig","nsig",40,0,30)
nbkg = RooRealVar("nbkg","nbkg",100,0,10000)
exp = RooExponential("bkg","bkg",v,b)
sig = RooGaussian("sig","sig",v,m,s)
model = RooAddPdf("model","model",RooArgList(sig,exp),RooArgList(nsig,nbkg))
data = exp.generate(RooArgSet(v),10)
Import = getattr(RooWorkspace, 'import')
Import(w,model)
Import(w,data) '''
## Generate the Branching Ratio RooRealVar

maxn = 30

#nsig2 = w.obj(nsig)
w.var(BF).setMax(3e-7)
w.var(BF).setMin(0)
w.var(nbkg).setMax(1000)
w.var(nbkg).setMin(0)

#Bmumu = RooFormulaVar("Bmumu","Bmumu","@0*@1/@2*@4/@3",RooArgList(Bphi,Effphi,Effmumu,Nphi,nsig2))
#Import(w,Bmumu)
w.defineSet("poi",BF)
w.defineSet("obs",obs)


#data = w.obj(data)

w.var(m).setConstant(True)
w.var(s).setConstant(True)
w.var(a).setConstant(True)
w.var(b).setConstant(True)
w.var(l).setConstant(True)
w.var(n).setConstant(True)
w.var(z).setConstant(True)
w.var(Bphi).setConstant(True)
w.var(Ephi).setConstant(True)
w.var(Emu).setConstant(True)
w.var(Npphi).setConstant(True)
w.var(c0).setConstant(True)
w.var(nbkg).setConstant(False)
w.var(BF).setConstant(False)
print model
b_model = RooStats.ModelConfig("B_model", w)
b_model.SetPdf(w.pdf(model))
b_model.SetObservables(w.set("obs"))
b_model.SetParametersOfInterest(w.set("poi"))
w.var(BF).setVal(0.0)
w.var(BF).setConstant(True)
#w.var("nbkg").setConstant(True)
b_model.SetSnapshot(w.set("poi"))    # sets up b hypothesis as s = 0

# create the alternate (s+b) ModelConfig with given value of s
sb_model = RooStats.ModelConfig("S+B_model", w)
sb_model.SetPdf(w.pdf(model))
sb_model.SetObservables(w.set("obs"))
sb_model.SetParametersOfInterest(w.set("poi"))
poi = sb_model.GetParametersOfInterest().first()
w.var(BF).setVal(1e-8)
w.var(BF).setConstant(False)
sb_model.SetSnapshot(w.set("poi"))   # sets up sb hypothesis with given s






# test statistic \lambda(s) = -log L(s,\hat\hat{b})/L(\hat{s},\hat{b})
profll = RooStats.ProfileLikelihoodTestStat(sb_model.GetPdf())
calc=None
# Set up Hybrid Calculator b is alternate, sb is null
hc = RooStats.HybridCalculator(w.obj(data), b_model, sb_model)
toymcs = hc.GetTestStatSampler()
toymcs.SetNEventsPerToy(1)
hc.SetToys(100, 100)                # of toy exp for each hypothesis
toymcs.SetTestStatistic(profll)

'''# Get the result and compute CLs
print hc
result = hc.GetHypoTest()
result.SetPValueIsRightTail(True)
psb = result.NullPValue()
result.SetPValueIsRightTail(False)

print "results based on toy MC:"

pb = result.AlternatePValue()
print "psb = ", psb
print "pb  = ", pb
clb = 1. - pb
clsb = psb
cls = 9999.
if clb > 0 : cls = clsb/clb

print "cls = ", cls

# Make a plot
c1 = TCanvas()
c1.SetLogy()
result.SetPValueIsRightTail(True)
plot = RooStats.HypoTestPlot(result, 80)
plot.Draw('EXP')
c1.SaveAs("SimpleCLs.pdf")
'''
# Now compute using asymptotic formula sb is alt, b is null
ac = RooStats.AsymptoticCalculator(w.obj(data), b_model, sb_model)
ac.SetOneSided(True)     # KLUDGE -- should want one sided (True) for limit
#RooStats.AsymptoticCalculator.SetPrintLevel(-1)
asympResult = ac.GetHypoTest()

asympResult.SetPValueIsRightTail(True)         # appears to do nothing?!
asymp_pb = 1. - asympResult.AlternatePValue() # KLUDGE!!! Needs 1 -   
asympResult.SetPValueIsRightTail(True)
asymp_psb = asympResult.NullPValue()

print "Results based on asymptotic formulae:"
print "psb = ", asymp_psb
print "pb  = ", asymp_pb
asymp_clb = 1. - asymp_pb
asymp_clsb = asymp_psb
asymp_cls = 9999.
if asymp_clb > 0 : asymp_cls = asymp_clsb/asymp_clb

print "cls = ", asymp_cls

# create hypotest inverter passing the desired calculator (hc or ac)
calc = RooStats.HypoTestInverter(ac)
calc.SetVerbose(True)
calc.SetConfidenceLevel(0.95)
calc.UseCLs(True)
profll.SetOneSided(True)  

npoints = maxn  # number of points to scan
# min and max for scan (better to choose smaller intervals)
poimin = poi.getMin()
poimax = poi.getMax()
print "Doing a fixed scan  in interval : ", poimin, poimax
calc.SetFixedScan(npoints, poimin, poimax)
r = calc.GetInterval()

upperLimit = r.UpperLimit()
ulError = r.UpperLimitEstimatedError()
print "The computed upper limit is: ", upperLimit, " +/- ", ulError

# compute expected limit
print "Expected upper limits using b-only model : "
print "median limit = ", r.GetExpectedUpperLimit(0)
print "med. limit (-1 sig) ", r.GetExpectedUpperLimit(-1)
print "med. limit (+1 sig) ", r.GetExpectedUpperLimit(1)

# plot result of the scan 
plot2 = RooStats.HypoTestInverterPlot("HTI_Result_Plot", "CLs upper limit", r)
c2 = TCanvas("HypoTestInverter Scan") 
plot2.ls()
c2.SetLogy(False)
#plot2.Draw()
plot2.Draw("2CL")
## Draw only expected plot with correct axis
hist = plot2.MakeExpectedPlot()
## Set X label and Y Label
hist.Draw("same")
plt.hist_param(hist,"B(#Lambda_{c}^{+}#rightarrow p#mu^{+}#mu^{-}) [10^{-6}]","p-value")
## Draw vertical line at 0.95 CL
cl = 0.95
alpha = 1-cl
x1= hist.GetXaxis().GetXmin()
x2 = hist.GetXaxis().GetXmax()
t = TLine(x1,alpha,x2,alpha)
t.SetLineColor(kRed)
## TLegend
y1 = 0.6
y2 = y1+ 0.3
leg = TLegend(0.6,y1,0.9,y2)
number_of_graphs = hist.GetListOfGraphs().GetSize()
for i in range(0,number_of_graphs):
    obj = hist.GetListOfGraphs().At(i)
    opt = "F"
    if i==number_of_graphs-1: opt="L"
    leg.AddEntry(obj,"",opt)

hist.Draw()
t.Draw()
leg.Draw()
c2.SaveAs(loc.PLOTS+"RARE/%sCLs.pdf"%args.mode)

w.var(nbkg).Print()
