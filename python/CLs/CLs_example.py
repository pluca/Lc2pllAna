from ROOT import *
import sys, argparse

parser = argparse.ArgumentParser()
parser.add_argument("--ntot",type=int,default=100)
parser.add_argument("--percsig",type=float,default=0.)
parser.add_argument("--hybrid",action="store_true")
opts = parser.parse_args()

ntot = float(opts.ntot)
perc_bkg = opts.percsig
print "Generating ", ntot, "events of which", ntot*perc_bkg, "signal"

## Create a model

w = RooWorkspace()
v = RooRealVar("x","x",100,0,200)
m = RooRealVar("m","m",100,95,105)
s = RooRealVar("s","s",10,0,30)
b = RooRealVar("b","b",0.0,-0.5,0)

nsig = RooRealVar("nsig","nsig",ntot*perc_bkg,0.,1e4)
nbkg = RooRealVar("nbkg","nbkg",ntot*(1.-perc_bkg),0.,1e4)
exp = RooExponential("bkg","bkg",v,b)
sig = RooGaussian("sig","sig",v,m,s)
model = RooAddPdf("model","model",RooArgList(sig,exp),RooArgList(nsig,nbkg))
data = model.generate(RooArgSet(v),ntot)
Import = getattr(RooWorkspace, 'import')
Import(w,model)
Import(w,data)
w.defineSet("poi","nsig")
w.defineSet("obs","x")

c = TCanvas()
pl = w.var('x').frame()
data.plotOn(pl)
model.plotOn(pl)
exp.plotOn(pl,RooFit.LineColor(2))
pl.Draw()
c.Print("Data_And_Model.pdf")

w.var("m").setConstant(True)
w.var("s").setConstant(True)
w.var("b").setConstant(True)
w.var("nbkg").setConstant(False)
#w.var("nsig").setConstant(True)

######################################################

b_model = RooStats.ModelConfig("B_model", w)
b_model.SetPdf(w.pdf("model"))
b_model.SetObservables(w.set("obs"))
b_model.SetParametersOfInterest(w.set("poi"))
#b_model.SetNuisanceParameters(RooSetList(w.var("nbkg")))
w.var("nsig").setVal(0.0)
w.var("nsig").setConstant(True)
b_model.SetSnapshot(w.set("poi"))    # sets up b hypothesis as s = 0

# create the alternate (s+b) ModelConfig with given value of s
sb_model = RooStats.ModelConfig("S+B_model", w)
sb_model.SetPdf(w.pdf("model"))
sb_model.SetObservables(w.set("obs"))
sb_model.SetParametersOfInterest(w.set("poi"))
poi = sb_model.GetParametersOfInterest().first()
w.var("nsig").setVal(ntot/2.)
w.var("nsig").setConstant(False)
sb_model.SetSnapshot(w.set("poi"))   # sets up sb hypothesis with given s

#plc = RooStats.ProfileLikelihoodCalculator(data,sb_model)
#poi.setConstant(True)
#pois = RooArgSet(poi)
#nullParams = pois.snapshot()
#nullParams.setRealValue("nsig",0)
#plc.SetNullParameters(nullParams)
#htr = plc.GetHypoTest()
#htr.Print()

#sys.exit()

##############################

# test statistic \lambda(s) = -log L(s,\hat\hat{b})/ L(\hat{s},\hat{b})
#profll = RooStats.ProfileLikelihoodTestStat(sb_model.GetPdf())
profll = RooStats.SimpleLikelihoodRatioTestStat(sb_model.GetPdf(),b_model.GetPdf())

calc = None
if opts.hybrid :

    # Set up Hybrid Calculator 
    calc = RooStats.FrequentistCalculator(data, b_model, sb_model)
    toymcs = calc.GetTestStatSampler()
    toymcs.SetNEventsPerToy(1)
    calc.SetToys(3000, 3000)                # of toy exp for each hypothesis
    toymcs.SetTestStatistic(profll)

    # Get the result and compute CLs
    result = calc.GetHypoTest()
    result.SetPValueIsRightTail(True)
    psb = result.NullPValue()
    result.SetPValueIsRightTail(False)
    pb = result.AlternatePValue()

    print "results based on toy MC:"
    print "psb = ", psb
    print "pb  = ", pb
    clb = 1. - pb
    clsb = psb
    cls = 9999.
    if clb > 0 : cls = clsb/clb
    print "cls = ", cls

    # Make a plot
    c1 = TCanvas()
    #c1.SetLogy()
    result.SetPValueIsRightTail(True)
    plot = RooStats.HypoTestPlot(result, 80)
    plot.Draw()
    c1.SaveAs("SimpleCLs.pdf")

else :

    # Compute using asymptotic formula
    calc = RooStats.AsymptoticCalculator(data, b_model, sb_model)
    calc.SetOneSided(True)     
    #RooStats.AsymptoticCalculator.SetPrintLevel(-1)
    asympResult = calc.GetHypoTest()

    asympResult.SetPValueIsRightTail(False)         # appears to do nothing?!
    asymp_pb = 1. - asympResult.AlternatePValue() 
    asympResult.SetPValueIsRightTail(True)
    asymp_psb = asympResult.NullPValue()

    print "Results based on asymptotic formulae:"
    print "psb = ", asymp_psb
    print "pb  = ", asymp_pb
    asymp_clb = 1. - asymp_pb
    asymp_clsb = asymp_psb
    asymp_cls = 9999.
    if asymp_clb > 0 : asymp_cls = asymp_clsb/asymp_clb

    print "cls = ", asymp_cls


# create hypotest inverter passing the desired calculator (hc or ac)
hti = RooStats.HypoTestInverter(calc)
hti.SetVerbose(False)
hti.SetConfidenceLevel(0.95)
hti.UseCLs(True)
#profll.SetOneSided(True)

npoints = 30  # number of points to scan
poimin = 0
#poi.getMin()
poimax = 50
#poi.getMax()
print "Doing a fixed scan  in interval : ", poimin, poimax
hti.SetFixedScan(npoints, poimin, poimax)
r = hti.GetInterval()

upperLimit = r.UpperLimit()
ulError = r.UpperLimitEstimatedError()
print "The computed upper limit is: ", upperLimit, " +/- ", ulError

# compute expected limit
print "Expected upper limits using b-only model : "
print "median limit = ", r.GetExpectedUpperLimit(0)
print "med. limit (-1 sig) ", r.GetExpectedUpperLimit(-1)
print "med. limit (+1 sig) ", r.GetExpectedUpperLimit(1)

# plot result of the scan 
plot2 = RooStats.HypoTestInverterPlot("HTI_Result_Plot", "CLs upper limit", r)
c2 = TCanvas("HypoTestInverter Scan") 
c2.SetLogy(False)
plot2.Draw("2CL")
c2.SaveAs("SimpleCLsLimit.pdf")


