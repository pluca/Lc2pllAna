from ROOT import *

def hist_param(hist,xlabel, ylabel):
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)

def plot_2_hists(hist1,hist2,cName="Canvas1",legName1="MC",legName2="Data"):
    c=TCanvas(cName,cName,1600,600)
    leg1=TLegend(0.7,0.7,0.9,0.9)
    leg1.AddEntry(hist1,legName1)
    leg2=TLegend(0.7,0.7,0.9,0.9)
    leg2.AddEntry(hist2,legName2)
    c.Divide(2)
    c.cd(1)
    hist1.Draw()
    leg1.Draw("same")
    c.cd(2)
    hist2.Draw()
    leg2.Draw("same")
    return c,leg1,leg2
def scale_hist(hist):
    hist.Scale(1./hist.Integral())
def draw_graph(canvas,graph,opts,xmin=0,xmax=1,ymin=0,ymax=1):
    canvas.cd()
    graph.Draw(opts)
    graph.GetXaxis().SetLimits(xmin,xmax)
    graph.GetYaxis().SetRangeUser(ymin,ymax)
    graph.Draw(opts)
    canvas.Update()
def compute_hist(tree,var,binning,name,cut=TCut() ,xlabel="", ylabel=""):
    if ylabel=="":
        ylabel="Events / %.2f MeV/c"%(float(binning[2]-binning[1])/binning[0])
    tree.Draw(var+">>"+name+"(%d,%d,%d)"%binning,cut)
    hist=gPad.GetPrimitive(name)
    hist_param(hist,xlabel,ylabel)
    hist.Scale(1/hist.Integral())
    hist.SetMaximum(.3)
    return hist

