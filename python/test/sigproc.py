import os
from multiprocessing import Pool
from functools import partial
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
from routines.LcAnalysis import loc,parseDatafiles

home=os.path.expanduser("~")

#Return name for path folder
def Prompt_SL(s):
    name=""
    if s=="-PROMPT":
        name="Prompt"
    else:
        name="SL"
    return name
#Compute option, run once with cut >0. if needed, returns option
def compute_option(s):
    name=Prompt_SL(s)
    o=1
    os.chdir(loc.REPO+"cpp/")
    if os.path.isfile("ressource/%sMCparsVariables.root"%(name))==False:
        os.system("./fitter.out -model Johnson - -OPT 0 %s"%(s))        
        o=1
    return o
# Call the cpp executable with o=option, s=sl, c=cut(array)
def process_exec(o,s,c):
    name=Prompt_SL(s)
    filename=loc.REPO+"cpp/dat/%s/P_PID/%s_lowerLimit_%.6f_variables.dat"%(name,name,c)
    if os.path.isfile(filename)==False:
        os.chdir(loc.REPO+"cpp/")
        cpp_exec="./p_ProbNNp_cut_optimisation.out -OPT %d %s -CUT %.6f"%(o,s,c)
        os.system(cpp_exec)
#Multipriocess the dat file generation with cpp exec
def multi_process_exec(p,o,s,c):
    pool=Pool()
    func=partial(p,o,s)
    pool.map(func,c)
    pool.close()
    pool.join()
#Read files, compute numpy arrays and return NCut, NSig, NBkg
def compute_numpy_arrays(s,path=""):
    NSig=[]
    NBkg=[]
    NCut=[]
    name=Prompt_SL(s)
    if path=="":
        path=loc.REPO+"cpp/dat/%s/P_PID/*.dat"%(name)
    else :
        path+="*"
    files=glob(path)
    for f in files:
        listf=open(f).read()   
        splitted=listf.split(' ')
        NCut.append(float(splitted[0]))
        NSig.append(float(splitted[1]))
        NBkg.append(float(splitted[2]))
    return np.array(NCut),np.array(NSig),np.array(NBkg)
#Compute significance, return Sig_np
def compute_significance(NSig,NBkg):
    NTot_sqrt=np.sqrt(np.add(NSig,NBkg))
    Sig=NSig/NTot_sqrt
    error=Sig*np.sqrt(1./NSig+1./(16*(NSig+NBkg)))
    return Sig,error
#Compute significance, plot NSig/NBkg 
def plot_significance(NCut,NSig,NBkg):
    Sig,err=compute_significance(NSig,NBkg)
    #NSig/NBkg
    plt.figure()
    plt.subplot(211)
    plt.plot(NCut,NSig,'ro',label="NSig")
    plt.plot(NCut,NBkg,'bo',label="NBkg")
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    plt.xlabel("prob_MC15TuneV1_probNNp >")
    plt.ylabel("Number of events")
    #Significance
    plt.subplot(212)
    plt.plot(NCut,Sig,'ro')
    plt.xlabel("prob_MC15TuneV1_probNNp >")
    plt.ylabel("Significance")
    #Show
    plt.show()
    return Sig
#Compute root histograms and display 
from ROOT import *
import plotting_tools as plt2
def compute_hist(s,path="",cutName="p ProbNN p",m="PID"):
    NCut,NSig,NBkg=compute_numpy_arrays_s28(s,path,m)
    Significance,err=compute_significance(NSig,NBkg)
    yBinning=float(max(np.amax(NSig),np.amax(NBkg))*1.1)
    histNSig=TH2F("NSig","NSig",200,0,1,200,0,yBinning)
    histNBkg=TH2F("NBkg","NBkg",200,0,1,200,0,yBinning)
    histSignificance=TH2F("Significance","Significance",200,0,1,200,0,float(np.amax(Significance)*1.1))
    for i in range(0,len(NCut)):
        histNSig.Fill(NCut[i],NSig[i])
        histNBkg.Fill(NCut[i],NBkg[i])
        histSignificance.Fill(NCut[i],Significance[i])
    plt2.hist_param(histNSig,cutName,"Events")
    plt2.hist_param(histNBkg,cutName,"Events")
    plt2.hist_param(histSignificance,cutName, "Significance")
    return histNSig, histNBkg, histSignificance, Significance,err,NCut
def draw_NSig_NBkg(histS, histB):
    c=TCanvas("NSig/NBkg","NSig/NBkg")
    histS.SetMarkerColor(kRed)
    histB.SetMarkerColor(kBlue)
    histS.Draw()
    histB.Draw("same")
    t=TLegend(0.8,0.8,0.9,0.9)
    t.AddEntry(histS,"NSig")
    t.AddEntry(histB,"NBkg")
    t.Draw("same")
    return c,t
def draw_Significance(hist):
    c=TCanvas("Signficiance","Significance")
    hist.Draw()
    return c
def move_file_toReport(fileN):
    os.system("cp %s %s"%(fileN,home+"/report/p_PID/"+fileN))
    print fileN, "moved to ~/report/p_PID/"
######################## S28 ################################
def Prompt_SL_s28(s):
    if s=="Prompt":
        infile,intree = parseDatafiles('Lc2pKpi_S28_100k')
        mcfile,mctree = parseDatafiles('MCLc2pKpi_S28')
        tuplePath = loc.MARKOTUPLE
    else:
        infile,intree = parseDatafiles('SLLc2pKpi_S28')
        mcfile,mctree = parseDatafiles('MCSLLc2pKpi_S28')
        tuplePath = loc.TUPLE
    return infile,intree,mcfile,mctree,tuplePath
def process_exec_s28(s,c):
    filename=loc.REPO+"cpp/dat/fitter/PID/%svariables%f.dat"%(s,c)
    if os.path.isfile(filename)==False:
        os.chdir(loc.REPO+"cpp/")
        infile,intree,mcfile,mctree,tuplePath=Prompt_SL_s28(s)
        cpp_exec="./fitter.out  -model Johnson -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -var Lc_M 2287 2250 2330 -fitPdfName Lc2pKpi -labelName '#Lambda_{c}^{+}#rightarrow pK#pi' -id %f -option PID %f -matchLc2pKpi -mode %s "%(infile,intree,mcfile,mctree,tuplePath,c,c,s)
        print cpp_exec
        os.system(cpp_exec)
def compute_numpy_arrays_s28(s,path="",m="PID"):
    NSig=[]
    NBkg=[]
    NCut=[]
    path=loc.REPO+"cpp/dat/fitter/%s/%s*.dat"%(m,s)
    files=glob(path)
    for f in files:
        listf=open(f).read()   
        splitted=listf.split(' ')
        NCut.append(float(splitted[0]))
        NSig.append(float(splitted[1]))
        NBkg.append(float(splitted[2]))
    return np.array(NCut),np.array(NSig),np.array(NBkg)
def multi_process_exec_s28(p,s,c):
    pool=Pool()
    func=partial(p,s)
    pool.map(func,c)
    pool.close()
    pool.join()




