from ROOT import *
import plotting_tools as plt
import os,sys
from glob import glob
import numpy as np
from routines.LcAnalysis import loc , parseDatafiles
from multiprocessing import Pool
from functools import partial

cpp_exec_path=loc.REPO+"/cpp/"
def compute_efficiency(mcTree,BDT_cut, stripping,Ntot,sl,mode=""):
    res=TH2F("Eff"+mode,"Eff",1000,-1,1,1000,0,1.1)
    eff=[]
    if sl=='Prompt':
        mvaVar="BDTout"
        mva="BDT"
    else:
        mvaVar="BDTout"
        mva="BDT"
    for c in BDT_cut:
        b_cutted="%s > %.5f"%(mvaVar,c)
        b_cut=TCut(b_cutted)+stripping
        mcTree.Draw("Lc_MM>>hist",b_cut)
        e=hist.GetEntries()/Ntot
        eff.append(e)
        res.Fill(c,e)
    plt.hist_param(res,"%s cut (>)"%mva,"MC #epsilon efficiency")
    return res,eff
import sigproc as sp
def process_bdt_exec(sl,cut):
    os.chdir(loc.REPO+"/cpp/")
    name=sp.Prompt_SL(sl)
    filePath=loc.REPO+"/cpp/dat/"+name+"/FoM/Lc2pmmBkgFit_BDTcut_%.6f.dat"%cut
    if os.path.isfile(filePath)==False:
        cpp_exec=cpp_exec_path+"./fitLc2pmm.out %s -BDT %.6f "%(sl,cut)
        print cpp_exec
        os.system(cpp_exec)
def multi_process_exec(p,s,c):
    pool=Pool()
    func=partial(p,s)
    pool.map(func,c)
    pool.close()  
    pool.join()
def compute_NBkg(s,BDT_cut):
    name=sp.Prompt_SL(s)
    path=loc.REPO+"/cpp/dat/"+name+"/FoM/"
    NBkg=[]
    for b in BDT_cut:
        f=path+"Lc2pmmBkgFit_BDTcut_%.6f.dat"%b
        NBkg.append(float(open(f).read()))
    return np.array(NBkg)
def compute_FoM(Eff,NBkg):
    FoM=Eff/(float(5./2)+np.sqrt(NBkg))
    return FoM
import math
def fill_hist(FoM,NBkg,BDT_cut,sl='Prompt',mode=""):
    hist=TH2F("FoM"+mode,"FoM",1000,-1,1,1000,0,1.5)
    hist_NBkg=TH2F("NBkg"+mode,"NBkg",1000,-1,1,1000,0,22000)
    hist_den=TH2F("Den"+mode,"#frac{1}{5/2 + #sqrt{NBkg}}",1000,-1,1,1000,0,1)
    for i in range(0,len(BDT_cut)):
        hist.Fill(BDT_cut[i],FoM[i])
        hist_NBkg.Fill(BDT_cut[i],NBkg[i])
        hist_den.Fill(BDT_cut[i],1/(float(5./2.)+math.sqrt(NBkg[i])))
    if sl=='Prompt': mva='BDT'
    else : mva='BDT'
    plt.hist_param(hist,"%s cut (>)"%mva,"Figure of Merit (normalized)")
    plt.hist_param(hist_NBkg,"%s cut (>)"%mva,"NBkg events")
    plt.hist_param(hist_den,"%s cut (>)"%mva," Denominator")
    return hist,hist_NBkg,hist_den

# S28 refactored code
def process_bdt_exec_S28(sl,cut):
    os.chdir(loc.REPO+"cpp/")
    if sl=="Prompt":
        infile, intree = parseDatafiles('Lc2pmm_S28_PIDAndBDT')
        mvaVar="BDT"
    else :
        infile, intree = parseDatafiles('SLLc2pmm_S28_PIDAndBDT')
        mvaVar="BDT"
    filePath=loc.REPO+"/cpp/dat/fitter/SIDEBAND/%svariables%.6f.dat"%(sl,cut)
    if os.path.isfile(filePath)==False:
        cpp_exec=cpp_exec_path+"./fitter.out -model Cheb1 -infile %s -intree %s -tuplePath %s -fitPdfName Lc2pmm -labelName '#Lambda_c^+ #rightarrow p#mu#mu'  -option SIDEBAND 2260 2320 -mvaVar %s %.6f -mode %s -var Lc_M -binning 2285 2200 2360 "%(infile,intree,loc.MARKOTUPLE,mvaVar,cut,sl)
        print cpp_exec
        os.system(cpp_exec)
def compute_NBkg_S28(s,BDT_cut):
    path=loc.REPO+"cpp/dat/fitter/SIDEBAND/"
    NBkg=[]
    for b in BDT_cut:
        f=path+"%svariables%.6f.dat"%(s,b)
        line = open(f).read().split(' ')
        NBkg.append(float(line[0]))
    return np.array(NBkg)

def fill_hist_S28(FoM,NBkg,BDT_cut,sl='Prompt',mode=""):
    hist=TH2F("FoM"+mode,"FoM",1000,-1,1,1000,0,1.5)
    hist_NBkg=TH2F("NBkg"+mode,"NBkg",1000,-1,1,1000,0,22000)
    hist_den=TH2F("Den"+mode,"#frac{1}{5/2 + #sqrt{NBkg}}",1000,-1,1,1000,0,1)
    hist=TGraph(len(BDT_cut),BDT_cut,FoM)
    hist_NBkg=TGraph(len(BDT_cut),BDT_cut,NBkg)
    den = 1./(5./2.+np.sqrt(NBkg))
    hist_den=TGraph(len(BDT_cut),BDT_cut,den)
    if sl=='Prompt': mva='BDT'
    else : mva='BDT'
    plt.hist_param(hist,"%s cut (>)"%mva,"Figure of Merit (normalized)")
    plt.hist_param(hist_NBkg,"%s cut (>)"%mva,"NBkg events")
    plt.hist_param(hist_den,"%s cut (>)"%mva," Denominator")
    return hist,hist_NBkg,hist_den
def compute_efficiency_S28(mcTree,BDT_cut, stripping,Ntot,sl,mode=""):
    res=TH2F("Eff"+mode,"Eff",1000,-1,1,1000,0,1.1)
    eff=[]
    if sl=='Prompt':
        mvaVar="BDTout"
        mva="BDT"
    else:
        mvaVar="BDTout"
        mva="BDT"
    for c in BDT_cut:
        b_cutted="%s > %.5f"%(mvaVar,c)
        b_cut=TCut(b_cutted)+stripping
        mcTree.Draw("Lc_MM>>hist",b_cut)
        e=hist.GetEntries()/Ntot
        eff.append(e)
        res.Fill(c,e)
    res=TGraph(len(BDT_cut),BDT_cut,np.array(eff))
    plt.hist_param(res,"%s cut (>)"%mva,"MC #epsilon efficiency")
    return res,eff






