import trigger_tools as tt
from routines.LcAnalysis import cuts,parseDatafiles, loc
import os
save_path=os.path.expanduser("~")+"/report/TRIGGERS/"
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="Prompt")
args=parser.parse_args()
if args.mode=="Prompt":
    oldLc2pmm,tupleLc2pmm=parseDatafiles('MCLc2pmm_S28_PIDAndBDT')
    oldLc2pKpi,tupleLc2pKpi=parseDatafiles('MCLc2pKpi_S28_PIDAndBDT')
    tupleLocation=loc.MARKOTUPLE
    fileLc2pmm=tt.tupleLocation+"MC16_Lc2pmm_forTriggers.root"
    fileLc2pKpi=tt.tupleLocation+"MC16_Lc2pKpi_forTriggers.root"
    fullCut= cuts.config_forTriggers
else:
    fileLc2pmm=tt.tupleLocation+"MC12_SLLc2pmm_CutStripPIDAndBDT.root"
    fileLc2pKpi=tt.tupleLocation+"MC12_SLLc2pKpi_CutStripPIDAndBDT.root"
    tupleLc2pmm="Lc2pmmTuple_SL"
    tupleLc2pKpi="Lc2pKpiTuple_SL"
    oldLc2pmm="MC12_SLLc2pmm_CutStripPID.root"
    oldLc2pKpi="MC12_SLLc2pKpi_CutStripPID.root"
    fullCut=cuts.config_forTriggers_SL

#********* Load Trees for trigger analysis *****************
pKpiTree=tt.load_tree(fileLc2pKpi,tupleLc2pKpi,oldLc2pKpi,fullCut + cuts.match_Lc2pKpi_Full)
pmmTree=tt.load_tree(fileLc2pmm,tupleLc2pmm,oldLc2pmm,fullCut + cuts.match_Lc2pmm_Full)

Ntot_Lc2pKpi=pKpiTree.GetEntries()
Ntot_Lc2pmm=pmmTree.GetEntries()

#********* L0 generation for pKpi and pmm ******************
if args.mode=="Prompt":
    pKpiLcL0=tt.fill_LcL0(pKpiTree,Ntot_Lc2pKpi,"pKpi")
    pmmLcL0=tt.fill_LcL0(pmmTree,Ntot_Lc2pmm,"pmm")
else:
    pKpiLcL0=tt.fill_SLLcL0(pKpiTree,Ntot_Lc2pKpi,"pKpi")
    pmmLcL0=tt.fill_SLLcL0(pmmTree,Ntot_Lc2pmm,"pmm")
c,t1,t2=tt.draw_trigger(pKpiLcL0,"#Lambda_{c}#rightarrow pK^{-}#pi^{+}",pmmLcL0,"#Lambda_{c}#rightarrow p #mu^{+}#mu^{-}","c1")
c.Print(save_path+args.mode+"L0_Analysis.pdf")
#********* Hlt1 *****************
pKpiLcHlt1=tt.fill_hlt1(pKpiTree,Ntot_Lc2pKpi,"pKpiHlt1")
pmmLcHlt1=tt.fill_hlt1(pmmTree,Ntot_Lc2pmm,"pmmLcHlt1")
c2,t21,t22=tt.draw_trigger(pKpiLcHlt1,"#Lambda_{c}#rightarrow pK^{-}#pi^{+}",pmmLcHlt1,"#Lambda_{c}#rightarrow p#mu^{+}#mu^{-}","c2")
c2.Print(save_path+args.mode+"Hlt1_Analysis.pdf")
#******** Hlt2 ******************
pKpiLcHlt2=tt.fill_hlt2(pKpiTree,Ntot_Lc2pKpi,"pKpiHlt2",False)
pmmLcHlt2=tt.fill_hlt2(pmmTree,Ntot_Lc2pmm,"pmmHlt2",True)
c3,t31,t32=tt.draw_trigger(pKpiLcHlt2,"#Lambda_{c}#rightarrow pK^{-}#pi^{+}",pmmLcHlt2,"#Lambda_{c}#rightarrow p#mu^{+}#mu^{-}","c3")




