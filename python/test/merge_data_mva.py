##########################
####### Not working ######


from routines.LcAnalysis import loc, cuts
from ROOT import *
#Path to files to be merged + path to save it
path_jobs=loc.JOBS
path_mva=loc.FRIENDS+"mva/"
save_path=loc.MARKOTUPLE
#Names
mvaName="mva.root"
tupleName="DVNtuple.root"
path_test="374/0/"

#Load files to be merged
oldName=path_jobs+path_test+tupleName
oldTree=TChain("Lc2pmmTuple/DecayTree")
oldTree.AddFile(oldName)
mvaName=path_mva+path_test+mvaName
mvaTree=TChain("Lc2pmmTuple_mva")
mvaTree.AddFile(mvaName)

print oldTree.GetEntries()
print mvaTree.GetEntries()

#Cut
cut=cuts.convergence+cuts.oldStripping+cuts.newStripping+cuts.p_PID+cuts.bdt
#New file
newFile=TFile(save_path+"tmp.root","recreate")
newTree=TChain("Lc2pmm")
newTree=oldTree.CloneTree(0)

bdtout=0
newBranch=newTree.Branch("BDTout",bdtout)
events=mvaTree.GetEntries()
print events
for i in range(events):
    print i
    newTree.GetEntry(i)
    bdtout=mvaTree.GetBranch("BDTout").GetEntry(i)
    newBranch.Fill()

newFile.Write()
newFile.Close()

