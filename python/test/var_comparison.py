from routines.LcAnalysis import loc,parseDatafiles,cuts
from ROOT import *
import os
from variables_DB import vardb,vardbS28,vardbSL

########## Parser ####################
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="PromptS28")
parser.add_argument("--fitRaw",action='store_true')
parser.add_argument("--draw",action='store_true')
parser.add_argument("--addVarDB",action='store_true')
args=parser.parse_args()

########### Initialize ###############

if args.mode == "Prompt":
    dataFileName, dataTreeName = parseDatafiles('Lc2pKpi_S28_100k')
    mcFileName, mcTreeName = parseDatafiles('MCLc2pKpi_S28')
    tupleLoc = loc.MARKOTUPLE
    varDB=vardbS28
    if args.addVarDB: varDB.update(vardb)
elif args.mode == "SL":
    dataFileName, dataTreeName = parseDatafiles('SLLc2pKpi_S28')
    mcFileName, mcTreeName = parseDatafiles('MCSLLc2pKpi_S28')
    tupleLoc = loc.TUPLE
    varDB=vardbSL
    if args.addVarDB: varDB.update(vardb)
if args.fitRaw:
    os.chdir(loc.CPP)
    cpp_exec="./fitter.out -model Johnson -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -var Lc_M 2287 2250 2330 -fitPdfName Lc2pKpi -labelName '#Lambda_{c}^{+}#rightarrow pK#pi' -id '' -option RAW -matchLc2pKpi -mode %s"%(dataFileName,dataTreeName,mcFileName,mcTreeName,tupleLoc,args.mode)
    print cpp_exec
    os.system(cpp_exec)

######### Retrieve S and B ratios, as well as lower and higher limit
ressource = loc.CPPFITTER+'RAW/'
listF=open(ressource+args.mode+'variables.dat').read().split(' ')
S=float(listF[0])
B=float(listF[1])
lower_limit = float(listF[2])
higher_limit = float(listF[3])
RBkg=B/(S+B)
print lower_limit,higher_limit, RBkg
########## trees ################
DataTree = TChain(dataTreeName)
DataTree.AddFile(tupleLoc+dataFileName)

MCTree = TChain(mcTreeName)
MCTree.AddFile(tupleLoc+mcFileName)

######### cuts #################
mc = cuts.match_Lc2pKpi_Full
if args.mode == "Prompt":
    signal = "(Lc_DTF_M > %.5f && Lc_DTF_M < %.5f)"%(lower_limit,higher_limit)
    side = "(Lc_DTF_M < 2260 || Lc_DTF_M > 2320)"
elif args.mode=="SL" :
    signal = "(Lc_M > %.5f && Lc_M < %.5f)"%(lower_limit,higher_limit)
    side = "(Lc_M < 2260 || Lc_M > 2320)"

######### draw #################
if args.draw:
    c = TCanvas()
    for vname,v in varDB.iteritems() :

        if 'Lc_DTF_PV_M' in v['formula']: continue
        if 'Lc_DTF_chi2_0'in v['formula']: continue
        expr = "{formula}>>[hname]({nbins},{xmin},{xmax})".format(**v).replace('[hname]','{hname}')
        
        data_expr = expr.format(hname="hdata")
        mc_expr = expr.format(hname="hmc")
        side_expr = expr.format(hname="hside")
        print v['formula']
        DataTree.Draw(data_expr,signal,"E")
        hData = gPad.GetPrimitive("hdata")
        DataTree.Draw(side_expr,side,"E")
        hSide = gPad.GetPrimitive("hside")
        print "Before ", hData.GetEntries(), hSide.GetEntries()
        MCTree.Draw(mc_expr,mc,"E")
        hMC = gPad.GetPrimitive("hmc")
        hSide2=hSide
        hSide2.Scale(B/hSide.Integral())
        hData = hData - hSide2
        for b in range(hData.GetNbinsX()):
            if hData.GetBinContent(b) < 0: hData.SetBinContent(b,0)            
        print "After ", hData.GetEntries(), hSide2.GetEntries(), RBkg

        #hData.SetMarkerColor(3)
        #hData.SetMarkerSize(1)
        #hData.SetMarkerStyle(22)
        hData.SetFillStyle(3004)
        hData.SetFillColor(4)
        hData.GetXaxis().SetTitle(vname)
        hData.Scale(1./hData.Integral())
        hMC.SetMarkerColor(2)
        hMC.SetMarkerSize(1)
        hMC.SetMarkerStyle(21)
        hMC.GetXaxis().SetTitle(vname)
        hMC.Scale(1./hMC.Integral())
        #hSide.SetMarkerColor(1)
        #hSide.SetMarkerSize(1)
        #hSide.SetMarkerStyle(20)
        hSide.SetFillColor(1)
        hSide.SetFillStyle(3005)
        hSide.Scale(1./hSide.Integral())
        hSide.GetXaxis().SetTitle(vname)

        if hSide.GetMaximum() > hData.GetMaximum() and hSide.GetMaximum() > hMC.GetMaximum() :
            hSide.Draw("hist")
            hData.Draw("hist same")
            hMC.Draw("same")
        elif hMC.GetMaximum() > hData.GetMaximum() and hMC.GetMaximum() > hSide.GetMaximum() :
            hMC.Draw()
            hSide.Draw("hist same")
            hData.Draw("hist same")
        else :
            hData.Draw("hist")
            hSide.Draw("hist same")
            hMC.Draw(" same")
        leg = TLegend(0.70,0.75,0.99,0.99)
        leg.AddEntry(hData,"Data signal","F")
        leg.AddEntry(hSide,"Data sideband","F")
        leg.AddEntry(hMC,"MC #Lambda_{c}#rightarrow pK#pi","P")
        leg.Draw()
        if 'nick' not in v : v['nick'] = v['formula']
        c.Print(loc.REPORTRAW+"/"+args.mode+"/"+v['nick']+'.pdf')



