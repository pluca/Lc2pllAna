import os,sys,subprocess
import re
from routines.LcAnalysis import loc


home=os.path.expanduser("~")
#path=loc.MARKOJOBS
path=loc.MARKOEOSJOBS
def make_output(cmd,path):
    out=subprocess.check_output(cmd,shell=True)
    f=open(path+'out','w')
    f.write(out)
    f.close()

def gen_optBDT(MaxDepth,MinNodeSize,AdaBoostBeta):
    option=":".join([
                    "!H",
                    "!V",
                    "NTrees=2000",
                    "MinNodeSize=%.3f"%(MinNodeSize),
                    "MaxDepth=%d"%(MaxDepth),
                    "BoostType=AdaBoost",
                    "AdaBoostBeta=%.2f"%(AdaBoostBeta),
                    "SeparationType=GiniIndex",
                    "nCuts=20",
                    "PruneMethod=NoPruning",
                    ])
    return option

def gen_trainMVA(option,mode,ml,optName):
    #trainMVA=home+"/Lc2pllAna/python/MVA/trainMVA.py --mode %s %s %s '%s'"%(mode,ml,optName,option)
    trainMVA="python "+home+"/Lc2pllAna/python/MVA/trainMVA.py --mode %s %s %s '%s'"%(mode,ml,optName,option)
    return trainMVA
def gen_directoryName(MaxDepth,MinNodeSize,AdaBoostBeta):
    directoryName="MD_%d_MNS_%.3f_ABB_%.2f"%(MaxDepth,MinNodeSize,AdaBoostBeta)
    return directoryName

def gen_outPath(directoryName,sl):
    out_path=path+"%s/optimisationMVA/"%(sl)+directoryName+"/out"
    return out_path
def notOvertrained(f,thresh):
    test2=re.findall('\d\.\d+ \(\d\.\d+\)',f)
    boolean=[False,False,False]
    test=test2
    if len(test2)>0:
        for i in range(0,len(test2)):
            test[i]=re.sub('[()]', '', test2[i])
            value=test[i].split(' ')
            if (float(value[0])>0 and float(value[1])>0):
                boolean[i]=(abs(float(value[0])-float(value[1]))<float(thresh))
            else:
                boolean.append(False)
    print test
    return (boolean[0] and boolean[1] and boolean[2])

def find_ROC_BDT(out_path,thresh):
    roc=0
    if os.path.isfile(out_path):
        print out_path
        f=open(out_path)
        text=f.read()
        if notOvertrained(text,thresh):    
            roc=float(re.findall('dataset       BDT            : (\d\.\d+)',text)[0])
        print roc
        f.close()
    return roc
def find_ROC_BDTG(out_path,thresh):
    roc=0
    if os.path.isfile(out_path):
        f=open(out_path)
        print out_path
        text=f.read()
        if notOvertrained(text,thresh):
            roc=float(re.findall('dataset       BDTG           : (\d\.\d+)',text)[0])
        print roc
        f.close()
    return roc
def find_ROC_MLP(out_path,thresh):
    roc=0
    if os.path.isfile(out_path):
        f=open(out_path)
        text=f.read()
        print out_path
        if notOvertrained(text,thresh):
            roc=float(re.findall('dataset       MLP            : (\d\.\d+)',text)[0])
        print roc
        f.close()
    return roc


def from_dirName_to_param(directoryName):
    MaxDepth=int(directoryName[3])
    MinNodeSize=float(directoryName[9:13])
    AdaBoostBeta=float(directoryName[19:22])
    return MaxDepth,MinNodeSize,AdaBoostBeta
def from_dirName_to_paramBDTG(directoryName):
    MaxDepth=int(directoryName[3])
    MinNodeSize=float(directoryName[9:13])
    NTrees=float(directoryName[19:22])
    return MaxDepth,MinNodeSize,NTrees


import numpy as np
def send_BDTJobs(MaxDepth,MinNodeSize,AdaBoostBeta,sendBDT,sl,time,thresh):
    name_vec=[]
    roc_vec=[]
    for m in MaxDepth:
        for n in MinNodeSize:
            for a in AdaBoostBeta:
                directoryName=gen_directoryName(m,n,a)                
                out_path=gen_outPath(directoryName,sl)
                if  sendBDT:
                    option=gen_optBDT(m,n,a)
                    trainMVA=gen_trainMVA(option,sl,"--BDT","--optBDT")
                    print trainMVA
                    os.system("python submit.py -D %s -d %s/optimisationMVA -q %s -n %s '%s'"%(path,sl,time,directoryName,trainMVA))
                else:
                    name_vec.append(directoryName)
                    roc_vec.append(find_ROC_BDT(out_path,thresh))
    return roc_vec,name_vec

def gen_optBDTG(MaxDepth,MinNodeSize,NTrees):
    option=":".join([
                    "!H",
                    "!V",
                    "NTrees=%d"%NTrees,
                    "BoostType=Grad",
                    "MinNodeSize=%.3f"%(MinNodeSize),
                    "MaxDepth=%d"%(MaxDepth),
                    "Shrinkage=0.3",
                    "SeparationType=GiniIndex",
                    "nCuts=20",
                    "UseBaggedBoost",
                    "GradBaggingFraction=0.5",
                    "PruneMethod=CostComplexity",
                    "PruneStrength=50",
                    "NegWeightTreatment=Pray"
                    ])
    return option

def gen_directoryNameBDTG(MaxDepth,MinNodeSize,NTree):
    directoryName="MD_%d_MNS_%.3f_NTR_%.2f"%(MaxDepth,MinNodeSize,NTree)
    return directoryName
def from_dirName_to_paramBDTG(directoryName):
    MaxDepth=int(directoryName[3])
    MinNodeSize=float(directoryName[9:13])
    Shrink=float(directoryName[19:22])
    return MaxDepth,MinNodeSize,Shrink
def send_BDTGJobs(MaxDepth,MinNodeSize,NTrees,sendBDTG,sl,time,thresh):
    name_vec=[]
    roc_vec=[]
    for m in MaxDepth:
        for n in MinNodeSize:
            for a in NTrees:
                directoryName=gen_directoryNameBDTG(m,n,a)                
                out_path=gen_outPath(directoryName,sl)
                if  sendBDTG:
                    option=gen_optBDTG(m,n,a)
                    trainMVA=gen_trainMVA(option,sl,"--BDTG","--optBDTG")
                    print trainMVA
                    os.system("python submit.py -D %s -d %s/optimisationMVA -q %s -n %s '%s'"%(path,sl,time,directoryName,trainMVA))
                else:
                    name_vec.append(directoryName)
                    roc_vec.append(find_ROC_BDTG(out_path,thresh))
    return roc_vec,name_vec
def gen_optMLP(Cycles,Hidden,TestRate):
    option=":".join([
                    "!H",
                    "!V",
                    "NeuronType=sigmoid",
                    "VarTransform=N",
                    "NCycles=%d"%(Cycles),
                    "HiddenLayers=%d"%(Hidden),
                    "TestRate=%d"%TestRate,
                    ])
    return option

def gen_directoryNameMLP(Cy,Hi,Ts):
    directoryName="CY_%d_HID_%.3f_TST_%.2f"%(Cy,Hi,Ts)
    return directoryName
def from_dirName_to_paramMLP(directoryName):
    Cy=int(directoryName[3:6])
    Hi=float(directoryName[11:13])
    Ts=float(directoryName[22:24])
    return Cy,Hi,Ts
def send_MLPJobs(Cycles,Hidden,TestRate,sendMLP,sl,time,thresh):
    name_vec=[]
    roc_vec=[]
    for m in Cycles:
        for n in Hidden:
            for a in TestRate:
                directoryName=gen_directoryNameMLP(m,n,a)                
                out_path=gen_outPath(directoryName,sl)
                if  sendMLP:
                    option=gen_optMLP(m,n,a)
                    trainMVA=gen_trainMVA(option,sl,"--MLP","--optMLP")
                    print trainMVA
                    os.system("python submit.py -D %s -d %s/optimisationMVA -q %s -n %s '%s'"%(path,sl,time,directoryName,trainMVA))
                else:
                    name_vec.append(directoryName)
                    roc_vec.append(find_ROC_MLP(out_path,thresh))
    return roc_vec,name_vec
def findMax(roc,name,dtype,mvaMODE):
    roc_np=np.array(roc)
    bestName=name[np.argmax(roc)]
    print "Maximum found to be",bestName, np.amax(roc_np), " between ",len(roc_np), " documents"
    print roc_np
    if mvaMODE=='BDT':
        bestParam1,bestParam2,bestParam3=from_dirName_to_param(bestName)
        opts = gen_optBDT(bestParam1,bestParam2,bestParam3)
        print opts
        wmode = 'w'
    elif mvaMODE=='MLP':
        bestParam1,bestParam2,bestParam3=from_dirName_to_paramMLP(bestName)
        opts = gen_optMLP(bestParam1,bestParam2,bestParam3)
        print opts
        wmode='a'
    else:
        bestParam1,bestParam2,bestParam3=from_dirName_to_paramBDTG(bestName)
        opts = gen_optBDTG(bestParam1,bestParam2,bestParam3)
        print opts
        wmode='a'
    write_best_opts(opts,dtype,wmode)
         
def write_best_opts(opts,mode,w='w'):
    f=open('ressource/bestMVA'+mode+'.dat',w)
    f.write(opts+"\n")
    f.close()
def read_best_opts(dtype):
    f=open('ressource/bestMVA'+dtype+'.dat','r')
    data = f.read().split('\n')
    return data[0],data[1],data[2] 
