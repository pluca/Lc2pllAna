from ROOT import *

def cut_file(save_path, raw_tree,cut):
    cut_file=TFile(save_path,"recreate")
    print cut.GetTitle()
    cut_tree=raw_tree.CopyTree(cut.GetTitle())
    cut_tree.Write()
    cut_file.Close()
    return cut_tree

import os
from routines.LcAnalysis import loc
tupleLocation=loc.MARKOTUPLE
home=os.path.expanduser("~")
base=loc.TUPLE
LcMMcut="&& Lc_MM<2350"
signal_cut="Lc_MM > 2265 && Lc_MM<2295"+LcMMcut
sideband_cut="Lc_MM<2240 || Lc_MM>2320"+LcMMcut


def load_fullCut(name,pmmTuple="Lc2pmmTuple",pKpiTuple="Lc2pKpiTuple"):
    fileName=tupleLocation+name
    pmmTree=TChain(pmmTuple)
    pKpiTree=TChain(pKpiTuple)
    pmmTree.AddFile(fileName)
    pKpiTree.AddFile(fileName)
    return pmmTree,pKpiTree
def load_pKpipmm(namepmm,namepKpi,pmmTuple="Lc2pmmTuple_mvaclone",pKpiTuple="Lc2pKpiTuple_mvaclone2"):
    fileNamepKpi=tupleLocation+namepKpi
    fileNamepmm=tupleLocation+namepmm
    pmmTree=TChain(pmmTuple)
    pKpiTree=TChain(pKpiTuple)
    pmmTree.AddFile(fileNamepmm)
    pKpiTree.AddFile(fileNamepKpi)
    return pmmTree,pKpiTree


def load_trees(cuttedName,oldName,cuts,pmmTuple="Lc2pmmTuple_mvaclone",pKpiTuple="Lc2pKpiTuple_mvaclone2"):
    fileName=tupleLocation+cuttedName
    if os.path.isfile(fileName):
        print fileName
        pmm,pKpi=load_fullCut(cuttedName,pmmTuple,pKpiTuple)
    else:
        dataFile=base+"/"+oldName
        pmmRawTree=TChain(pmmTuple)
        pKpiRawTree=TChain(pKpiTuple)
        pmmRawTree.AddFile(dataFile)
        pKpiRawTree.AddFile(dataFile)
        pmm=cut_file(fileName,pmmRawTree,cuts)
        pKpi=cut_file(fileName,pKpiRawTree,cuts)
    return pmm, pKpi
import plotting_tools as plt
def draw_hist_1D(sig,sideband,leg):
    sig.Draw("e")
    sideband.Draw("same e")
    leg.Draw("same")
def draw_hist_2D(sig,sideband):
    sig.Draw()
    sideband.Draw("same")
def scale_hist(hist):
    hist.Scale(1./hist.Integral())
def draw_signal_sideband(tree,var,binning,xlabel,ylabel,sigName,sideName):
    mode=(len(binning)==3)
    if mode:
        binned="(%d,%d,%d)"%binning
    else:
        binned="(%d,%d,%d,%d,%d,%d)"%binning
    tree.Draw(var+">>"+sigName+binned,signal_cut,"e")
    if mode:
        sig=gPad.GetPrimitive(sigName)
    else:
        sig=gDirectory.Get(sigName)
    tree.Draw(var+">>"+sideName+binned,sideband_cut,"e")    
    if mode:
        sideband=gPad.GetPrimitive(sideName)
    else:
        sideband=gDirectory.Get(sideName)
    leg=TLegend(0.6,0.7,0.8,0.9)
    leg.AddEntry(sig,"( m > 2265 and m < 2295)")
    leg.AddEntry(sideband,"( m < 2240 | m > 2320)")
    leg.SetTextSize(0.035)
    plt.hist_param(sig,xlabel,ylabel)
    plt.hist_param(sideband, xlabel,ylabel)
    if mode:
        scale_hist(sig)
        scale_hist(sideband)
    sig.SetMarkerColor(kRed)
    if not mode :
        sideband.SetMarkerColorAlpha(kBlue,0.0)
    else:
          sideband.SetMarkerColor(kBlue)
    draw_hist_1D(sig,sideband,leg)
    return sig,sideband,leg
def draw_colz(tree,var,binning,xlabel,ylabel,sigName,newcut=""):
    binned="(%d,%d,%d,%d,%d,%d)"%binning
    tree.Draw(var+">>"+sigName+binned,signal_cut+newcut,"colz")
    sig=gDirectory.Get(sigName)
    plt.hist_param(sig,xlabel,ylabel)
    sig.Draw("colz")
from math import sqrt
def compute_efficiency(N,Ntot):
    eff=float(N)/float(Ntot)
    err=eff*sqrt(1./N+1./Ntot)
    return eff,err
def compute_efficiencies(tree,cut,sigName,sideName,treeNameSig,treeNameSide):
    tree.Draw("Lc_MM>>"+treeNameSig,signal_cut)
    treeHistSig=gPad.GetPrimitive(treeNameSig)
    tree.Draw("Lc_MM>>"+treeNameSide,sideband_cut)
    treeHistSide=gPad.GetPrimitive(treeNameSide)
    tree.Draw("Lc_MM>>"+sigName,cut+"&&"+signal_cut)
    sigHist=gPad.GetPrimitive(sigName)
    tree.Draw("Lc_MM>>"+sideName,cut+"&&"+sideband_cut)
    sideHist=gPad.GetPrimitive(sideName)
    sigEff,sigEffErr=compute_efficiency(sigHist.GetEntries(),treeHistSig.GetEntries())
    sideEff,sideEffErr=compute_efficiency(sideHist.GetEntries(),treeHistSide.GetEntries())    
    print cut, "Signal cut efficiency : %.4f +- %.4f"%(sigEff,sigEffErr)
    print cut, "Sideband cut efficiency : %.4f +- %.4f"%(sideEff,sideEffErr)
    return sigEff,sideEff 



