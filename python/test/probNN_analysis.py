import os
from ROOT import *
from routines.LcAnalysis import cuts
import probNN_tools as pbt
import plotting_tools as plt
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="Prompt")
args=parser.parse_args()

#******************** Lc2pmm Analysis ********************

save_path=pbt.home + "/report/fig/"

if args.mode=="Prompt":
    full_cut=cuts.convergence + cuts.oldStripping + cuts.p_PID + cuts.bdt
    fileName="CL16_Lc2pKpi_CutStripPID.root"
    dataName="CL16_Lc2pKpi_CutStripPID.root"
    save_path+="Prompt"
else:
    full_cut=cuts.convergence+cuts.p_PID_SL+cuts.bdt_SL
    fileName="CL16_SL_fullCut.root"
     ### To be done ####

pmmTree,pKpiTree=pbt.load_pKpipmm("CL16_Lc2pmm_CutStripPIDAndBDT.root","CL16_Lc2pKpi_CutStripPID.root")


#pmmTree,pKpiTree=pbt.load_trees(fileName,dataName,full_cut,"Lc2pmmTuple_mvaclone","Lc2pKpiTuple_mvaclone2")
binningProbNN=(100,0,1)
binningString="(100,0,1)"
perEventsProbNN=float(binningProbNN[2]-binningProbNN[1])/binningProbNN[0]
#************ Lc2pmm ProbNNmu 1D distribution *******************
c=TCanvas("Lc2pmm_ProbNNmu","Lc2pmm_L1_MC15TunveV1_ProbNNmu")
pmmTree.Draw("L1_MC15TuneV1_ProbNNmu >>L1ProbNNMu(%d,%d,%d)"%binningProbNN)
plt.hist_param(L1ProbNNMu,"#mu ProbNN #mu"," Events/ / %.2f"%perEventsProbNN)
L1ProbNNMu.Draw()
c.SaveAs(save_path+"CL16_fullCut_Lc2pmm_L1_MC15TunveV1_ProbNNmu.pdf")


#************ Lc2pKpi ProbNN p K pi 1D distributions L1  **************
c2=TCanvas("Lc2pKpi_L1","#lambda #rightarrow pK#pi K")
c2.SetLogy()
L1ProbNNpsig,L1ProbNNpside,L1ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp",binningProbNN,"K ProbNN p","Normalized  Events/ %.2f"%perEventsProbNN,"L1ProbNNpSig","L1ProbNNpSideband")
c2.Print(save_path+"CL16_fullCUT_L1ProbNNp.pdf")
L1ProbNNpsig,L1ProbNNpside,L1ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp",binningProbNN,"K ProbNN p","Normalized  Events/ %.2f"%perEventsProbNN,"L1ProbNNpSig","L1ProbNNpSideband")
L1ProbNNksig,L1ProbNNkside,L1ProbNNkLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNk",binningProbNN,"K ProbNN K","Normalized  Events/ %.2f"%perEventsProbNN,"L1ProbNNkSig","L1ProbNNkSideband")
c2.Print(save_path+"CL16_fullCUT_L1ProbNNk.pdf")
L1ProbNNpsig,L1ProbNNpside,L1ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp",binningProbNN,"K ProbNN p","Normalized  Events/ %.2f"%perEventsProbNN,"L1ProbNNpSig","L1ProbNNpSideband")
L1ProbNNpisig,L1ProbNNpiside,L1ProbNNpiLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNpi",binningProbNN,"K ProbNN #pi","Normalized  Events/ %.2f"%perEventsProbNN,"L1ProbNNpiSig","L1ProbNNpiSideband")
c2.Print(save_path+"CL16_fullCUT_L1ProbNNpi.pdf")

#************* Lc2pKpi ProbNN pKpi 1 Distributions L2 *****************
c3=TCanvas("Lc2pKpi_L2_p","#lambda #rightarrow pK#pi #pi")
c3.SetLogy()
L1ProbNNpsig,L1ProbNNpside,L1ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp",binningProbNN,"K ProbNN p","Normalized  Events/%.2f"%perEventsProbNN,"L1ProbNNpSig","L1ProbNNpSideband")
L2ProbNNpsig,L2ProbNNsideband,L2ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNp",binningProbNN,"#pi ProbNN p","Normalized  Events/%.2f"%perEventsProbNN,"L2ProbNNpSig","L2ProbNNpSideband")
c3.Print(save_path+"CL16_fullCUT_L2ProbNNp.pdf")
gPad.SetLogy()
L1ProbNNpsig,L1ProbNNpside,L1ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp",binningProbNN,"K ProbNN p","Normalized  Events/%.2f"%perEventsProbNN,"L1ProbNNpSig","L1ProbNNpSideband")
L2ProbNNksig,L2ProbNNksideband,L2ProbNNkLeg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNk",binningProbNN,"#pi ProbNN K","Normalized  Events/%.2f"%perEventsProbNN,"L2ProbNNkSig","L2ProbNNkSideband")
c3.Print(save_path+"CL16_fullCUT_L2ProbNNK.pdf")
gPad.SetLogy()
L1ProbNNpsig,L1ProbNNpside,L1ProbNNpLeg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp",binningProbNN,"K ProbNN p","Normalized  Events/%.2f"%perEventsProbNN,"L1ProbNNpSig","L1ProbNNpSideband")
L2ProbNNpisig,L2ProbNNpisideband,L2ProbNNpiLeg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNpi",binningProbNN,"#pi ProbNN #pi","Normalized  Events/%.2f"%perEventsProbNN,"L2ProbNNpiSig","L2ProbNNpSidebande")
c3.Print(save_path+"CL16_fullCUT_L2ProbNNpi.pdf")


#************** Lc2pKpi ProbNN L1 and L2  p/K or pi 2D scatter plot *************
binningProbNN2D=(100,0,1,100,0,1)
c4=TCanvas("Lc2pKpi_ProbNN_p/Kpi_L1","Lc2pKpi_ProbNN_p/Kpi L1",1600,600)
c4.Divide(2)
c4.cd(1)
L1_pk_sig,L1_pk_side,L1_pk_leg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp:L1_MC15TuneV1_ProbNNk",binningProbNN2D,"K ProbNN K","K ProbNN p","L1_pksig","L1_pkside")
c4.cd(2)
L1_ppi_sig,L1_ppi_side,L1_ppi_leg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNp:L1_MC15TuneV1_ProbNNpi",binningProbNN2D,"K ProbNN #pi","K ProbNN p","L1_ppisig","L1_ppiside")
c4.SaveAs(save_path+"CL16_fullCut_L1ProbNN_p_Kpi_2D.pdf")


#How to get the title for the histograms
c5=TCanvas("Lc2pKpi_ProbNN_p/Kpi_L2","Lc2pKpi_ProbNN_p/Kpi L2",1600,600)
c5.Divide(2)
c5.cd(1)
L2_pk_sig,L2_pk_side,L2_pk_leg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNp:L2_MC15TuneV1_ProbNNk",binningProbNN2D,"#pi ProbNN K","#pi ProbNN p","L2_pksig","L2_pkside")
c5.cd(2)
L2_ppi_sig,L2_ppi_side,L2_ppi_leg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNp:L2_MC15TuneV1_ProbNNpi",binningProbNN2D,"#pi ProbNN #pi","#pi ProbNN p","L2_ppisig","L2_ppiside")
c5.SaveAs(save_path+"CL16_fullCut_L2ProbNN_p_Kpi_2D.pdf")


#************** Lc2pKpi L1 and L2 K / pi 
c6=TCanvas("Lc2pKpi_ProbNN_K_pi","Lc2pKpi_ProbNN_K_pi",1600,600)
c6.Divide(2)
c6.cd(1)
L1_kpi_sig,L1_Kpi_side,L1_kpi_leg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNk:L1_MC15TuneV1_ProbNNpi",binningProbNN2D,"K ProbNN #pi","K ProbNN K","L1_kpisig","L1_kpiside")
c6.cd(2)
L2_kpi_sig,L2_Kpi_side,L2_kpi_leg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNk:L2_MC15TuneV1_ProbNNpi",binningProbNN2D,"#pi ProbNN #pi","#pi ProbNN K","L2_kpisig","L2_kpiside")
c6.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_ProbNN_k_pi_2D.pdf")


#************** Lc2pKpi L1_ProbNNi*(1-L1_ProbNNi) ********
c7=TCanvas("L1_MixedProb","L1_MixedProb")
c7.SetLogy()
L1_knotpi_sig,L1_knotpi_side,L1_knotpi_leg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNk*(1-L1_MC15TuneV1_ProbNNpi)",binningProbNN,"K ProbNN K#times (1-ProbNN #pi)","Normalized  Events/%.2f"%perEventsProbNN,"L1_knotpisig","L1_knotpisideband")
c7.Print(save_path+"CL16_fullCUT_L1Knotpi.pdf")
gPad.SetLogy()
L1_pinotk_sig,L1_pinotk_side,L1_pinotk_leg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNpi*(1-L1_MC15TuneV1_ProbNNk)",binningProbNN,"K ProbNN#pi#times (1-ProbNN K)","Normalized  Events/%.2f"%perEventsProbNN,"L1_pinotksig","L1_pinotksideband")
c7.Print(save_path+"CL16_fullCUT_L1piNotK.pdf")


#************** Lc2pKpi L2_ProbNNi *(1-L2_ProbNNi) *********
c8=TCanvas("L2_MixedProb","L2_MixedProb")
c8.SetLogy()
L2_knotpi_sig,L2_knotpi_side,L2_knotpi_leg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNk*(1-L2_MC15TuneV1_ProbNNpi)",binningProbNN,"#pi ProbNN K#times (1-ProbNN #pi)","Normalized  Events/%.2f"%perEventsProbNN,"L2_knotpisig","L2_knotpisideband")
c8.Print(save_path+"CL16_fullCUT_L2Knotpi.pdf")
L2_pinotk_sig,L2_pinotk_side,L2_pinotk_leg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNpi*(1-L2_MC15TuneV1_ProbNNk)",binningProbNN,"#pi ProbNN#pi#times (1-ProbNN K)","Normalized  Events/%.2f"%perEventsProbNN,"L2_pinotksig","L2_pinotksideband")
c8.Print(save_path+"CL16_fullCUT_L2piNotK.pdf")
#20.03.17 Modifications
#************** Colz drawings of p/K and p/pi **************
c9=TCanvas("ColzP_K","ColzP_K")
c9.SetLogz()
gStyle.SetPalette(1)
pbt.draw_colz(pKpiTree, "L1_MC15TuneV1_ProbNNp:L1_MC15TuneV1_ProbNNk",binningProbNN2D,"K ProbNN K","K ProbNN p","L1_colz_pK")
c9.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_L1_pK_colz.pdf")
#************* Prob K not pi not p********
c10=TCanvas("KnotPinotP","KnotPinotP")
c10.SetLogy()
L1_KnotPinotPsig,L1_KnotPinotP_side,L1_KnotPinotPleg=pbt.draw_signal_sideband(pKpiTree,"L1_MC15TuneV1_ProbNNk*(1-L1_MC15TuneV1_ProbNNpi)*(1-L1_MC15TuneV1_ProbNNp)",binningProbNN,"K ProbNN K #times(1-ProbNN #pi)#times(1-ProbNN p) ","Normalized  Events/%.2f"%perEventsProbNN,"L1KnPinPsig","L1KnPinPside")
c10.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_L1_Knotpinotp.pdf")
#************* Prob K / pi **********
c11=TCanvas("ColzK_pi","ColzK_pi")
c11.SetLogz()
gStyle.SetPalette(1)
pbt.draw_colz(pKpiTree, "L1_MC15TuneV1_ProbNNpi:L1_MC15TuneV1_ProbNNk",binningProbNN2D,"K ProbNN K","K ProbNN #pi","L1_colz_Kpi")
c11.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_L1_Kpi_colz.pdf")
#************** Colz drawings of p/K and p/pi **************
c12=TCanvas("ColzP_Pi","ColzP_Pi")
c12.SetLogz()
gStyle.SetPalette(1)
pbt.draw_colz(pKpiTree, "L2_MC15TuneV1_ProbNNp:L2_MC15TuneV1_ProbNNpi",binningProbNN2D,"#pi ProbNN #pi","K ProbNN p","L2_colz_ppi")
c12.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_L2_ppi_colz.pdf")
#************* Prob K not pi not p********
c13=TCanvas("PinotKnotP","PinotKnotP")
c13.SetLogy()
L2_PinotKnotPsig,L2_PinotKnotP_side,L2_PinotKnotPleg=pbt.draw_signal_sideband(pKpiTree,"L2_MC15TuneV1_ProbNNpi*(1-L2_MC15TuneV1_ProbNNk)*(1-L2_MC15TuneV1_ProbNNp)",binningProbNN,"#pi ProbNN #pi #times(1-ProbNN K)#times(1-ProbNN p) ","Normalized  Events/%.2f"%perEventsProbNN,"L2PinKinPsig","L2PinKnPside")
c13.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_L2_Pinotknotp.pdf")
#************* Prob K / pi **********
c14=TCanvas("ColzK_pi2","ColzK_pi2")
c14.SetLogz()
gStyle.SetPalette(1)
pbt.draw_colz(pKpiTree, "L2_MC15TuneV1_ProbNNpi:L2_MC15TuneV1_ProbNNk",binningProbNN2D,"#pi ProbNN K","#pi ProbNN #pi","L2_colz_Kpi")
c14.SaveAs(save_path+"CL16_fullCut_Lc2pKpi_L2_Kpi_colz.pdf")
c15=TCanvas("Stuff","Stuff")
L2PiCutEff,L2PiCutEffSide=pbt.compute_efficiencies(pKpiTree,"L2_MC15TuneV1_ProbNNpi>0.8","L2PiCut","L2PiCutside","treeNormSig1","treeNormSide1")
L2MixedProb, L2MixedProbSide=pbt.compute_efficiencies(pKpiTree,"L2_MC15TuneV1_ProbNNpi*(1-L2_MC15TuneV1_ProbNNk)>0.8","L2MixedCut","L2MixedCutSide","treeNormSig2","treeNormSide2")
L2PinotKnotP, L2PinotKnotPside=pbt.compute_efficiencies(pKpiTree,"L2_MC15TuneV1_ProbNNpi*(1-L2_MC15TuneV1_ProbNNk)*(1-L2_MC15TuneV1_ProbNNp)>0.8","L2piNotKnotp","L2piNotKnotpside","treeNormSig6","treeNormSide6")
L1KCutEff, L1KCutEffSide=pbt.compute_efficiencies(pKpiTree,"L1_MC15TuneV1_ProbNNk>0.8","L1KCut","L1KCutSide","treeNormSig3","treeNormSide3")
L1MixedProb, L1MixedProbSide=pbt.compute_efficiencies(pKpiTree,"L1_MC15TuneV1_ProbNNk*(1-L1_MC15TuneV1_ProbNNpi)>0.8","L1MixedProb","L1MixedProbSide","treeNormSig4","treeNormSide4")
L1KnotPinotP, L1KnotPinotPSide=pbt.compute_efficiencies(pKpiTree,"L1_MC15TuneV1_ProbNNk*(1-L1_MC15TuneV1_ProbNNpi)*(1-L1_MC15TuneV1_ProbNNp)>0.8","L1KnotPinotP","L1knotpinotpside","treeNormSig5","treeNormSide5")




