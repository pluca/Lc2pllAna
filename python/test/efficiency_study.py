import os
from routines.LcAnalysis import loc,cuts,strings

from ROOT import *

import argparse
parser=argparse.ArgumentParser()
parser.add_argument('--Lc2pKpi',action='store_true')
parser.add_argument('--Lc2pmm',action='store_true')
parser.add_argument('--var',default='Lc_M')
args=parser.parse_args()

mass=1.01945
window=0.035

q2True="(TMath::Power((TMath::Sqrt(105.65836*105.65836 + L1_TRUEP_Z*L1_TRUEP_Z+ L1_TRUEP_X*L1_TRUEP_X + L1_TRUEP_Y*L1_TRUEP_Y)+TMath::Sqrt(105.6*105.6 + L2_TRUEP_Z*L2_TRUEP_Z + L2_TRUEP_Y*L2_TRUEP_Y +  L2_TRUEP_X*L2_TRUEP_X)),2) - (TMath::Power(L1_TRUEP_X+L2_TRUEP_X,2) +TMath::Power(L1_TRUEP_Y+L2_TRUEP_Y,2) + TMath::Power(L1_TRUEP_Z+L2_TRUEP_Z,2)))/1000000."
mcIDS = { 'MCLc2pKpi' : 'MC12_Lc2pKpi_CutStripPIDAndBDT.root' , 'MCLc2pmm' : 'MC12_Lc2pmm_CutStripPIDAndBDT.root'}
q2=strings.q2
regions = [TCut("TMath::Sqrt(%s)<%f"%(q2,mass-window)),TCut("TMath::Sqrt(%s)>=%f && TMath::Sqrt(%s)<=%f"%(q2,mass-window,q2,mass+window)),TCut("TMath::Sqrt(%s)>%f"%(q2,mass+window))] 
regionsTrue = [TCut("TMath::Sqrt(%s)<%f"%(q2True,mass-window)),TCut("TMath::Sqrt(%s)>=%f && TMath::Sqrt(%s)<=%f"%(q2True,mass-window,q2True,mass+window)),TCut("TMath::Sqrt(%s)>%f"%(q2True,mass+window))] 

import plotting_tools as plt
def compute_hist(var,dtype,tupleName,cuts,binning,xlabel="m(p#mu#mu) [MeV/c^{2}]",ylabel=""):
    print ""
    print cut.GetTitle()
    print ""
    if ylabel=="":
        ylabel="Events / (%.3f MeV/c^{2})"%(float(binning[2]-binning[1])/binning[0])
    fileName=loc.MARKOTUPLE+mcIDS[dtype]
    mcTree=TChain(tupleName)
    mcTree.AddFile(fileName)
    mcTree.Draw(var+">>"+dtype+"(%d,%d,%d)"%binning,cuts)
    hist=gPad.GetPrimitive(dtype)
    plt.hist_param(hist,xlabel,ylabel)
    return hist
c=TCanvas()
binning=(180,2200,2380)
cut=cuts.full_cut
cut_muon= cuts.trigger_muon_configuration
import eventtuple_tools as et
if args.Lc2pKpi:
    dtype="MCLc2pKpi"
    tupleN="Lc2pKpiTupleOld_mvaclone2"
    hist=compute_hist(args.var,dtype,tupleN,cut,binning)
    NTot=et.findNTot(dtype)
    print "Number of events : ",hist.GetEntries(),"Total number of events :", NTot, "Efficiency : ", et.compute_eff(hist.GetEntries(),NTot)

if args.Lc2pmm:
    dtype="MCLc2pmm"
    tupleN="Lc2pmmTuple_mvaclone"
    for i in range(0,len(regions)):
        hist=compute_hist(args.var,dtype,tupleN,cut+regions[i],binning)
        hist.Draw()
#        N=hist.GetEntries()
        N=et.findNTot(dtype,cut_muon+regions[i],"Lc2pmmTuple/DecayTree")
        NTot=et.findNTot(dtype,regionsTrue[i],"MCTuple/MCDecayTree")
        print regions[i].GetTitle(), regionsTrue[i].GetTitle()
        print N,NTot
        print "Efficiencies : " , et.compute_eff(N,NTot)



