import os
from routines.LcAnalysis import loc,cuts

import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--dtype",default='MCLc2pKK')
parser.add_argument("--drawAll",action='store_true')
parser.add_argument("--var",default='Lc_M')
args=parser.parse_args()

tupleLocation=loc.MARKOTUPLE
save_path=os.path.expanduser('~')+"/report/BkgStudy/"
rootIDS = {
        'MCLc2pKpi' : 'MC12_Lc2pKpi_CutStripPID.root',
#        'MCLc2pmm'  : 'MC12_Lc2pmm_CutStripPID.root',
        'MCLc2ppipi': 'MC12_Lc2ppipi_CutStripPID.root',
        'MCLc2pKK'  : 'MC12_Lc2pKK_CutStripPID.root',
        'MCD2KKpi'  : 'MC12_D2KKpi_CutStripPID.root',
        'MCD2Kpipi' : 'MC12_D2Kpipi_CutStripPID.root',
        'MCD2Kmumu'  : 'MC12_D2Kmumu_CutStripPID.root',
        'MCD2pimumu' : 'MC12_D2pimumu_CutStripPID.root'        
        }
#Branching ratios IDS
brIDS = {
        'MCLc2pKpi' : 5.3e-2,
        'MCLc2ppipi': 3.5e-3,
        'MCLc2pKK'  : 7.7e-4,
        'MCD2KKpi'  : 9.54e-3,
        'MCD2Kpipi' : 5.27e-4,
        'MCD2Kmumu' : 4.3e-6,
        'MCD2pimumu': 7.3e-8        
        }
#NpKpi=2039.0
NpKpi=182660.89

from ROOT import *
import eventtuple_tools as et
import plotting_tools as plt
def tupleName(dtype):
    return dtype[2:]+"Tuple_BkgStudypmm"
def compute_hist(var,dtype,cuts,binning,xlabel="m(p#mu#mu) [MeV/c^{2}]",ylabel=""):
    if ylabel=="":
        ylabel="Events / (%.3f MeV/c^{2})"%(float(binning[2]-binning[1])/binning[0])
    if dtype=='MCLc2pKpi':
        tupleN="Lc2pKpiTupleOld_mvaclone2"
    else:
        tupleN="Lc2pmmTuple_BkgStudypmm"
    fileN=tupleLocation+rootIDS[dtype]
    mcTree=TChain(tupleN)
    mcTree.AddFile(fileN)
    mcTree.Draw(var+">>"+dtype+"(%d,%d,%d)"%binning,cuts)
    hist=gPad.GetPrimitive(dtype)
    plt.hist_param(hist,xlabel,ylabel)
    return hist,mcTree.GetEntries()
#Full cuts on Prompt Selection No PID since MC
full_cut=cuts.full_cut +cuts.trigger_muon_configuration +TCut("Lc_TRUEID != 0")
#binning
binned=(180,2200,2380)

hpKpi,N=compute_hist(args.var,'MCLc2pKpi',cuts.full_cut,binned)
NTotpKpi=et.findNTot('MCLc2pKpi')
EntriespKpi=hpKpi.GetEntries()
effpKpi,errpKpi=et.compute_eff(EntriespKpi,NTotpKpi)

def compute_expected_events(dtype, eff):
    expectedEvents=NpKpi *(float(eff)/effpKpi)*float(brIDS[dtype])/brIDS['MCLc2pKpi']         
    return expectedEvents


# Compute one background study
c=TCanvas()
#hist,Ntot=compute_hist(args.var,args.dtype,full_cut,binned)
#hist.Draw()
#print hist.GetEntries(),Ntot
import eventtuple_tools as et
for key in rootIDS:
    hist,NTot=compute_hist(args.var,key,full_cut,binned)
    NTot=et.findNTot(key)
    Entries=hist.GetEntries()
    if Entries<2.3:
        Entries=2.3
        print ""
        print "Scaled to 90 %  of confidence level"
    eff,err=et.compute_eff(Entries,NTot)
    print "%s : %.10f +- %.10f remaining entries after the full prompt cut"%(key,eff,err),Entries,NTot
    expected=compute_expected_events(key,eff)
    print "Number of Events to expect for : ", key, "   ", expected
    hist.Draw()
    c.Print(save_path+key+".pdf")

print full_cut.GetTitle()
print effpKpi, errpKpi


