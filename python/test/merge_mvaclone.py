import os
from routines.LcAnalysis import loc, dataids
from glob import glob
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--dtype",default="Prompt")
parser.add_argument("--filePath",default="mvaclone2")
parser.add_argument("--saveName",default="CL16_Lc2pKpi_beforeBDT.root")
parser.add_argument("--friendsPath",default="friends")
parser.add_argument("--S28",action='store_true')
args=parser.parse_args()

mvaclone_path=args.filePath+"/"
mvaclone_file=args.filePath+".root"

#friends_path=loc.MARKOFRIENDS
friends_path=loc.MARKOTUPLE+args.friendsPath+"/"
Prompt=dataids['Prompt']
MCLc2pmm=dataids['MCLc2pmm']

if args.S28:
    friends_path=loc.JOBS+"/"
    mvaclone_path=""
    mvaclone_file=""
dataID=dataids[args.dtype]
files=[]
for index in dataID:
    path="%s/*/"%index
    if args.S28:
        path="%s/*/DVNtuple.root"%index
    print friends_path+mvaclone_path+path+mvaclone_file
    files+=glob(friends_path+mvaclone_path+path+mvaclone_file)
sources=""
for f in files:
    sources+=f+" "

save_name=loc.MARKOTUPLE+args.saveName

print "hadd %s %s "%(save_name,sources)
os.system("hadd -f %s %s "%(save_name,sources))



