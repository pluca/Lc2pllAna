from ROOT import *
import sys,os
from routines.LcAnalysis import *
retrieve_files()
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--dtype",default="MCLc2pKpi")
parser.add_argument("--Lc2pKpi",action='store_true')
parser.add_argument("--Lc2pmm",action='store_true')
parser.add_argument("--findPrompt",action='store_true')
parser.add_argument("--findSL",action='store_true')
opts = parser.parse_args()
import math
def compute_eff(NSig,NTot):
    eff=float(NSig)/NTot
    error_eff=eff*math.sqrt(1./NSig+1./NTot)
    return eff, error_eff
def to_print_eff(eff,e):
    return "%.5f +- %.5f"%(eff,e)
def process_efficiency(EventTuple,TreeTuple,dtype,cuts):
    total = TChain(EventTuple)
    new = TChain(TreeTuple)
    for f in datafiles[dtype] : total.AddFile(f)
    for g in datafiles[dtype] : new.AddFile(g)
    efficiency, error= compute_eff(new.GetEntries(cuts.GetTitle()),total.GetEntries())
    print "Lc2pmm "
    print "Cuts : " ,cuts.GetTitle()
    print "Efficiency : ", to_print_eff(efficiency,error)
def findNTot(dtype,cut=TCut()):
    total = TChain("EventTuple/EventTuple")
    for f in datafiles[dtype] : total.AddFile(f)
    return total.GetEntries(cut.GetTitle())
############## Lc2pKpi ######################
if opts.Lc2pKpi:
    data = TChain("EventTuple/EventTuple")
    dataOld = TChain("Lc2pKpiTupleOld/DecayTree")
    dataNew = TChain("Lc2pKpiTuple/DecayTree")
    for f in datafiles[opts.dtype] : data.AddFile(f)
    for i in datafiles[opts.dtype] : dataOld.AddFile(i)
    for g in datafiles[opts.dtype] : dataNew.AddFile(g)

    effNew,eeNew=compute_eff(10*dataNew.GetEntries(),data.GetEntries())
    effOld,eeOld=compute_eff(dataOld.GetEntries(),data.GetEntries())
    effOldNew,eeOldNew=compute_eff(dataOld.GetEntries(cuts.newStripping.GetTitle()),data.GetEntries())
    effNewOld,eeNewOld=compute_eff(10*dataNew.GetEntries(cuts.oldStripping.GetTitle()),data.GetEntries())
 
    print "NewStripping : ",to_print_eff(effNew,eeNew)  
    print "OldStripping : ",to_print_eff(effOld,eeOld)  
    print "OldNewtripping : ",to_print_eff(effOldNew,eeOldNew)  
    print "NewOldStripping : ",to_print_eff(effNewOld,eeNewOld) 
    print "Ratio New/NewOld : ", effNew/effNewOld
    print "Number of new data : ", 10*dataNew.GetEntries()
    print "Number of old data : ", dataOld.GetEntries()
############### Lc2pmm ######################
if opts.Lc2pmm:
    pmm = TChain("EventTuple/EventTuple")
    pmmNew = TChain("Lc2pmmTuple/DecayTree")
    dataType= "MCLc2pmm"
    for f in datafiles[dataType] : pmm.AddFile(f)
    for g in datafiles[dataType] : pmmNew.AddFile(g)

    pmmE,pmmEE=compute_eff(pmmNew.GetEntries(),pmm.GetEntries())
    pmmEO,pmmEEO=compute_eff(pmmNew.GetEntries(cuts.oldStripping.GetTitle()),pmm.GetEntries())

    print "Lc2pmm newStripping :",to_print_eff(pmmE,pmmEE)
    print "Lc2pmm (old+new): ", to_print_eff(pmmEO,pmmEEO)
    print "Ratio : ", pmmE/pmmEO
if opts.findPrompt:
    pKpi=TChain("Lc2pKpiTuple/DecayTree")
    pmm=TChain("Lc2pmmTuple/DecayTree")
    for f in datafiles['Prompt'] : 
        pKpi.AddFile(f)
        pmm.AddFile(f)
    print "Lc2pKpi Tuple Old : " , pKpi.GetEntries((cuts.oldStripping + cuts.newStripping).GetTitle())
    print "Lc2pmm Tuple : " , pmm.GetEntries((cuts.oldStripping + cuts.newStripping).GetTitle())
if opts.findSL:
    pKpi=TChain("Lc2pKpiTuple/DecayTree")
    pmm=TChain("Lc2pmmTuple/DecayTree")
    for f in datafiles['SL'] : 
        pKpi.AddFile(f)
        pmm.AddFile(f)
    print "Lc2pKpi Tuple : " , pKpi.GetEntries()
    print "Lc2pmm Tuple : " , pmm.GetEntries()
   



