import numpy as np
import test.sigproc as sp
from ROOT import *
#Initialization
sl="-PROMPT";

import argparse,sys
parser=argparse.ArgumentParser()
parser.add_argument("-m","--mode",default="Prompt", help="Mode Prompt or SL")
args=parser.parse_args()
sl=args.mode
print sl

opt=sp.compute_option(sl)
x0=0
x1=1
step=0.05
cut=np.arange(x0,x1,step)
#Process
sp.multi_process_exec_s28(sp.process_exec_s28,sl,cut)
NCut, NSig, NBkg= sp.compute_numpy_arrays_s28(sl)
Sig,sig_err=sp.compute_significance(NSig,NBkg)#sp.plot_significance(NCut,NSig,NBkg)
#New Cut
max_cut=NCut[np.argmax(Sig)]
new_step=step/10
new_cut=np.arange(max_cut-step,max_cut+step,new_step)
sp.multi_process_exec_s28(sp.process_exec_s28,sl,new_cut)
histNSig,histNBkg,histSigni,new_Sig,new_err,new_NCut=sp.compute_hist(sl)
c1,t1=sp.draw_NSig_NBkg(histNSig,histNBkg)
c2=sp.draw_Significance(histSigni)
c1.SaveAs(sp.home+"/report/PID/"+sl+"NSig_NBkg.pdf")
c2.SaveAs(sp.home+"/report/PID/"+sl+"Significance.pdf")

bestCut=new_NCut[np.argmax(new_Sig)]

bestFit=sl+"Lc2pKpi%.6f_fitAndRes.pdf"%(bestCut)
bestLogFit=sl+"Lc2pKpi%.6f_log_fitAndRes.pdf"%(bestCut)
fitMC=sl+"Lc2pKpi_MC0.000000_fitAndRes.pdf"

sp.move_file_toReport(bestFit)
sp.move_file_toReport(bestLogFit)
sp.move_file_toReport(fitMC)
#import os
#os.system("mv *.pdf plots")
print("Maximum %f at cut %f"%(np.amax(new_Sig),bestCut))
