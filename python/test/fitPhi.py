from routines.LcAnalysis import *
import os
from ROOT import *
import plotting_tools as plt
import argparse
import numpy as np
import test.sigproc as sp
parser=argparse.ArgumentParser()
parser.add_argument("--fitPhi",action='store_true')
parser.add_argument("--plotQ2",action='store_true')
parser.add_argument("--MPhi",action='store_true')
parser.add_argument("--MRho",action='store_true')
parser.add_argument("--mode",default="Prompt")
parser.add_argument("--plotSigni",action='store_true')
args=parser.parse_args()

if args.mode=="Prompt":
    infile,intree = parseDatafiles('CL16_Lc2pmm_S28_forFit')
    inMCfile,inMCtree = parseDatafiles('MCLc2pmm_S28_forFit')
else:
    tupleName="Lc2pmmTuple_SL"
    append="SL"
path=loc.REPO+"/cpp/dat/fitter/PHI/"
save_path=os.path.expanduser("~")+"/report/PHI/"
def generate_options(cheb,q2):
    options=[]
    for c in cheb:
        for q in q2:
            options.append(" -CHEB %d -Q2CUT %.3f "%(c,q))
    return options
def generate_exec(infile,intree,inMCfile,inMCtree,tuplePath,cut,mode):
    command="./fitter.out -model Johnson -infile %s -intree %s -inMCfile %s -inMCtree %s -tuplePath %s -var Lc_M 2287 2250 2330 -fitPdfName Lc2pmm -labelName '#Lambda_c^+#rightarrow p#phi(#mu#mu)' -id %f -option PHI %f -matchLc2pmm  -mode %s "%(infile,intree,inMCfile,inMCtree,tuplePath,cut,cut,mode)
    print command
    return command
def generate_hist(mcTree,dataTree,var,cut,binning,mcName,dataName):
    print cut
    perEvents=float(binning[2]-binning[1])/binning[0]
    sideband=TCut("Lc_M <2260 || Lc_M> 2320")
    signal=TCut("Lc_M > 2265 && Lc_M < 2295")
    mcTree.Draw(var+">>"+mcName+"(%d,%d,%d)"%binning,cut+signal)
    mcHist=gPad.GetPrimitive(mcName)
    dataTree.Draw(var+">>"+dataName+"(%d,%d,%d)"%binning,cut+sideband)
    dataHist=gPad.GetPrimitive(dataName)
    plt.scale_hist(mcHist)
    plt.scale_hist(dataHist)
    return mcHist,dataHist
m_cut=[0.035, 0.025] #Window region on TMath::Abs(m-m_i)< opt
cheb=[1] #chebyshev polynomial order to be test in fit phi->mu mu

x_max=1
x_min=0
step=float(x_max-x_min)/20
mu_cuts=np.arange(x_min,x_max,step)
option = "-PHICUT %.5f"%m_cut[0] #default value
if args.MPhi:
    mcut=m_cut[0]
    option="-PHICUT %.5f"%m_cut[0]
    path+="/phi/"
    name="Phi"
elif args.MRho: 
    mcut=m_cut[1]
    option="-RHOCUT %.5f"%m_cut[1]
    path+="/rho/"
    name="Rho"

if args.fitPhi:
    for c in mu_cuts:
        os.chdir(loc.REPO+"cpp")
        command=generate_exec(infile,intree,inMCfile,inMCtree,loc.MARKOTUPLE,c,args.mode)
        os.system(command)
    NCut,NSig,NBkg=sp.compute_numpy_arrays_s28(args.mode,path,"PHI")
    Sig=sp.compute_significance(NSig,NBkg)
    max_cut=NCut[np.argmax(Sig)]
    new_step=step/10
    new_cuts=np.arange(max_cut-step,max_cut+step,new_step)
    if(new_cuts[0]<0):
        new_cuts+=step
    print new_cuts
    for nc in new_cuts:
        os.chdir(loc.REPO+"cpp")
        command=generate_exec(infile,intree,inMCfile,inMCtree,loc.MARKOTUPLE,nc,args.mode)
        os.system(command)
import eventtuple_tools as et
if args.plotSigni:
    histNSig,histNBkg,histSigni,new_Sig,new_err_Sig,new_NCut=sp.compute_hist(args.mode,path,"#mu ProbNN #mu","PHI")
    c1,t1=sp.draw_NSig_NBkg(histNSig,histNBkg)
    c2=sp.draw_Significance(histSigni)
    bestCut=new_NCut[np.argmax(new_Sig)]
    errSigni=new_err_Sig[np.argmax(new_Sig)]
    print "Maximum %f +/- %f at cut %f"%(np.amax(new_Sig),errSigni,bestCut)
    c1.Print(save_path+args.mode+"NsigNBkg.pdf")
    c2.Print(save_path+args.mode+"Significance.pdf")



if args.plotQ2:
    #Draw q2
    var_q2="q2"
    var_m="TMath::Sqrt(q2)"
    config="MuonConfig_forPhiFit.root"
    mcFile=loc.MARKOTUPLE+"MC12_"+append+config
    dataFile=loc.MARKOTUPLE+ "CL16_"+append+config
    mcTree=TChain(inMCtree)
    dataTree=TChain(tupleName)
    mcTree.AddFile(mcFile)
    dataTree.AddFile(dataFile)
    cut_q2_0=TCut("TMath::Abs(%s-1)< %.5f"%(var_m,5))
    binning=(100,0,3)
    mcQ2_0,dataQ2_0=generate_hist(mcTree,dataTree,var_q2,cut_q2_0,binning,"mcQ20","dataQ20")
    mcQ2_1,dataQ2_1=generate_hist(mcTree,dataTree,var_m,cut_q2_0,binning,"mcQ21","dataQ21")
    c=TCanvas("plot","plot")
    t=TLegend(0.6,0.6,0.9,0.9)
    t.AddEntry(mcQ2_0,"MC")
    t.AddEntry(dataQ2_0,"Data sideband")
    t.SetTextSize(0.04)
    mcQ2_0.SetMarkerColor(kRed)
    dataQ2_0.SetMarkerColor(kBlue)
    plt.hist_param(mcQ2_0,"q^{2}(#mu^{+}#mu^{-})i  [GeV^{2}/c^{4}]","Events")
    plt.hist_param(dataQ2_0,"q^{2}(#mu^{+}#mu^{-}) [GeV^{2}/c^{4}]","Events")
    dataQ2_0.Draw()
    mcQ2_0.Draw("same")
    t.Draw()
    c.Print(save_path+name+"Q2.pdf")
    t2=TLegend(0.6,0.6,0.9,0.9)
    t2.AddEntry(mcQ2_1,"MC")
    t2.AddEntry(dataQ2_1,"Data sideband")
    t2.SetTextSize(0.04)
    mcQ2_1.SetMarkerColor(kRed)
    dataQ2_1.SetMarkerColor(kBlue)
    plt.hist_param(mcQ2_1,"m(#mu^{+}#mu^{-})  [GeV/c^{2}]","Events")
    plt.hist_param(dataQ2_1,"m(#mu^{+}#mu^{-})  [GeV/c^{2}]","Events")
    dataQ2_1.Draw()
    mcQ2_1.Draw("same")
    t2.Draw()
    c.Print(save_path+name+"M.pdf")
    c.SetLogy()
    dataQ2_0.Draw()
    mcQ2_0.Draw("same")
    t.Draw()
    c.Print(save_path+name+"Q2Log.pdf")
    dataQ2_1.Draw()
    mcQ2_1.Draw("same")
    t2.Draw()
    c.Print(save_path+name+"MLog.pdf")
 
    #c,leg10,leg20=plt.plot_2_hists(mcQ2_0,dataQ2_0,"q2_3")
    #c2,leg11,leg21=plt.plot_2_hists(mcQ2_1,dataQ2_1,"q2_0.2")
    
    
