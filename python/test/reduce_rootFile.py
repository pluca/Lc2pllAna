from routines.LcAnalysis import loc, cuts, parseDatafiles
from ROOT import *
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--rootfilesID",default="Lc2pKpi_S28_beforeBDT")
parser.add_argument("--saveName",default="CL16_Lc2pKpi_S28_before_BDT_100k.root")
parser.add_argument("--tuplePath",default=loc.MARKOTUPLE)
parser.add_argument("--maxEvent",default=100000)
parser.add_argument("--cuts",default="")
args=parser.parse_args()

infile,intree = parseDatafiles(args.rootfilesID)
data = TChain(intree)
data.AddFile(args.tuplePath+infile)
print "Saving files to %s%s"%(args.tuplePath,args.saveName)
save_data = TFile(args.tuplePath+args.saveName,"recreate")
newData = data.CopyTree(args.cuts,"",int(args.maxEvent))
newData.Write()
save_data.Write()
save_data.Close()
print "Data saved ..." 
