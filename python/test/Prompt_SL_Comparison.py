import os
from routines.LcAnalysis import loc, cuts
from ROOT import *
from plotting_tools import compute_hist
tuplePath=loc.MARKOTUPLE

SLFile=loc.MARKOTUPLE+"CL16_SLLc2pmm_CutStripPID.root"
PromptFile=loc.MARKOTUPLE+"CL16_Lc2pmm_CutStripPID.root"

slTuple="Lc2pmmTuple_SL"
promptTuple="Lc2pmmTuple_mvaclone"

pTree=TChain(promptTuple)
pTree.AddFile(PromptFile)

slTree=TChain(slTuple)
slTree.AddFile(SLFile)

var="p_PX"
binning=(100,0,100000)

cut=TCut("TMath::Abs(TMath::Sqrt(q2)-1.01945)<0.035")
c=TCanvas()
pPT=compute_hist(pTree,var,binning,"PROMPT",cut+cuts.probNNmu_cut,"p P")
slPT=compute_hist(slTree,var,binning,"SL",cut+cuts.bdt_SL,"p P")
pPT.SetLineColor(kBlue)
slPT.SetLineColor(kRed)
t=TLegend(0.6,0.6,0.9,0.9)
t.AddEntry(pPT,"Prompt")
t.AddEntry(slPT,"SL")
pPT.Draw("hist")
slPT.Draw("same hist")
t.Draw()
var="p_PZ"
binning=(100,0,20000)
c2=TCanvas()
pP=compute_hist(pTree,var,binning,"Prompt P",cut+cuts.probNNmu_cut,"p PT")
slP=compute_hist(slTree,var,binning,"SL P",cut+cuts.bdt_SL,"#p PT")
pP.SetLineColor(kBlue)
slP.SetLineColor(kRed)
t2=TLegend(0.6,0.6,0.9,0.9)
t2.AddEntry(pP,"Prompt")
t2.AddEntry(slP,"SL")
pP.Draw("hist")
slP.Draw("same hist")
t2.Draw()


