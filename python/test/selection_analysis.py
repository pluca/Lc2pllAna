import os, sys
from routines.LcAnalysis import loc, cuts
import argparse

#Parse all arguments
parser=argparse.ArgumentParser()
parser.add_argument("-m","--mode",default="Prompt", help="Mode Prompt or SL")
parser.add_argument("--fitPID", action='store_true', help="Fit Lc2pKpi again: True or False")
parser.add_argument("--sendJobs",action='store_true')
args=parser.parse_args()
sl=args.mode

#p_PID analysis
#1) Option to recreate the files in the cpp/dat/ directory
if args.fitPID:
    path_dat=loc.REPO+"cpp/dat/%s/P_PID/*.dat"%sl
    path_ressource=loc.REPO+"cpp/ressource/Prompt*.root"
    os.system("rm %s"%path_dat)
    os.system("rm %s"%path_ressource)
os.system("python test/process_significance.py --mode %s"%sl)
#MVA training after p_PID cut added
if args.sendJobs:
    os.system("python generateMVAParam.py --sendJobs --mode %s"%sl)
#else:
#    os.system("python generateMVAParam.py --mode %s"%sl)
#os.system("python plotting/draw_BDT_LcMM.py")
