from routines.LcAnalysis import loc

base = loc.TUPLE
home = loc.REPO
tupleLocation = loc.MARKOTUPLE

from probNN_tools import cut_file
from ROOT import *
def load_one_tree(fileName, treeName):
    tree=TChain(treeName)
    tree.AddFile(fileName)
    return tree

import os
def load_tree(fileName, treeName,oldName,cut):  
    print fileName
    if os.path.isfile(fileName):
        tree=load_one_tree(fileName,treeName)
    else:
        oldFile=tupleLocation+oldName
        rawTree=TChain(treeName)
        rawTree.AddFile(oldFile)
        tree=cut_file(fileName,rawTree,cut)
    return tree
def LcL0_xlabel_ylabel_max(hist):
    hist.GetXaxis().SetBinLabel(1,"L0:Global_TOS")    
    hist.GetXaxis().SetBinLabel(2,"Electron_TOS")
    hist.GetXaxis().SetBinLabel(3,"Hadron_TOS")
    hist.GetXaxis().SetBinLabel(4,"Muon_TOS")
    hist.GetXaxis().SetBinLabel(5,"Photon_TOS")
    hist.GetXaxis().SetBinLabel(6,"Electron_TIS")
    hist.GetXaxis().SetBinLabel(7,"Photon_TIS")

    hist.GetYaxis().SetTitle("L0 Activation Efficiency")
    hist.SetMaximum(1.)
def LcSLL0_xlabel_ylabel_max(hist):
    hist.GetXaxis().SetBinLabel(1,"L0:Lb Global_TOS")    
    hist.GetXaxis().SetBinLabel(2,"Electron_TOS")
    hist.GetXaxis().SetBinLabel(3,"Hadron_TOS")
    hist.GetXaxis().SetBinLabel(4,"Muon_TOS")
    hist.GetXaxis().SetBinLabel(5,"Photon_TOS")
    hist.GetXaxis().SetBinLabel(6,"Electron_TIS")
    hist.GetXaxis().SetBinLabel(7,"Photon_TIS")
    hist.GetXaxis().SetBinLabel(8,"Lc Global_TOS")    
    hist.GetXaxis().SetBinLabel(9,"Electron_TOS")
    hist.GetXaxis().SetBinLabel(10,"Hadron_TOS")
    hist.GetXaxis().SetBinLabel(11,"Muon_TOS")
    hist.GetXaxis().SetBinLabel(12,"Photon_TOS")
    hist.GetXaxis().SetBinLabel(13,"Electron_TIS")
    hist.GetXaxis().SetBinLabel(14,"Photon_TIS")



    hist.GetYaxis().SetTitle("L0 Activation Efficiency")
    hist.SetMaximum(1.)

#    hist.Scale(1./hist.Integral())
def fill_LcL0(tree,Ntot,name):
    hist_LcL0=TH1F("L0"+name,"L0"+name,7,0,7)
    countGlobal=0
    countElectron=0
    countHadron=0
    countMuon=0
    countPhoton=0
    countETIS=0
    countPTIS=0
    for events in tree:
        Global=events.Lc_L0Global_TOS
        Electron=events.Lc_L0ElectronDecision_TOS
        Hadron=events.Lc_L0HadronDecision_TOS
        Muon=events.Lc_L0MuonDecision_TOS
        Photon=events.Lc_L0PhotonDecision_TOS
        ElectronTIS=events.Lc_L0ElectronDecision_TIS
        PhotonTIS=events.Lc_L0PhotonDecision_TIS
        if Global>0:
            countGlobal+=1
        if Electron>0:
            countElectron+=1
        if Hadron>0:
            countHadron+=1
        if Muon>0:    
            countMuon+=1
        if Photon>0:
            countPhoton+=1
        if ElectronTIS>0:
            countETIS+=1
        if PhotonTIS>0:
            countPTIS+=1
    print countGlobal, countElectron, countHadron, countMuon, countPhoton,countETIS ,countPTIS ,Ntot
    hist_LcL0.SetBinContent(1,float(countGlobal)/Ntot)
    hist_LcL0.SetBinContent(2,float(countElectron)/Ntot)
    hist_LcL0.SetBinContent(3,float(countHadron)/Ntot)
    hist_LcL0.SetBinContent(4,float(countMuon)/Ntot)
    hist_LcL0.SetBinContent(5,float(countPhoton)/Ntot)
    hist_LcL0.SetBinContent(6,float(countETIS)/Ntot)
    hist_LcL0.SetBinContent(7,float(countPTIS)/Ntot)
    LcL0_xlabel_ylabel_max(hist_LcL0)
    return hist_LcL0
def fill_SLLcL0(tree,Ntot,name):
    hist_LcL0=TH1F("L0"+name,"L0"+name,14,0,14)
#Lb
    countLbGlobal=0
    countLbElectron=0
    countLbHadron=0
    countLbMuon=0
    countLbPhoton=0
    countLbETIS=0
    countLbPTIS=0
#Lc
    countGlobal=0
    countElectron=0
    countHadron=0
    countMuon=0
    countPhoton=0
    countETIS=0
    countPTIS=0
    for events in tree: 
        GlobalLb=events.Lb_L0Global_TOS
        ElectronLb=events.Lb_L0ElectronDecision_TOS
        HadronLb=events.Lb_L0HadronDecision_TOS
        MuonLb=events.Lb_L0MuonDecision_TOS
        PhotonLb=events.Lb_L0PhotonDecision_TOS
        ElectronTISLb=events.Lb_L0ElectronDecision_TIS
        PhotonTISLb=events.Lb_L0PhotonDecision_TIS
 
        Global=events.Lc_L0Global_TOS
        Electron=events.Lc_L0ElectronDecision_TOS
        Hadron=events.Lc_L0HadronDecision_TOS
        Muon=events.Lc_L0MuonDecision_TOS
        Photon=events.Lc_L0PhotonDecision_TOS
        ElectronTIS=events.Lc_L0ElectronDecision_TIS
        PhotonTIS=events.Lc_L0PhotonDecision_TIS
        
        if GlobalLb>0:
            countLbGlobal+=1
        if ElectronLb>0:
            countLbElectron+=1
        if HadronLb>0:
            countLbHadron+=1
        if MuonLb>0:    
            countLbMuon+=1
        if PhotonLb>0:
            countLbPhoton+=1
        if ElectronTISLb>0:
            countLbETIS+=1
        if PhotonTISLb>0:
            countLbPTIS+=1
       
        if Global>0:
            countGlobal+=1
        if Electron>0:
            countElectron+=1
        if Hadron>0:
            countHadron+=1
        if Muon>0:    
            countMuon+=1
        if Photon>0:
            countPhoton+=1
        if ElectronTIS>0:
            countETIS+=1
        if PhotonTIS>0:
            countPTIS+=1
    print "Lb ", countLbGlobal, countLbElectron, countLbHadron, countLbMuon, countLbPhoton,countLbETIS ,countLbPTIS ,Ntot
    print "Lc ", countGlobal, countElectron, countHadron, countMuon, countPhoton,countETIS ,countPTIS ,Ntot
    hist_LcL0.SetBinContent(1,float(countLbGlobal)/Ntot)
    hist_LcL0.SetBinContent(2,float(countLbElectron)/Ntot)
    hist_LcL0.SetBinContent(3,float(countLbHadron)/Ntot)
    hist_LcL0.SetBinContent(4,float(countLbMuon)/Ntot)
    hist_LcL0.SetBinContent(5,float(countLbPhoton)/Ntot)
    hist_LcL0.SetBinContent(6,float(countLbETIS)/Ntot)
    hist_LcL0.SetBinContent(7,float(countLbPTIS)/Ntot)

    hist_LcL0.SetBinContent(8,float(countGlobal)/Ntot)
    hist_LcL0.SetBinContent(9,float(countElectron)/Ntot)
    hist_LcL0.SetBinContent(10,float(countHadron)/Ntot)
    hist_LcL0.SetBinContent(11,float(countMuon)/Ntot)
    hist_LcL0.SetBinContent(12,float(countPhoton)/Ntot)
    hist_LcL0.SetBinContent(13,float(countETIS)/Ntot)
    hist_LcL0.SetBinContent(14,float(countPTIS)/Ntot)
    LcSLL0_xlabel_ylabel_max(hist_LcL0)
    return hist_LcL0
def hlt1_param(hist):
    hist.GetXaxis().SetBinLabel(1,"Hlt1:Global")
    hist.GetXaxis().SetBinLabel(2,"Phys")
    hist.GetXaxis().SetBinLabel(3,"Track All L0")
    hist.GetXaxis().SetBinLabel(4,"Track Muon")
    hist.GetYaxis().SetTitle("Hlt1 Activation Efficiency")
    hist.SetMaximum(1)
           
def fill_hlt1(tree,Ntot,name):
    hist_hlt1=TH1F("Hlt1"+name,"Hlt1"+name,4,0,4)
    countGlobal=0
    countPhys=0
    countAllL0=0
    countMuon=0
    for events in tree:
        Global=events.Lc_Hlt1Global_TOS
        Phys=events.Lc_Hlt1Phys_TOS
        AllL0=events.Lc_Hlt1TrackAllL0Decision_TOS
        Muon=events.Lc_Hlt1TrackMuonDecision_TOS
        if Global>0:
            countGlobal+=1
        if Phys>0:
            countPhys+=1
        if AllL0>0:
            countAllL0+=1
        if Muon>0:
            countMuon+=1
    print "Hlt1:", countGlobal, countPhys, countAllL0, countMuon, Ntot
    hist_hlt1.SetBinContent(1,float(countGlobal)/Ntot)
    hist_hlt1.SetBinContent(2,float(countPhys)/Ntot)
    hist_hlt1.SetBinContent(3,float(countAllL0)/Ntot)
    hist_hlt1.SetBinContent(4,float(countMuon)/Ntot)
    hlt1_param(hist_hlt1)
    return hist_hlt1

def draw_trigger(hist1,name1,hist2,name2,name):
    c=TCanvas(name,name,1800,600)
    c.Divide(2)
    c.cd(1)
    t=TLegend(0.6,0.7,0.9,0.9)
    t.AddEntry(hist1,name1)
    hist1.Draw()
    t.Draw("same")
    c.cd(2)
    t2=TLegend(0.6,0.7,0.9,0.9)
    t2.AddEntry(hist2,name2)
    hist2.Draw()
    t2.Draw("same")
    return c,t,t2
def hlt2_param(hist,mu):
    hist.GetXaxis().SetBinLabel(1,"Hlt2:Global")
    hist.GetXaxis().SetBinLabel(2,"Phys")
    hist.GetXaxis().SetBinLabel(3,"Topo2Body")
    hist.GetXaxis().SetBinLabel(4,"Topo3Body")
    hist.GetXaxis().SetBinLabel(5,"Topo4Body")
    hist.GetXaxis().SetBinLabel(6,"Lc2PMuMu")
    if mu:
        hist.GetXaxis().SetBinLabel(7,"Muon2Body")
        hist.GetXaxis().SetBinLabel(8,"Muon3Body")
        hist.GetXaxis().SetBinLabel(9,"Muon4Body")
    hist.GetYaxis().SetTitle("Hlt2 Activation Efficiency")
    hist.SetMaximum(1)

def fill_hlt2(tree,Ntot,name,mu=False):
    binning=6
    start=0
    end=0
    if mu:
        binning=9
        start=0
        end=9
    hist=TH1F("Hlt2"+name,"Hlt2"+name,binning,start,end)
    countGlobal=0
    countPhys=0
    countTopo2B=0
    countTopo3B=0
    countTopo4B=0
    countMu2=0
    countMu3=0
    countMu4=0
    countPMuMu=0
    for events in tree:
        Global=events.Lc_Hlt2Global_TOS
        Phys=events.Lc_Hlt2Phys_TOS
        Topo2B=events.Lc_Hlt2Topo2BodyDecision_TOS
        Topo3B=events.Lc_Hlt2Topo3BodyDecision_TOS
        Topo4B=events.Lc_Hlt2Topo4BodyDecision_TOS
        if mu:
            Mu2=events.Lc_Hlt2TopoMu2BodyDecision_TOS
            Mu3=events.Lc_Hlt2TopoMu3BodyDecision_TOS
            Mu4=events.Lc_Hlt2TopoMu4BodyDecision_TOS
        PMuMu=events.Lc_Hlt2RareCharmLc2PMuMuDecision_TOS
        if Global>0:
            countGlobal+=1
        if Phys>0:
            countPhys+=1
        if Topo2B>0:
            countTopo2B+=1
        if Topo3B>0:
            countTopo3B+=1
        if Topo4B>0:
            countTopo4B+=1
        if mu:
            if Mu2>0:
                countMu2+=1
            if Mu3>0:
                countMu3+=1
            if Mu4>0:
                countMu4+=1
        if PMuMu>0:
            countPMuMu+=1
    print "Hlt2:",countGlobal, countPhys, countTopo2B, countTopo3B, countTopo4B, countMu2,countMu3, countMu4, countPMuMu, Ntot
    hist.SetBinContent(1,float(countGlobal)/Ntot)
    hist.SetBinContent(2,float(countPhys)/Ntot)
    hist.SetBinContent(3,float(countTopo2B)/Ntot)
    hist.SetBinContent(4,float(countTopo3B)/Ntot)
    hist.SetBinContent(5,float(countTopo4B)/Ntot)
    hist.SetBinContent(6,float(countPMuMu)/Ntot)
    if mu:
        hist.SetBinContent(7,float(countMu2)/Ntot)
        hist.SetBinContent(8,float(countMu3)/Ntot)
        hist.SetBinContent(9,float(countMu4)/Ntot)
    hlt2_param(hist,mu)
    return hist

