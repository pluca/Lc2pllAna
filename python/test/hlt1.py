import os
from routines.LcAnalysis import loc, hlt1_Prompt, hlt1_SL, dumpHLT, dumpAll
from ROOT import *
import eventtuple_tools as et
import plotting_tools as plt

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--mode',default = 'Prompt')
args = parser.parse_args()

if args.mode == 'Prompt':
    pmm = 'MCLc2pmm'
    pKpi = 'MCLc2pKpi'
else :
    pmm = 'MCSLLc2pmm'
    pKpi = 'MCSLLc2pKpi'

MCpmm = et.compute_tree(pmm,'Lc2pmmTuple/DecayTree')
MCpKpi = et.compute_tree(pKpi,'Lc2pKpiTuple/DecayTree')
################ HLT1 ####################

def make_tos(var):
    return 'Lc_'+var+'_TOS'
def make_tis(var):
    return 'Lc_'+var+'_TIS'


hlt1_tos = ['Hlt1Global',
            'Hlt1Phys',
            'Hlt1TrackAllL0Decision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1SingleMuonHighPTDecision',
            'Hlt1SingleMuonDecision']

hlt1_tis = ['Hlt1Global',
            'Hlt1Phys']
Npmm = MCpmm.GetEntries()
GLOBAL = make_tos('Hlt1Global')
Nglobpmm = MCpmm.GetEntries(GLOBAL)

NpKpi = MCpKpi.GetEntries()
NglobpKpi = MCpKpi.GetEntries(GLOBAL)


save_dict = {}

for c in hlt1_tos:
    selection = make_tos(c) 
    print "Selection ",selection
    events = MCpmm.GetEntries(selection)
    print Npmm, events
    dic={}
    eff,err = et.compute_eff(events,Npmm)
    eff_tex, err_tex ,order_tex = plt.scientific_notation(eff,err)
    events_pKpi = MCpKpi.GetEntries(selection)
    eff_pKpi, err_pKpi = et.compute_eff(events_pKpi,NpKpi)
    eff_pKpi_tex, err_pKpi_tex, order_pKpi_tex = plt.scientific_notation(eff_pKpi,err_pKpi)
    dic = {'Lc2pmm': (eff_tex, err_tex ,order_tex), 'Lc2pKpi': (eff_pKpi_tex, err_pKpi_tex ,order_pKpi_tex)}
    if args.mode=='Prompt':
        hlt1_Prompt[c]=dic
    else :
        hlt1_SL[c]=dic
print save_dict


for c in hlt1_tis:
    selection = make_tis(c)
    print "Selection ", selection
    events = MCpmm.GetEntries(selection)
    events_pKpi = MCpKpi.GetEntries(selection)
    print Npmm,events, NpKpi,events_pKpi, 
    eff,err = et.compute_eff(events,Npmm)
    eff_pKpi,err_pKpi = et.compute_eff(events_pKpi,NpKpi)
    eff_tex, err_tex, order_tex = plt.scientific_notation(eff,err)
    eff_pKpi_tex, err_pKpi_tex, order_pKpi_tex = plt.scientific_notation(eff_pKpi,err_pKpi)
    dic = {'Lc2pKpi':(eff_pKpi_tex, err_pKpi_tex, order_pKpi_tex), 'Lc2pmm':(eff_tex, err_tex, order_tex)}
    if args.mode=='Prompt':
        hlt1_Prompt[c+'TIS']=dic 
    else:
        hlt1_SL[c+'TIS'] = dic
print "Hlt1 : ", hlt1_Prompt
dumpAll()
os.system("python test/fill_template.py")
