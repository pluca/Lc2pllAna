import os
from routines.LcAnalysis import loc
from multiprocessing import Pool
from functools import partial
from ROOT import *

import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--KROC",action="store_true")
parser.add_argument("--PiROC",action="store_true")
parser.add_argument("--plotKROC",action="store_true")
parser.add_argument("--plotPiROC",action="store_true")
parser.add_argument("--force",action="store_true")
args=parser.parse_args()

ressource=loc.REPO+"cpp/dat/Prompt/ROC/"
save_path=os.path.expanduser('~')+"/report/ROC/"
import numpy as np
def generateFileName(cutK,cutPi):
    ck=cutK.split(' ')
    cpi=cutPi.split(' ')
    name="%s%.6f%s%.6f"%(ck[0][1:],float(ck[2]),cpi[0][1:],float(cpi[2]))+"_variables.dat"
    return name

def process_exec(cutK,cutPi):
    os.chdir(loc.REPO+"cpp/")    
    cpp_exec="./ROC_optimisation.out %s %s"%(cutK,cutPi)
    print "Testing the following process : ", cpp_exec
    if args.force:
        os.system(cpp_exec)
        print "Running ...!"
    if not os.path.isfile(ressource+generateFileName(cutK,cutPi)):
        os.system(cpp_exec)
        print "Running ... ! "
    else:
        print "File already found "
def multi_processK(proc,cK,cPi):
    pool=Pool()
    funcK=partial(proc,cutPi=cutPi[0])
    pool.map(funcK,cK)
    pool.close()
    pool.join()

def multi_processPi(proc,cK,cPi):
    pool=Pool()
    funcPi=partial(proc,cK[0])
    pool.map(funcPi,cPi)
    pool.close()
    pool.join()

def generate_options(mode,cut,kaon=True):
    if kaon:
        typed="-KCUT"
    else:
        typed="-PiCUT"
    cutted="%s %s %.6f"%(mode,typed,cut)
    return cutted

def gen_vec(mode,cut,kaon=True):
    vec=[]
    vecName=[]
    for c in cut:
        vec.append(generate_options(mode,c,kaon))
        vecName.append("%s%.6f"%(mode[1:],c))
    return vec,vecName

def fileNames(nameK,namePi,Kaon=True):
    vecNames=[]
    if Kaon:
        for n in nameK:
            vecNames.append(n+namePi[0]+"_variables.dat")
    else:
        for n in namePi:
            vecNames.append(nameK[0]+n+"_variables.dat")
    return vecNames

def readFiles(name):
    NSig=[]
    NBkg=[]
    KCut=[]
    PiCut=[]
    for n in name:
        listF=open(ressource+n).read()
        listF=listF.split(' ')
        KCut.append(float(listF[0]))
        PiCut.append(float(listF[1]))
        NSig.append(float(listF[2]))
        NBkg.append(float(listF[3]))
    return np.array(KCut),np.array(PiCut),np.array(NSig),np.array(NBkg)
def find0_values():
    name=ressource+"K0.000000Pi0.000000_variables.dat"
    listF=open(name).read()
    listF=listF.split(' ')
    return np.array(float(listF[2])),np.array(float(listF[3]))
import plotting_tools as plt
from math import sqrt
def compute_eff_errors(N,NTot):
    eff=N/NTot
    err=eff*(np.sqrt(1./N+1./NTot))
    return eff, err
def compute_ROC(name,histName):
    NSTot,NBkgTot=find0_values()
    KCut,PiCut,NSig,NBkg=readFiles(name)
    effSig,errSig=compute_eff_errors(NSig,NSTot)
    effBkg,errBkg=compute_eff_errors(NBkg,NBkgTot)
    rejBkg=1-effBkg
    hist=TGraph(len(effSig),effSig,rejBkg)
    print histName, "  ",NSTot, NBkgTot
    for i in range(0,len(NSig)):
        print "KCut :",KCut[i], "PiCut :",PiCut[i],"NSig :",NSig[i]/NSTot," NBkg :" ,1-NBkg[i]/NBkgTot, " Nsig : ",NSig[i] , " NBkg :", NBkg[i]
    
    plt.hist_param(hist,"Signal efficiency","Backgroudn rejection")
    return hist
cK=np.arange(0,1.01,0.01)
cPi=np.arange(0,1.01,0.01)

modeKaon=["-K","-KNotPi","-KNotPiNotP"]
modePion=["-Pi","-PiNotK","-PiNotKNotP"]

cutK,nameK=gen_vec(modeKaon[0],cK)
cutKNotPi,nameKNotPi=gen_vec(modeKaon[1],cK)
cutKNotPiNotP,nameKNotPiNotP=gen_vec(modeKaon[2],cK)

cutPi,namePi=gen_vec(modePion[0],cPi,False)
cutPiNotK,namePiNotK=gen_vec(modePion[1],cPi,False)
cutPiNotKNotP,namePiNotKNotP=gen_vec(modePion[2],cPi,False)


if args.KROC:
    multi_processK(process_exec,cutK,cutPi)
    multi_processK(process_exec,cutKNotPi,cutPi)
    multi_processK(process_exec,cutKNotPiNotP,cutPi)
if args.PiROC:
    multi_processPi(process_exec,cutK,cutPi)
    multi_processPi(process_exec,cutK,cutPiNotK)
    multi_processPi(process_exec,cutK,cutPiNotKNotP)

if args.plotKROC:
   fileNameK=fileNames(nameK,namePi)
   fileNameKNotPi=fileNames(nameKNotPi,namePi)
   fileNameKNotPiNotP=fileNames(nameKNotPiNotP,namePi)
   histK=compute_ROC(fileNameK,"Hist_K")
   histKNotPi=compute_ROC(fileNameKNotPi,"Hist_KNotPi")
   histKNotPiNotP=compute_ROC(fileNameKNotPiNotP,"Hist_KNotPiNotP")
   histK.SetLineColor(kRed)
   histK.SetLineWidth(4)
   histKNotPi.SetLineColor(kBlue)
   histKNotPi.SetLineWidth(4)
   histKNotPiNotP.SetLineColor(kBlack)
   histKNotPiNotP.SetLineWidth(4)
   t=TLegend(0.2,0.2,0.5,0.5)
   t.SetTextSize(0.05)
   t.AddEntry(histK,"K ProbNNK","l")
   t.AddEntry(histKNotPi,"K ProbNNK*(1-K ProbNN#pi)","l")
   t.AddEntry(histKNotPiNotP,"K ProbNNK*(1-K ProbNN#pi)*(1- KProbNNp) ","l")
   c=TCanvas("ROC K","ROCK")
   plt.draw_graph(c,histKNotPiNotP,"AC")
   plt.draw_graph(c,histK,"C")
   plt.draw_graph(c,histKNotPi,"C")
   t.Draw()
   c.SaveAs(save_path+"ROC_Kaon.pdf")
   print float(histK.Integral()),histKNotPi.Integral(),histKNotPiNotP.Integral()
if args.plotPiROC:
   fileNamePi=fileNames(nameK,namePi,False)
   fileNamePiNotK=fileNames(nameK,namePiNotK,False)
   fileNamePiNotKNotP=fileNames(nameK,namePiNotKNotP,False)
   histPi=compute_ROC(fileNamePi,"Hist_K")
   histPiNotK=compute_ROC(fileNamePiNotK,"Hist_KNotPi")
   histPiNotKNotP=compute_ROC(fileNamePiNotKNotP,"Hist_KNotPiNotP")
   histPi.SetLineColor(kRed)
   histPi.SetLineWidth(4)
   histPiNotK.SetLineColor(kBlue)
   histPiNotK.SetLineWidth(4)
   histPiNotKNotP.SetLineColor(kBlack)
   histPiNotKNotP.SetLineWidth(4)
   t=TLegend(0.2,0.2,0.5,0.5)
   t.SetTextSize(0.05)
   t.AddEntry(histPi,"#pi ProbNN#pi","l")
   t.AddEntry(histPiNotK,"#pi ProbNN#pi*(1-#pi ProbNN#pi)","l")
   t.AddEntry(histPiNotKNotP,"#pi ProbNN#pi*(1-#pi ProbNN#pi)*(1-#pi ProbNNp)","l")
   c=TCanvas("ROC Pi","ROC Pi")
   plt.draw_graph(c,histPi,"AC")
   plt.draw_graph(c,histPiNotK,"C")
   plt.draw_graph(c,histPiNotKNotP,"C")
   t.Draw()
   c.SaveAs(save_path+"ROC_Pion.pdf")
   print histPi.Integral(),histPiNotK.Integral(),histPiNotKNotP.Integral()





