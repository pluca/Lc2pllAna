import os
import argparse
import subprocess
import re
from routines.LcAnalysis import db , effForTex, dumpAll, loc
import tools.plotting_tools as plt
parser = argparse.ArgumentParser()
parser.add_argument("--Mag",default = 'MagUp')
parser.add_argument("--Vars",default = 'P')
args = parser.parse_args()

def summed(e1,e2):
    print "E1 ",e1,"E2 ",e2
    eff = (e1[0]+e2[0])
    err = e1[1] + e2[1]
    return eff,err
def divided(mean):
    return (mean[0]/2,mean[1])
def find_eff(cmd):
    output = subprocess.check_output(cmd,shell=True)
    val = re.findall("(\d+\.\d+)",output)
    eff = float(val[len(val)-2])/100
    err = float(val[len(val)-1])/100
    return (eff,err)
os.chdir(loc.PID+"PerfHistsMuon/")
Magnet = ["MagUp","MagDown"]
Variables = { 'P' : "'[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]'",
              'L' : "'[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'",
              'Both' :"'[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]' '[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'"
            }

if args.Vars == 'P':
    var = "'[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]'"
elif args.Vars =='L':
    var = "'[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'"
else:
    var = " '[p,P,Brunel_MC15TuneV1_ProbNNp > 0.805]' '[L1,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]' '[L2,Mu,Brunel_MC15TuneV1_ProbNNmu > 0.225]'"

path= os.getenv("PIDPERFSCRIPTSROOT")
tuplePath = os.getenv("MARKOTUPLELOCATION")

infile = {'low':'/Low_Phi_Mass.root','in':'/Phi_Mass.root','high':'/High_Phi_Mass.root', 'full' : '/MC16_Lc2pmm_PIDAndBDT_reduced_PID-corrected_new.root'}
intree = "Lc2pmmTuple_Prompt"

path += "/scripts/python/MultiTrack/PerformMultiTrackCalib.py "
test={}
for var in Variables:
    forDB = {}
    forTex = {}
    for inf in infile:
        mean_eff = (0,0)
        for m in Magnet:
            cmd = "python %s 'Turbo16' '%s' '%s' '%s' 'MyResults.root' %s -X 'P' -Y 'ETA' -Z '' "%(path,m,tuplePath+infile[inf],intree,Variables[var])
            Eff = find_eff(cmd)
            mean_eff = summed(Eff,mean_eff)
            print var, m,inf, Eff
        mean_eff = divided(mean_eff)
        forDB[inf]=mean_eff
        forTex[inf]=plt.scientific_notation(mean_eff[0],mean_eff[1])
    db['PIDCalib_'+var]=forDB
    effForTex['PIDCalib_'+var]=forTex


print db
#dumpAll()

