import os
from routines.LcAnalysis import loc, cuts, parseDatafiles, db, trigger, full_eff, effForTex,full_effForTex, dumpAll
from ROOT import *
######### Import file, generate list of cuts
from math import sqrt
from tools.plotting_tools import find_order, scientific_notation
mass = 1.01946
   
def multiply_eff(e1,e2):
    eff = e1[0]*e2[0]
    err = eff * sqrt((e1[1]/e1[0])**2+(e2[1]/e2[0])**2)
    return (eff,err)


dtype = 'MCLc2pmm'
dtypeID = 'MCLc2pmm_S28_corrected'

inMCfile, inMCtree = parseDatafiles(dtypeID)
mcTree = TChain(inMCtree)
mcTree.AddFile(loc.TUPLELOCATION+inMCfile)

q2True="(TMath::Power((TMath::Sqrt(105.65836*105.65836 + L1_TRUEP_Z*L1_TRUEP_Z+ L1_TRUEP_X*L1_TRUEP_X + L1_TRUEP_Y*L1_TRUEP_Y)+TMath::Sqrt(105.65836*105.65836 + L2_TRUEP_Z*L2_TRUEP_Z + L2_TRUEP_Y*L2_TRUEP_Y +  L2_TRUEP_X*L2_TRUEP_X)),2) - (TMath::Power(L1_TRUEP_X+L2_TRUEP_X,2) +TMath::Power(L1_TRUEP_Y+L2_TRUEP_Y,2) + TMath::Power(L1_TRUEP_Z+L2_TRUEP_Z,2)))/1000000."




cutsList = {'Strip':TCut("1"),'Convergence':cuts.convergence2, 'Strip+BDT':cuts.convergence2 + cuts.bdt_cut,'Strip+BDT+L0':cuts.convergence2 + cuts.bdt_cut + cuts.triggers,'Strip+BDT+L0+p_PID':cuts.convergence2 + cuts.bdt_cut + cuts.triggers + cuts.p_PID_corr,'Strip+BDT+L0+p_PID+mu_PID':cuts.convergence2 + cuts.bdt_cut + cuts.triggers + cuts.p_PID_corr + cuts.probNN_mu_corr,'full': cuts.full_selection_MC , 'No Triggers': cuts.convergence2 + cuts.bdt_cut, "ProbNNp": cuts.convergence2 + cuts.bdt_cut + cuts.p_PID_corr, "ProbNNmu" : cuts.convergence2 + cuts.bdt_cut + cuts.p_PID_corr + cuts.probNN_mu_corr, 'L0': cuts.convergence2 + cuts.bdt_cut + cuts.L0_triggers , 'L0+Hlt1' : cuts.convergence2 + cuts.bdt_cut  + cuts.L0_triggers + cuts.Hlt1_triggers , 'L0+Hlt1+Hlt2' : cuts.convergence2 + cuts.bdt_cut + cuts.L0_triggers + cuts.Hlt1_triggers + cuts.Hlt2_triggers, 'full_NoPID' : cuts.convergence2 + cuts.bdt_cut + cuts.L0_triggers + cuts.Hlt1_triggers + cuts.Hlt2_triggers}

nameList = {'Strip':'Strip',
            'Convergence': 'Strip',
            'Strip+BDT': 'Strip',
            'Strip+BDT+L0':'Strip+BDT',
            'Strip+BDT+L0+p_PID': 'Strip+BDT+L0',
            'Strip+BDT+L0+p_PID+mu_PID':'Strip+BDT+L0+p_PID',
            'full':'Strip+BDT+L0+p_PID',
            'No Triggers':'Strip',
            'ProbNNp':'Strip+BDT',
            'ProbNNmu':'Strip+BDT',
            'L0':'No Triggers',
            'L0+Hlt1':'L0',
            'L0+Hlt1+Hlt2': 'L0+Hlt1',
            'full_NoPID':'Strip',
            }


print "******************************** Full efficiency on the normalisation decay *******************************"
mass_regions = {'low':cuts.low_mass_phi,'in': cuts.in_mass_phi,'high':cuts.high_mass_phi}
mass_regions_true = {'low':TCut("TMath::Sqrt(%s)< %f - 0.035"%(q2True,mass)), 'in':TCut("TMath::Abs(TMath::Sqrt(%s)-%f) < 0.035"%(q2True,mass)), 'high':TCut("TMath::Sqrt(%s) > %f + 0.035"%(q2True,mass))}
import tools.eventtuple_tools as et
NTot={}
for m in mass_regions_true:
    NTot[m]=et.findNTot(dtype,mass_regions_true[m],"MCTuple/MCDecayTree")
print NTot
for c in cutsList:
    effDict = {}
    texDict = {}
    print
    print "Cut :",c,"NTot :", NTot
    for m in mass_regions:
        N = mcTree.GetEntries((cutsList[c]+mass_regions[m]).GetTitle())
        print m,N
        eff, err = et.compute_eff(N,NTot[m])
        tex_eff,tex_err,tex_order = scientific_notation(eff,err)
        effDict[m]=(eff,err)
        texDict[m]=(tex_eff,tex_err,tex_order)
    print "Low :", effDict['low'] , 'in',effDict['in'], 'high',effDict['high']
    db[c]={'low':effDict['low'],'in':effDict['in'],'high':effDict['high']}
    effForTex[c]={'low':texDict['low'],'in':texDict['in'],'high':texDict['high']}
print db
print effForTex
print "******************************** Full efficiency on the rare decay *******************************"
NFull=0
veto_list = { 'Phi':cuts.blind_phi ,'Omega':cuts.blind_omega, 'Eta':cuts.blind_eta, 'All': cuts.blind_phi + cuts.blind_omega + cuts.blind_eta}
for key in NTot:
    NFull+=NTot[key]
print NFull
N=mcTree.GetEntries((cutsList['full_NoPID']+cuts.blind_phi+cuts.blind_omega+cuts.blind_eta).GetTitle())
Nselection=mcTree.GetEntries(cutsList['full_NoPID'].GetTitle())
eff,err = et.compute_eff(N,NFull)
eff_tex,err_tex,order_tex = scientific_notation(eff,err)
eff2,err2 = et.compute_eff(Nselection,NFull)
eff2_tex,err2_tex,order2_tex = scientific_notation(eff2,err2)
print "Full efficiency on the rare channel : " ,(eff,err)," N NFull NTot Nselection",N, NFull, NTot, Nselection
full_eff['rare']=(eff,err)
full_effForTex['rare']=(eff_tex,err_tex,order_tex)
full_eff['selection_only']=(eff2,err2)
full_effForTex['selection_only']=(eff2_tex,err2_tex,order2_tex)
for key in veto_list:
    Nv = mcTree.GetEntries((veto_list[key]+cutsList['full_NoPID']).GetTitle())
    effv, errv = et.compute_eff(Nv,NFull)
    print "Key Nv NFull effv, errv" ,key, Nv, NFull, effv, errv
    effv_only, errv_only = et.compute_eff_err((effv,errv),full_eff['selection_only'])
    full_eff[key]=(effv_only,errv_only)
    print effv_only, errv_only
    effv_only_tex, errv_only_tex, order_tex = scientific_notation(effv_only,errv_only)
    full_effForTex[key]=(effv_only_tex, errv_only_tex, order_tex)

print full_effForTex



print "******************************** Triggers effciciency *******************************"
############ Triggers
trigger['trigger']={}
for key in db['full']:
    eff_trig,err_trig = et.compute_eff_err(db['full'][key],db['No Triggers'][key])
    eff_trig_tex,err_trig_tex,order_trig_tex = scientific_notation(eff_trig,err_trig)
    print "Full", db['full'][key],"No triggers",db['No Triggers'][key]
    print "Triggers key :", key , (eff_trig,err_trig)
    trigger['trigger'][key] =(eff_trig_tex,err_trig_tex,order_trig_tex)
for selection in db:
    if 'full' not in selection and selection!='Strip' and selection!='PIDGen' and selection != 'PIDCalib_P' and selection != 'PIDCalib_L' and selection != 'PIDCalib_Both' and selection != 'FinalPIDCorr':
        for key in db[selection]:
            print selection, nameList[selection], key
            efficiency, error = et.compute_eff_err(db[selection][key],db[nameList[selection]][key])
            print efficiency, error
            efficiency_tex, error_tex, order_tex = scientific_notation(efficiency, error)
            print efficiency_tex, error_tex, order_tex
            effForTex[selection][key]=(efficiency_tex, error_tex, order_tex)
print NFull, mcTree.GetEntries(), NTot
tmp = {}
tmp_forTex = {}
for key in db['full_NoPID']:
    Eff = multiply_eff(db['full_NoPID'][key],db['PIDCalib_Both'][key])
    tmp[key] = Eff
    tmp_forTex[key] = scientific_notation(Eff[0],Eff[1])
    
db['FinalPIDCorr'] = tmp
effForTex['FinalPIDCorr'] = tmp_forTex 

tmp_rare = multiply_eff(full_eff['rare'], db['PIDCalib_Both']['full'])

full_eff['PIDCalib_Both'] = db['PIDCalib_Both']['full']
full_effForTex['PIDCalib_Both'] = scientific_notation(db['PIDCalib_Both']['full'][0],db['PIDCalib_Both']['full'][1])
full_eff['rare'] = tmp_rare
full_effForTex['rare'] = scientific_notation(tmp_rare[0],tmp_rare[1])
print "Efficiency rare + veto + PIDCalib corrected " , tmp_rare
print effForTex['FinalPIDCorr']
#dumpAll()
#os.system("python test/fill_template.py")
