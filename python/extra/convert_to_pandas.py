import os, sys, pickle, pandas
from ROOT import *
from variables_DB import MVAvariables as vars
from LcAnalysis import loc
import random
from sklearn_routines import *

base = loc.TUPLE
sigfile = base+"MC12_Lc_pKpi_reformat.root"
bkgfile = base+"Lc2pKpi_sW.root"
print "Signal file:     ", sigfile
print "Background file :", bkgfile

sigtree = TChain("DecayTree")
sigtree.AddFile(sigfile)
bkgtree = TChain("Lc2pKpi_sW")
bkgtree.AddFile(bkgfile)

def getArray(tree,myvars,label) :

    out = []
    for i in range(tree.GetEntries()) :
        entry = [label]
        tree.GetEntry(i)
        for iv,v in enumerate(myvars) :
            formula = TTreeFormula(v,v,tree)
            entry.append(formula.EvalInstance())
        out.append(tuple(entry))
        if i >= 10000 : return out
    return out

sigvars = getArray(sigtree,vars,1)
bkgvars = getArray(bkgtree,vars,0)
data = sigvars+bkgvars
random.shuffle(data) 
df = pandas.DataFrame(data, columns = ['myclass']+vars)

#print df.head()
#pickle.dump(df,open("Datasets_for_sklearn_training.pkl","w"))





bg = df.myclass < 0.5
sig = df.myclass > 0.5

signal_background(df[bg], df[sig], column=vars, bins=20)

correlations(df[bg].drop('myclass', 1))
correlations(df[sig].drop('myclass', 1))





from sklearn.cross_validation import train_test_split

X_dev,X_eval, y_dev,y_eval = train_test_split(X, y,
                                              test_size=0.33, random_state=42)
X_train,X_test, y_train,y_test = train_test_split(X_dev, y_dev,
                                                  test_size=0.33, random_state=492)

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import classification_report, roc_auc_score

dt = DecisionTreeClassifier(max_depth=3,
                            min_samples_leaf=0.05*len(X_train))
bdt = AdaBoostClassifier(dt,
                         algorithm='SAMME',
                         n_estimators=800,
                         learning_rate=0.5)

bdt.fit(X_train, y_train)

y_predicted = bdt.predict(X_test)
print classification_report(y_test, y_predicted,
                            target_names=["background", "signal"])
print "Area under ROC curve: %.4f"%(roc_auc_score(y_test,
                                                  bdt.decision_function(X_test)))


from sklearn.metrics import roc_curve, auc

decisions = bdt.decision_function(X_test)
fpr, tpr, thresholds = roc_curve(y_test, decisions)
roc_auc = auc(fpr, tpr)

plt.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)'%(roc_auc))

plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.grid()
plt.show()


