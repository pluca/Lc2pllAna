import random

import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import classification_report, roc_auc_score

import pandas.core.common as com
from pandas.core.index import Index
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix

#from root_numpy import root2array, rec2array

#signal = root2array("/tmp/HIGGS-signal.root","tree",branch_names)
#signal = rec2array(signal)

#backgr = root2array("/tmp/HIGGS-background.root","tree",branch_names)
#backgr = rec2array(backgr)

#X = np.concatenate((signal, backgr))
#y = np.concatenate((np.ones(signal.shape[0]),
#                        np.zeros(backgr.shape[0])))


def signal_background(data1, data2, column=None, grid=True,
        xlabelsize=None, xrot=None, ylabelsize=None,
        yrot=None, ax=None, sharex=False,
        sharey=False, figsize=None,
        layout=None, bins=10, **kwds):



