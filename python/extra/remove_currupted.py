from glob import glob
from ROOT import *
import os

data = TChain("Lc2pmmTuple/DecayTree")
files = glob("/eos/lhcb/user/p/pluca/ganga/167/*/DVNtuples.root")

for f in files:
    ff = TFile(f)
    if ff.IsZombie() :
        print "Zombie"
        print "Removing", f
        os.remove(f)
        continue 
    if ff.TestBit(TFile.kRecovered) :
        print "Recovered"
        print "Removing", f
        os.remove(f)
        continue
