import tools.trigger_tools as tt
from routines.LcAnalysis import cuts,parseDatafiles, loc
import os
save_path=loc.PLOTS+'/TRIGGERS/'
from ROOT import *
import argparse
parser=argparse.ArgumentParser()
parser.add_argument("--mode",default="Prompt")
args=parser.parse_args()
if args.mode=="Prompt":
    oldLc2pmm,tupleLc2pmm=parseDatafiles('MCLc2pmm_S28_PIDAndBDT')
    oldLc2pKpi,tupleLc2pKpi=parseDatafiles('MCLc2pKpi_S28_PIDAndBDT')
    tupleLocation=loc.TUPLELOCATION
    fileLc2pmm=tupleLocation+"MC16_Lc2pmm_forTriggers.root"
    fileLc2pKpi=tupleLocation+"MC16_Lc2pKpi_forTriggers.root"
    fullCut= cuts.config_forTriggers
else:
    oldLc2pmm,tupleLc2pmm=parseDatafiles('MCSLLc2pmm_S28_PIDAndBDT')
    oldLc2pKpi,tupleLc2pKpi=parseDatafiles('MCSLLc2pKpi_S28_PIDAndBDT')
    tupleLocation=loc.TUPLELOCATION
    fileLc2pmm=tupleLocation+"MC16_SLLc2pmm_forTriggers.root"
    fileLc2pKpi=tupleLocation+"MC16_SLLc2pKpi_forTriggers.root"
    fullCut= cuts.config_forTriggers_SL
'''
    fileLc2pmm=tt.tupleLocation+"MC12_SLLc2pmm_CutStripPIDAndBDT.root"
    fileLc2pKpi=tt.tupleLocation+"MC12_SLLc2pKpi_CutStripPIDAndBDT.root"
    tupleLc2pmm="Lc2pmmTuple_SL"
    tupleLc2pKpi="Lc2pKpiTuple_SL"
    oldLc2pmm="MC12_SLLc2pmm_CutStripPID.root"
    oldLc2pKpi="MC12_SLLc2pKpi_CutStripPID.root"
    fullCut=cuts.config_forTriggers_SL'''

#********* Load Trees for trigger analysis *****************
pKpiTree=tt.load_tree(fileLc2pKpi,tupleLc2pKpi,oldLc2pKpi,fullCut + cuts.match_Lc2pKpi_Full)
pmmTree=tt.load_tree(fileLc2pmm,tupleLc2pmm,oldLc2pmm,fullCut + cuts.match_Lc2pmm_Full)

Ntot_Lc2pKpi=pKpiTree.GetEntries()
Ntot_Lc2pmm=pmmTree.GetEntries()

#********* L0 generation for pKpi and pmm ******************
if args.mode=="Prompt":
    pKpiLcL0=tt.fill_LcL0(pKpiTree,Ntot_Lc2pKpi,"pKpi")
    pmmLcL0=tt.fill_LcL0(pmmTree,Ntot_Lc2pmm,"pmm")
else:
    pKpiLcL0=tt.fill_SLLcL0(pKpiTree,Ntot_Lc2pKpi,"pKpi")
    pmmLcL0=tt.fill_SLLcL0(pmmTree,Ntot_Lc2pmm,"pmm")
c,t1,t2=tt.draw_trigger(pKpiLcL0,"#Lambda_{c}#rightarrow pK^{-}#pi^{+}",pmmLcL0,"#Lambda_{c}#rightarrow p #mu^{+}#mu^{-}","c1")
c.Print(save_path+args.mode+"L0_Analysis.pdf")
#********* Hlt1 *****************
pKpiLcHlt1=tt.fill_hlt1(pKpiTree,Ntot_Lc2pKpi,"pKpiHlt1",args.mode)
pmmLcHlt1=tt.fill_hlt1(pmmTree,Ntot_Lc2pmm,"pmmLcHlt1",args.mode)
c2,t21,t22=tt.draw_trigger(pKpiLcHlt1,"#Lambda_{c}#rightarrow pK^{-}#pi^{+} (TOS)",pmmLcHlt1,"#Lambda_{c}#rightarrow p#mu^{+}#mu^{-} (TOS)","c2")
c2.Print(save_path+args.mode+"Hlt1_Analysis.pdf")
#******** Hlt2 ******************
if args.mode == "Prompt":
    Kpi = True
else :
    Kpi = False
pKpiLcHlt2=tt.fill_hlt2(pKpiTree,Ntot_Lc2pKpi,"pKpiHlt2",Kpi)
pmmLcHlt2=tt.fill_hlt2(pmmTree,Ntot_Lc2pmm,"pmmHlt2",Kpi)
c3,t31,t32=tt.draw_trigger(pKpiLcHlt2,"#Lambda_{c}#rightarrow pK^{-}#pi^{+} (TOS)",pmmLcHlt2,"#Lambda_{c}#rightarrow p#mu^{+}#mu^{-} (TOS)","c3")
c3.Print(save_path+args.mode+"Hlt2_Analysis.pdf")
#******** Hlt2 bis ******************
if args.mode == "Prompt":
    pKpiLcHlt2bis=tt.fill_hlt2bis(pKpiTree,Ntot_Lc2pKpi,"pKpiHlt2bis",False)
    pmmLcHlt2bis=tt.fill_hlt2bis(pmmTree,Ntot_Lc2pmm,"pmmHlt2bis",False)
    c4,t41,t42=tt.draw_trigger(pKpiLcHlt2bis,"#Lambda_{c}#rightarrow pK^{-}#pi^{+} (TOS)",pmmLcHlt2bis,"#Lambda_{c}#rightarrow p#mu^{+}#mu^{-} (TOS)","c4")
    c4.Print(save_path+args.mode+"Hlt2_bis_Analysis.pdf")
