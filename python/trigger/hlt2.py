import os
from routines.LcAnalysis import loc,hlt2_Prompt,dumpHLT
from ROOT import *
import tools.eventtuple_tools as et
from tools.trigger_tools import make_tos, make_tis
import tools.plotting_tools as plt
MCpmm = et.compute_tree('MCLc2pmm','Lc2pmmTuple/DecayTree')
MCpKpi = et.compute_tree('MCLc2pmm','Lc2pKpiTuple/DecayTree')

hlt2_tos = ['Hlt2Global',
        'Hlt2Phys',
        'Hlt2Topo2BodyDecision',
        'Hlt2Topo3BodyDecision',
        'Hlt2Topo4BodyDecision',
        'Hlt2TopoE2BodyDecision',
        'Hlt2TopoE3BodyDecision',
        'Hlt2TopoE4BodyDecision',
        'Hlt2TopoMu2BodyDecision',
        'Hlt2TopoMu3BodyDecision',
        'Hlt2TopoMu4BodyDecision',
        'Hlt2TopoMuE2BodyDecision', 
        'Hlt2TopoMuE3BodyDecision',
        'Hlt2TopoMuE4BodyDecision',
        'Hlt2RareCharmLc2PMuMuDecision',
        'Hlt2RareCharmLc2PMuMuSSDecision',
        'Hlt2RareCharmLc2PMueDecision',
        'Hlt2RareCharmLc2PeeDecision',
        'Hlt2CharmHadD2HHHDecision',
        'Hlt2DiMuonDetachedDecision',
        'Hlt2CharmSemilep3bodyD2KMuMuDecision',
        'Hlt2TopoMu2BodyBBDTDecision']

Npmm = MCpmm.GetEntries()
NpKpi = MCpKpi.GetEntries()

for c in hlt2_tos:
    selection=make_tos(c)
    events = MCpmm.GetEntries(selection)
    events_pKpi = MCpKpi.GetEntries(selection)
    eff,err = et.compute_eff(events, Npmm)
    eff_pKpi, err_pKpi = et.compute_eff(events_pKpi, NpKpi)
    eff_tex, err_tex, order_tex = plt.scientific_notation(eff,err)
    eff_pKpi_tex, err_pKpi_tex, order_pKpi_tex = plt.scientific_notation(eff_pKpi,err_pKpi)
    dic = {'Lc2pKpi':(eff_pKpi_tex, err_pKpi_tex, order_pKpi_tex ), 'Lc2pmm':(eff_tex, err_tex, order_tex)}
    hlt2_Prompt[c]=dic
print hlt2_Prompt
#dumpHLT()
#os.system('python test/fill_template.py')
